//==========================================================================
// 召喚エフェクト[Summonseffect.h]
// author: tatuya ogawa
//==========================================================================
#include "Summonseffect.h"
#include "UserCamera.h"

//==========================================================================
// 実体
//==========================================================================
int CSummonseffect::m_NumData = 0; // データ数
int CSummonseffect::m_IDCount = 0; // IDカウンタ
CSummonseffect::CData *CSummonseffect::m_pStartAddress = nullptr; // 先頭アドレス

CSummonseffect::CSummonseffect()
{
	this->m_poly = nullptr;
}

CSummonseffect::~CSummonseffect()
{
}

bool CSummonseffect::Init(void)
{
	this->New(this->m_poly);

	this->m_NumData = 0; // データ数
	this->m_IDCount = 0; // IDカウンタ
	this->m_pStartAddress = nullptr;

	return this->m_poly->Init("resource/texture/Game/Effect/num27.DDS", 3, 27, 27, CBillboard::PlateList::Vertical);
}

void CSummonseffect::Uninit(void)
{
	CData *pData = this->m_pStartAddress; // 初期アドレスを記憶
	CData *pNextDelete = nullptr; // 次のアドレス

	// アドレスがnullではない限り繰り返す
	for (;;)
	{
		// アドレスがnullだった場合終了
		if (pData == nullptr)
		{
			break;
		}

		// 次のアドレスを記憶
		pNextDelete = pData->m_pNext;

		// メモリ解放
		this->Delete(pData);
		this->m_NumData--;

		pData = pNextDelete;

		// データ数が0の時
		if (this->m_NumData == 0)
		{
			this->m_IDCount = 0;
		}
	}

	this->m_poly->Uninit();

	this->m_NumData = 0; // データ数
	this->m_IDCount = 0; // IDカウンタ
	this->m_pStartAddress = nullptr;

	this->Delete(this->m_poly);
}

void CSummonseffect::Update(void)
{
	// 先頭アドレスを記憶
	CData *pData = this->m_pStartAddress;
	CData *pDeleteData = nullptr; // 消去アドレス

	// アドレスがnullではない限り繰り返す
	for (;;)
	{
		// アドレスがnullだった場合終了
		if (pData == nullptr)
		{
			break;
		}

		this->m_poly->UpdateAnimation(&pData->m_AnimCounter);

		// エフェクトの終了判定が出ている場合アドレスを記憶
		if (this->m_poly->GetPattanNum(pData->m_pos.getindex(), &pData->m_AnimCounter))
		{
			pDeleteData = pData;
		}

		// 次のアドレスを記憶
		pData = pData->m_pNext;

		// 消去対象のアドレスが入っている場合解放
		if (pDeleteData != nullptr)
		{
			this->PinpointDelete(&pDeleteData->m_ID);
			pDeleteData = nullptr;
		}
	}
}

void CSummonseffect::Draw(void)
{
	// 先頭アドレスを記憶
	CData *pData = this->m_pStartAddress;

	// アドレスがnullではない限り繰り返す
	for (;;)
	{
		// アドレスがnullだった場合終了
		if (pData == nullptr)
		{
			break;
		}

		// 描画
		pData->m_pos.Draw(10, CDirectXDevice::GetD3DDevice());
		for (int i = 0; i < 3; i++)
		{
			this->m_poly->Draw(&pData->m_pos, CUserCamera::GetView(), &pData->m_AnimCounter, true);
		}

		// 次のアドレスを記憶
		pData = pData->m_pNext;
	}
}

void CSummonseffect::create(const C3DObject * Input)
{
	CData *pMain = nullptr; // 生成時のアドレス
	CData *pData = nullptr; // 次のアドレス
	m_NumData++;
	m_IDCount++;

	// メモリを確保
	New(pMain);
	pMain->m_pos.Init(0);
	pMain->m_pNext = nullptr;
	pMain->m_pBack = nullptr;
	pMain->m_ID = m_IDCount;
	pMain->m_pos.Info.pos = Input->Info.pos;
	pMain->m_pos.Scale(5.0f);
	pMain->m_pos.Info.pos.y = 2.4f;
	pMain->m_AnimCounter = -1;

	// 先頭アドレスがなかった場合
	if (m_pStartAddress == nullptr)
	{
		m_pStartAddress = pMain;
	}
	else if (m_pStartAddress != nullptr)
	{
		// 先頭アドレスを記憶
		pData = m_pStartAddress;

		// アドレスがnullではない限り検索を繰り返す
		for (;;)
		{
			// 次のアドレスがnullの場合
			if (pData->m_pNext == nullptr)
			{
				break;
			}

			// 検索をするアドレスを記憶
			pData = pData->m_pNext;
		}

		// 次のアドレスを記憶
		pData->m_pNext = pMain;

		// ひとつ前のアドレスに記憶
		pMain->m_pBack = pData;
	}
}

void CSummonseffect::PinpointDelete(const int * ID)
{
	CData *pData = this->m_pStartAddress; // 先頭アドレスを記憶
	CData *pBack = nullptr; // 前のデータ
	CData *pNext = nullptr; // 次のデータ

	// 解放対象が見つかるまで検索を続ける
	for (;;)
	{
		// アドレスが無ければ終了
		if (pData == nullptr)
		{
			break;
		}

		// 検索IDとヒット
		if (pData->m_ID == (*ID))
		{
			// 先頭アドレスと検索IDが同じ場合先頭アドレスを書き換える
			if (this->m_pStartAddress->m_ID == pData->m_ID)
			{
				this->m_pStartAddress = pData->m_pNext;
			}

			pBack = pData->m_pBack; // ひとつ前のアドレスを記憶
			pNext = pData->m_pNext; // 次のアドレスを記憶

			// アドレスがある場合次のアドレスをつなぎなおす
			if (pBack != nullptr)
			{
				pBack->m_pNext = pNext;
			}

			// アドレスがある場合ひとつ前のアドレスをつなぎなおす
			if (pNext != nullptr)
			{
				pNext->m_pBack = pBack;
			}

			// メモリ解放
			this->Delete(pData);
			this->m_NumData--;

			// データ数が0の時
			if (this->m_NumData == 0)
			{
				this->m_pStartAddress = nullptr;
				this->m_IDCount = 0;
			}

			break; // 終了
		}

		// 次のアドレスを記憶
		pData = pData->m_pNext;
	}
}
