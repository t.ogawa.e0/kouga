#ifndef _titlelogounder_h_
#define _titlelogounder_h_

//==========================================================================
// Include
//==========================================================================
#include "dxlib.h"

class CTitlelogounder : public CObject
{
public:
	CTitlelogounder();
	~CTitlelogounder();
	// 初期化
	bool Init(void);
	// 解放
	void Uninit(void);
	// 更新
	void Update(void);
	// 描画
	void Draw(void);
private:
	CTemplates m_temp;
	C2DPolygon * m_poly; // ぽりごん
	C2DObject * m_pos; // 座標
};

#endif // !_titlelogounder_h_
