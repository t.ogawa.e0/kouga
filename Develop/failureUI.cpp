//==========================================================================
// 失敗演出[failureUI.cpp]
// author : 
//==========================================================================
#include "failureUI.h"
#include "Screen.h"

//==========================================================================
// 実体化
//==========================================================================
bool CFailureUI::m_key = false;

CFailureUI::CFailureUI()
{
	this->m_masterpos = nullptr;
	this->m_poly = nullptr;
	this->m_pos = nullptr;
	this->m_numdata = 0;
	this->m_AnimCount = 0;
	this->m_key2 = false;
}

CFailureUI::~CFailureUI()
{
}

//==========================================================================
// 初期化
bool CFailureUI::Init(void)
{
	// テクスチャのパス
	const char * pTexList[]=
	{
		"./resource/texture/Game/UI/shippai.DDS",	//任務失敗テクスチャ
	};

	float fUIScale = 0.75f; // UIスケール調整用

	// データ数記録
	this->m_numdata = this->Sizeof(pTexList);

	// 動的メモリ確保 確保領域1
	this->New(this->m_masterpos, this->m_numdata);
	this->New(this->m_pos, this->m_numdata);
	this->New(this->m_poly);

	// 非アクティブ化
	this->m_key = false;

	// テクスチャの初期化
	if (this->m_poly->Init(pTexList, this->m_numdata, true))
	{
		return true;
	}

	// テクスチャのサイズ補正
	for (int i = 0; i < this->m_numdata; i++)
	{
		this->m_poly->SetTexScale(i, fUIScale);
	}

	// 初期化処理
	for (int i = 0; i < this->m_numdata; i++)
	{
		this->m_pos[i].Init(0);
	}

	// ここにUIの座標設定
	m_pos->SetCentralCoordinatesMood(true);
	m_pos->SetX((float)(CDirectXDevice::GetWindowsSize().m_Width / 2));
	m_pos->SetY((float)(CDirectXDevice::GetWindowsSize().m_Height / 2));
	m_pos->Scale(-1.0f);


	// データのコピー
	for (int i = 0; i < this->m_numdata; i++)
	{
		this->m_masterpos[i] = this->m_pos[i];
	}

	this->m_key2 = false;

	return false;
}

//==========================================================================
// 解放
void CFailureUI::Uninit(void)
{
	// マテリアルの解放
	this->m_poly->Uninit();

	// メモリ解放
	this->Delete(this->m_masterpos);
	this->Delete(this->m_poly);
	this->Delete(this->m_pos);

	// 非アクティブ化
	this->m_key = false;
	this->m_numdata = 0;
	this->m_AnimCount = 0;

	this->m_key2 = false;
}

//==========================================================================
// 更新
void CFailureUI::Update(void)
{
	// アクティブな時のみ処理
	if (this->m_key)
	{
		this->m_key2 = true;
		if (m_pos->GetScale() <= 1.0f)	//失敗テクスチャが100%になるまで
		{
			m_pos->Scale(0.05f);	//１フレーム+5%し続ける
		}
		else
		{
			m_AnimCount++;	//1フレームカウント
		}
		// 演出処理
		if (m_AnimCount >= 30)	//60フレーム(１秒後)
		{
			m_pos->SetYPlus(8.0f);	//失敗テクスチャを下へ
			CScreen::screenchange(CScreen::scenelist_t::Result);
		}
	}
}

//==========================================================================
// 描画
void CFailureUI::Draw(void)
{
	// アクティブな時のみ処理
	if (this->m_key2)
	{
		// 描画処理
		for (int i = 0; i < this->m_numdata; i++)
		{
			this->m_poly->Draw(&this->m_pos[i]);
		}
	}
}
