//==========================================================================
// ワールド[world.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _WORLD_H_
#define _WORLD_H_

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

//==========================================================================
//
// class  : CWorld
// Content: ワールド
//
//==========================================================================
class CWorld : private CTemplate, public CObject
{
public:
	CWorld();
	~CWorld();
	// 初期化
	bool Init(void);
	// 解放
	void Uninit(void);
	// 更新
	void Update(void);
	// 描画
	void Draw(void);
	// ビューカメラのゲッター
	static D3DXMATRIX *GetCamera(void) { return m_WorldCamera->CreateView(); }
private:
	static CCamera *m_WorldCamera; // ワールドビューカメラ
};

#endif // !_WORLD_H_
