//==========================================================================
// �^�C�g�����S[titlelogo.cpp]
// author: tatuya ogawa
//==========================================================================
#include "titlelogo.h"
#include "teamlogo.h"
#include "menu.h"
#include "menu.h"

//==========================================================================
// ����
//==========================================================================
bool CTitlelogo::m_key;

CTitlelogo::CTitlelogo()
{
	this->m_poly = nullptr;
	this->m_pos = nullptr;
	this->m_fadepos = nullptr;
	this->m_fade�� = 255;
	this->m_logo�� = 255;
	this->m_frame = 0;
}

CTitlelogo::~CTitlelogo()
{
}

//==========================================================================
// ������
bool CTitlelogo::Init(void)
{
	const char * ptexlink[] =
	{
		"resource/texture/titlefont/title.DDS",
		"resource/texture/ColorTex.DDS",
	};

	this->m_temp.New_(this->m_poly);
	this->m_temp.New_(this->m_pos);
	this->m_temp.New_(this->m_fadepos);

	this->m_fadepos->Init(1);
	this->m_fadepos->SetColor(0, 0, 0, 255);

	this->m_pos->Init(0);
	this->m_pos->SetCentralCoordinatesMood(true);
	this->m_pos->SetX((float)CDirectXDevice::GetWindowsSize().m_Width / 2);
	this->m_pos->SetY((float)CDirectXDevice::GetWindowsSize().m_Height / 2);

	if (this->m_poly->Init(ptexlink, this->m_temp.Sizeof_(ptexlink), true))
	{
		return true;
	}

	this->m_poly->SetTexSize(this->m_fadepos->getindex(), CDirectXDevice::GetWindowsSize().m_Width, CDirectXDevice::GetWindowsSize().m_Height);
	this->m_poly->SetTexScale(this->m_pos->getindex(), 0.2f);

	this->m_fade�� = 255;
	this->m_logo�� = 255;
	this->m_frame = 0;
	this->m_key = false;

	return false;
}

//==========================================================================
// ���
void CTitlelogo::Uninit(void)
{
	this->m_poly->Uninit();

	this->m_temp.Delete_(this->m_poly);
	this->m_temp.Delete_(this->m_fadepos);
	this->m_temp.Delete_(this->m_pos);

	this->m_fade�� = 255;
	this->m_logo�� = 255;
	this->m_frame = 0;
	this->m_key = false;
}

//==========================================================================
// �X�V
void CTitlelogo::Update(void)
{
	if (CTeamlogo::endkey())
	{
		this->m_frame++;
		if (60 < this->m_frame)
		{
			this->m_fade�� -= 3;
			CMenu::OpenSound();
			if (this->m_fade�� < 0)
			{
				this->m_fade�� = 0;
				this->m_key = true;
			}
		}
	}

	if (CMenu::getmenukey() == false)
	{
		this->m_logo�� += 7;
		if (255 < this->m_logo��)
		{
			this->m_logo�� = 255;
		}
	}
	else if (CMenu::getmenukey() == true)
	{
		this->m_logo�� -= 7;
		if (this->m_logo�� < 0)
		{
			this->m_logo�� = 0;
		}
	}

	this->m_fadepos->SetColor(0, 0, 0, this->m_fade��);
	this->m_pos->SetColor(255, 255, 255, this->m_logo��);
}

//==========================================================================
// �`��
void CTitlelogo::Draw(void)
{
	this->m_poly->Draw(this->m_pos);
	this->m_poly->Draw(this->m_fadepos);
}
