//==========================================================================
// プレイヤーパラメーター[PlayerParam.coo]
// author: keita yanagidate
//==========================================================================
#include "PlayerParam.h"
#include "Player.h"

CPlayerParam::CPlayerParam()
{
	this->m_HpGage = nullptr;
	this->m_GagePos = nullptr;
	this->m_MpGage = nullptr; //MPパラメーターゲージ
	this->m_HpPos = nullptr; //HPゲージ座標
	this->m_MpPos = nullptr; //HPゲージ座標
	this->m_Poly = nullptr; //描画
	this->m_num = 0;
}

CPlayerParam::~CPlayerParam()
{
}

//==========================================================================
// 初期化
bool CPlayerParam::Init(void)
{
	const char* TexLink[] =
	{
		"resource/texture/Game/lifegage.DDS",
		"resource/texture/ColorTex.DDS",
		"resource/texture/ColorTex.DDS",
	};
	float fscale = 0.5f;

	this->New(this->m_GagePos);
	this->New(this->m_HpGage);
	this->New(this->m_MpGage);
	this->New(this->m_HpPos);
	this->New(this->m_MpPos);
	this->New(this->m_Poly);

	this->m_HpPos->Init(1);
	this->m_MpPos->Init(2);
	this->m_GagePos->Init(0);

	if (this->m_Poly->Init(TexLink, this->Sizeof(TexLink), true))
	{
		return true;
	}

	this->m_Poly->GetTexSize(this->m_HpPos->getindex())->w = (int)((this->m_Poly->GetTexSize(0)->w*0.65f));
	this->m_Poly->GetTexSize(this->m_HpPos->getindex())->h = (int)(147 * CDirectXDevice::screenbuffscale());

	this->m_Poly->GetTexSize(this->m_MpPos->getindex())->w = (int)((this->m_Poly->GetTexSize(0)->w*0.27f));
	this->m_Poly->GetTexSize(this->m_MpPos->getindex())->h = (int)(60 * CDirectXDevice::screenbuffscale());

	for (int i = 0; i < 2; i++)
	{
		this->m_Poly->SetTexScale(i, fscale);
	}

	this->m_HpGage->Init((float)CPlayer::GetParam()->Get().m_HP, (float)this->m_Poly->GetTexSize(this->m_HpPos->getindex())->w);
	this->m_MpGage->Init((float)CPlayer::GetParam()->Get().m_MP, (float)this->m_Poly->GetTexSize(this->m_MpPos->getindex())->w);

	this->m_HpPos->SetPos(150 * CDirectXDevice::screenbuffscale(), 35 * CDirectXDevice::screenbuffscale());
	this->m_HpPos->SetColor(0, 255, 0, 255);

	//MPゲージセット
	this->m_MpPos->SetPos(150 * CDirectXDevice::screenbuffscale(), 110 * CDirectXDevice::screenbuffscale());
	this->m_MpPos->SetColor(0, 0, 255, 255);

	return false;
}

//==========================================================================
// 終了
void CPlayerParam::Uninit(void)
{
	this->m_Poly->Uninit();

	this->Delete(this->m_HpGage);
	this->Delete(this->m_GagePos);
	this->Delete(this->m_MpGage);
	this->Delete(this->m_HpPos);
	this->Delete(this->m_MpPos);
	this->Delete(this->m_Poly);
}

//==========================================================================
// 更新
void CPlayerParam::Update(void)
{
	this->m_HpGage->Update((float)CPlayer::GetParam()->Get().m_HP);
	this->m_MpGage->Update((float)CPlayer::GetParam()->Get().m_MP);
}

//==========================================================================
// 描画
void CPlayerParam::Draw(void)
{
	// HPゲージの描画
	this->m_Poly->GetTexSize(this->m_HpPos->getindex())->w = (int)this->m_HpGage->GetData()->m_CurrentGaugeLength;
	this->m_Poly->Draw(this->m_HpPos);

	// MPゲージの描画
	this->m_Poly->GetTexSize(this->m_MpPos->getindex())->w = (int)this->m_MpGage->GetData()->m_CurrentGaugeLength;
	this->m_Poly->Draw(this->m_MpPos);

	// MPゲージの描画
	this->m_Poly->Draw(this->m_GagePos);
}
