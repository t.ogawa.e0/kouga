//==========================================================================
// `[S[teamlogo.cpp]
// author: tatuya ogawa
//==========================================================================
#include "teamlogo.h"

//==========================================================================
// ÀÌ
//==========================================================================
bool CTeamlogo::m_updateend; // I¹

CTeamlogo::CTeamlogo()
{
	this->m_poly = nullptr;
	this->m_pos = nullptr;
	this->m_bakpos = nullptr;
	this->m_frame = 0;
	this->m_¿ = 255;
	this->m_bak¿ = 255;
	this->m_end = false;
	this->m_updateend = false;
}

CTeamlogo::~CTeamlogo()
{
}

//==========================================================================
// ú»
bool CTeamlogo::Init(void)
{
	const char * ptexlink[] =
	{
		"resource/texture/ColorTex.DDS",
		"resource/texture/logo/team_logo.DDS",
	};

	this->m_temp.New_(this->m_poly);
	this->m_temp.New_(this->m_pos);
	this->m_temp.New_(this->m_bakpos);

	if (this->m_poly->Init(ptexlink, this->m_temp.Sizeof_(ptexlink), true))
	{
		return true;
	}

	this->m_bakpos->Init(0);
	this->m_bakpos->SetColor(0, 0, 0, 255);

	this->m_pos->Init(1);
	this->m_pos->SetCentralCoordinatesMood(true);
	this->m_pos->SetX((float)CDirectXDevice::GetWindowsSize().m_Width / 2);
	this->m_pos->SetY((float)CDirectXDevice::GetWindowsSize().m_Height / 2);

	this->m_poly->SetTexSize(this->m_bakpos->getindex(), CDirectXDevice::GetWindowsSize().m_Width, CDirectXDevice::GetWindowsSize().m_Height);
	this->m_poly->SetTexScale(this->m_pos->getindex(), 0.2f);

	this->m_frame = 0;
	this->m_¿ = 255;
	this->m_bak¿ = 255;
	this->m_end = false;
	this->m_updateend = false;

	return false;
}

//==========================================================================
// ðú
void CTeamlogo::Uninit(void)
{
	this->m_poly->Uninit();

	this->m_temp.Delete_(this->m_poly);
	this->m_temp.Delete_(this->m_bakpos);
	this->m_temp.Delete_(this->m_pos);

	this->m_frame = 0;
	this->m_¿ = 255;
	this->m_bak¿ = 255;
	this->m_end = false;
	this->m_updateend = false;
}

//==========================================================================
// XV
void CTeamlogo::Update(void)
{
	if (!this->m_end)
	{
		this->m_Imgui.NewWindow("logo", true);

		this->m_Imgui.Text("Draw...");

		this->m_frame++;
		if (120 < this->m_frame)
		{
			this->m_pos->SetColor(255, 255, 255, this->m_¿);
			this->m_¿ -= 3;
			if (this->m_¿ <= 0)
			{
				this->m_¿ = 0;
			}
			this->m_Imgui.Text("LogoDraw...\n a=%d", this->m_¿);
		}

		if (180 < this->m_frame)
		{
			this->m_updateend = true;
			this->m_bakpos->SetColor(0, 0, 0, this->m_bak¿);
			this->m_bak¿ -= 3;
			if (this->m_bak¿ <= 0)
			{
				this->m_end = true;
				this->m_bak¿ = 0;
			}
			this->m_Imgui.Text("BbackDraw... \n a=%d", this->m_bak¿);
		}

		this->m_Imgui.EndWindow();
	}
}

//==========================================================================
// `æ
void CTeamlogo::Draw(void)
{
	if (!this->m_end)
	{
		this->m_poly->Draw(this->m_bakpos);
		this->m_poly->Draw(this->m_pos);
	}
}
