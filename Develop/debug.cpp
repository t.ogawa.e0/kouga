//==========================================================================
// デバッグ[debug.cpp]
// author: tatuya ogawa
//==========================================================================
#include "debug.h"

CDebug::CDebug()
{
	this->m_FontPosY = 0;
}

CDebug::~CDebug()
{
}

//==========================================================================
// シーンセット
void CDebug::Set(CSceneManager::SceneName Input)
{
#if defined(_DEBUG) || defined(DEBUG)
	this->m_Name = Input;
#else
	this->Decoy(&Input);
#endif
}

//==========================================================================
// 描画
void CDebug::Draw(void)
{
#if defined(_DEBUG) || defined(DEBUG)
	this->m_FontPosY = 3;
	switch (this->m_Name)
	{
	case CSceneManager::SceneName::Title:
		this->Title();
		break;
	case CSceneManager::SceneName::Home:
		this->Home();
		break;
	case CSceneManager::SceneName::Game:
		this->Game();
		break;
	case CSceneManager::SceneName::Result:
		this->Result();
		break;
	case CSceneManager::SceneName::Screen_Saver:
		this->Screen_Saver();
		break;
	case CSceneManager::SceneName::Practice:
		this->Practice();
		break;
	case CSceneManager::SceneName::Load:
		this->Loat();
		break;
	default:
		break;
	}
#endif
}

void CDebug::Title(void)
{
#if defined(_DEBUG) || defined(DEBUG)
	this->Text("Title");
#endif
}

void CDebug::Home(void)
{
#if defined(_DEBUG) || defined(DEBUG)
	this->Text("Home");
#endif
}

void CDebug::Game(void)
{
#if defined(_DEBUG) || defined(DEBUG)
	this->Text("Game");
#endif
}

void CDebug::Result(void)
{
#if defined(_DEBUG) || defined(DEBUG)
	this->Text("Result");
#endif
}

void CDebug::Screen_Saver(void)
{
#if defined(_DEBUG) || defined(DEBUG)
	this->Text("Screen_Saver");
#endif
}

void CDebug::Practice(void)
{
#if defined(_DEBUG) || defined(DEBUG)
	this->Text("Practice");
#endif
}

void CDebug::Loat(void)
{
#if defined(_DEBUG) || defined(DEBUG)
	this->Text("Loat");
#endif
}
