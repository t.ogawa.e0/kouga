//==========================================================================
// 進入禁止[NoEntry.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _NoEntry_H_
#define _NoEntry_H_

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

//==========================================================================
//
// class  : CNoEntry
// Content: 進入禁止
//
//==========================================================================
class CNoEntry : private CTemplate, public CObject
{
public:
	static constexpr int m_snumdata = 4;
private:
	static constexpr int m_WallY = 2;
	static constexpr int m_WallX = 200;
	static constexpr int m_WallZ = 200;
public:
	CNoEntry();
	~CNoEntry();
	// 初期化
	bool Init(void);
	// 解放
	void Uninit(void);
	// 更新
	void Update(void);
	// 描画
	void Draw(void);

	static C3DObject *GetPos(int num) { return &m_vpos[num]; }
private:
	CMesh *m_poly; // フィールドデータ
	C3DObject *m_pos; // 座標
	int m_numdata;
	static C3DObject *m_vpos;
};

#endif // !_NoEntry_H_
