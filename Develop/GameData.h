//==========================================================================
// ゲームデータ[GameData.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _GameData_H_
#define _GameData_H_

//==========================================================================
// Include
//==========================================================================
#include "dxlib.h"
#include "KeyboardTextureTemplate.h"

//==========================================================================
//
// class  : CGameData
// Content : CGameData
//
//==========================================================================
class CGameData
{
public:
	static constexpr char *m_FileName = "resource/data/ranking/data.bin"; // ランキングのファイル
	static constexpr char *m_FileNamePin = "resource/data/ranking/pin.bin"; // 個々のデータ
	static constexpr int m_limitname = 10;
private:
	static constexpr char *m_FileNameLog = "resource/data/logdata/log.txt"; // ログ
public:
	class CNameCase
	{
	public:
		CKeyboardTextureTemplate::keycase_t m_namedata[m_limitname];
		int m_namecount;
		int m_Serect;
	};

	// データ管理
	class CData
	{
	public:
		CNameCase m_name;
		int m_score;
		int m_rank;
	};
public:
	// 読み取り専用
	bool Load(CData *& pdata, const char * pname, int * numdata);
	// ランキング更新時の読み取り専用
	bool Update(CData *& pdata, const char * pname, int * numdata);
	// 保存処理
	void Save(CData *& pdata, const char * pname, int * numdata);
	// ソート処理
	void Sort(CData *& pInput, CData *& pNewData, int * numdata);
private:
	CTemplates m_temp;
};

#endif // !_GameData_H_
