//==========================================================================
// シーン遷移[SceneChange.cpp]
// author: tatuya ogawa
//==========================================================================
#include "SceneChange.h"
#include "make.h"

#include "title.h"
#include "game.h"
#include "result.h"

CSceneManager::CSceneManager()
{
	this->m_pScene = nullptr;
}

CSceneManager::~CSceneManager()
{
	if (this->m_pScene != nullptr)
	{
		this->m_pScene->Uninit();
		delete[] this->m_pScene;
		this->m_pScene = nullptr;
	}
}

//==========================================================================
// シーンの切り替え
bool CSceneManager::ChangeScene(SceneName Name)
{
	// 解放
	this->Uninit();

	switch (Name)
	{
	case SceneName::NOME:
		CDirectXDevice::ErrorMessage("Scene Ellor : -1");
		return true;
		break;

	case SceneName::Title:
		this->m_pScene = new CTitleScene;
		break;

	case SceneName::Home:
		this->m_pScene = nullptr;
		break;

	case SceneName::Game:
		this->m_pScene = new CGameScene;
		break;

	case SceneName::Result:
		this->m_pScene = new CResultScene;
		break;

	case SceneName::Screen_Saver:
		this->m_pScene = nullptr;
		break;

	case SceneName::Practice:
		this->m_pScene = nullptr;
		break;

	case SceneName::Load:
		this->m_pScene = nullptr;
		break;

	case SceneName::Default:
		this->m_pScene = new CMake;
		break;

	default:
		CDirectXDevice::ErrorMessage("Scene Ellor : -1");
		return true;
		break;
	}

	// 初期化
	return this->Init();
}

//==========================================================================
// 初期化
bool CSceneManager::Init(void)
{
	return this->m_pScene->Init();
}

//==========================================================================
// 解放
void CSceneManager::Uninit(void)
{
	if (this->m_pScene != nullptr)
	{
		this->m_pScene->Uninit();
		delete[] this->m_pScene;
		this->m_pScene = nullptr;
	}
}

//==========================================================================
// 更新
void CSceneManager::Update(void)
{
	this->m_pScene->Update();
}

//==========================================================================
// 描画
void CSceneManager::Draw(void)
{
	this->m_pScene->Draw();
}