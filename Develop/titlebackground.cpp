//==========================================================================
// タイトルの背景[titlebackground.cpp]
// author: tatuya ogawa
//==========================================================================
#include "titlebackground.h"

CCamera CTitlebackground::m_Camera; // カメラ

CTitlebackground::CTitlebackground()
{
	this->m_poly = nullptr;
	this->m_pos = nullptr;
	this->m_CameraPos = nullptr;
}

CTitlebackground::~CTitlebackground()
{
}

//==========================================================================
// 初期化
bool CTitlebackground::Init(void)
{
	this->m_temp.New_(this->m_poly);
	this->m_temp.New_(this->m_pos);
	this->m_temp.New_(this->m_CameraPos);

	// 初期化
	this->m_pos->Init(0);

	this->m_Camera.Init();

	C3DObject Eye = *m_pos;
	C3DObject At = *m_pos;

	this->m_CameraPos->Init(0);
	this->m_Camera.Init();

	At = Eye;
	Eye.MoveZ(-20);
	this->m_Camera.Init(&Eye.Info.pos, &At.Info.pos);
	this->m_Camera.RotViewX(-0.7f);
	this->m_Camera.RotViewY(1.0f);

	this->m_pos->Scale(3);

	// 初期化
	return m_poly->Init("resource/texture/Field001.DDS", 100, 100);
}

//==========================================================================
// 解放
void CTitlebackground::Uninit(void)
{
	this->m_poly->Uninit();

	this->m_temp.Delete_(this->m_poly);
	this->m_temp.Delete_(this->m_pos);
	this->m_temp.Delete_(this->m_CameraPos);
}

//==========================================================================
// 更新
void CTitlebackground::Update(void)
{
}

//==========================================================================
// 描画
void CTitlebackground::Draw(void)
{
	this->m_pos->Draw(10, CDirectXDevice::GetD3DDevice());
	this->m_poly->Draw(this->m_pos);
}
