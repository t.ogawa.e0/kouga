#include "titlelogounder.h"

CTitlelogounder::CTitlelogounder()
{
	this->m_poly = nullptr;
	this->m_pos = nullptr;
}

CTitlelogounder::~CTitlelogounder()
{
}

bool CTitlelogounder::Init(void)
{
	this->m_temp.New_(this->m_poly);
	this->m_temp.New_(this->m_pos);

	this->m_pos->Init(0);

	if (this->m_poly->Init("resource/texture/line.DDS"))
	{
		return true;
	}

	return false;
}

void CTitlelogounder::Uninit(void)
{
	this->m_poly->Uninit();

	this->m_temp.Delete_(this->m_poly);
	this->m_temp.Delete_(this->m_pos);
}

void CTitlelogounder::Update(void)
{
}

void CTitlelogounder::Draw(void)
{
	this->m_poly->Draw(this->m_pos);
}
