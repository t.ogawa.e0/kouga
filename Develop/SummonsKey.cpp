//==========================================================================
// 召喚キー[SummonsKey.cpp]
// author: tatuya ogawa
//==========================================================================
#include "SummonsKey.h"

//==========================================================================
// 実体
//==========================================================================
CSummonsKey::KeyList CSummonsKey::m_summon;
bool CSummonsKey::m_key[(int)CSummonsKey::KeyList::max] = { false };

CSummonsKey::CSummonsKey()
{
	this->m_pos = nullptr;
	this->m_poly = nullptr;
	this->m_numdata = 0;

	this->m_pos2 = nullptr;
	this->m_poly2 = nullptr;
	this->m_numdata2 = 0;
}

CSummonsKey::~CSummonsKey()
{
}

//==========================================================================
// 初期化
bool CSummonsKey::Init(void)
{
	const char * ptexlist[] =
	{
		"resource/texture/SummonsKey/recast_UI_b.DDS",
		"resource/texture/SummonsKey/recast_UI.DDS",
	};

	float fscale = 1.5f;
	float fscale1 = 0.3f;
	float fscale2 = 0.6f;
	CVector<float> vpos = CVector<float>(0.0f, 0.0f, 0.0f, 0.0f);

	this->m_numdata = (int)this->Sizeof(ptexlist);
	this->m_numdata2 = (int)KeyList::max;

	this->New(this->m_pos, this->m_numdata);
	this->New(this->m_poly);

	this->New(this->m_pos2, this->m_numdata2);
	this->New(this->m_poly2);

	if (this->m_poly->Init(ptexlist, this->m_numdata, true))
	{
		return true;
	}

	if (this->m_poly2->Init("resource/texture/SummonsKey/gauge.DDS", true))
	{
		return true;
	}

	for (int i = 0; i < this->m_numdata; i++)
	{
		this->m_poly->SetTexScale(i, fscale);
		this->m_poly->SetTexScale(i, fscale1);
	}

	this->m_poly2->SetTexScale(0, fscale);
	this->m_poly2->SetTexScale(0, fscale1);
	this->m_poly2->SetTexScale(0, fscale2);

	for (int i = 0; i < this->m_numdata; i++)
	{
		this->m_pos[i].Init(i);
		this->m_pos[i].SetCentralCoordinatesMood(true);
		vpos = CVector<float>((float)CDirectXDevice::GetWindowsSize().m_Width, (float)CDirectXDevice::GetWindowsSize().m_Height, 0.0f, 0.0f);
		vpos -= CVector<float>(this->m_poly->GetTexSize(i)->w / 2.0f, this->m_poly->GetTexSize(i)->h / 2.0f, 0.0f, 0.0f);
		this->m_pos[i].SetPos(vpos);
	}

	for (int i = 0; i < this->m_numdata2; i++)
	{
		this->m_charge[i] = 0.0f;
		this->m_pos2[i].Init(0);
		vpos = CVector<float>((float)CDirectXDevice::GetWindowsSize().m_Width, (float)CDirectXDevice::GetWindowsSize().m_Height, 0.0f, 0.0f);
		vpos -= CVector<float>((float)this->m_poly2->GetTexSize(0)->w, (float)this->m_poly2->GetTexSize(0)->h, 0.0f, 0.0f);
		vpos -= CVector<float>(this->m_poly->GetTexSize(0)->w / 2.0f, this->m_poly->GetTexSize(0)->h / 2.0f, 0.0f, 0.0f);
		vpos += CVector<float>(this->m_poly2->GetTexSize(0)->w / 2.0f, this->m_poly2->GetTexSize(0)->h / 2.0f, 0.0f, 0.0f);

		this->m_pos2[i].SetPos(vpos);
	}

	// 右
	vpos = *this->m_pos2[(int)KeyList::RightButton3].GetPos();
	vpos += CVector<float>(this->m_poly->GetTexSize(0)->w * 0.35f, 0.0f, 0.0f, 0.0f);
	this->m_pos2[(int)KeyList::RightButton3].SetPos(vpos);

	// 左
	vpos = *this->m_pos2[(int)KeyList::RightButton1].GetPos();
	vpos -= CVector<float>(this->m_poly->GetTexSize(0)->w * 0.35f, 0.0f, 0.0f, 0.0f);
	this->m_pos2[(int)KeyList::RightButton1].SetPos(vpos);

	// 上
	vpos = *this->m_pos2[(int)KeyList::RightButton4].GetPos();
	vpos -= CVector<float>(0.0f, this->m_poly->GetTexSize(0)->h * 0.28f, 0.0f, 0.0f);
	this->m_pos2[(int)KeyList::RightButton4].SetPos(vpos);

	// 下
	vpos = *this->m_pos2[(int)KeyList::RightButton2].GetPos();
	vpos += CVector<float>(0.0f, this->m_poly->GetTexSize(0)->h * 0.28f, 0.0f, 0.0f);
	this->m_pos2[(int)KeyList::RightButton2].SetPos(vpos);

	this->m_summon = KeyList::max;

	for (int i = 0; i < (int)KeyList::max; i++)
	{
		this->m_key[i] = false;
	}
	
	return false;
}

//==========================================================================
// 解放
void CSummonsKey::Uninit(void)
{
	this->m_poly->Uninit();
	this->m_poly2->Uninit();

	this->Delete(this->m_pos);
	this->Delete(this->m_poly);
	this->Delete(this->m_pos2);
	this->Delete(this->m_poly2);

	this->m_numdata = 0;
	this->m_numdata2 = 0;
	this->m_summon = KeyList::max;
	for (int i = 0; i < (int)KeyList::max; i++)
	{
		this->m_key[i] = false;
	}
}

//==========================================================================
// 更新
void CSummonsKey::Update(void)
{
	this->m_Imgui.NewWindow("SummonsPower", true);

	//	調節用
	{
		//	if (CKeyboard::Trigger(CKeyboard::KeyList::KEY_UP) || CController::CPS4::CButton::Trigger(CController::CPS4::CButton::Ckey::RightButton4))
		//	{
		//		if (CSummonsKey::SummonsGetkey(CSummonsKey::KeyList::RightButton4))
		//		{
		//			CSummonsKey::SummonsSetkey(CSummonsKey::KeyList::RightButton4);
		//		}
		//	}

		//	if (CKeyboard::Trigger(CKeyboard::KeyList::KEY_DOWN) || CController::CPS4::CButton::Trigger(CController::CPS4::CButton::Ckey::RightButton2))
		//	{
		//		if (CSummonsKey::SummonsGetkey(CSummonsKey::KeyList::RightButton2))
		//		{
		//			CSummonsKey::SummonsSetkey(CSummonsKey::KeyList::RightButton2);
		//		}
		//	}

		//	if (CKeyboard::Trigger(CKeyboard::KeyList::KEY_LEFT) || CController::CPS4::CButton::Trigger(CController::CPS4::CButton::Ckey::RightButton1))
		//	{
		//		if (CSummonsKey::SummonsGetkey(CSummonsKey::KeyList::RightButton1))
		//		{
		//			CSummonsKey::SummonsSetkey(CSummonsKey::KeyList::RightButton1);
		//		}
		//	}

		//	if (CKeyboard::Trigger(CKeyboard::KeyList::KEY_RIGHT) || CController::CPS4::CButton::Trigger(CController::CPS4::CButton::Ckey::RightButton3))
		//	{
		//		if (CSummonsKey::SummonsGetkey(CSummonsKey::KeyList::RightButton3))
		//		{
		//			CSummonsKey::SummonsSetkey(CSummonsKey::KeyList::RightButton3);
		//		}
		//	}
	}

	if ((this->m_summon < KeyList::max) && (KeyList::start<this->m_summon))
	{
		if (this->m_charge[(int)this->m_summon] == this->m_chargelimit)
		{
			this->m_charge[(int)this->m_summon] = 0.0f;
		}
	}

	this->m_summon = KeyList::max;

	for (int i = 0; i < this->m_numdata2; i++)
	{
		// ゲージ回復
		switch ((KeyList)i)
		{
		case KeyList::RightButton1:// □ シカク
			this->m_charge[i] += 0.4f;
			break;
		case KeyList::RightButton2:// Ｘ バツ
			this->m_charge[i] += 0.275f;
			break;
		case KeyList::RightButton3:// 〇 マル
			this->m_charge[i] += 0.8f;
			break;
		case KeyList::RightButton4:// △ サンカク
			this->m_charge[i] += 0.45f;
			break;
		default:
			break;
		}

		this->m_key[i] = false;

		// ゲージのオーバーフロー回避
		if (this->m_chargelimit < this->m_charge[i])
		{
			this->m_key[i] = true;
			this->m_charge[i] = this->m_chargelimit;
		}
		this->m_Imgui.Text(" Key : %d \n Power : %.2f", i, this->m_charge[i]);
	}

	this->m_Imgui.EndWindow();
}

//==========================================================================
// 描画
void CSummonsKey::Draw(void)
{
	this->m_poly->Draw(&this->m_pos[0]);
	for (int i = 0; i < this->m_numdata2; i++)
	{
		this->m_pos2[i].AngleReset();

		for (int s = 0; s < (int)this->m_charge[i]; s++)
		{
			this->m_poly2->Draw(&this->m_pos2[i], true);
			this->m_pos2[i].Angle(0.15f);
		}
	}
	this->m_poly->Draw(&this->m_pos[1]);
}

