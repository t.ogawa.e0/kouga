//==========================================================================
// ポーズの背景[pausebackground.h]
// author : 
//==========================================================================
#ifndef _pausebackground_h_
#define _pausebackground_h_

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

//==========================================================================
//
// class  : CPausebackground
// Content: ポーズの背景
//
//==========================================================================
class CPausebackground : private CTemplate
{
public:
	CPausebackground();
	~CPausebackground();
	// 初期化
	bool Init(void);
	// 解放
	void Uninit(void);
	// 更新
	void Update(void);
	// 描画
	void Draw(void);

	static bool GetKey(void) { return m_key; }
	static void SetLockKey(void) { m_key = false; }
private:
	C2DObject *m_pos; // 座標
	C2DObject *m_masterpos; // マスター座標
	C2DPolygon *m_poly; // ポリゴン
	int m_numdata; // データ数
public:
	static bool m_key; // 鍵
};

#endif // !_pausebackground_h_
