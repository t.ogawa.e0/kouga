//==========================================================================
// タイトルの背景[titlebackground.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _titlebackground_h_
#define _titlebackground_h_

//==========================================================================
// Include
//==========================================================================
#include "dxlib.h"

//==========================================================================
//
// class  : CTitlebackground
// Content: タイトルの背景
//
//==========================================================================
class CTitlebackground : public CObject
{
public:
	CTitlebackground();
	~CTitlebackground();
	// 初期化
	bool Init(void);
	// 解放
	void Uninit(void);
	// 更新
	void Update(void);
	// 描画
	void Draw(void);

	static D3DXMATRIX * GetCamera(void) { return m_Camera.CreateView(); }
private:
	CMesh * m_poly; // ぽりごん
	C3DObject * m_pos; // 座標
	C3DObject *m_CameraPos;
	CTemplates m_temp;
	static CCamera m_Camera; // カメラ
};

#endif // !_titlebackground_h_
