//==========================================================================
// パラメーターのゲージ[ParameterGauge.cpp]
// author: tatuya ogawa
//==========================================================================
#include "ParameterGauge.h"

CParameterGauge::CParameterGauge()
{
}

CParameterGauge::~CParameterGauge()
{
}

//==========================================================================
// ゲージの初期化
void CParameterGauge::Init(float MaxParam, float ParamGauge)
{
	int Limit = 1; // ゲージの幅を出すための変数
	int SetPercent = 9; // HPの上限を調べるカウンタ
	for (;;)
	{
		if ((int)MaxParam <= SetPercent)
		{
			break;
		}

		Limit *= 10;
		SetPercent *= 10;
		SetPercent += 9;
	}

	this->m_Param.m_Percent = ParamGauge / 100; // 1％のHPゲージ
	this->m_Param.m_PhysicalFitnessPercent = MaxParam / 100; // 1％のHP
	this->m_Param.m_Limit = Limit;
	this->m_Param.m_SetPercent = SetPercent;
	this->m_Param.m_MaximumGaugeLength = ParamGauge;
	this->m_Param.m_CurrentGaugeLength = ParamGauge;
}

//==========================================================================
// ゲージの更新
void CParameterGauge::Update(float CurrentParam)
{
	float fPhysicalFitnessGage = 0.0f; // パラメーターゲージのカウンタ

	for (float i = 0.0f; i < CurrentParam; i += this->m_Param.m_PhysicalFitnessPercent)
	{
		fPhysicalFitnessGage += this->m_Param.m_Percent;
	}
	this->m_Param.m_CurrentGaugeLength = fPhysicalFitnessGage;
}
