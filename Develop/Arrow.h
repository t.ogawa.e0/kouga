//==========================================================================
// ���[Arrow.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _Arrow_h_
#define _Arrow_h_

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

//==========================================================================
//
// class  : CArrow
// Content: ���
//
//==========================================================================
class CArrow
{
public:
	CArrow();
	~CArrow();
	// ������
	bool Init(const char * ptexname);
	// ���
	void Uninit(void);
	// �X�V
	void Update(C3DObject * Input);
	// �`��
	void Draw(void);
private:
	CBillboard * m_poly;
	C3DObject * m_pos;
	CTemplates m_temp;
};

#endif // !_Arrow_h_
