//==========================================================================
// デバッグ[debug.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _DEBUG_H_
#define _DEBUG_H_

//==========================================================================
// Include
//==========================================================================
#include "dxlib.h"
#include "SceneChange.h"

//==========================================================================
//
// class  : CDebug
// Content: デバッグ
//
//==========================================================================
class CDebug : private CTemplate
{
public:
	CDebug();
	~CDebug();

	// シーンセット
	void Set(CSceneManager::SceneName Input);

	// 描画
	void Draw(void);
private:
	void Title(void);
	void Home(void);
	void Game(void);
	void Result(void);
	void Screen_Saver(void);
	void Practice(void);
	void Loat(void);
	char *BoolFont(bool Input) { return ((Input) & 1) ? "true" : "false"; }
	template <typename ... Args>
	void Text(const char * text, Args const & ... args);
private:
	CSceneManager::SceneName m_Name;
	int m_FontPosY;
};

template<typename ...Args>
inline void CDebug::Text(const char * text, Args const & ...args)
{
	this->m_FontPosY += CDirectXDebug::GetFontSize();
	CDirectXDebug::PosAndColor(1, this->m_FontPosY);
	CDirectXDebug::DPrintf(text, args ...);
}

#endif // !_DEBUG_H_

