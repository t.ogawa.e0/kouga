//==========================================================================
// ベースキャンプ[Base.cpp]
// author: tatuya ogawa
//==========================================================================
#include "Base.h"
#include "Enemy.h"

#include "Player.h"
#include "Ally_AI.h"

#include "ExplosionEffect.h"

//==========================================================================
// 実体
//==========================================================================
CBase::CParam *CBase::m_BaseData;
int CBase::m_MaxBase; // ベースの総数
int CBase::m_scor;

// 拠点のパラメーター
static CBase::CData g_Baseparam[] =
{
	// 生成 範囲 耐久値
	{ 9 ,20.0f, 2000 },

	{ 6 ,10.0f, 1000 },
	{ 6 ,10.0f, 1000 },
	{ 6 ,10.0f, 1000 },
	{ 6 ,10.0f, 1000 },

	{ 3 ,10.0f, 500 },
	{ 3 ,10.0f, 500 },
	{ 3 ,10.0f, 500 },
	{ 3 ,10.0f, 500 },

	{ 9 ,20.0f, 2000 },
	{ 9 ,20.0f, 2000 },
	{ 9 ,20.0f, 2000 },
	{ 9 ,20.0f, 2000 },

	{ 3 ,5.0f, 500 },
	{ 3 ,5.0f, 500 },
	{ 3 ,5.0f, 500 },
	{ 3 ,5.0f, 500 },

	{ 3 ,5.0f, 500 },
	{ 3 ,5.0f, 500 },
	{ 3 ,5.0f, 500 },
	{ 3 ,5.0f, 500 },

	{ 3 ,5.0f, 500 },
	{ 3 ,5.0f, 500 },
	{ 3 ,5.0f, 500 },
	{ 3 ,5.0f, 500 },

	{ 3 ,5.0f, 500 },
	{ 3 ,5.0f, 500 },
	{ 3 ,5.0f, 500 },
	{ 3 ,5.0f, 500 },

	{ 3 ,5.0f, 500 },
	{ 3 ,5.0f, 500 },
	{ 3 ,5.0f, 500 },
	{ 3 ,5.0f, 500 },
};

// 拠点の座標
static D3DXVECTOR3 g_ManagerPos[] =
{
	//  x    y    z
	{ 0.0f,1.4f,0.0f },

	{ 10.0f,1.4f,10.0f },
	{ -10.0f,1.4f,-10.0f },
	{ 10.0f,1.4f,-10.0f },
	{ -10.0f,1.4f,10.0f },

	{ 0.0f,1.4f,20.0f },
	{ -0.0f,1.4f,-20.0f },
	{ 20.0f,1.4f,-0.0f },
	{ -20.0f,1.4f,0.0f },

	{ 95.0f,1.4f,95.0f },
	{ -95.0f,1.4f,-95.0f },
	{ 95.0f,1.4f,-95.0f },
	{ -95.0f,1.4f,95.0f },

	{ 80.0f,1.4f,53.0f },
	{ -20.0f,1.4f,-41.0f },
	{ 49.0f,1.4f,-70.0f },
	{ -50.0f,1.4f,50.0f },

	{ 90.0f,1.4f,75.0f },
	{ -20.0f,1.4f,-34.0f },
	{ 36.0f,1.4f,-30.0f },
	{ -46.0f,1.4f,34.0f },

	{ 60.0f,1.4f,45.0f },
	{ -50.0f,1.4f,-60.0f },
	{ 86.0f,1.4f,-30.0f },
	{ -86.0f,1.4f,4.0f },

	{ 20.0f,1.4f,95.0f },
	{ -46.0f,1.4f,-40.0f },
	{ 60.0f,1.4f,-39.0f },
	{ -26.0f,1.4f,74.0f },

	{ 83.0f,1.4f,37.0f },
	{ -31.0f,1.4f,-42.0f },
	{ 46.0f,1.4f,-22.0f },
	{ -96.0f,1.4f,64.0f },
};

CBase::CBase()
{
	this->m_poly = nullptr;
	this->m_key = false;
}

CBase::~CBase()
{
}

//==========================================================================
// 初期化
bool CBase::Init(void)
{
	C3DObject Pos;

	this->m_MaxBase = (int)this->m_temp.Sizeof_(g_ManagerPos);

	// メモリ確保
	this->m_temp.New_(this->m_poly);
	this->m_temp.New_(this->m_BaseData, this->m_MaxBase);

	this->m_scor = 0;


	for (int i = 0; i < this->m_MaxBase; i++)
	{
		Pos.Init(0);
		Pos.Scale(5.0f);
		Pos.Info.pos = g_ManagerPos[i];
		this->m_BaseData[i].m_Stock.Init(g_Baseparam[i].m_createlimit);
		this->m_BaseData[i].Init(&Pos, g_Baseparam[i].m_searchscale, g_Baseparam[i].m_life);
	}

	this->m_key = false;

	// 初期化
	return this->m_poly->Init("resource/texture/Game/object/kyoten_kakougo.DDS", CBillboard::PlateList::Up);
}

//==========================================================================
// 解放
void CBase::Uninit(void)
{
	// 解放
	this->m_poly->Uninit();
	this->m_key = false;

	// メモリ解放
	this->m_temp.Delete_(this->m_poly);
	this->m_temp.Delete_(this->m_BaseData);
}

//==========================================================================
// 更新
void CBase::Update(void)
{
	this->createtrap();

	for (int i = 0; i < this->m_MaxBase; i++)
	{
		this->m_BaseData[i].Update();
	}
}

//==========================================================================
// 描画
void CBase::Draw(void)
{
	for (int i = 0; i < this->m_MaxBase; i++)
	{
		if (this->m_BaseData[i].m_Existence)
		{
			CDirectXDebug::HitRound(&this->m_BaseData[i].m_Pos, 10, this->m_BaseData[i].m_SearchScale, CDirectXDevice::GetD3DDevice());
			this->m_BaseData[i].m_Pos.Draw(10, CDirectXDevice::GetD3DDevice());
			this->m_poly->Draw(this->m_BaseData[i].GetPos(), false);
		}
	}
}

void CBase::createtrap(void)
{
	if (!this->m_key)
	{
		for (int i = 0; i < this->m_MaxBase; i++)
		{
			float fpos = 0.0f;
			C3DObject Pos;
			Pos.Init(0);
			Pos.Info.pos = g_ManagerPos[i];
			Pos.Info.pos.y = 1.4f;
			Pos.Scale(5.0f);
			fpos = (float)(rand() % 10);
			Pos.MoveZ(fpos);
			fpos = (float)(rand() % 10);
			Pos.MoveX(fpos);
			CEnemy::Summons(&Pos, CSummons::TypeList::Trap);
		}
		this->m_key = true;
	}
}

//==========================================================================
// 初期化
void CBase::CParam::Init(C3DObject * pPos, float SearchScale, int bDurableValue)
{
	this->m_Pos = *pPos;
	this->m_SearchScale = SearchScale;
	this->m_DurableValue = bDurableValue;
	this->m_Existence = true;
	this->m_select = 0;
}

//==========================================================================
// 更新
void CBase::CParam::Update(void)
{
	if (this->m_Existence)
	{
		this->m_Stock.Create();
		this->Base();
	}

	if (this->m_DurableValue < 0 && this->m_Existence == true)
	{
		this->m_DurableValue = 0;
		CExplosionEffect::create(&this->m_Pos);
		CBase::setscore(250);
		this->m_Existence = false;
	}
}

//==========================================================================
// ベース
void CBase::CParam::Base(void)
{
	CSummons::Data_t * pPos = nullptr;

	for (int i = 0; ; i++)
	{
		// アドレスを記憶
		pPos = CAlly_AI::GetData(i);

		if (pPos == nullptr)
		{
			break;
		}

		// 判定内ひ入ったとき
		if (this->m_hit.Ball(&this->m_Pos, &pPos->m_PosData.m_Pos, this->m_SearchScale))
		{
			if (this->m_Stock.Release())
			{
				if (this->m_select == 0)
				{
					C3DObject Pos;
					Pos.Init(0);
					Pos.Info.pos = this->m_Pos.Info.pos;
					Pos.Info.pos.y = 1.4f;
					Pos.Scale(5.0f);
					CEnemy::Summons(&Pos, CSummons::TypeList::Spped);
					this->m_select++;
				}
				else if (this->m_select == 1)
				{
					C3DObject Pos;
					Pos.Init(0);
					Pos.Info.pos = this->m_Pos.Info.pos;
					Pos.Info.pos.y = 1.4f;
					Pos.Scale(5.0f);
					CEnemy::Summons(&Pos, CSummons::TypeList::Attacker);
					this->m_select++;
				}
				else if (this->m_select == 2)
				{
					C3DObject Pos;
					Pos.Init(0);
					Pos.Info.pos = this->m_Pos.Info.pos;
					Pos.Info.pos.y = 1.4f;
					Pos.Scale(5.0f);
					CEnemy::Summons(&Pos, CSummons::TypeList::Defender);
					this->m_select = 0;
				}
			}
		}
	}
}

//==========================================================================
// 生成上限指定
void CBase::CStock::Init(int nLimit)
{
	this->m_CreateCount = -1;
	this->m_ReleaseFlame = -1;
	this->m_Limit = nLimit;
	this->m_Num = (int)(nLimit - (nLimit / 2) + 0.5f);
}

//==========================================================================
// 生成
void CBase::CStock::Create(void)
{
	// 上限と一致していない時
	if (this->m_Limit != this->m_Num)
	{
		this->m_CreateCount++;
		// カウンタの間隔
		if (this->m_CreateCount == this->m_CreateLimit)
		{
			this->m_CreateCount = -1;
			this->m_Num++;
		}
	}
}

//==========================================================================
// 生成した数分排出
// 排出時true 非排出時 false
bool CBase::CStock::Release(void)
{
	this->m_ReleaseFlame++;
	if (this->m_ReleaseFlame == this->m_ReleaseFlameLimit)
	{
		this->m_ReleaseFlame = -1;
		if (this->m_Num != 0)
		{
			this->m_Num--;
			return true;
		}
	}
	return false;
}
