//==========================================================================
// 向きベクトル[OrientationVector.cpp]
// author: tatuya ogawa
//==========================================================================
#include "OrientationVector.h"

//==========================================================================
// 前進
bool COrientationVector::Advance(D3DXVECTOR3 vec)
{
	D3DXVECTOR3 vec1 = D3DXVECTOR3(0, 0, 1); // 右
	D3DXVECTOR3 vec2 = D3DXVECTOR3(-1, 0, 0); // 左

	D3DXVec3Normalize(&vec, &vec);

	if (vec2.x <= vec.x&&vec.x <= vec1.x)
	{
		if (vec2.z <= vec.z&&vec.z <= vec1.z)
		{
			return true;
		}
	}

	return false;
}

//==========================================================================
// 後進
bool COrientationVector::Backward(D3DXVECTOR3 vec)
{
	D3DXVECTOR3 vec1 = D3DXVECTOR3(1, 0, 0); // 右
	D3DXVECTOR3 vec2 = D3DXVECTOR3(0, 0, -1); // 左

	D3DXVec3Normalize(&vec, &vec);

	if (vec2.x <= vec.x&&vec.x <= vec1.x)
	{
		if (vec2.z <= vec.z&&vec.z <= vec1.z)
		{
			return true;
		}
	}

	return false;
}

//==========================================================================
// 右
bool COrientationVector::Right(D3DXVECTOR3 vec)
{
	D3DXVECTOR3 vec1 = D3DXVECTOR3(1, 0, 0); // 右前
	D3DXVECTOR3 vec2 = D3DXVECTOR3(0, 0, 1); // 右後

	D3DXVec3Normalize(&vec, &vec);

	if (vec2.x <= vec.x&&vec.x <= vec1.x)
	{
		if (vec.z <= vec2.z&&vec1.z <= vec.z)
		{
			return true;
		}
	}

	return false;
}

//==========================================================================
// 左
bool COrientationVector::Left(D3DXVECTOR3 vec)
{
	D3DXVECTOR3 vec1 = D3DXVECTOR3(-1, 0, 0); // 左前
	D3DXVECTOR3 vec2 = D3DXVECTOR3(0, 0, -1); // 左後

	D3DXVec3Normalize(&vec, &vec);

	if (vec.x <= vec2.x&&vec1.x <= vec.x)
	{
		if (vec2.z <= vec.z&&vec.z <= vec1.z)
		{
			return true;
		}
	}

	return false;
}

//==========================================================================
// 右前進
bool COrientationVector::AdvanceRight(D3DXVECTOR3 vec)
{
	D3DXVECTOR3 vec1 = D3DXVECTOR3(-1, 0, 1); // 前
	D3DXVECTOR3 vec2 = D3DXVECTOR3(1, 0, 1); // 右

	D3DXVec3Normalize(&vec, &vec);

	if (vec.x <= vec2.x&&vec1.x <= vec.x)
	{
		if (vec.z <= vec2.z&&vec.z <= vec1.z)
		{
			return true;
		}
	}

	return false;
}

//==========================================================================
// 左前進
bool COrientationVector::AdvanceLeft(D3DXVECTOR3 vec)
{
	D3DXVECTOR3 vec1 = D3DXVECTOR3(-1, 0, 1); // 前
	D3DXVECTOR3 vec2 = D3DXVECTOR3(-1, 0, -1); // 左

	D3DXVec3Normalize(&vec, &vec);

	if (vec2.x <= vec.x&&vec1.x <= vec.x)
	{
		if (vec2.z <= vec.z&&vec.z <= vec1.z)
		{
			return true;
		}
	}

	return false;
}

//==========================================================================
// 右後進
bool COrientationVector::BackwardRight(D3DXVECTOR3 vec)
{
	D3DXVECTOR3 vec1 = D3DXVECTOR3(1, 0, -1); // 後
	D3DXVECTOR3 vec2 = D3DXVECTOR3(1, 0, 1); // 右

	D3DXVec3Normalize(&vec, &vec);

	if (vec.x <= vec2.x&&vec.x <= vec1.x)
	{
		if (vec.z <= vec2.z&&vec1.z <= vec.z)
		{
			return true;
		}
	}

	return false;
}

//==========================================================================
// 左後進
bool COrientationVector::BackwardLeft(D3DXVECTOR3 vec)
{
	D3DXVECTOR3 vec1 = D3DXVECTOR3(1, 0, -1); // 後
	D3DXVECTOR3 vec2 = D3DXVECTOR3(-1, 0, -1); // 左

	D3DXVec3Normalize(&vec, &vec);

	if (vec2.x <= vec.x&&vec.x <= vec1.x)
	{
		if (vec2.z <= vec.z&&vec1.z <= vec.z)
		{
			return true;
		}
	}

	return false;
}
