//==========================================================================
// ポーズの背景[pausebackground.cpp]
// author : 
//==========================================================================
#include "pausebackground.h"

//==========================================================================
// 実体化
//==========================================================================
bool CPausebackground::m_key = false;

CPausebackground::CPausebackground()
{
	this->m_masterpos = nullptr;
	this->m_poly = nullptr;
	this->m_pos = nullptr;
	this->m_numdata = 0;
}

CPausebackground::~CPausebackground()
{
}

//==========================================================================
// 初期化
bool CPausebackground::Init(void)
{
	// テクスチャのパス
	const char * pTexList[]=
	{
		"resource/texture/Game/UI/pose_window.DDS",
	};

	float fUIScale = 1.0f; // UIスケール調整用

	// データ数記録
	this->m_numdata = this->Sizeof(pTexList);

	// 動的メモリ確保 確保領域1
	this->New(this->m_masterpos, this->m_numdata);
	this->New(this->m_pos, this->m_numdata);
	this->New(this->m_poly);

	// 非アクティブ化
	this->m_key = false;

	// テクスチャの初期化
	if (this->m_poly->Init(pTexList, this->m_numdata, true))
	{
		return true;
	}

	// テクスチャのサイズ補正
	for (int i = 0; i < this->m_numdata; i++)
	{
		this->m_poly->SetTexScale(i, fUIScale);
	}

	// 初期化処理
	for (int i = 0; i < this->m_numdata; i++)
	{
		this->m_pos[i].Init(0);
	}

	// ここにUIの座標設定
	this->m_pos[0].SetCentralCoordinatesMood(true);
	this->m_pos[0].SetX((float)(CDirectXDevice::GetWindowsSize().m_Width / 2));
	this->m_pos[0].SetY((float)(CDirectXDevice::GetWindowsSize().m_Height / 2));

	// データのコピー
	for (int i = 0; i < this->m_numdata; i++)
	{
		this->m_masterpos[i] = this->m_pos[i];
	}

	return false;
}

//==========================================================================
// 解放
void CPausebackground::Uninit(void)
{
	// マテリアルの解放
	this->m_poly->Uninit();

	// メモリ解放
	this->Delete(this->m_masterpos);
	this->Delete(this->m_poly);
	this->Delete(this->m_pos);

	// 非アクティブ化
	this->m_key = false;
	this->m_numdata = 0;
}

//==========================================================================
// 更新
void CPausebackground::Update(void)
{
	static bool bkey = false;
	if (this->m_key == false)
	{
		if (CKeyboard::Trigger(CKeyboard::KeyList::KEY_P) || CController::CButton::Trigger(CController::CButton::Ckey::SOPTIONSButton) || CController::CButton::Trigger(CController::CButton::Ckey::SHAREButton) || CController::CButton::Trigger(CController::CButton::Ckey::PSButton) || CController::CButton::Trigger(CController::CButton::Ckey::TouchPad))
		{
			if (!bkey)
			{
				this->m_key = true;
			}
			bkey = true;
		}
		else
		{
			bkey = false;
		}
	}

	if (this->m_key == true)
	{
		if (CKeyboard::Trigger(CKeyboard::KeyList::KEY_P) || CController::CButton::Trigger(CController::CButton::Ckey::SOPTIONSButton) || CController::CButton::Trigger(CController::CButton::Ckey::SHAREButton) || CController::CButton::Trigger(CController::CButton::Ckey::PSButton) || CController::CButton::Trigger(CController::CButton::Ckey::TouchPad))
		{
			if (!bkey)
			{
				this->m_key = false;
			}
			bkey = true;
		}
		else
		{
			bkey = false;
		}
	}
}

//==========================================================================
// 描画
void CPausebackground::Draw(void)
{
	// アクティブな時のみ処理
	if (this->m_key)
	{
		// 描画処理
		for (int i = 0; i < this->m_numdata; i++)
		{
			this->m_poly->Draw(&this->m_pos[i]);
		}
	}
}
