//==========================================================================
// ���[Arrow.cpp]
// author: tatuya ogawa
//==========================================================================
#include "Arrow.h"

CArrow::CArrow()
{
	this->m_poly = nullptr;
	this->m_pos = nullptr;
}

CArrow::~CArrow()
{
}

//==========================================================================
// ������
bool CArrow::Init(const char * ptexname)
{
	this->m_temp.New_(this->m_poly);
	this->m_temp.New_(this->m_pos);

	this->m_pos->Init(0);

	return this->m_poly->Init(ptexname, CBillboard::PlateList::Up);
}

//==========================================================================
// ���
void CArrow::Uninit(void)
{
	this->m_poly->Uninit();
	this->m_pos->Uninit();
	this->m_temp.Delete_(this->m_poly);
	this->m_temp.Delete_(this->m_pos);
}

//==========================================================================
// �X�V
void CArrow::Update(C3DObject * Input)
{
	this->m_pos->Info = Input->Info;
	this->m_pos->Look = Input->Look;
	this->m_pos->Vec = Input->Vec;
	this->m_pos->Info.pos.y = 1.4f;
}

//==========================================================================
// �`��
void CArrow::Draw(void)
{
	this->m_pos->Draw(10, CDirectXDevice::GetD3DDevice());
	this->m_poly->Draw(this->m_pos, false);
}
