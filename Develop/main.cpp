//==========================================================================
// メイン関数[main.cpp]
// author: tatuya ogawa
//==========================================================================
#include <crtdbg.h>
#include "GameWindow.h"
#include "SystemWindow.h"

//==========================================================================
//	メイン関数
int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
	hPrevInstance;
	lpCmdLine; 

#if defined(_DEBUG) || defined(DEBUG)
	WNDCLASSEX gamewcex; // ウインドウクラス構造体
	CGameWindow gamewin;

	// ゲームウィンドウ登録成功時
	if (!gamewin.Window(&gamewcex, hInstance))
	{
		gamewin.SetSerectMode(gamewin.NumWindowSize() - 3);

		gamewin.SetGraphicMode(false);

		// ゲームウィンドウの更新
		return gamewin.WindowUpdate(&gamewcex, nCmdShow);
	}
#else
	WNDCLASSEX gamewcex; // ウインドウクラス構造体
	WNDCLASSEX syswcex; // ウインドウクラス構造体
	CGameWindow gamewin;
	CSystemWindow syswin;

	// ゲームウィンドウ登録成功時
	if (!gamewin.Window(&gamewcex, hInstance))
	{
		// ウィンドウモードの取得
		for (int i = 0; i < gamewin.NumWindowSize(); i++)
		{
			int msesa = gamewin.GetWinSize(i).w;
			msesa = msesa;
			syswin.setwindata(gamewin.GetWinSize(i).w, gamewin.GetWinSize(i).h);
		}

		// システムウィンドウ登録
		syswin.Window(&syswcex, hInstance);

		// ウィンドウの更新
		syswin.WindowUpdate(&syswcex, nCmdShow);

		gamewin.SetSerectMode(syswin.GetData().m_serect);

		gamewin.SetGraphicMode(syswin.GetData().m_graphicmode);

		// ゲーム開始キーが入力されている時
		if (syswin.GetData().m_key)
		{
			// ゲームウィンドウの更新
			return gamewin.WindowUpdate(&gamewcex, nCmdShow);
		}
	}
#endif
	return 0;
}
