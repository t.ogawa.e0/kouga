//==========================================================================
// 成功演出[successUI.h]
// author : 
//==========================================================================
#ifndef _successUI_h_
#define _successUI_h_

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

//==========================================================================
//
// class  : CSuccessUI
// Content: 成功演出
//
//==========================================================================
class CSuccessUI : private CTemplate, public CObject
{
public:
	CSuccessUI();
	~CSuccessUI();
	// 初期化
	bool Init(void);
	// 解放
	void Uninit(void);
	// 更新
	void Update(void);
	// 描画
	void Draw(void);
	// 処理のアクティブ化
	static void ActiveKey(void) { m_key = true; }
private:
	C2DObject *m_pos; // 座標
	C2DObject *m_masterpos; // マスター座標
	C2DPolygon *m_poly; // ポリゴン
	int m_numdata; // データ数
	int m_AnimCount; //フレームカウント変数
	int m_alpha;	//α値管理変数
	bool m_key2;
	static bool m_key; // 鍵
};

#endif // !_successUI_h_
