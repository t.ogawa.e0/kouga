//==========================================================================
// スクリーン[Screen.cpp]
// author: tatuya ogawa
//==========================================================================
#include "Screen.h"
#include "Player.h"
#include "Ally_AI.h"

//==========================================================================
// 実体
//==========================================================================
bool CScreen::m_screenchange;
CScreen::scenelist_t CScreen::m_name; // name

CScreen::CScreen()
{
	this->m_change = false;
	this->m_count = 0;
	this->m_start = false;
	this->m_debugkey = false;
	this->m_bFillmode = false;
	this->m_bFittering = false;
	this->m_bUpdatestop = false;
	this->m_bstartinitdata = true; // 初期時のみ読み込もデータがあるときtrueに
	this->m_bstartdraw = false;
}

CScreen::~CScreen()
{
}

//==========================================================================
// 初期化
bool CScreen::Init(HINSTANCE hInstance, HWND hWnd)
{
	srand((unsigned)time(nullptr));
	if (CDirectXDevice::CreateDevice()) { return true; }
	if (CKeyboard::Init(hInstance, hWnd)) { return true; }
	if (CMouse::Init(hInstance, hWnd)) { return true; }
	if (CController::Init(hInstance, hWnd)) { return true; }
	if (CSoundDevice::Init(hWnd)) { return true; }
#if defined(_DEBUG) || defined(DEBUG)
	CDirectXDebug::Init(CDirectXDevice::GetWindowsSize().m_Width, CDirectXDevice::GetWindowsSize().m_Height, CDirectXDevice::GetD3DDevice());
#endif
	this->m_change = false;
	this->m_count = 0;
	this->m_start = false;
	this->m_debugkey = false;
	this->m_name = scenelist_t::NOME;
	this->m_screenchange = false;
	this->m_bFillmode = false;
	this->m_bFittering = CDirectXDevice::GetGraphicMode();

	if (this->m_fade.Init()) { return true; }

	this->m_ImGui.Init(hWnd, CDirectXDevice::GetD3DDevice());

	this->m_text.size(30);
	this->m_text.init(CDirectXDevice::GetWindowsSize().m_Width, CDirectXDevice::GetWindowsSize().m_Height);
	this->m_text.pos((int)(CDirectXDevice::GetWindowsSize().m_Width / 2.1f), CDirectXDevice::GetWindowsSize().m_Height / 2);

	return this->m_load.Init();
}

//==========================================================================
// 解放
void CScreen::Uninit(void)
{
	this->m_scene.Uninit();
	this->m_fade.Uninit();
	this->m_load.Uninit();
	CDirectXDevice::Uninit();
	CKeyboard::Uninit();
	CMouse::Uninit();
	CController::Uninit();
	CSoundDevice::Uninit();
	this->startdatarelease();
	this->m_text.uninit();
#if defined(_DEBUG) || defined(DEBUG)
	CDirectXDebug::Uninit();
#endif
	this->m_ImGui.Uninit();
}

//==========================================================================
// 更新処理
bool CScreen::Update(void)
{
	this->m_ImGui.NewFrame();
	this->m_ImGui.Update();

	if (this->startinit()) { return true; }

	if (this->m_bstartinitdata == false)
	{
		if (this->screeninit()) { return true; }

		CKeyboard::Update();
		CMouse::Update();
		CController::Update();
		this->m_fade.Update();
		this->fade();

		// ロードが終わっているとき
		if (this->m_change == true)
		{
			if (!this->Updatestop())
			{
				this->m_scene.Update();
				this->m_camera.Update();
			}
		}
		else
		{
			this->m_load.Update();
		}
	}

	this->m_ImGui.EndFrame();

	return false;
}

//==========================================================================
// 描画処理
void CScreen::Draw(void)
{
	LPDIRECT3DDEVICE9 pDevice = CDirectXDevice::GetD3DDevice();	//デバイス渡し	
	D3DPRESENT_PARAMETERS pd3dpp = CDirectXDevice::Getd3dpp();

	pDevice->Clear(0, nullptr, (D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER), D3DCOLOR_RGBA(50, 50, 50, 255), 1.0f, 0);

	//Direct3Dによる描画の開始
	if (SUCCEEDED(pDevice->BeginScene()))
	{
		this->Fillmode(pDevice);
		this->Fittering(pDevice);

		// 画面切り替えが終わっているとき
		if (this->m_change == true)
		{
			this->m_scene.Draw();
			this->m_debug.Draw();
			this->m_fade.Draw();
		}
		else
		{
			this->m_load.Draw();
			this->startdraw();
		}
		this->m_ImGui.Draw();
		pDevice->EndScene();//Direct3Dによる描画の終了
	}

	this->m_ImGui.DeviceReset(pDevice->Present(nullptr, nullptr, nullptr, nullptr), pDevice, &pd3dpp);
}

//==========================================================================
// シーン変更キー
void CScreen::screenchange(scenelist_t Name)
{
	m_screenchange = true;
	m_name = Name;
}

//==========================================================================
// スクリーンの切り替え
bool CScreen::change(scenelist_t Name)
{
	// 各シーンのカメラ切り替え
	this->m_camera.Set(Name);

	// デバッグの切り替え
	this->m_debug.Set(Name);

	// すべての初期化
	return this->m_scene.ChangeScene(Name);
}

//==========================================================================
// 最初の初期化
bool CScreen::startinit(void)
{
	// 最初に読み込むデータがある場合のみ有効
	if (this->m_bstartdraw)
	{
		// この間にセット
		if (CPlayer::InitTex()) { return true; }
		if (CAlly_AI::InitTex()) { return true; }
		// この間にセット
		this->m_bstartinitdata = false;
		this->m_bstartdraw = false;
	}

	return false;
}

//==========================================================================
// 最初の初期化時の描画
void CScreen::startdraw(void)
{
	// 最初に読み込むデータがある場合のみ有効
	if (this->m_bstartinitdata)
	{
		this->m_text.textnew();
		this->m_text.text("読み込み中");
		this->m_text.textend();
		this->m_bstartdraw = true;
	}
}

//==========================================================================
// 最初の初期化データの破棄
void CScreen::startdatarelease(void)
{
	CPlayer::UninitTex();
	CAlly_AI::UninitTex();
}

//==========================================================================
// スクリーンの初期化
bool CScreen::screeninit(void)
{
	//==========================================================================
	// デバッグ用
#if defined(_DEBUG) || defined(DEBUG)
	if (this->m_count == 50 && this->m_change == false && this->m_start == true&& this->m_debugkey == true)
	{
		if (this->change(scenelist_t::Title)) { return true; }
		this->m_debugkey = false;
	}
#endif
	//==========================================================================
	// 最初の画面
	if (this->m_count == 50 && this->m_change == false && this->m_start == false)
	{
		this->m_start = true;
		if (this->change(scenelist_t::Title)) { return true; }
	}

	//==========================================================================
	// シーン変更
	if (this->m_count == 50 && this->m_change == false && this->m_name != scenelist_t::NOME)
	{
		this->m_start = true;
		if (this->change(this->m_name)) { return true; }
		this->m_name = scenelist_t::NOME;
	}

	//==========================================================================
	// ここから下カウンタ
	if (this->m_count == 100 && this->m_change == false)
	{
		this->m_fade.Out();
		this->m_change = true;
	}

	if (this->m_change == false)
	{
		this->m_count++;
	}

	return false;
}

//==========================================================================
// フェード
void CScreen::fade(void)
{
	// フェードイン
	if (this->m_screenchange)
	{
		if (!this->m_fade.GetDraw())
		{
			this->m_fade.In();
		}
	}

#if defined(_DEBUG) || defined(DEBUG)
	// フェード処理がされていないとき
	if (!this->m_fade.GetDraw())
	{
		// 制作用
		if (CKeyboard::Trigger(CKeyboard::KeyList::KEY_F1))
		{
			// フェード処理開始フラグ
			this->m_debugkey = true;
			this->m_fade.In();
		}
	}
#endif

	// フェードが終わりスクリーンがロード画面ではない場合
	if (this->m_fade.FeadInEnd() && this->m_change == true)
	{
		this->m_screenchange = false;
		this->m_change = false;
		this->m_count = 0;
	}
}

//==========================================================================
// 描画モード
void CScreen::Fillmode(LPDIRECT3DDEVICE9 pDevice)
{
#if defined(_DEBUG) || defined(DEBUG)
	if (CKeyboard::Trigger(CKeyboard::KeyList::KEY_F2))
	{
		this->Bool(&this->m_bFillmode);
	}

	if (this->m_bFillmode)
	{
		this->SetRenderWIREFRAME(pDevice);
	}
	else
	{
		this->SetRenderSOLID(pDevice);
	}
#else
	this->Decoy(pDevice);
#endif
}

void CScreen::Fittering(LPDIRECT3DDEVICE9 pDevice)
{
#if defined(_DEBUG) || defined(DEBUG)
	if (CKeyboard::Trigger(CKeyboard::KeyList::KEY_F3))
	{
		this->Bool(&this->m_bFittering);
	}

	if (this->m_bFittering)
	{
		this->SamplerFitteringGraphical(pDevice);
	}
	else
	{
		this->SamplerFitteringLINEAR(pDevice);
	}
#else
	this->Decoy(pDevice);
#endif
}

bool CScreen::Updatestop(void)
{
#if defined(_DEBUG) || defined(DEBUG)
	if (CKeyboard::Trigger(CKeyboard::KeyList::KEY_F4))
	{
		this->Bool(&this->m_bUpdatestop);
	}
#endif
	return this->m_bUpdatestop;
}
