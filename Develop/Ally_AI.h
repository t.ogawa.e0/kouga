//==========================================================================
// 味方のAI[Ally_AI.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _Ally_AI_H_
#define _Ally_AI_H_

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "Summons.h"
#include "OrientationVector.h"

//==========================================================================
//
// class  : CAlly_AI
// Content: 味方のAI
//
//==========================================================================
class CAlly_AI : public CObject, private COrientationVector
{
public:
	enum class Motionpattern
	{
		idou,
		kougeki,
	};
public:
	CAlly_AI();
	~CAlly_AI();
	// 初期化
	bool Init(void);
	// 解放
	void Uninit(void);
	// 更新
	void Update(void);
	// 描画
	void Draw(void);
	// 召喚
	static void Summons(C3DObject * pPos, CSummons::TypeList Type);

	// ゲッタ
	static CSummons::Data_t * GetData(int num);

	static bool InitTex(void);
	static void UninitTex(void);
private:
	// 移動
	void Move(CSummons::Data_t * pData);
	// 攻撃
	bool Attack(CSummons::Data_t * pData);
	// ロックオン
	void Rockoon(CSummons::Data_t * pData);
	// 向きアニメーション
	void SelectAnimation(CSummons::Data_t * pData, Motionpattern motion);
private:
	static CSummons * m_Summons; // 召喚
	static CTemplates m_temp;
	CHitDetermination m_hit;
	CImGui_Dx9 m_ImGui;
};


#endif // !_Ally_AI_H_
