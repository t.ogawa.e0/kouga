//==========================================================================
// 召喚サークル[Circle.cpp]
// author: tatuya ogawa
//==========================================================================
#include "Circle.h"
#include "Player.h"

//==========================================================================
// 実体
//==========================================================================
C3DObject * CCircle::m_pos = nullptr;
int CCircle::m_serect = 0;
int CCircle::m_serectdrawcount = 0;
CSummonsKey::KeyList CCircle::m_Inputkey = CSummonsKey::KeyList::max;

CCircle::CCircle()
{
	this->m_poly = nullptr;
	this->m_movepos = nullptr;
	this->m_count = 0;
	this->m_count2 = 0;
	this->m_drawserect = false;
}

CCircle::~CCircle()
{
}

//==========================================================================
// 初期化
bool CCircle::Init(void)
{
	this->m_temp.New_(this->m_pos);
	this->m_temp.New_(this->m_poly);
	this->m_temp.New_(this->m_movepos);

	this->m_movepos->Init(0);
	this->m_movepos->Scale(5.0f);

	*this->m_pos = *this->m_movepos;

	this->m_count = 0;
	this->m_count2 = 0;
	this->m_drawserect = false;
	this->m_serect = 0;
	this->m_serectdrawcount = this->m_Updateframe;
	this->m_Inputkey = CSummonsKey::KeyList::max;

	return this->m_poly->Init("resource/texture/Game/enn.DDS", this->m_Updateframe, 4, 4, CBillboard::PlateList::Up);
}

//==========================================================================
// 解放
void CCircle::Uninit(void)
{
	this->m_poly->Uninit();
	this->m_count = 0;
	this->m_count2 = 0;
	this->m_drawserect = false;
	this->m_serect = 0;
	this->m_serectdrawcount = this->m_Updateframe;
	this->m_Inputkey = CSummonsKey::KeyList::max;

	this->m_temp.Delete_(this->m_pos);
	this->m_temp.Delete_(this->m_poly);
	this->m_temp.Delete_(this->m_movepos);
}

//==========================================================================
// 更新
void CCircle::Update(void)
{
	this->Move();
	this->m_movepos->Info.pos.y = 1.4f;

	this->m_pos->RotX(0.025f);

	this->m_pos->Info.pos = this->m_movepos->Info.pos;
	this->m_pos->Info.pos += CPlayer::GetPos()->Info.pos;
	this->m_serectdrawcount++;

	if (this->m_serectdrawcount <= this->m_Updateframe)
	{
		switch (this->m_Inputkey)
		{
		case CSummonsKey::KeyList::RightButton1:
			m_serect = ((int)1)*m_Updateframe;
			break;
		case CSummonsKey::KeyList::RightButton2:
			m_serect = ((int)2)*m_Updateframe;
			break;
		case CSummonsKey::KeyList::RightButton3:
			m_serect = ((int)3)*m_Updateframe;
			break;
		case CSummonsKey::KeyList::RightButton4:
			m_serect = ((int)0)*m_Updateframe;
			break;
		default:
			break;
		}

		if (5 <= this->m_count2)
		{
			this->m_count2 = 0;
			this->m_temp.Bool_(&this->m_drawserect);
		}
		this->m_count2++;
	}

	if (this->m_Updateframe <= this->m_serectdrawcount)
	{
		this->m_poly->UpdateAnimation(&this->m_count);
	}
	else
	{
		if (this->m_drawserect)
		{
			this->m_poly->UpdateAnimation(&this->m_serect);
		}
		else
		{
			this->m_poly->UpdateAnimation(&this->m_serect);
		}
	}

	// 召喚サークルの移動制限
	if (!this->m_hit.Ball(this->m_pos, CPlayer::GetPos(), CPlayer::GetPos()->Info.sca.x))
	{
		this->m_movepos->MoveZ(-this->m_MoveSpeed);
	}
}

//==========================================================================
// 描画
void CCircle::Draw(void)
{
	this->m_pos->Draw(10,CDirectXDevice::GetD3DDevice());

	if (this->m_Updateframe <= this->m_serectdrawcount)
	{
		this->m_poly->Draw(this->m_pos, &this->m_count, false);
	}
	else
	{
		if (this->m_drawserect)
		{
			this->m_poly->Draw(this->m_pos, &this->m_serect, true);
		}
		else
		{
			this->m_poly->Draw(this->m_pos, &this->m_serect, false);
		}
	}
}

//==========================================================================
// 召喚キーセット
void CCircle::setsummonkey(CSummonsKey::KeyList input)
{
	m_Inputkey = input;
	m_serectdrawcount = -1;
}

//==========================================================================
// 移動
void CCircle::Move(void)
{
	// ↑
	if ((float)CController::RightStick().m_LeftRight == 0.0f && (float)CController::RightStick().m_UpUnder <= -1.0f)
	{
		this->Advance();
	}
	// ↓
	else if ((float)CController::RightStick().m_LeftRight == 0.0f && 1.0f <= (float)CController::RightStick().m_UpUnder)
	{
		this->Backward();
	}
	// →
	else if (1.0f <= (float)CController::RightStick().m_LeftRight && (float)CController::RightStick().m_UpUnder == 0.0f)
	{
		this->Right();
	}
	// ←
	else if ((float)CController::RightStick().m_LeftRight <= -1.0f && (float)CController::RightStick().m_UpUnder == 0.0f)
	{
		this->Left();
	}
	// 右上
	else if (1.0f <= (float)CController::RightStick().m_LeftRight && (float)CController::RightStick().m_UpUnder <= -1.0f)
	{
		this->AdvanceRight();
	}
	// 左上
	else if ((float)CController::RightStick().m_LeftRight <= -1.0f && (float)CController::RightStick().m_UpUnder <= -1.0f)
	{
		this->AdvanceLeft();
	}
	// 右下
	else if (1.0f <= (float)CController::RightStick().m_LeftRight && 1.0f <= (float)CController::RightStick().m_UpUnder)
	{
		this->BackwardRight();
	}
	// 左下
	else if ((float)CController::RightStick().m_LeftRight <= -1.0f && 1.0f <= (float)CController::RightStick().m_UpUnder)
	{
		this->BackwardLeft();
	}
	else if (CKeyboard::Press(CKeyboard::KeyList::KEY_I) && CKeyboard::Press(CKeyboard::KeyList::KEY_J))
	{
		this->AdvanceLeft();
	}
	else if (CKeyboard::Press(CKeyboard::KeyList::KEY_I) && CKeyboard::Press(CKeyboard::KeyList::KEY_L))
	{
		this->AdvanceRight();
	}
	else if (CKeyboard::Press(CKeyboard::KeyList::KEY_K) && CKeyboard::Press(CKeyboard::KeyList::KEY_J))
	{
		this->BackwardLeft();
	}
	else if (CKeyboard::Press(CKeyboard::KeyList::KEY_K) && CKeyboard::Press(CKeyboard::KeyList::KEY_L))
	{
		this->BackwardRight();
	}
	else if (CKeyboard::Press(CKeyboard::KeyList::KEY_I))
	{
		this->Advance();
	}
	else if (CKeyboard::Press(CKeyboard::KeyList::KEY_J))
	{
		this->Left();
	}
	else if (CKeyboard::Press(CKeyboard::KeyList::KEY_K))
	{
		this->Backward();
	}
	else if (CKeyboard::Press(CKeyboard::KeyList::KEY_L))
	{
		this->Right();
	}
}

//==========================================================================
// 前進
CVector<float> CCircle::Advance(void)
{
	this->m_movepos->SetVecFront({ -1,0,1 });
	this->m_movepos->MoveZ(this->m_MoveSpeed);

	return CVector<float>(0.0f, 0.0f, 0.0f, 0.0f);
}

//==========================================================================
// 後進
CVector<float> CCircle::Backward(void)
{
	this->m_movepos->SetVecFront({ 1,0,-1 });
	this->m_movepos->MoveZ(this->m_MoveSpeed);

	return CVector<float>(0.0f, 0.0f, 0.0f, 0.0f);
}

//==========================================================================
// 右
CVector<float> CCircle::Right(void)
{
	this->m_movepos->SetVecFront({ 1,0,1 });
	this->m_movepos->MoveZ(this->m_MoveSpeed);

	return CVector<float>(0.0f, -1.3f, 0.0f, 0.0f);
}

//==========================================================================
// 左
CVector<float> CCircle::Left(void)
{
	this->m_movepos->SetVecFront({ -1,0,-1 });
	this->m_movepos->MoveZ(this->m_MoveSpeed);

	return CVector<float>(0.0f, -1.3f, 0.0f, 0.0f);
}

//==========================================================================
// 右前進
CVector<float> CCircle::AdvanceRight(void)
{
	this->m_movepos->SetVecFront({ 0,0,1 });
	this->m_movepos->MoveZ(this->m_MoveSpeed);

	return CVector<float>(0.0f, -1.3f, 0.0f, 0.0f);
}

//==========================================================================
// 左前進
CVector<float> CCircle::AdvanceLeft(void)
{
	this->m_movepos->SetVecFront({ -1,0,0 });
	this->m_movepos->MoveZ(this->m_MoveSpeed);

	return CVector<float>(0.0f, -1.3f, 0.0f, 0.0f);
}

//==========================================================================
// 右後進
CVector<float> CCircle::BackwardRight(void)
{
	this->m_movepos->SetVecFront({ 1,0,0 });
	this->m_movepos->MoveZ(this->m_MoveSpeed);

	return CVector<float>(0.0f, -1.3f, 0.0f, 0.0f);
}

//==========================================================================
// 左後進
CVector<float> CCircle::BackwardLeft(void)
{
	this->m_movepos->SetVecFront({ 0,0,-1 });
	this->m_movepos->MoveZ(this->m_MoveSpeed);

	return CVector<float>(0.0f, -1.3f, 0.0f, 0.0f);
}
