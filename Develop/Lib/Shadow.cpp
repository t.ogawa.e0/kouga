//==========================================================================
// 影[Shadow.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Shadow.h"

CShadow::CShadow()
{
	this->m_pIndexBuffer = nullptr;
	this->m_pVertexBuffer = nullptr;
	this->m_pPseudo = nullptr;
	this->m_Lock = false;
}

CShadow::~CShadow()
{
	// バッファ解放
	this->m_temp.Release_(this->m_pVertexBuffer);

	// インデックスバッファ解放
	this->m_temp.Release_(this->m_pIndexBuffer);

	this->m_texture.release();

	this->m_Lock = false;
}

//==========================================================================
// 初期化 失敗時true
// Input = 使用するテクスチャのパス
bool CShadow::Init(const char * Input)
{
	CUv<float> uUV;
	WORD* pIndex;

	if (this->m_Lock == false)
	{
		if (this->m_texture.init(Input))
		{
			return true;
		}

		this->m_pIndexBuffer = nullptr;
		this->m_pVertexBuffer = nullptr;

		uUV = CUv<float>(0.0f, 0.0f, 1.0f, 1.0f);

		if (this->CreateVertexBuffer(sizeof(VERTEX_4) * 4, D3DUSAGE_WRITEONLY, FVF_VERTEX_4, D3DPOOL_MANAGED, &this->m_pVertexBuffer, nullptr))
		{
			return true;
		}

		if (this->CreateIndexBuffer(sizeof(WORD) * 6, D3DUSAGE_WRITEONLY, D3DFMT_INDEX16, D3DPOOL_MANAGED, &this->m_pIndexBuffer, nullptr))
		{
			return true;
		}

		this->m_pVertexBuffer->Lock(0, 0, (void**)&this->m_pPseudo, D3DLOCK_DISCARD);
		this->VertexBuffer(this->m_pPseudo, &uUV);
		this->m_pVertexBuffer->Unlock(); // ロック解除

		this->m_pIndexBuffer->Lock(0, 0, (void**)&pIndex, D3DLOCK_DISCARD);
		this->RectangleIndex(pIndex, 1);
		this->m_pIndexBuffer->Unlock();	// ロック解除
	}

	return false;
}

//==========================================================================
// 解放
void CShadow::Uninit(void)
{
	// バッファ解放
	this->m_temp.Release_(this->m_pVertexBuffer);

	// インデックスバッファ解放
	this->m_temp.Release_(this->m_pIndexBuffer);

	this->m_texture.release();

	this->m_Lock = false;
}

//==========================================================================
// 描画
// 影をつけたい対象のC3DObject
void CShadow::Draw(const C3DObject * pInput)
{
	LPDIRECT3DDEVICE9 pDevice = CDirectXDevice::GetD3DDevice();

	if (this->m_Lock == true)
	{
		D3DXMATRIX aMtxWorld; // ワールド行列
		C3DObject aObject = *pInput;
		int nColor = DefaltColor - (int)((aObject.Info.pos.y * ColorBuf) + 0.5f);

		aObject.Info.rot = D3DXVECTOR3(0, 0, 0);
		aObject.Info.sca *= aObject.Info.pos.y + 1.0f;

		this->m_temp.New_(aObject.Collar);
		if (nColor <= 0) { nColor = 0; }
		if (aObject.Info.pos.y <= 0) { nColor = 0; }
		*aObject.Collar = CColor<int>(255, 255, 255, nColor);
		this->m_pVertexBuffer->Lock(0, 0, (void**)&this->m_pPseudo, D3DLOCK_DISCARD);
		this->m_pPseudo[0].color = aObject.Collar->get();
		this->m_pPseudo[1].color = aObject.Collar->get();
		this->m_pPseudo[2].color = aObject.Collar->get();
		this->m_pPseudo[3].color = aObject.Collar->get();
		this->m_pVertexBuffer->Unlock(); // ロック解除

		// サイズ
		pDevice->SetStreamSource(0, this->m_pVertexBuffer, 0, sizeof(VERTEX_4));	// パイプライン

		pDevice->SetIndices(this->m_pIndexBuffer);

		// FVFの設定
		pDevice->SetFVF(FVF_VERTEX_4);

		pDevice->SetTexture(0, nullptr);
		pDevice->SetTexture(0, this->m_texture.get(0)->gettex());

		// ライト設定
		pDevice->SetRenderState(D3DRS_LIGHTING, FALSE);

		// 減算処理
		this->SetRenderREVSUBTRACT(pDevice);

		// 各種行列の設定
		aObject.Info.pos.y = 0.0f;

		pDevice->SetTransform(D3DTS_WORLD, aObject.CreateMtxWorld2(&aMtxWorld));

		// 描画設定
		pDevice->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, 4, 0, 2);

		// 戻す
		this->SetRenderADD(pDevice);

		aObject.Uninit();
	}
}

//==========================================================================
// 頂点バッファの生成
void CShadow::VertexBuffer(VERTEX_4 * Output, CUv<float> * uv)
{
	VERTEX_4 vVertex[] =
	{
		// 上
		{ D3DXVECTOR3(-VCRCT,0.0f, VCRCT),D3DXVECTOR3(0,1,0),D3DCOLOR_RGBA(255,255,255,80),D3DXVECTOR2(uv->u0,uv->v0) }, // 左上
		{ D3DXVECTOR3(VCRCT,0.0f, VCRCT),D3DXVECTOR3(0,1,0),D3DCOLOR_RGBA(255,255,255,80),D3DXVECTOR2(uv->u1,uv->v0) }, // 右上
		{ D3DXVECTOR3(VCRCT,0.0f,-VCRCT),D3DXVECTOR3(0,1,0),D3DCOLOR_RGBA(255,255,255,80),D3DXVECTOR2(uv->u1,uv->v1) }, // 右下
		{ D3DXVECTOR3(-VCRCT,0.0f,-VCRCT),D3DXVECTOR3(0,1,0),D3DCOLOR_RGBA(255,255,255,80),D3DXVECTOR2(uv->u0,uv->v1) }, // 左下
	};

	for (int i = 0; i < (int)(sizeof(vVertex) / sizeof(VERTEX_4)); i++)
	{
		Output[i] = vVertex[i];
	}
}
