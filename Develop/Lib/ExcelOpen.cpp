//==========================================================================
// Excelオープン[ExcelOpen.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "ExcelOpen.h"

CExcelOpen::CExcelOpen()
{
}

CExcelOpen::~CExcelOpen()
{
}

//==========================================================================
// ファイルパスのみ読み込み
// 戻り値 bool
bool CExcelOpen::OpenFile(const char* pFile, FILELINK1 *&pOutput, int * Numoutput)
{
	FILE *fp;
	char cText[m_MaxData] = { 0, };
	int ret = 0;
	int nCount = 0;

	fp = fopen(pFile, "r");
	if (fp)
	{
		for (;;)
		{
			cText[0] = 0;
			ret = fscanf(fp, "%s", &cText);
			if (ret == EOF) { break; }
			nCount++;
		}
		fclose(fp);
		(*Numoutput) = nCount - 1;
		pOutput = new FILELINK1[(*Numoutput)];
		fp = fopen(pFile, "r");
		if (fp)
		{
			fscanf(fp, "%s", cText);
			for (int i = 0; i < (*Numoutput); i++)
			{
				fscanf(fp, "%s", &pOutput[i].m_Data);
			}
			fclose(fp);
		}
	}
	else
	{
		CDirectXDevice::ErrorMessage("ファイルが開けません");
		return true;
	}

	return false;
}

//==========================================================================
// ファイルパス &面の数読み込み
// 戻り値 bool
bool CExcelOpen::OpenFile(const char* pFile, FILELINK2 *&pOutput, int * Numoutput)
{
	FILE *fp;
	char cText[m_MaxData] = { 0, };
	int ret = 0;
	int nCount = 0;

	fp = fopen(pFile, "r");
	if (fp)
	{
		for (;;)
		{
			cText[0] = 0;
			ret = fscanf(fp, "%s", &cText);
			if (ret == EOF) { break; }
			nCount++;
		}
		fclose(fp);
		(*Numoutput) = nCount - 1;
		pOutput = new FILELINK2[(*Numoutput)];
		fp = fopen(pFile, "r");
		if (fp)
		{
			fscanf(fp, "%s", cText);
			for (int i = 0; i < (*Numoutput); i++)
			{
				fscanf(fp, "%[^,],%d,%d", &pOutput[i].m_Data, &pOutput[i].m_NumMeshX, &pOutput[i].m_NumMeshZ);
				for (int nBaffa = 1; nBaffa < m_MaxData; nBaffa++)
				{
					pOutput[i].m_Data[nBaffa - 1] = pOutput[i].m_Data[nBaffa];
				}
			}
			fclose(fp);
		}
	}
	else
	{
		CDirectXDevice::ErrorMessage("ファイルが開けません");
		return true;
	}

	return false;
}
