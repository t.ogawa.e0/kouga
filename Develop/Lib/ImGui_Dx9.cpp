//==========================================================================
// ImGui_Dx9[ImGui_Dx9.cpp]
// author: tatuya ogawa
//==========================================================================
#include "ImGui_Dx9.h"

#if defined(_DEBUG) || defined(DEBUG)
extern LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
#endif

CImGui_Dx9::CImGui_Dx9()
{
#if defined(_DEBUG) || defined(DEBUG)
#endif
}

CImGui_Dx9::~CImGui_Dx9()
{
#if defined(_DEBUG) || defined(DEBUG)
#endif
}

//==========================================================================
// 初期化
bool CImGui_Dx9::Init(HWND hWnd, LPDIRECT3DDEVICE9 pDevice)
{
#if defined(_DEBUG) || defined(DEBUG)
	// Setup ImGui binding
	ImGui::CreateContext();
	ImGuiIO&io = ImGui::GetIO();
	(void)io;
	ImGui_ImplDX9_Init(hWnd, pDevice);
	// io.NavFlags |= ImGuiNavFlags_EnableKeyboard;  // Enable Keyboard Controls

	// Setup style
	ImGui::StyleColorsDark();
	ImGui::StyleColorsClassic();

	ImGui::PushStyleColor(ImGuiCol_TitleBgActive, ImVec4(0.5f, 0.0f, 1.0f, 1.0f));
	ImGui::PushStyleColor(ImGuiCol_TitleBg, ImVec4(0.5f, 0.0f, 1.0f, 0.5f));
	ImGui::SetNextWindowPos(ImVec2(20, 20), ImGuiSetCond_Once);
	ImGui::SetNextWindowSize(ImVec2(500, 500), ImGuiSetCond_Once);
#else
	hWnd;pDevice;
#endif
	return false;
}

//==========================================================================
// 解放
void CImGui_Dx9::Uninit(void)
{
#if defined(_DEBUG) || defined(DEBUG)
	ImGui_ImplDX9_Shutdown();
	ImGui::DestroyContext();
#endif
}

//==========================================================================
// 更新
void CImGui_Dx9::Update(void)
{
#if defined(_DEBUG) || defined(DEBUG)
	bool key = true;
	// 1. Show a simple window.
	// Tip: if we don't call ImGui::Begin()/ImGui::End() the widgets automatically appears in a window called "Debug".
	ImGui::Begin("main windows", &key);

	// Display some text (you can use a format string too)
	ImGui::Separator();

	ImGui::Text("Elapsed time %.2f ", ImGui::GetTime());
	ImGui::Separator();
	ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
	ImGui::Separator();
	ImGui::Text("Windows size X=%.2f Y=%.2f", ImGui::GetIO().DisplaySize.x, ImGui::GetIO().DisplaySize.y);
	ImGui::Separator();

	// Rendering
	ImGui::End();
#endif
}

//==========================================================================
// 描画
void CImGui_Dx9::Draw(void)
{
#if defined(_DEBUG) || defined(DEBUG)
	ImGui::Render();
#endif
}

//==========================================================================
// Handle loss of D3D9 device
void CImGui_Dx9::DeviceReset(HRESULT result, LPDIRECT3DDEVICE9 pDevice, D3DPRESENT_PARAMETERS * pd3dpp)
{
#if defined(_DEBUG) || defined(DEBUG)
	if (result == D3DERR_DEVICELOST && pDevice->TestCooperativeLevel() == D3DERR_DEVICENOTRESET)
	{
		ImGui_ImplDX9_InvalidateDeviceObjects();
		pDevice->Reset(pd3dpp);
		ImGui_ImplDX9_CreateDeviceObjects();
	}
#else
	result;pDevice;pd3dpp;
#endif
}

//==========================================================================
// Process Win32 mouse/keyboard inputs. 
IMGUI_API LRESULT CImGui_Dx9::ImGui_WndProcHandler(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
#if defined(_DEBUG) || defined(DEBUG)
	return ImGui_ImplWin32_WndProcHandler(hWnd, uMsg, wParam, lParam);
#else
	hWnd; uMsg; wParam; lParam;
	return LRESULT(0);
#endif
}

//==========================================================================
// メッセージ
LRESULT CImGui_Dx9::SetMenu(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam, LPDIRECT3DDEVICE9 pDevice, D3DPRESENT_PARAMETERS * pd3dpp)
{
#if defined(_DEBUG) || defined(DEBUG)
	switch (uMsg)
	{
	case WM_SIZE:
		if (pDevice != nullptr && wParam != SIZE_MINIMIZED)
		{
			ImGui_ImplDX9_InvalidateDeviceObjects();
			pd3dpp->BackBufferWidth = LOWORD(lParam);
			pd3dpp->BackBufferHeight = HIWORD(lParam);
			HRESULT hr = pDevice->Reset(pd3dpp);
			if (hr == D3DERR_INVALIDCALL)
				IM_ASSERT(0);
			ImGui_ImplDX9_CreateDeviceObjects();
		}
		return 0;
	case WM_SYSCOMMAND:// Disable ALT application menu
		if ((wParam & 0xfff0) == SC_KEYMENU)
		{
			return 0;
		}
		break;
	}
#else
	hWnd; uMsg; wParam; lParam; pDevice; pd3dpp;
#endif
	return DefWindowProc(hWnd, uMsg, wParam, lParam);
}

//==========================================================================
// 新しいウィンドウ
void CImGui_Dx9::NewWindow(const char * name, bool key)
{
#if defined(_DEBUG) || defined(DEBUG)
	// Display some text (you can use a format string too)
	ImGui::Begin(name, &key);
	ImGui::Separator();
#else
	name;
	key;
#endif
}

//==========================================================================
// ウィンドウ終了
void CImGui_Dx9::EndWindow(void)
{
#if defined(_DEBUG) || defined(DEBUG)
	ImGui::End();
#endif
}

//==========================================================================
// 新しいフレーム 一回のみ
void CImGui_Dx9::NewFrame(void)
{
#if defined(_DEBUG) || defined(DEBUG)
	ImGui_ImplDX9_NewFrame();
#endif
}

//==========================================================================
// フレーム終わり 一回のみ
void CImGui_Dx9::EndFrame(void)
{
#if defined(_DEBUG) || defined(DEBUG)
	ImGui::EndFrame();
#endif
}
