//==========================================================================
// メッシュ[Mesh.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Mesh.h"

CMesh::CMesh()
{
	this->m_Link = nullptr;
}

CMesh::~CMesh()
{
	this->m_temp.Delete_(this->m_Link);
	this->m_texture.release();
	this->m_MeshData.clear();
}

////==========================================================================
//// 初期化 Excel 失敗時true
//bool CMesh::Init(const char * pFile)
//{
//	int num = 0;
//	// ファイルオープン
//	if (this->OpenFile(pFile, this->m_Link, &num)) { return true; }
//
//	for (int i = 0; i < num; i++)
//	{
//		if (this->Init(this->m_Link[i].m_Data, this->m_Link[i].m_NumMeshX, this->m_Link[i].m_NumMeshZ))
//		{
//			return true;
//		}
//	}
//
//	this->m_temp.Delete_(this->m_Link);
//
//	return false;
//}
//
////==========================================================================
//// 初期化 失敗時false
//bool CMesh::Init(const EXCELLINK * pFile, int Size)
//{
//	this->InputLink(pFile, Size);
//
//	for (int i = 0; i < Size; i++)
//	{
//		if (this->Init(this->m_Link[i].m_Data, this->m_Link[i].m_NumMeshX, this->m_Link[i].m_NumMeshZ))
//		{
//			return true;
//		}
//	}
//
//	this->m_temp.Delete_(this->m_Link);
//
//	return false;
//}

//==========================================================================
// 初期化 失敗時true
// Input=テクスチャ
// x=横幅
// z=奥行
bool CMesh::Init(const char * Input, int x, int z)
{
	VERTEX_4* pPseudo = nullptr;
	LPWORD* pIndex = nullptr;
	DEGENERATEPOLYGON * pdepoly = nullptr;

	if (this->m_texture.init(Input))
	{
		return true;
	}

	this->m_temp.New_(pdepoly);

	this->m_MeshData.push_back(pdepoly);

	// メッシュフィールドの情報算出
	this->MeshFieldInfo(&pdepoly->Info, x, z);

	if (this->CreateVertexBuffer(sizeof(VERTEX_4) * pdepoly->Info.MaxVertex, D3DUSAGE_WRITEONLY, FVF_VERTEX_4, D3DPOOL_MANAGED, &pdepoly->pVertexBuffer, nullptr))
	{
		return true;
	}

	if (this->CreateIndexBuffer(sizeof(LPWORD) * pdepoly->Info.MaxIndex, D3DUSAGE_WRITEONLY, D3DFMT_INDEX32, D3DPOOL_MANAGED, &pdepoly->pIndexBuffer, nullptr))
	{
		return true;
	}

	pdepoly->pVertexBuffer->Lock(0, 0, (void**)&pPseudo, D3DLOCK_DISCARD);
	this->CreateVertex(pPseudo, &pdepoly->Info);
	pdepoly->pVertexBuffer->Unlock();	// ロック解除

	pdepoly->pIndexBuffer->Lock(0, 0, (void**)&pIndex, D3DLOCK_DISCARD);
	this->CreateIndex(pIndex, &pdepoly->Info);
	pdepoly->pIndexBuffer->Unlock();	// ロック解除

	this->MaterialSetting(pdepoly);

	return false;
}

//==========================================================================
// 初期化 失敗時true
// Input=テクスチャ
// x=横幅
// z=奥行
bool CMesh::Init(const char ** Input, int Size, int x, int z)
{
	for (int i = 0; i < Size; i++)
	{
		if (this->Init(Input[i], x, z))
		{
			return true;
		}
	}
	return false;
}

//==========================================================================
// 解放
void CMesh::Uninit(void)
{
	for (auto itr = this->m_MeshData.begin(); itr != this->m_MeshData.end(); ++itr)
	{
		// バッファ解放
		this->m_temp.Release_((*itr)->pVertexBuffer);

		// インデックスバッファ解放
		this->m_temp.Release_((*itr)->pIndexBuffer);

		this->m_temp.Delete_((*itr));
	}

	this->m_temp.Delete_(this->m_Link);
	this->m_texture.release();
	this->m_MeshData.clear();
}

//==========================================================================
// 描画
void CMesh::Draw(C3DObject * Input)
{
	LPDIRECT3DDEVICE9 pDevice = CDirectXDevice::GetD3DDevice();
	D3DXMATRIX aMtxWorld; // ワールド行列
	DEGENERATEPOLYGON * pdepoly = this->m_MeshData[Input->getindex()];

	// サイズ
	pDevice->SetStreamSource(0, pdepoly->pVertexBuffer, 0, sizeof(VERTEX_4));	// パイプライン

	pDevice->SetIndices(pdepoly->pIndexBuffer);

	// FVFの設定
	pDevice->SetFVF(FVF_VERTEX_4);

	// ライト設定
	pDevice->SetRenderState(D3DRS_LIGHTING, FALSE);

	this->SetRenderALPHAREF_START(pDevice, 100);//アルファ画像によるブレンド

	pDevice->SetMaterial(&pdepoly->pMatMesh);	// マテリアル情報をセット
	pDevice->SetTexture(0, nullptr);

	pDevice->SetTexture(0, this->m_texture.get(Input->getindex())->gettex());

	// 各種行列の設定
	pDevice->SetTransform(D3DTS_WORLD, Input->CreateMtxWorld2(&aMtxWorld));

	pDevice->DrawIndexedPrimitive(D3DPT_TRIANGLESTRIP, 0, 0, pdepoly->Info.MaxVertex, 0, pdepoly->Info.MaxPrimitive);

	this->SetRenderALPHAREF_END(pDevice);
}

//==========================================================================
// リンク入力
void CMesh::InputLink(const EXCELLINK * pInput, int num)
{
	this->m_Link = nullptr;
	this->m_temp.New_(this->m_Link, num);

	for (int i = 0; i < num; i++)
	{
		this->m_Link[i].m_Data[0] = 0;
		this->m_Link[i].m_NumMeshX = pInput[i].m_NumMeshX;
		this->m_Link[i].m_NumMeshZ = pInput[i].m_NumMeshZ;
		strcpy(this->m_Link[i].m_Data, pInput[i].m_Data);
	}
}

//==========================================================================
// メッシュ情報の生成
void CMesh::MeshFieldInfo(DEGENERATEPOLYGONINFORMATION * Output, const int numX, const int numZ)
{
	Output->NumMeshX = numX;
	Output->NumMeshZ = numZ;
	Output->NumXVertex = numX + 1; // 基礎頂点数
	Output->NumZVertex = numZ + 1; // 基礎頂点数
	Output->MaxVertex = Output->NumXVertex * Output->NumZVertex; // 最大頂点数
	Output->NumXVertexWey = x_2 * Output->NumXVertex; // 視覚化されている1列の頂点数
	Output->VertexOverlap = x_2 * (numZ - 1); // 重複する頂点数
	Output->NumMeshVertex = Output->NumXVertexWey * numZ; // 視覚化されている全体の頂点数
	Output->MaxIndex = Output->NumMeshVertex + Output->VertexOverlap; // 最大Index数
	Output->MaxPrimitive = ((numX * numZ) * x_2) + (Output->VertexOverlap * x_2); // プリミティブ数
}

//==========================================================================
// インデックス情報の生成
void CMesh::CreateIndex(LPWORD * Output, const DEGENERATEPOLYGONINFORMATION * Input)
{
	for (int i = 0, Index1 = 0, Index2 = Input->NumXVertex, Overlap = 0; i < Input->MaxIndex; Index1++, Index2++)
	{
		// 通常頂点
		Output[i] = (LPWORD)Index2;
		i++;

		// 重複点
		if (Overlap == Input->NumXVertexWey&&i < Input->MaxIndex)
		{
			Output[i] = (LPWORD)Index2;
			i++;
			Overlap = 0;
		}

		// 通常頂点
		Output[i] = (LPWORD)Index1;
		i++;

		Overlap += x_2;

		// 重複点
		if (Overlap == Input->NumXVertexWey&&i < Input->MaxIndex)
		{
			Output[i] = (LPWORD)Index1;
			i++;
		}
	}
}

//==========================================================================
// バーテックス情報の生成
void CMesh::CreateVertex(VERTEX_4 * Output, const DEGENERATEPOLYGONINFORMATION * Input)
{
	bool bZ = false;
	bool bX = false;
	float fPosZ = Input->NumMeshZ*CorrectionValue;
	float fPosX = 0.0f;

	for (int iZ = 0, Counter = 0; iZ < Input->NumZVertex; iZ++, fPosZ--)
	{
		bX = true;
		fPosX = -(Input->NumMeshX*CorrectionValue);
		for (int iX = 0; iX < Input->NumXVertex; iX++, Counter++, fPosX++, Output++)
		{
			Output->pos = D3DXVECTOR3(fPosX, 0.0f, fPosZ);
			Output->color = D3DCOLOR_RGBA(255, 255, 255, 255);
			Output->Tex = D3DXVECTOR2((FLOAT)bX, (FLOAT)bZ);
			Output->Normal = D3DXVECTOR3(0, 1, 0);
			bX = bX ^ 1;
		}
		bZ = bZ ^ 1;
	}
}

//==========================================================================
// マテリアルの設定
void CMesh::MaterialSetting(DEGENERATEPOLYGON *Output)
{
	ZeroMemory(&Output->pMatMesh, sizeof(Output->pMatMesh));
	Output->pMatMesh.Diffuse.r = 1.0f; // ディレクショナルライト
	Output->pMatMesh.Diffuse.g = 1.0f; // ディレクショナルライト
	Output->pMatMesh.Diffuse.b = 1.0f; // ディレクショナルライト
	Output->pMatMesh.Diffuse.a = 1.0f; // ディレクショナルライト
	Output->pMatMesh.Ambient.r = 0.7f; // アンビエントライト
	Output->pMatMesh.Ambient.g = 0.7f; // アンビエントライト
	Output->pMatMesh.Ambient.b = 0.7f; // アンビエントライト
	Output->pMatMesh.Ambient.a = 1.0f; // アンビエントライト
}

CMesh::DEGENERATEPOLYGON::DEGENERATEPOLYGON()
{
	this->pVertexBuffer = nullptr;
	this->pIndexBuffer = nullptr;
}

CMesh::DEGENERATEPOLYGON::~DEGENERATEPOLYGON()
{
}
