//==========================================================================
// 球体[Screen.cpp]
// author: tatuya ogawa
//==========================================================================
#include "Sphere.h"

CSphere::CSphere()
{
}

CSphere::~CSphere()
{
	this->m_matdata.clear();
	this->m_texture.release();
}

//==========================================================================
// 初期化
// Input = 使用するテクスチャのパス
// SubdivisionRatio = 品質 20あれば十分綺麗な球体になる
bool CSphere::Init(const char * ptex, int SubdivisionRatio)
{
	LPDIRECT3DDEVICE9 pDevice = CDirectXDevice::GetD3DDevice();
	CSphereData* matdata = nullptr;

	if (this->m_texture.init(ptex)) { return true; }

	if (SubdivisionRatio <= 2)
	{
		SubdivisionRatio = 2;
	}

	this->m_temp.New_(matdata);
	this->m_matdata.push_back(matdata);

	if (this->OverlapSearch(matdata, SubdivisionRatio) == false)
	{
		if (matdata->CreateSphere(SubdivisionRatio, pDevice))
		{
			return true;
		}

		matdata->convert();

		matdata->setmat();
	}

	return false;
}

//==========================================================================
// 初期化
// Input = 使用するテクスチャのパス ダブルポインタに対応
// num = 要素数
// SubdivisionRatio = 品質 20あれば十分綺麗な球体になる
bool CSphere::Init(const char ** ptex, int num, int SubdivisionRatio)
{
	for (int i = 0; i < num; i++)
	{
		if (this->Init(ptex[i], SubdivisionRatio))
		{
			return true;
		}
	}

	return false;
}

//==========================================================================
// 初期化
// Input = 使用するテクスチャのパス ダブルポインタに対応
// num = 要素数
// SubdivisionRatio = 品質 20あれば十分綺麗な球体になる
bool CSphere::Init(const char ** ptex, int num, int *SubdivisionRatio)
{
	for (int i = 0; i < num; i++)
	{
		if (this->Init(ptex[i], SubdivisionRatio[i]))
		{
			return true;
		}
	}

	return false;
}

//==========================================================================s
// 解放
void CSphere::Uninit(void)
{
	for (auto itr = this->m_matdata.begin(); itr != this->m_matdata.end(); ++itr)
	{
		(*itr)->release();
		this->m_temp.Delete_((*itr));
	}
	this->m_matdata.clear();
	this->m_texture.release();
}

//==========================================================================
// 描画
void CSphere::Draw(C3DObject * Input)
{
	LPDIRECT3DDEVICE9 pDevice = CDirectXDevice::GetD3DDevice();
	D3DXMATRIX aMtxWorld; // ワールド行列
	CSphereData* matdata = this->m_matdata[Input->getindex()];

	pDevice->SetFVF(matdata->m_mesh2->GetFVF());

	pDevice->SetTransform(D3DTS_WORLD, Input->CreateMtxWorld1(&aMtxWorld));

	// パンチ抜き
	this->SetRenderALPHAREF_START(pDevice, 100);
	pDevice->SetRenderState(D3DRS_WRAP0, D3DWRAPCOORD_0);
	pDevice->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW); // 反時計回りの面を消去
	pDevice->SetRenderState(D3DRS_LIGHTING, D3DZB_FALSE); // ライティング
	pDevice->SetRenderState(D3DRS_ZENABLE, D3DZB_TRUE);

	// マテリアルの設定
	pDevice->SetMaterial(&matdata->m_mat);

	pDevice->SetTexture(0, nullptr);
	pDevice->SetTexture(0, this->m_texture.get(Input->getindex())->gettex());

	matdata->m_mesh2->DrawSubset(0);

	this->SetRenderALPHAREF_END(pDevice);
	pDevice->SetRenderState(D3DRS_WRAP0, 0);
}

//==========================================================================
// 重複検索
bool CSphere::OverlapSearch(CSphereData* matdata, int SubdivisionRatio)
{
	for (auto itr = this->m_matdata.begin(); itr != this->m_matdata.end(); ++itr)
	{
		if (SubdivisionRatio == (*itr)->m_SubdivisionRatio)
		{
			CSphereData * pinp = (*itr);
			matdata->copy(pinp);
			return true;
		}
	}

	return false;
}

CSphere::CSphereData::CSphereData()
{
	this->m_Lock = false;
	this->m_SubdivisionRatio = 0;
	this->m_mesh1 = nullptr;
	this->m_mesh2 = nullptr;
	this->m_vertexbuffer = nullptr;
	this->m_pseudo = nullptr;
	ZeroMemory(&this->m_mat, sizeof(this->m_mat));
}

CSphere::CSphereData::~CSphereData()
{
	this->m_temp.Release_(this->m_vertexbuffer);
	this->m_temp.Release_(this->m_mesh2);
	this->m_temp.Release_(this->m_mesh1);
	this->m_pseudo = nullptr;
	this->m_Lock = false;
}

//==========================================================================
// 解放
void CSphere::CSphereData::release(void)
{
	if (this->m_Lock)
	{
		this->m_temp.Release_(this->m_vertexbuffer);
		this->m_temp.Release_(this->m_mesh2);
		this->m_temp.Release_(this->m_mesh1);
		this->m_pseudo = nullptr;
		this->m_Lock = false;
	}
}

//==========================================================================
// クリエイト
bool CSphere::CSphereData::CreateSphere(int SubdivisionRatio, LPDIRECT3DDEVICE9 pDevice)
{
	this->m_Lock = true;
	this->m_SubdivisionRatio = SubdivisionRatio;
	if (FAILED(D3DXCreateSphere(pDevice, 1.0f, this->m_SubdivisionRatio * 2, this->m_SubdivisionRatio, &this->m_mesh1, nullptr)))
	{
		return true;
	}

	this->m_mesh1->CloneMeshFVF(0, FVF_VERTEX_4, pDevice, &this->m_mesh2);
	this->m_mesh2->GetVertexBuffer(&this->m_vertexbuffer);

	return false;
}

//==========================================================================
// コンバート
void CSphere::CSphereData::convert(void)
{
	float r = 1.0f;
	this->m_vertexbuffer->Lock(0, 0, (void**)&this->m_pseudo, 0);
	for (DWORD i = 0; i < this->m_mesh2->GetNumVertices(); i++)
	{
		float q = 0.0f;
		float q2 = 0.0f;
		VERTEX_4* pPseudo = &this->m_pseudo[i];

		q = atan2(pPseudo->pos.z, pPseudo->pos.x);

		pPseudo->Tex.x = q / (2.0f * 3.1415f);
		q2 = asin(pPseudo->pos.y / r);
		pPseudo->Tex.y = (1.0f - q2 / (3.1415f / 2.0f)) / 2.0f;
		if (pPseudo->Tex.x > 1.0f)
		{
			pPseudo->Tex.x = 1.0f;
		}

		pPseudo->color = D3DCOLOR_RGBA(255, 255, 255, 255);
		pPseudo->Normal = D3DXVECTOR3(1, 1, 1);
	}
	this->m_vertexbuffer->Unlock();
}

//==========================================================================
// マテリアル情報のセット
void CSphere::CSphereData::setmat(void)
{
	// マテリアルの設定
	ZeroMemory(&this->m_mat, sizeof(this->m_mat));
	this->m_mat.Diffuse.r = 1.0f; // ディレクショナルライト
	this->m_mat.Diffuse.g = 1.0f; // ディレクショナルライト
	this->m_mat.Diffuse.b = 1.0f; // ディレクショナルライト
	this->m_mat.Diffuse.a = 1.0f; // ディレクショナルライト
	this->m_mat.Ambient.r = 0.7f; // アンビエントライト
	this->m_mat.Ambient.g = 0.7f; // アンビエントライト
	this->m_mat.Ambient.b = 0.7f; // アンビエントライト
	this->m_mat.Ambient.a = 1.0f; // アンビエントライト
}

//==========================================================================
// コピー
void CSphere::CSphereData::copy(CSphereData * pinp)
{
	this->m_SubdivisionRatio = pinp->m_SubdivisionRatio;
	this->m_mat = pinp->m_mat;
	this->m_mesh1 = pinp->m_mesh1;
	this->m_mesh2 = pinp->m_mesh2;
	this->m_vertexbuffer = pinp->m_vertexbuffer;
	this->m_pseudo = pinp->m_pseudo;
	this->m_Lock = false;
}
