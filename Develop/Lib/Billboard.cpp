//==========================================================================
// ビルボード[Billboard.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Billboard.h"

CBillboard::CBillboard()
{
}

CBillboard::~CBillboard()
{
	this->m_tex.release();
	this->m_texture.clear();
}

//==========================================================================
// 初期化 失敗時true
// Input = 使うテクスチャのパス
// plate = 板ポリの種類
// bCenter = 中心座標を下の真ん中にします
bool CBillboard::Init(const char* Input, PlateList plate, bool bCenter)
{
	CAnimationParam *panimparam = nullptr;

	// テクスチャの格納
	if (this->m_tex.init(Input)) { return true; }

	// メモリ確保
	this->m_tenp.New_(panimparam);

	// 登録
	this->m_texture.push_back(panimparam);

	// バッファの生成
	if (this->CreateBuffer(panimparam, plate, bCenter)) { return true; }

	return false;
}

//==========================================================================
// アニメーション用初期化 失敗時true
// Input = 使うテクスチャのパス
// UpdateFrame = 更新フレーム
// Pattern = アニメーション数
// Direction = 横一列のアニメーション数
// plate = 板ポリの種類
// bCenter = 中心座標を下の真ん中にします
bool CBillboard::Init(const char* Input, int UpdateFrame, int Pattern, int Direction, PlateList plate, bool bCenter)
{
	CAnimationParam *panimparam = nullptr;

	// テクスチャの格納
	if (this->m_tex.init(Input)) { return true; }

	// メモリ確保
	this->m_tenp.New_(panimparam);

	// 登録
	this->m_texture.push_back(panimparam);

	// バッファの生成
	if (this->CreateBuffer(panimparam, plate, bCenter)) { return true; }

	// アニメーションの生成
	this->CreateAnimation(panimparam, UpdateFrame, Pattern, Direction);

	return false;
}

//==========================================================================
// 初期化 失敗時true
// Input = 使うテクスチャのパス ダブルポインタ用
// numdata = 要素数
// plate = 板ポリの種類
// bCenter = 中心座標を下の真ん中にします
bool CBillboard::Init(const char ** Input, int numdata, PlateList plate, bool bCenter)
{
	for (int i = 0; i < numdata; i++)
	{
		if (this->Init(Input[i], plate, bCenter))
		{
			return true;
		}
	}
	return false;
}

//==========================================================================
// アニメーション用初期化 失敗時true
// Input = 使うテクスチャのパス ダブルポインタ用
// numdata = 要素数
// UpdateFrame = 更新フレーム
// Pattern = アニメーション数
// Direction = 横一列のアニメーション数
// plate = 板ポリの種類
// bCenter = 中心座標を下の真ん中にします
bool CBillboard::Init(const char ** Input, int numdata, int UpdateFrame, int Pattern, int Direction, PlateList plate, bool bCenter)
{
	for (int i = 0; i < numdata; i++)
	{
		if (this->Init(Input[i], UpdateFrame, Pattern, Direction, plate, bCenter))
		{
			return true;
		}
	}
	return false;
}

//==========================================================================
// 解放
void CBillboard::Uninit(void)
{
	// メモリの破棄
	for (auto itr = this->m_texture.begin(); itr != this->m_texture.end(); ++itr)
	{
		// バッファ情報の親データ判定があるとき
		if ((*itr)->m_key)
		{
			(*itr)->m_buffer->release();
			this->m_tenp.Delete_((*itr)->m_buffer);
		}
		this->m_tenp.Delete_((*itr)->m_pAnimation);
		this->m_tenp.Delete_((*itr));
	}

	this->m_tex.release();
	this->m_texture.clear();
}

//==========================================================================
// アニメーション更新
// AnimationCount = アニメーション用変数を入れるところ
void CBillboard::UpdateAnimation(int * AnimationCount)
{
	(*AnimationCount)++;
}

//==========================================================================
// 描画
void CBillboard::BaseDraw(C3DObject * pInput, D3DXMATRIX * MtxView, int * AnimationCount, bool *bADD)
{
	LPDIRECT3DDEVICE9 pDevice = CDirectXDevice::GetD3DDevice();
	D3DXMATRIX aMtxWorld; // ワールド行列
	if (pInput != nullptr)
	{
		CAnimationParam *ptexture = this->m_texture[pInput->getindex()];

		// アニメーション用カウンタとアニメーション情報が存在するとき
		if (AnimationCount != nullptr&&ptexture->m_pAnimation != nullptr)
		{
			this->AnimationPalam(ptexture, AnimationCount);
			this->UV(ptexture);
			ptexture->m_buffer->m_pVertexBuffer->Lock(0, 0, (void**)&ptexture->m_buffer->m_pPseudo, D3DLOCK_DISCARD);
			ptexture->m_buffer->m_pPseudo[0].Tex = D3DXVECTOR2(ptexture->m_uv.u0, ptexture->m_uv.v0);
			ptexture->m_buffer->m_pPseudo[1].Tex = D3DXVECTOR2(ptexture->m_uv.u1, ptexture->m_uv.v0);
			ptexture->m_buffer->m_pPseudo[2].Tex = D3DXVECTOR2(ptexture->m_uv.u1, ptexture->m_uv.v1);
			ptexture->m_buffer->m_pPseudo[3].Tex = D3DXVECTOR2(ptexture->m_uv.u0, ptexture->m_uv.v1);
			ptexture->m_buffer->m_pVertexBuffer->Unlock();	// ロック解除
		}
		else
		{
			ptexture->m_buffer->m_pVertexBuffer->Lock(0, 0, (void**)&ptexture->m_buffer->m_pPseudo, D3DLOCK_DISCARD);
			ptexture->m_buffer->m_pPseudo[0].Tex = D3DXVECTOR2(ptexture->m_uv.u0, ptexture->m_uv.v0);
			ptexture->m_buffer->m_pPseudo[1].Tex = D3DXVECTOR2(ptexture->m_uv.u1, ptexture->m_uv.v0);
			ptexture->m_buffer->m_pPseudo[2].Tex = D3DXVECTOR2(ptexture->m_uv.u1, ptexture->m_uv.v1);
			ptexture->m_buffer->m_pPseudo[3].Tex = D3DXVECTOR2(ptexture->m_uv.u0, ptexture->m_uv.v1);
			ptexture->m_buffer->m_pVertexBuffer->Unlock();	// ロック解除
		}

		// 色が設定されているとき
		if (pInput->Collar != nullptr)
		{
			ptexture->m_buffer->m_pPseudo[0].color = pInput->Collar->get();
			ptexture->m_buffer->m_pPseudo[1].color = pInput->Collar->get();
			ptexture->m_buffer->m_pPseudo[2].color = pInput->Collar->get();
			ptexture->m_buffer->m_pPseudo[3].color = pInput->Collar->get();
		}

		// パイプライン
		pDevice->SetStreamSource(0, ptexture->m_buffer->m_pVertexBuffer, 0, sizeof(VERTEX_4));

		// インデックス登録
		pDevice->SetIndices(ptexture->m_buffer->m_pIndexBuffer);

		// FVFの設定
		pDevice->SetFVF(FVF_VERTEX_4);

		// テクスチャの登録
		pDevice->SetTexture(0, nullptr);
		pDevice->SetTexture(0, this->m_tex.get(pInput->getindex())->gettex());

		// ライト設定
		pDevice->SetRenderState(D3DRS_LIGHTING, FALSE);

		// 表示モード操作がある場合
		if (bADD != nullptr)
		{
			if (*bADD)
			{
				// 全加算合成
				this->SetRenderSUB(pDevice);
				this->SetRenderZWRITEENABLE_START(pDevice);
			}
			else
			{
				// パンチ抜き
				this->SetRenderALPHAREF_START(pDevice, 100);
			}
		}

		// 板ポリの種類
		switch (ptexture->m_buffer->m_plate)
		{
		case PlateList::Vertical:
			if (MtxView != nullptr)
			{
				pDevice->SetTransform(D3DTS_WORLD, pInput->CreateMtxWorld2(&aMtxWorld, MtxView));
			}
			else if (MtxView == nullptr)
			{
				pDevice->SetTransform(D3DTS_WORLD, pInput->CreateMtxWorld2(&aMtxWorld));
			}
			break;
		case PlateList::Up:
			if (MtxView != nullptr)
			{
				pDevice->SetTransform(D3DTS_WORLD, pInput->CreateMtxWorld1(&aMtxWorld, MtxView));
			}
			else if (MtxView == nullptr)
			{
				pDevice->SetTransform(D3DTS_WORLD, pInput->CreateMtxWorld1(&aMtxWorld));
			}
			break;
		default:
			break;
		}

		// 描画設定
		pDevice->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, 4, 0, 2);

		// 元に戻す
		if (bADD != nullptr)
		{
			if (*bADD)
			{
				this->SetRenderADD(pDevice);
				this->SetRenderZWRITEENABLE_END(pDevice);
			}
			else
			{
				this->SetRenderALPHAREF_END(pDevice);
			}
		}
	}
}

//==========================================================================
// 頂点バッファの生成
void CBillboard::CreateVertex(VERTEX_4 * Output, CAnimationParam *ptexture, bool bCenter)
{
	// 板ポリの種類
	switch (ptexture->m_buffer->m_plate)
	{
	case PlateList::Vertical:
	{
		if (bCenter == true)
		{
			VERTEX_4 vVertex[] =
			{
				// 手前
				{ D3DXVECTOR3(-VCRCT, VCRCT,0.0f),D3DXVECTOR3(0,0,-1),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(ptexture->m_uv.u0,ptexture->m_uv.v0) }, // 左上
				{ D3DXVECTOR3(VCRCT, VCRCT,0.0f),D3DXVECTOR3(0,0,-1),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(ptexture->m_uv.u1,ptexture->m_uv.v0) }, // 右上
				{ D3DXVECTOR3(VCRCT,-VCRCT,0.0f),D3DXVECTOR3(0,0,-1),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(ptexture->m_uv.u1,ptexture->m_uv.v1) }, // 右下
				{ D3DXVECTOR3(-VCRCT,-VCRCT,0.0f),D3DXVECTOR3(0,0,-1),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(ptexture->m_uv.u0,ptexture->m_uv.v1) }, // 左下
			};

			for (int i = 0; i < (int)this->m_tenp.Sizeof_(vVertex); i++)
			{
				Output[i] = vVertex[i];
			}
		}
		else if (bCenter == false)
		{
			VERTEX_4 vVertex[] =
			{
				// 手前
				{ D3DXVECTOR3(-VCRCT, 1.0f,0.0f),D3DXVECTOR3(0,0,-1),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(ptexture->m_uv.u0,ptexture->m_uv.v0) }, // 左上
				{ D3DXVECTOR3(VCRCT, 1.0f,0.0f),D3DXVECTOR3(0,0,-1),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(ptexture->m_uv.u1,ptexture->m_uv.v0) }, // 右上
				{ D3DXVECTOR3(VCRCT,0.0f,0.0f),D3DXVECTOR3(0,0,-1),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(ptexture->m_uv.u1,ptexture->m_uv.v1) }, // 右下
				{ D3DXVECTOR3(-VCRCT,0.0f,0.0f),D3DXVECTOR3(0,0,-1),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(ptexture->m_uv.u0,ptexture->m_uv.v1) }, // 左下
			};

			for (int i = 0; i < (int)this->m_tenp.Sizeof_(vVertex); i++)
			{
				Output[i] = vVertex[i];
			}
		}
	}
	break;
	case PlateList::Up:
	{
		VERTEX_4 vVertex[] =
		{
			// 上
			{ D3DXVECTOR3(-VCRCT,0.0f, VCRCT),D3DXVECTOR3(0,1,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(ptexture->m_uv->u0,ptexture->m_uv->v0) }, // 左上
			{ D3DXVECTOR3(VCRCT,0.0f, VCRCT),D3DXVECTOR3(0,1,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(ptexture->m_uv->u1,ptexture->m_uv->v0) }, // 右上
			{ D3DXVECTOR3(VCRCT,0.0f,-VCRCT),D3DXVECTOR3(0,1,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(ptexture->m_uv->u1,ptexture->m_uv->v1) }, // 右下
			{ D3DXVECTOR3(-VCRCT,0.0f,-VCRCT),D3DXVECTOR3(0,1,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(ptexture->m_uv->u0,ptexture->m_uv->v1) }, // 左下
		};
		for (int i = 0; i < (int)this->m_tenp.Sizeof_(vVertex); i++)
		{
			Output[i] = vVertex[i];
		}
	}
	break;
	default:
		break;
	}
}

//==========================================================================
// アニメーション情報のセット
void CBillboard::AnimationPalam(CAnimationParam *ptexture, int * AnimationCount)
{
	int PattanNum = ((*AnimationCount) / ptexture->m_pAnimation->Frame) % ptexture->m_pAnimation->Pattern;	//パターン数
	int patternV = PattanNum % ptexture->m_pAnimation->Direction; // 横方向のパターン
	int patternH = PattanNum / ptexture->m_pAnimation->Direction; // 縦方向のパターン

	ptexture->m_pAnimation->m_Cut.x = patternV * ptexture->m_pAnimation->m_Cut.w; // 切り取り座標X
	ptexture->m_pAnimation->m_Cut.y = patternH * ptexture->m_pAnimation->m_Cut.h; // 切り取り座標Y
}

//==========================================================================
// アニメーション切り替わり時の判定 切り替え時true 
// index = 判定路調べたい板ポリの番号
// AnimationCount = アニメーション用変数を入れるところ
bool CBillboard::GetPattanNum(int index, int * AnimationCount)
{
	CAnimationParam *ptexture = this->m_texture[index];

	// セットされているとき
	if (AnimationCount != nullptr)
	{
		// フレームが一致したとき
		if ((*AnimationCount) == (ptexture->m_pAnimation->Frame*ptexture->m_pAnimation->Pattern) - 1)
		{
			this->AnimationCountInit(AnimationCount);
			return true;
		}
	}
	return false;
}

//==========================================================================
// アニメーション用のカウンタの初期化 引数はカウンタ用変数
// AnimationCount = アニメーション用変数を入れるところ
int *CBillboard::AnimationCountInit(int * AnimationCount)
{
	if (AnimationCount != nullptr)
	{
		(*AnimationCount) = -1;
	}

	return AnimationCount;
}

//==========================================================================
// UVの生成
void CBillboard::UV(CAnimationParam *ptexture)
{
	// 左上
	ptexture->m_uv.u0 = (float)ptexture->m_pAnimation->m_Cut.x / ptexture->m_pAnimation->m_TexSize.w;
	ptexture->m_uv.v0 = (float)ptexture->m_pAnimation->m_Cut.y / ptexture->m_pAnimation->m_TexSize.h;

	// 左下
	ptexture->m_uv.u1 = (float)(ptexture->m_pAnimation->m_Cut.x + ptexture->m_pAnimation->m_Cut.w) / ptexture->m_pAnimation->m_TexSize.w;
	ptexture->m_uv.v1 = (float)(ptexture->m_pAnimation->m_Cut.y + ptexture->m_pAnimation->m_Cut.h) / ptexture->m_pAnimation->m_TexSize.h;
}

//==========================================================================
// 重複検索
bool CBillboard::OverlapSearch(CAnimationParam * panimparam, PlateList plate)
{
	for (auto itr = this->m_texture.begin(); itr != this->m_texture.end(); ++itr)
	{
		// 板ポリの種類が同じとき
		if ((*itr)->m_buffer != nullptr)
		{
			if ((*itr)->m_buffer->m_plate == plate)
			{
				panimparam->m_buffer = (*itr)->m_buffer;
				panimparam->m_key = false;
				return true;
			}
		}
	}

	return false;
}

//==========================================================================
// バッファの生成
bool CBillboard::CreateBuffer(CAnimationParam * panimparam, PlateList plate, bool bCenter)
{
	WORD* pIndex = nullptr;

	// 重複判定が出なかったとき
	if (!this->OverlapSearch(panimparam, plate))
	{
		// メモリ確保
		this->m_tenp.New_(panimparam->m_buffer);

		panimparam->m_buffer->m_plate = plate;

		if (this->CreateVertexBuffer(sizeof(VERTEX_4) * 4, D3DUSAGE_WRITEONLY, FVF_VERTEX_4, D3DPOOL_MANAGED, &panimparam->m_buffer->m_pVertexBuffer, nullptr))
		{
			return true;
		}

		if (this->CreateIndexBuffer(sizeof(WORD) * 6, D3DUSAGE_WRITEONLY, D3DFMT_INDEX16, D3DPOOL_MANAGED, &panimparam->m_buffer->m_pIndexBuffer, nullptr))
		{
			return true;
		}

		panimparam->m_buffer->m_pVertexBuffer->Lock(0, 0, (void**)&panimparam->m_buffer->m_pPseudo, D3DLOCK_DISCARD);
		this->CreateVertex(panimparam->m_buffer->m_pPseudo, panimparam, bCenter);
		panimparam->m_buffer->m_pVertexBuffer->Unlock();	// ロック解除

		panimparam->m_buffer->m_pIndexBuffer->Lock(0, 0, (void**)&pIndex, D3DLOCK_DISCARD);
		this->RectangleIndex(pIndex, 1);
		panimparam->m_buffer->m_pIndexBuffer->Unlock();	// ロック解除

		panimparam->m_key = true;
	}

	return false;
}

//==========================================================================
// アニメーション情報の生成
void CBillboard::CreateAnimation(CAnimationParam * panimparam, int UpdateFrame, int Pattern, int Direction)
{
	int nCount = 0;

	// メモリ確保
	this->m_tenp.New_(panimparam->m_pAnimation);

	*panimparam->m_pAnimation = CAnimation(UpdateFrame, Pattern, Direction, CTexvec<float>(0.0f, 0.0f, 0.0f, 0.0f), *this->m_tex.get(this->m_tex.maxdata() - 1)->getsize());

	panimparam->m_pAnimation->m_Cut.w = (float)panimparam->m_pAnimation->m_TexSize.w / panimparam->m_pAnimation->Direction;

	// 切り取る縦幅を計算
	for (int i = 0;; i += panimparam->m_pAnimation->Direction)
	{
		if (panimparam->m_pAnimation->Pattern <= i) { break; }
		nCount++;
	}

	panimparam->m_pAnimation->m_Cut.h = (float)panimparam->m_pAnimation->m_TexSize.h / nCount;
	panimparam->m_pAnimation->m_Cut.x = panimparam->m_pAnimation->m_Cut.y = 0.0f;
}

CBillboard::CAnimationParam::CAnimationParam()
{
	this->m_pAnimation = nullptr;
	this->m_buffer = nullptr;
	this->m_uv = CUv<float>(0.0f, 0.0f, 1.0f, 1.0f);
	this->m_key = false;
}

CBillboard::CAnimationParam::~CAnimationParam()
{

}

CBillboard::CBuffer::CBuffer()
{
	this->m_plate = (PlateList)-1;
	this->m_pIndexBuffer = nullptr;
	this->m_pVertexBuffer = nullptr;
	this->m_pPseudo = nullptr;
}

CBillboard::CBuffer::~CBuffer()
{
	// バッファ解放
	this->m_tenp.Release_(this->m_pVertexBuffer);

	// インデックスバッファ解放
	this->m_tenp.Release_(this->m_pIndexBuffer);

	this->m_pPseudo = nullptr;
}

//==========================================================================
// 解放
void CBillboard::CBuffer::release(void)
{
	// バッファ解放
	this->m_tenp.Release_(this->m_pVertexBuffer);

	// インデックスバッファ解放
	this->m_tenp.Release_(this->m_pIndexBuffer);

	this->m_pPseudo = nullptr;
}
