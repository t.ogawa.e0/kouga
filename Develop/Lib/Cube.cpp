//==========================================================================
// キューブ[Cube.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Cube.h"

CCube::CCube()
{
	this->m_pVertexBuffer = nullptr;
	this->m_pIndexBuffer = nullptr;
	this->m_Lock = false;
}

CCube::~CCube()
{
	// バッファ解放
	this->m_temp.Release_(this->m_pVertexBuffer);

	// インデックスバッファ解放
	this->m_temp.Release_(this->m_pIndexBuffer);

	this->m_texture.release();
}

//==========================================================================
// 初期化 失敗時true
// Input = 使用するテクスチャのパス ダブルポインタに対応
// size = 要素数 
bool CCube::Init(const char** Input, int size)
{
	for (int i = 0; i < size; i++)
	{
		if (this->Init(Input[i]))
		{
			return true;
		}
	}

	return false;
}

//==========================================================================
// 初期化 失敗時true
// Input = 使用するテクスチャのパス
bool CCube::Init(const char * Input)
{
	if (this->m_texture.init(Input))
	{
		return true;
	}

	if (!this->m_Lock)
	{
		CUv<float> aUv[6]; // 各面のUV
		VERTEX_4* pPseudo = nullptr;// 頂点バッファのロック
		WORD* pIndex = nullptr;
		TEXTURE texparam;

		texparam.Direction1 = 4;
		texparam.Direction2 = 4;
		texparam.Pattern = 6;

		// 画像データの格納
		texparam.texsize = CTexvec<float>(0, 0, 1.0f, 1.0f);
		texparam.texcutsize = CTexvec<float>(0, 0, 0, 0);
		texparam.texcutsize.w = texparam.texsize.w / texparam.Direction1;
		texparam.texcutsize.h = texparam.texsize.h / texparam.Direction2;

		for (int s = 0; s < texparam.Pattern; s++)
		{
			this->SetUV(&texparam, &aUv[s], s);
		}

		if (this->CreateVertexBuffer(sizeof(VERTEX_4) * this->nNumVertex, D3DUSAGE_WRITEONLY, FVF_VERTEX_4, D3DPOOL_MANAGED, &this->m_pVertexBuffer, nullptr))
		{
			return true;
		}

		if (this->CreateIndexBuffer(sizeof(WORD) * this->nMaxIndex, D3DUSAGE_WRITEONLY, D3DFMT_INDEX16, D3DPOOL_MANAGED, &this->m_pIndexBuffer, nullptr))
		{
			return true;
		}

		this->m_pVertexBuffer->Lock(0, 0, (void**)&pPseudo, D3DLOCK_DISCARD);
		this->SetCube(pPseudo, aUv);
		this->m_pVertexBuffer->Unlock();	// ロック解除

		this->m_pIndexBuffer->Lock(0, 0, (void**)&pIndex, D3DLOCK_DISCARD);
		this->RectangleIndex(pIndex, this->nMaxIndex / this->m_NumDfaltIndex);
		this->m_pIndexBuffer->Unlock();	// ロック解除

		// マテリアルの設定
		ZeroMemory(&this->m_aMat, sizeof(this->m_aMat));
		this->m_aMat.Diffuse.r = 1.0f; // ディレクショナルライト
		this->m_aMat.Diffuse.g = 1.0f; // ディレクショナルライト
		this->m_aMat.Diffuse.b = 1.0f; // ディレクショナルライト
		this->m_aMat.Diffuse.a = 1.0f; // ディレクショナルライト
		this->m_aMat.Ambient.r = 0.7f; // アンビエントライト
		this->m_aMat.Ambient.g = 0.7f; // アンビエントライト
		this->m_aMat.Ambient.b = 0.7f; // アンビエントライト
		this->m_aMat.Ambient.a = 1.0f; // アンビエントライト

		this->m_Lock = true;
	}

	return false;
}

//==========================================================================
// 解放
void CCube::Uninit(void)
{
	// バッファ解放
	this->m_temp.Release_(this->m_pVertexBuffer);

	// インデックスバッファ解放
	this->m_temp.Release_(this->m_pIndexBuffer);

	this->m_texture.release();
}

//==========================================================================
// 描画
// Input = C3DObjectのアドレス渡し
// DrawType = ワールド行列の種類の指定をします
void CCube::Draw(C3DObject * Input, DrawTypeName DrawType)
{
	LPDIRECT3DDEVICE9 pDevice = CDirectXDevice::GetD3DDevice();
	D3DXMATRIX aMtxWorld; // ワールド行列

	// サイズ
	pDevice->SetStreamSource(0, this->m_pVertexBuffer, 0, sizeof(VERTEX_4));	// パイプライン

	pDevice->SetIndices(this->m_pIndexBuffer);

	// FVFの設定
	pDevice->SetFVF(FVF_VERTEX_4);

	pDevice->SetTexture(0, nullptr);
	pDevice->SetTexture(0, this->m_texture.get(Input->getindex())->gettex());

	// ライト設定
	pDevice->SetRenderState(D3DRS_LIGHTING, TRUE);

	// マテリアルの設定
	pDevice->SetMaterial(&this->m_aMat);

	switch (DrawType)
	{
	case CCube::DrawType::MtxWorld1:
		pDevice->SetTransform(D3DTS_WORLD, Input->CreateMtxWorld1(&aMtxWorld));
		break;
	case CCube::DrawType::MtxWorld2:
		pDevice->SetTransform(D3DTS_WORLD, Input->CreateMtxWorld2(&aMtxWorld));
		break;
	default:
		pDevice->SetTransform(D3DTS_WORLD, Input->CreateMtxWorld2(&aMtxWorld));
		break;
	}

	// 描画設定
	pDevice->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, this->nNumVertex, 0, this->nNumTriangle);
}

//==========================================================================
// キューブ生成	
void CCube::SetCube(VERTEX_4 * Output, const CUv<float> * _this)
{
	VERTEX_4 vVertex[] =
	{
		// 手前
		{ D3DXVECTOR3(-VCRCT, VCRCT,-VCRCT),D3DXVECTOR3(0,0,-1),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[0].u0,_this[0].v0) }, // 左上
		{ D3DXVECTOR3(VCRCT, VCRCT,-VCRCT),D3DXVECTOR3(0,0,-1),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[0].u1,_this[0].v0) }, // 右上
		{ D3DXVECTOR3(VCRCT,-VCRCT,-VCRCT),D3DXVECTOR3(0,0,-1),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[0].u1,_this[0].v1) }, // 右下
		{ D3DXVECTOR3(-VCRCT,-VCRCT,-VCRCT),D3DXVECTOR3(0,0,-1),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[0].u0,_this[0].v1) }, // 左下

		// 奥
		{ D3DXVECTOR3(-VCRCT,-VCRCT,VCRCT),D3DXVECTOR3(0,0,1),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[1].u0,_this[1].v0) }, // 左上
		{ D3DXVECTOR3(VCRCT,-VCRCT,VCRCT),D3DXVECTOR3(0,0,1),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[1].u1,_this[1].v0) }, // 右上
		{ D3DXVECTOR3(VCRCT, VCRCT,VCRCT),D3DXVECTOR3(0,0,1),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[1].u1,_this[1].v1) }, // 右下
		{ D3DXVECTOR3(-VCRCT, VCRCT,VCRCT),D3DXVECTOR3(0,0,1),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[1].u0,_this[1].v1) }, // 左下

		// 右
		{ D3DXVECTOR3(-VCRCT,-VCRCT,-VCRCT),D3DXVECTOR3(-1,0,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[2].u0,_this[2].v0) }, // 左上
		{ D3DXVECTOR3(-VCRCT,-VCRCT, VCRCT),D3DXVECTOR3(-1,0,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[2].u1,_this[2].v0) }, // 右上
		{ D3DXVECTOR3(-VCRCT, VCRCT, VCRCT),D3DXVECTOR3(-1,0,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[2].u1,_this[2].v1) }, // 右下
		{ D3DXVECTOR3(-VCRCT, VCRCT,-VCRCT),D3DXVECTOR3(-1,0,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[2].u0,_this[2].v1) }, // 左下

		// 左
		{ D3DXVECTOR3(VCRCT, VCRCT,-VCRCT),D3DXVECTOR3(1,0,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[3].u0,_this[3].v0) }, // 左上
		{ D3DXVECTOR3(VCRCT, VCRCT, VCRCT),D3DXVECTOR3(1,0,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[3].u1,_this[3].v0) }, // 右上
		{ D3DXVECTOR3(VCRCT,-VCRCT, VCRCT),D3DXVECTOR3(1,0,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[3].u1,_this[3].v1) }, // 右下
		{ D3DXVECTOR3(VCRCT,-VCRCT,-VCRCT),D3DXVECTOR3(1,0,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[3].u0,_this[3].v1) }, // 左下

		// 上
		{ D3DXVECTOR3(-VCRCT,VCRCT, VCRCT),D3DXVECTOR3(0,1,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[4].u0,_this[4].v0) }, // 左上
		{ D3DXVECTOR3(VCRCT,VCRCT, VCRCT),D3DXVECTOR3(0,1,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[4].u1,_this[4].v0) }, // 右上
		{ D3DXVECTOR3(VCRCT,VCRCT,-VCRCT),D3DXVECTOR3(0,1,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[4].u1,_this[4].v1) }, // 右下
		{ D3DXVECTOR3(-VCRCT,VCRCT,-VCRCT),D3DXVECTOR3(0,1,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[4].u0,_this[4].v1) }, // 左下

		// 下
		{ D3DXVECTOR3(-VCRCT,-VCRCT,-VCRCT),D3DXVECTOR3(0,-1,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[5].u0,_this[5].v0) }, // 左上
		{ D3DXVECTOR3(VCRCT,-VCRCT,-VCRCT),D3DXVECTOR3(0,-1,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[5].u1,_this[5].v0) }, // 右上
		{ D3DXVECTOR3(VCRCT,-VCRCT, VCRCT),D3DXVECTOR3(0,-1,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[5].u1,_this[5].v1) }, // 右下
		{ D3DXVECTOR3(-VCRCT,-VCRCT, VCRCT),D3DXVECTOR3(0,-1,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[5].u0,_this[5].v1) }, // 左下
	};

	for (int i = 0; i < (int)(sizeof(vVertex) / sizeof(VERTEX_4)); i++) { Output[i] = vVertex[i]; }
}

//==========================================================================
// UV生成
void CCube::SetUV(const TEXTURE * Input, CUv<float> * Output, const int nNum)
{
	int patternNum = (nNum / 1) % Input->Pattern; //2フレームに１回	%10=パターン数
	int patternV = patternNum % Input->Direction1; //横方向のパターン
	int patternH = patternNum / Input->Direction2; //縦方向のパターン
	float fTcx = patternV * Input->texcutsize.w; //切り取りサイズ
	float fTcy = patternH * Input->texcutsize.h; //切り取りサイズ

	Output->u0 = fTcx;// 左
	Output->v0 = fTcy;// 上
	Output->u1 = fTcx + Input->texcutsize.w;// 右
	Output->v1 = fTcy + Input->texcutsize.h;//下
}
