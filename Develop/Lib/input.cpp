//==========================================================================
// キーボード入力処理[input.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "input.h"

//==========================================================================
// 実体
//==========================================================================
LPDIRECTINPUT8 CKeyboard::m_DInput;
LPDIRECTINPUTDEVICE8 CKeyboard::m_DIDevice;
BYTE CKeyboard::m_State[m_KeyMax];
BYTE CKeyboard::m_StateTrigger[m_KeyMax];
BYTE CKeyboard::m_StateRelease[m_KeyMax];
BYTE CKeyboard::m_StateRepeat[m_KeyMax];
int CKeyboard::m_StateRepeatCnt[m_KeyMax];

CKeyboard::CKeyboard()
{
}

CKeyboard::~CKeyboard()
{
}

//==========================================================================
// 初期化
HRESULT CKeyboard::Init(HINSTANCE hInstance, HWND hWnd)
{
	m_DInput = nullptr;
	m_DIDevice = nullptr;

	// DirectInputオブジェクトの作成
	if (FAILED(DirectInput8Create(hInstance, DIRECTINPUT_VERSION, IID_IDirectInput8, (void**)&m_DInput, nullptr)))
	{
		MessageBox(hWnd, "DirectInputオブジェクトが作れませんでした", "警告", MB_ICONWARNING);
		return E_FAIL;
	}

	// デバイスの作成
	if (FAILED(m_DInput->CreateDevice(GUID_SysKeyboard, &m_DIDevice, nullptr)))
	{
		MessageBox(hWnd, "キーボードがありませんでした", "警告", MB_ICONWARNING);
		return E_FAIL;
	}

	if (m_DIDevice != nullptr)
	{
		// データフォーマットを設定
		if (FAILED(m_DIDevice->SetDataFormat(&c_dfDIKeyboard)))
		{
			MessageBox(hWnd, "キーボードのデータフォーマットを設定できませんでした。", "警告", MB_ICONWARNING);
			return E_FAIL;
		}

		// 協調モードを設定（フォアグラウンド＆非排他モード）
		if (FAILED(m_DIDevice->SetCooperativeLevel(hWnd, (DISCL_FOREGROUND/*DISCL_BACKGROUND*/ | DISCL_NONEXCLUSIVE))))
		{
			MessageBox(hWnd, "キーボードの協調モードを設定できませんでした。", "警告", MB_ICONWARNING);
			return E_FAIL;
		}

		// キーボードへのアクセス権を獲得(入力制御開始)
		m_DIDevice->Acquire();
	}

	return S_OK;
}

//==========================================================================
// 解放
void CKeyboard::Uninit(void)
{
	if (m_DIDevice != nullptr)
	{// 入力デバイス(キーボード)の開放
	 // キーボードへのアクセス権を開放(入力制御終了)
		m_DIDevice->Unacquire();

		m_DIDevice->Release();
		m_DIDevice = nullptr;
	}

	if (m_DInput != nullptr)
	{// DirectInputオブジェクトの開放
		m_DInput->Release();
		m_DInput = nullptr;
	}
}

//==========================================================================
// 更新
void CKeyboard::Update(void)
{
	BYTE aState[m_KeyMax];

	if (m_DIDevice != nullptr)
	{
		// デバイスからデータを取得
		if (SUCCEEDED(m_DIDevice->GetDeviceState(sizeof(aState), aState)))
		{
			// aKeyState[m_KeyMax]&0x80
			for (int i = 0; i < m_KeyMax; i++)
			{
				// キートリガー・リリース情報を生成
				m_StateTrigger[i] = (m_State[i] ^ aState[i]) & aState[i];
				m_StateRelease[i] = (m_State[i] ^ aState[i]) & m_State[i];

				// キーリピート情報を生成
				if (aState[i])
				{
					if (m_StateRepeatCnt[i] < m_CountRepeat)
					{
						m_StateRepeatCnt[i]++;
						if (m_StateRepeatCnt[i] == 1 || m_StateRepeatCnt[i] >= m_CountRepeat)
						{// キーを押し始めた最初のフレーム、または一定時間経過したらキーリピート情報ON
							m_StateRepeat[i] = aState[i];
						}
						else
						{
							m_StateRepeat[i] = 0;
						}
					}
				}
				else
				{
					m_StateRepeatCnt[i] = 0;
					m_StateRepeat[i] = 0;
				}

				// キープレス情報を保存
				m_State[i] = aState[i];
			}
		}
		else
		{
			// キーボードへのアクセス権を取得
			m_DIDevice->Acquire();
		}
	}
}

//==========================================================================
// プレス
bool CKeyboard::Press(KeyList key)
{
	if (m_DIDevice == nullptr)
	{
		return false;
	}

	return (m_State[(int)key] & 0x80) ? true : false;
}

//==========================================================================
// トリガー
bool CKeyboard::Trigger(KeyList key)
{
	if (m_DIDevice == nullptr)
	{
		return false;
	}

	return (m_StateTrigger[(int)key] & 0x80) ? true : false;
}

//==========================================================================
// リピート
bool CKeyboard::Repeat(KeyList key)
{
	if (m_DIDevice == nullptr)
	{
		return false;
	}

	return (m_StateRelease[(int)key] & 0x80) ? true : false;
}

//==========================================================================
// リリ−ス
bool CKeyboard::Release(KeyList key)
{
	if (m_DIDevice == nullptr)
	{
		return false;
	}

	return (m_StateRelease[(int)key] & 0x80) ? true : false;
}
