//==========================================================================
// モデルデータ[Model.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Model.h"

CXmodel::CXmodel()
{
}

CXmodel::~CXmodel()
{
	this->m_data.clear();
}

//==========================================================================
// 初期化
// Input = 使用するテクスチャのパス
// mode = マテリアルのライティングの設定
bool CXmodel::Init(const char * Input, LightMoad mode)
{
	LPDIRECT3DDEVICE9 pDevice = CDirectXDevice::GetD3DDevice();
	CXdata * px = nullptr;
	bool bkey = false;

	// データの重複判定
	for (auto itr = this->m_data.begin(); itr != this->m_data.end(); ++itr)
	{
		// 重複しているとき
		if (!strcmp((*itr)->gettag(), Input))
		{
			CXdata *ppx = (*itr);
			this->create(px, Input);
			ppx->copy(ppx);
			bkey = true;
			break;
		}
	}

	// 重複していないとき
	if (bkey == false)
	{
		this->create(px, Input);

		// modelの読み込み
		if (FAILED(px->load(mode, pDevice)))
		{
			return true;
		}
	}

	return false;
}

//==========================================================================
// 初期化
// Input = 使用するテクスチャのパス ダブルポインタに対応
// num = 要素数
// mode = マテリアルのライティングの設定
bool CXmodel::Init(const char ** Input, int num, LightMoad mode)
{
	for (int i = 0; i < num; i++)
	{
		if (this->Init(Input[i], mode))
		{
			return true;
		}
	}
	return false;
}

//==========================================================================
// 解放
void CXmodel::Uninit(void)
{
	for (auto itr = this->m_data.begin(); itr != this->m_data.end(); ++itr)
	{
		(*itr)->release();
		if ((*itr) != nullptr)
		{
			delete (*itr);
			(*itr) = nullptr;
		}
	}
	this->m_data.clear();
}

//==========================================================================
// 描画
// Input = 座標クラス(C3DObject)をアドレス渡し
void CXmodel::Draw(C3DObject * Input)
{
	LPDIRECT3DDEVICE9 pDevice = CDirectXDevice::GetD3DDevice();
	D3DXMATRIX aMtxWorld; // ワールド行列
	CXdata * pXdata = this->m_data[Input->getindex()];

	pDevice->SetTransform(D3DTS_WORLD, Input->CreateMtxWorld1(&aMtxWorld));

	pDevice->SetRenderState(D3DRS_LIGHTING, TRUE);// ライト設定

	pXdata->draw(pDevice);
}

//==========================================================================
// 生成
CXmodel::CXdata * CXmodel::create(CXdata *& pinp, const char * Input)
{
	pinp = new CXdata();

	// テクスチャ情報の登録
	this->m_data.push_back(pinp);

	pinp->settag(Input);

	return pinp;
}

CXmodel::CXdata::CXdata()
{
	this->m_MaterialMood = LightMoad::System;
	this->m_Mesh = nullptr;
	this->m_NumMaterial = (DWORD)0;
	this->m_MaterialMesh = nullptr;
	this->m_tag = nullptr;
	this->m_Lock = false;
}

CXmodel::CXdata::~CXdata()
{
	this->m_temp.Delete_(this->m_MaterialMesh);
	this->m_temp.Release_(this->m_Mesh);
	this->m_Lock = false;
	this->m_texture.release();
	this->m_temp.Delete_(this->m_tag);
}

//==========================================================================
// tagのセット
void CXmodel::CXdata::settag(const char * ptag)
{
	int tagsize = 1; // \0

	if (ptag != nullptr)
	{
		// 文字数算出
		for (int i = 0; ptag[i] != '\0'; i++, tagsize++);
	}

	this->m_tag = new char[tagsize];

	if (ptag != nullptr)
	{
		// tagの登録
		strcpy(this->m_tag, ptag);
	}
	else
	{
		this->m_tag[0] = 0;
	}
}

//==========================================================================
// コピー
void CXmodel::CXdata::copy(CXdata * pinp)
{
	this->m_MaterialMood = pinp->m_MaterialMood;
	this->m_Mesh = pinp->m_Mesh;
	this->m_NumMaterial = pinp->m_NumMaterial;
	this->m_MaterialMesh = pinp->m_MaterialMesh;
	this->m_texture = pinp->m_texture;
	this->m_Lock = false;
}

//==========================================================================
// 読み込み
HRESULT CXmodel::CXdata::load(LightMoad mode, LPDIRECT3DDEVICE9 pDevice)
{
	LPD3DXBUFFER pAdjacency = nullptr; // 隣接情報
	LPD3DXMESH pTempMesh = nullptr; // テンプレートメッシュ
	LPD3DXBUFFER pMaterialBuffer = nullptr; // マテリアルバッファ
	D3DVERTEXELEMENT9 pElements[MAXD3DDECLLENGTH + 1];
	HRESULT hr = (HRESULT)0;
	LPSTR lhr = nullptr;

	// マテリアルのモードを記録
	this->m_MaterialMood = mode;

	// モデルの読み込み
	hr = this->read(&pAdjacency, &pMaterialBuffer, pDevice);
	if (FAILED(hr))
	{
		CDirectXDevice::ErrorMessage("Xデータが読み込めませんでした。\n %s", this->m_tag);
		return hr;
	}
	
	// 最適化
	hr = this->optimisation(pAdjacency);
	if (FAILED(hr))
	{
		CDirectXDevice::ErrorMessage("Xデータの最適化に失敗しました。\n %s", this->m_tag);
		return hr;
	}

	// 頂点の宣言
	hr = this->declaration(pElements);
	if (FAILED(hr))
	{
		CDirectXDevice::ErrorMessage("頂点の宣言に失敗しました。\n %s", this->m_tag);
		return hr;
	}

	// 複製
	hr = this->replication(pElements, &pTempMesh, pDevice);
	if (FAILED(hr))
	{
		CDirectXDevice::ErrorMessage("複製に失敗しました。\n %s", this->m_tag);
		return hr;
	}

	// テクスチャの読み込み
	lhr = this->loadtexture((LPD3DXMATERIAL)pMaterialBuffer->GetBufferPointer());
	if (lhr != nullptr)
	{
		CDirectXDevice::ErrorMessage("読み込みに失敗しました\n model : %s \n texture : %s ", this->m_tag, lhr);
		return (HRESULT)-1;
	}

	// マテリアルの設定
	this->materialsetting((LPD3DXMATERIAL)pMaterialBuffer->GetBufferPointer());

	this->m_temp.Release_(pMaterialBuffer);
	this->m_temp.Release_(this->m_Mesh);
	this->m_temp.Release_(pAdjacency);
	this->m_Mesh = pTempMesh;
	this->m_Lock = true;

	return hr;
}

//==========================================================================
// 解放
void CXmodel::CXdata::release(void)
{
	if (this->m_Lock)
	{
		this->m_temp.Delete_(this->m_MaterialMesh);
		this->m_temp.Release_(this->m_Mesh);
		this->m_Lock = false;
	}
	this->m_texture.release();
	this->m_temp.Delete_(this->m_tag);
}

//==========================================================================
// 描画
void CXmodel::CXdata::draw(LPDIRECT3DDEVICE9 pDevice)
{
	pDevice->SetFVF(this->m_Mesh->GetFVF());
	for (int i = 0; i < (int)this->m_NumMaterial; i++)
	{
		// マテリアル情報をセット
		pDevice->SetMaterial(&this->m_MaterialMesh[i]);
		// テクスチャが存在するときにセット
		pDevice->SetTexture(0, nullptr);
		// テクスチャ情報をセット
		pDevice->SetTexture(0, this->m_texture.get(i)->gettex());
		// メッシュを描画
		this->m_Mesh->DrawSubset(i);
	}
}

//==========================================================================
// 読み込み
HRESULT CXmodel::CXdata::read(LPD3DXBUFFER * pAdjacency, LPD3DXBUFFER * pMaterialBuffer, LPDIRECT3DDEVICE9 pDevice)
{
	return D3DXLoadMeshFromX(this->m_tag, D3DXMESH_SYSTEMMEM, pDevice, pAdjacency, pMaterialBuffer, nullptr, &this->m_NumMaterial, &this->m_Mesh);
}

//==========================================================================
// 最適化
HRESULT CXmodel::CXdata::optimisation(LPD3DXBUFFER pAdjacency)
{
	return this->m_Mesh->OptimizeInplace(D3DXMESHOPT_COMPACT | D3DXMESHOPT_ATTRSORT | D3DXMESHOPT_VERTEXCACHE, (DWORD*)pAdjacency->GetBufferPointer(), nullptr, nullptr, nullptr);
}

//==========================================================================
// 頂点の宣言
HRESULT CXmodel::CXdata::declaration(D3DVERTEXELEMENT9 *pElements)
{
	return this->m_Mesh->GetDeclaration(pElements);
}

//==========================================================================
// 複製
HRESULT CXmodel::CXdata::replication(D3DVERTEXELEMENT9 *pElements, LPD3DXMESH * pTempMesh, LPDIRECT3DDEVICE9 pDevice)
{
	return this->m_Mesh->CloneMesh(D3DXMESH_MANAGED | D3DXMESH_WRITEONLY, pElements, pDevice, pTempMesh);
}

//==========================================================================
// テクスチャの読み込み
const LPSTR CXmodel::CXdata::loadtexture(const LPD3DXMATERIAL Input)
{
	// マテリアルの数だけ
	for (int i = 0; i < (int)this->m_NumMaterial; i++)
	{
		D3DXMATERIAL * pmat = &Input[i];

		// 使用しているテクスチャがあれば読み込む
		if (pmat->pTextureFilename != nullptr &&lstrlen(pmat->pTextureFilename) > 0)
		{
			if (this->m_texture.init(pmat->pTextureFilename)) { return pmat->pTextureFilename; }
		}
		else
		{
			this->m_texture.init(nullptr);
		}
	}

	// material数が0のとき
	if ((int)this->m_NumMaterial == 0)
	{
		this->m_texture.init(nullptr);
	}

	return nullptr;
}

//==========================================================================
// マテリアルの設定
void CXmodel::CXdata::materialsetting(const LPD3DXMATERIAL Input)
{
	// メッシュの数だけ確保
	this->m_temp.New_(this->m_MaterialMesh, (int)this->m_NumMaterial);

	for (int i = 0; i < (int)this->m_NumMaterial; i++)
	{
		switch (this->m_MaterialMood)
		{
		case CXmodel::LightMoad::System:
			this->materialsetting_system(&this->m_MaterialMesh[i]);
			break;
		case CXmodel::LightMoad::Material:
			this->materialsetting_material(&this->m_MaterialMesh[i], &Input[i].MatD3D);
			break;
		default:
			break;
		}
	}
}

//==========================================================================
// マテリアルの設定 システム設定
void CXmodel::CXdata::materialsetting_system(D3DMATERIAL9 * pMatMesh)
{
	// マテリアルの設定
	ZeroMemory(pMatMesh, sizeof(D3DMATERIAL9));
	pMatMesh->Diffuse.r = 1.0f; // ディレクショナルライト
	pMatMesh->Diffuse.g = 1.0f; // ディレクショナルライト
	pMatMesh->Diffuse.b = 1.0f; // ディレクショナルライト
	pMatMesh->Diffuse.a = 1.0f; // ディレクショナルライト
	pMatMesh->Ambient.r = 0.7f; // アンビエントライト
	pMatMesh->Ambient.g = 0.7f; // アンビエントライト
	pMatMesh->Ambient.b = 0.7f; // アンビエントライト
	pMatMesh->Ambient.a = 1.0f; // アンビエントライト
}

//==========================================================================
// マテリアルの設定 マテリアル素材設定
void CXmodel::CXdata::materialsetting_material(D3DMATERIAL9 * pMatMesh, const D3DMATERIAL9 * Input)
{
	ZeroMemory(pMatMesh, sizeof(D3DMATERIAL9));
	*pMatMesh = *Input;
}
