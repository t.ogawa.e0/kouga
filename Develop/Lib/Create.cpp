//==========================================================================
// クリエイト処理[Create.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Create.h"

//==========================================================================
// バーテックスバッファの生成
bool CCreate::CreateVertexBuffer(UINT Length, DWORD Usage, DWORD FVF, D3DPOOL Pool, IDirect3DVertexBuffer9** ppVertexBuffer, HANDLE* pSharedHandle)
{
	LPDIRECT3DDEVICE9 pDevice = CDirectXDevice::GetD3DDevice();
	if (FAILED(pDevice->CreateVertexBuffer(Length, Usage, FVF, Pool, ppVertexBuffer, pSharedHandle)))
	{
		CDirectXDevice::ErrorMessage("頂点バッファが作れませんでした。");
		return true;
	}

	return false;
}

//==========================================================================
// インデックスバッファの生成
bool CCreate::CreateIndexBuffer(UINT Length, DWORD Usage, D3DFORMAT Format, D3DPOOL Pool, IDirect3DIndexBuffer9 ** ppIndexBuffer, HANDLE * pSharedHandle)
{
	LPDIRECT3DDEVICE9 pDevice = CDirectXDevice::GetD3DDevice();
	if (FAILED(pDevice->CreateIndexBuffer(Length, Usage, Format, Pool, ppIndexBuffer, pSharedHandle)))
	{
		CDirectXDevice::ErrorMessage("インデックスバッファが作れませんでした。");
		return true;
	}
	return false;
}
