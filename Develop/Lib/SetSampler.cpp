//==========================================================================
// 画像の品質調整[SetSampler.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "SetSampler.h"

CSetSampler::CSetSampler()
{
}

CSetSampler::~CSetSampler()
{
}

//==========================================================================
// 初期値
void CSetSampler::SamplerFitteringNONE(LPDIRECT3DDEVICE9 pDevice)
{
	pDevice->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_NONE); // 小さくなった時に白枠
	pDevice->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_NONE); // 常に白枠
	pDevice->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_NONE); // 元のサイズより小さい時綺麗にする
}

//==========================================================================
// ぼや
void CSetSampler::SamplerFitteringLINEAR(LPDIRECT3DDEVICE9 pDevice)
{
	pDevice->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR); // 小さくなった時に白枠
	pDevice->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR); // 常に白枠
	pDevice->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_LINEAR); // 元のサイズより小さい時綺麗にする
}

//==========================================================================
// グラフィカル
void CSetSampler::SamplerFitteringGraphical(LPDIRECT3DDEVICE9 pDevice)
{
	pDevice->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR); // 小さくなった時に白枠
	pDevice->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR); // 常に白枠
	pDevice->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_NONE); // 元のサイズより小さい時綺麗にする
}
