//==========================================================================
// 2Dポリゴン[2DPolygon.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "2DPolygon.h"

C2DPolygon::C2DPolygon()
{
	this->m_pVertexBuffer = nullptr;
	this->m_Lock = false;
}

C2DPolygon::~C2DPolygon()
{
	this->m_temp.Release_(this->m_pVertexBuffer);
	this->m_texture.release();
	this->m_Lock = false;
}

//==========================================================================
// 初期化 失敗時true
// Input = 使用するテクスチャのパス ダブルポインタに対応
// NumData = 要素数
// AutoTextureResize = テクスチャのサイズ自動調節機能 trueで有効化
bool C2DPolygon::Init(const char ** Input, int NumData, bool AutoTextureResize)
{
	for (int i = 0; i < NumData; i++)
	{
		if (this->Init(Input[i], AutoTextureResize))
		{
			return true;
		}
	}

	return false;
}

//==========================================================================
// 初期化 失敗時true
// Input = 使用するテクスチャのパス
// AutoTextureResize = テクスチャのサイズ自動調節機能 trueで有効化
bool C2DPolygon::Init(const char * Input, bool AutoTextureResize)
{
	// テクスチャの格納
	if (this->m_texture.init(Input))
	{
		return true;
	}

	// バッファが登録されていないとき
	if (!this->m_Lock)
	{
		if (this->CreateVertexBuffer(sizeof(VERTEX_3) * 4, D3DUSAGE_WRITEONLY, FVF_VERTEX_3, D3DPOOL_MANAGED, &this->m_pVertexBuffer, nullptr))
		{
			return true;
		}
		this->m_Lock = true;
	}

	// 画像サイズ自動調節
	if (AutoTextureResize)
	{
		for (int i = 0; i < this->m_texture.maxdata(); i++)
		{
			float scale = CDirectXDevice::screenbuffscale();
			Tex_t * ptex = this->m_texture.get(i);
			CTexvec<int> vtex = CTexvec<int>(0, 0, 0, 0);

			ptex->resetsize();
			vtex = CTexvec<int>(0, 0, (int)((ptex->getsize()->w*scale) + 0.5f), (int)((ptex->getsize()->h*scale) + 0.5f));
			ptex->setsize(vtex);
		}
	}

	return false;
}

//==========================================================================
// 解放
void C2DPolygon::Uninit(void)
{
	this->m_temp.Release_(this->m_pVertexBuffer);
	this->m_texture.release();
	this->m_Lock = false;
}

//==========================================================================
// 描画
// Input = 座標クラス(C2DObject)をアドレス渡し 
// ADD = 加算合成の有無
void C2DPolygon::Draw(C2DObject * Input, bool ADD)
{
	LPDIRECT3DDEVICE9 pDevice = CDirectXDevice::GetD3DDevice();
	VERTEX_3* pPseudo = nullptr;
	Tex_t * ptex = this->m_texture.get(Input->getindex());

	// バッファのロック
	this->m_pVertexBuffer->Lock(0, sizeof(VERTEX_3) * 4, (void**)&pPseudo, D3DLOCK_DISCARD);
	Input->CreateVertexAngle(pPseudo, ptex->getsize());
	Input->CreateVertex(pPseudo, ptex->getsize());
	this->m_pVertexBuffer->Unlock();	// ロック解除

	// 加算合成
	if (ADD == true)
	{
		this->SetRenderZWRITEENABLE_START(pDevice);
		this->SetRenderSUB(pDevice);
	}

	pDevice->SetStreamSource(0, this->m_pVertexBuffer, 0, sizeof(VERTEX_3));

	pDevice->SetFVF(FVF_VERTEX_3);

	pDevice->SetTexture(0, nullptr);
	pDevice->SetTexture(0, ptex->gettex());

	this->SetRenderZENABLE_START(pDevice);

	pDevice->DrawPrimitive(D3DPT_TRIANGLESTRIP, 0, 2);

	if (ADD == true)
	{
		this->SetRenderZWRITEENABLE_END(pDevice);
		this->SetRenderADD(pDevice);
	}

	this->SetRenderZENABLE_END(pDevice);
}

//==========================================================================
// テクスチャのスケール変更
// index = テクスチャの番号
// scale = 入力した値が加算されます
void C2DPolygon::SetTexScale(int index, float scale)
{
	if (scale != 1.0f)
	{
		Tex_t * ptex = this->m_texture.get(index);
		CTexvec<int> vtex = CTexvec<int>(0, 0, (int)((ptex->getsize()->w*scale) + 0.5f), (int)((ptex->getsize()->h*scale) + 0.5f));

		ptex->setsize(vtex);
	}
}

//==========================================================================
// アニメーション切り替わり時の判定 切り替え時true 
// Input = アニメーションの初期化をしたC2DObject
// AnimationCount = アニメーションカウンタ
bool C2DPolygon::GetPattanNum(C2DObject * Input, int * AnimationCount)
{
	if (AnimationCount != nullptr)
	{
		// フレームが一致したとき
		if ((*AnimationCount) == (Input->GetAnimParam()->Frame*Input->GetAnimParam()->Pattern) - 1)
		{
			this->AnimationCountInit(AnimationCount);
			this->AnimationCountInit(&Input->GetAnimParam()->Count);
			return true;
		}
	}
	return false;
}

//==========================================================================
// アニメーション切り替わり時の判定 切り替え時true 
// Input = アニメーションの初期化をしたC2DObject
bool C2DPolygon::GetPattanNum(C2DObject * Input)
{
	if (Input->GetAnimParam()->Key)
	{
		// フレームが一致したとき
		if (Input->GetAnimParam()->Count == (Input->GetAnimParam()->Frame*Input->GetAnimParam()->Pattern) - 1)
		{
			this->AnimationCountInit(&Input->GetAnimParam()->Count);
			return true;
		}
	}
	return false;
}

//==========================================================================
// アニメーション用のカウンタの初期化
// AnimationCount = アニメーションカウンタ
int * C2DPolygon::AnimationCountInit(int * AnimationCount)
{
	if (AnimationCount != nullptr)
	{
		(*AnimationCount) = -1;
	}
	return AnimationCount;
}
