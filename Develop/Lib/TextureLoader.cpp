//==========================================================================
// テクスチャローダー[TextureLoader.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "TextureLoader.h"

CTextureLoader::CTextureLoader()
{
	this->m_numtex = 0;
}

CTextureLoader::~CTextureLoader()
{
	this->m_texture.clear();
}

//==========================================================================
// 読み込み
bool CTextureLoader::init(const char ** Input, int Num)
{
	for (int i = 0; i < Num; i++)
	{
		if (this->init(Input[i]))
		{
			return true;
		}
	}

	return false;
}

//==========================================================================
// 読み込み
bool CTextureLoader::init(const char * Input)
{
	CTexture *ptex = nullptr;
	bool bkey = false; // 鍵

	// ファイルパスがある場合
	if (Input != nullptr)
	{
		// 重複読み込みのチェック
		for (auto itr = this->m_texture.begin(); itr != this->m_texture.end(); ++itr)
		{
			// 重複しているとき
			if (!strcmp((*itr)->gettag(), Input))
			{
				CTexture *pptex = (*itr);
				this->create(ptex, Input);
				ptex->copy(pptex);
				bkey = true;
				break;
			}
		}

		// 重複していないとき
		if (bkey == false)
		{
			this->create(ptex, Input);

			// テクスチャの読み込み
			if (FAILED(ptex->load()))
			{
				CDirectXDevice::ErrorMessage("テクスチャが存在しません \n %s", ptex->gettag());
				return true;
			}
		}
	}
	else if (Input == nullptr)
	{
		this->create(ptex, Input);
	}

	// データ数の更新
	this->m_numtex = (int)this->m_texture.size();

	return false;
}

//==========================================================================
// 解放
void CTextureLoader::release(void)
{
	// 登録済みデータの破棄
	for (auto itr = this->m_texture.begin(); itr != this->m_texture.end(); ++itr)
	{
		(*itr)->release();
		if ((*itr) != nullptr)
		{
			delete(*itr);
			(*itr) = nullptr;
		}
	}
	this->m_numtex = 0;
	this->m_texture.clear();
}

//==========================================================================
// データのゲッター
CTextureLoader::CTexture * CTextureLoader::get(int nID)
{
	if (this->m_numtex <= nID)
	{
		return nullptr;
	}
	else
	{
		return this->m_texture[nID];
	}
}

//==========================================================================
// 生成
CTextureLoader::CTexture * CTextureLoader::create(CTexture *& pinp, const char * Input)
{
	pinp = new CTexture();

	// テクスチャ情報の登録
	this->m_texture.push_back(pinp);

	pinp->settag(Input);

	return pinp;
}

CTextureLoader::CTexture::CTexture()
{
	this->m_Lock = false;
	this->m_texture = nullptr;
	this->m_tag = nullptr;
	this->m_texparam = CTexvec<int>(0, 0, 0, 0);
	this->m_mastersize = CTexvec<int>(0, 0, 0, 0);
}

CTextureLoader::CTexture::~CTexture()
{
	if (this->m_texture != nullptr)
	{
		this->m_texture->Release();
		this->m_texture = nullptr;
		this->m_Lock = false;
	}
	if (this->m_tag != nullptr)
	{
		delete[]this->m_tag;
		this->m_tag = nullptr;
	}
}

//==========================================================================
// サイズリセット
void CTextureLoader::CTexture::resetsize(void)
{
	this->m_texparam = this->m_mastersize;
}

//==========================================================================
// サイズのセット
void CTextureLoader::CTexture::setsize(CTexvec<int> Input)
{
	this->m_texparam = Input;
}

//==========================================================================
// 読み込み
HRESULT CTextureLoader::CTexture::load(void)
{
	LPDIRECT3DDEVICE9 pDevice = CDirectXDevice::GetD3DDevice();
	D3DXIMAGE_INFO dil; // 画像データの管理
	HRESULT hr = D3DXCreateTextureFromFile(pDevice, this->m_tag, &this->m_texture);

	if (!FAILED(hr))
	{
		// 画像データの格納
		D3DXGetImageInfoFromFile(this->m_tag, &dil);
		this->m_texparam = CTexvec<int>(0, 0, dil.Width, dil.Height);
		this->m_mastersize = CTexvec<int>(0, 0, dil.Width, dil.Height);
		this->m_Lock = true;
	}

	return hr;
}

//==========================================================================
// 解放
void CTextureLoader::CTexture::release(void)
{
	if (this->m_Lock)
	{
		if (this->m_texture != nullptr)
		{
			this->m_texture->Release();
			this->m_texture = nullptr;
			this->m_Lock = false;
		}
	}

	if (this->m_tag != nullptr)
	{
		delete[]this->m_tag;
		this->m_tag = nullptr;
	}
}

//==========================================================================
// tagのセット
void CTextureLoader::CTexture::settag(const char * ptag)
{
	int tagsize = 1; // \0

	if (ptag != nullptr)
	{
		// 文字数算出
		for (int i = 0; ptag[i] != '\0'; i++, tagsize++);
	}

	this->m_tag = new char[tagsize];

	if (ptag != nullptr)
	{
		// tagの登録
		strcpy(this->m_tag, ptag);
	}
	else
	{
		this->m_tag[0] = 0;
	}
}

//==========================================================================
// コピー
void CTextureLoader::CTexture::copy(CTexture * pinp)
{
	this->m_Lock = false;
	this->m_texture = pinp->m_texture;
	this->m_texparam = pinp->m_texparam;
	this->m_mastersize = pinp->m_mastersize;
}
