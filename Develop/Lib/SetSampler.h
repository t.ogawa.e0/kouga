//==========================================================================
// 画像の品質調整[SetSampler.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _SetSampler_H_
#define _SetSampler_H_

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>

//==========================================================================
//
// class  : CSetSampler
// Content: 画像の品質を調整するクラス
//
//==========================================================================
class CSetSampler
{
protected:
	CSetSampler();
	~CSetSampler();
	// 初期値
	void SamplerFitteringNONE(LPDIRECT3DDEVICE9 pDevice);
	// ぼや
	void SamplerFitteringLINEAR(LPDIRECT3DDEVICE9 pDevice);
	// グラフィカル
	void SamplerFitteringGraphical(LPDIRECT3DDEVICE9 pDevice);
};

#endif // !_SetSampler_H_
