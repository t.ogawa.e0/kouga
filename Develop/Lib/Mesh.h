//==========================================================================
// メッシュ[Mesh.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _Mesh_H_
#define _Mesh_H_

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>
#include <vector>
#include "ExcelOpen.h"
#include "SetRender.h"
#include "Vertex3D.h"
#include "3DObject.h"
#include "Template.h"
#include "DXDevice.h"
#include "TextureLoader.h"
#include "Create.h"

//==========================================================================
//
// class  : CMesh
// Content: 縮退ポリゴン
//
//==========================================================================
class CMesh : private CExcelOpen, private VERTEX_3D, private CSetRender, private CCreate
{
private:
	static constexpr int xInfo = 1;
	static constexpr int x_2 = 2;
	static constexpr float CorrectionValue = 0.5f;
private:
	//==========================================================================
	//
	// class  : DEGENERATEPOLYGONINFORMATION
	// Content: 縮退ポリゴンのパラメーター
	//
	//==========================================================================
	class DEGENERATEPOLYGONINFORMATION
	{
	public:
		int NumMeshX; // 面の数
		int NumMeshZ; // 面の数
		int VertexOverlap; // 重複する頂点数
		int	NumXVertexWey; // 視覚化されている1列の頂点数
		int	NumZVertex; // 基礎頂点数
		int	NumXVertex; // 基礎頂点数
		int	NumMeshVertex; // 視覚化されている全体の頂点数
		int	MaxPrimitive; // プリミティブ数
		int	MaxIndex; // 最大Index数
		int	MaxVertex; // 最大頂点数
	};

	//==========================================================================
	//
	// class  : DEGENERATEPOLYGON
	// Content: 縮退ポリゴンの情報
	//
	//==========================================================================
	class DEGENERATEPOLYGON
	{
	public:
		DEGENERATEPOLYGON();
		~DEGENERATEPOLYGON();
	public:
		D3DMATERIAL9 pMatMesh; // メッシュ
		LPDIRECT3DVERTEXBUFFER9 pVertexBuffer; // 頂点バッファ
		LPDIRECT3DINDEXBUFFER9 pIndexBuffer; // インデックスバッファ
		DEGENERATEPOLYGONINFORMATION Info; // 縮退ポリゴンの情報
	};
public:
	typedef FILELINK2 EXCELLINK; // Excel用リンク格納
public:
	CMesh();
	~CMesh();

	//// 初期化 Excel 失敗時true
	//bool Init(const char * pFile);

	//// 初期化 失敗時true
	//bool Init(const EXCELLINK * pFile, int Size);

	// 初期化 失敗時true
	// Input = テクスチャ
	// x = 横幅
	// z = 奥行
	bool Init(const char * Input, int x, int z);

	// 初期化 失敗時true
	// Input = 使用するテクスチャのパス ダブルポインタに対応
	// x = 横幅
	// z = 奥行
	bool Init(const char ** Input, int Size, int x, int z);

	// 解放
	void Uninit(void);

	// 描画
	// Input = 座標クラス(C3DObject)をアドレス渡し 
	void Draw(C3DObject * Input);

	// データ数取得
	int GetNumData(void) { return m_texture.maxdata(); }

	// メッシュデータゲッター
	DEGENERATEPOLYGONINFORMATION GetMeshData(int num) { return this->m_MeshData[num]->Info; }
private:
	// リンク入力
	void InputLink(const EXCELLINK * pInput, int num);

	// メッシュ情報の生成
	void MeshFieldInfo(DEGENERATEPOLYGONINFORMATION * Output, const int numX, const int numZ);

	// インデックス情報の生成
	void CreateIndex(LPWORD * Output, const DEGENERATEPOLYGONINFORMATION * Input);

	// バーテックス情報の生成
	void CreateVertex(VERTEX_4 * Output, const DEGENERATEPOLYGONINFORMATION * Input);

	// マテリアルの設定
	void MaterialSetting(DEGENERATEPOLYGON *Output);
private:
	CTemplates m_temp; // テンプレート
	CTextureLoader m_texture; // テクスチャの格納
	std::vector<DEGENERATEPOLYGON*> m_MeshData; // メッシュデータ
	EXCELLINK *m_Link; // リンク
};

#endif // !_Mesh_H_
