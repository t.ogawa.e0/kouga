//==========================================================================
// テンプレートヘッダ[Template.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _Template_H_ // インクルードガード
#define _Template_H_

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <math.h>
#include <stdio.h>
#include <iostream>
#include <iomanip>

//==========================================================================
//
// class  : CTemplate
// Content: テンプレート(継承用)
//
//==========================================================================
class CTemplate
{
protected:
	// データ要素数
	// 引数に要素数を知りたい変数を入れてください。要素数を出してくれます
	template <typename T, size_t p> static int Sizeof(const T(&)[p]);

	// メモリ確保
	// *&p = new T[Size];
	template <typename T, typename S> static T* New(T*& p, S size);

	// メモリ確保
	// *&p = new T[1];
	template <typename T> static T* New(T*& p);

	// メモリ解放 delete[]
	// delete[] p;
	template <typename T> static void Delete(T*& p);

	// メモリ内のデータ破棄
	// p->Release();
	template <typename T> static void Release(T*& p);

	// true⇔false 変換
	template <typename T> static T Bool(T *p);

	// デコイ
	template <typename T> static void Decoy(const T *p);
};

//==========================================================================
//
// class  : CTemplates
// Content: テンプレート(非継承用)
//
//==========================================================================
class CTemplates : private CTemplate
{
public:
	// データ要素数
	template <typename T, size_t p>  int Sizeof_(const T(&)[p]) { return (int)size_t(p); }

	// メモリ確保
	// *&p = new T[Size];
	template <typename T, typename S>  T* New_(T*& p, S size) { return this->New(p, size); }

	// メモリ確保
	// *&p = new T[1];
	template <typename T>  T* New_(T*& p) { return this->New(p); }

	// メモリ解放 delete[]
	// delete[] p;
	template <typename T>  void Delete_(T*& p) { this->Delete(p); }

	// メモリ内のデータ破棄
	// p->Release();
	template <typename T>  void Release_(T*& p) { this->Release(p); }

	// true⇔false 変換
	template <typename T>  T Bool_(T* p) { return this->Bool(p); }

	// デコイ
	template <typename T>  void Decoy_(const T *p) { (*p); }
};

//==========================================================================
// データ要素数
template<typename T, size_t p>
inline int CTemplate::Sizeof(const T(&)[p])
{
	return (int)size_t(p);
}

//==========================================================================
// メモリ確保
// *&p = new T[Size];
template<typename T, typename S>
inline T * CTemplate::New(T *& p, S size)
{
	p = nullptr;
	p = new T[(size)];

	return p;
}

//==========================================================================
// メモリ確保
// *&p = new T[1];
template<typename T>
inline T * CTemplate::New(T *& p)
{
	p = nullptr;
	p = new T[1];

	return p;
}

//==========================================================================
// メモリ解放 delete[]
// delete[] p;
template<typename T>
inline void CTemplate::Delete(T *& p)
{
	if (p != nullptr)
	{
		delete[]p;
		p = nullptr;
	}
}

//==========================================================================
// メモリ内のデータ破棄
// p->Release();
template<typename T>
inline void CTemplate::Release(T *& p)
{
	if (p != nullptr)
	{
		p->Release();
		p = nullptr;
	}
}

//==========================================================================
// true⇔false 変換
template<typename T>
inline T CTemplate::Bool(T * p)
{
	(*p) = ((*p) ^ 1);

	return (*p);
}

//==========================================================================
// デコイ
template<typename T>
inline void CTemplate::Decoy(const T * p)
{
	(*p);
}

#endif // !_Template_H_
