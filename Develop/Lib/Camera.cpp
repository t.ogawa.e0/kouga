//==========================================================================
// カメラ[Camera.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Camera.h"

CCamera::CCamera()
{
}

CCamera::~CCamera()
{
}

//==========================================================================
// 初期化
void CCamera::Init(void)
{
	this->m_Eye = D3DXVECTOR3(0.0f, 3.0f, -10.0f); // 注視点
	this->m_At = D3DXVECTOR3(0.0f, 1.0f, 0.0f); // カメラ座標
	this->m_Eye2 = D3DXVECTOR3(0.0f, 0.0f, 0.0f); // 注視点
	this->m_At2 = D3DXVECTOR3(0.0f, 0.0f, 0.0f); // カメラ座標
	this->m_Up = D3DXVECTOR3(0.0f, 1.0f, 0.0f); // ベクター
	this->m_VecUp = D3DXVECTOR3(0, 1, 0); // 上ベクトル
	this->m_VecFront = D3DXVECTOR3(0, 0, 1); // 前ベクトル
	this->m_VecRight = D3DXVECTOR3(1, 0, 0); //  右ベクトル

	// 前ベクトルの正規化
	D3DXVec3Normalize(&this->m_VecFront, &this->m_VecFront);
	D3DXVec3Normalize(&this->m_VecUp, &this->m_VecUp);
	D3DXVec3Normalize(&this->m_VecRight, &this->m_VecRight);
}

//==========================================================================
// 初期化
void CCamera::Init(D3DXVECTOR3 * pEye, D3DXVECTOR3 * pAt)
{
	this->m_Eye = D3DXVECTOR3(pEye->x, pEye->y, pEye->z); // 注視点
	this->m_At = D3DXVECTOR3(pAt->x, pAt->y, pAt->z); // カメラ座標
	this->m_Eye2 = D3DXVECTOR3(0.0f, 0.0f, 0.0f); // 注視点
	this->m_At2 = D3DXVECTOR3(0.0f, 0.0f, 0.0f); // カメラ座標
	this->m_Up = D3DXVECTOR3(0.0f, 1.0f, 0.0f); // ベクター
	this->m_VecUp = D3DXVECTOR3(0, 1, 0); // 上ベクトル
	this->m_VecFront = D3DXVECTOR3(0, 0, 1); // 前ベクトル
	this->m_VecRight = D3DXVECTOR3(1, 0, 0); //  右ベクトル

	// 前ベクトルの正規化
	D3DXVec3Normalize(&this->m_VecFront, &this->m_VecFront);
	D3DXVec3Normalize(&this->m_VecUp, &this->m_VecUp);
	D3DXVec3Normalize(&this->m_VecRight, &this->m_VecRight);
}

//==========================================================================
// 解放
void CCamera::Uninit(void)
{
}

//==========================================================================
// 更新
// この関数は一か所に設置すれば全て自動で処理されます
// MtxView = ビュー行列を入れてください
// fWidth = 画面の幅を入れてください
// fHeight = 画面の高さを入れてください
// pDevice = デバイスを入れてください
void CCamera::Update(D3DXMATRIX * MtxView, int fWidth, int fHeight, LPDIRECT3DDEVICE9 pDevice)
{
	if (MtxView != nullptr)
	{
		D3DXMATRIX aMtxProjection; // プロジェクション行列

		// プロジェクション行列の作成
		// ズームイン、ズームアウトのような物
		D3DXMatrixPerspectiveFovLH(&aMtxProjection, D3DXToRadian(60)/*D3DX_PI/3*/, (float)fWidth / (float)fHeight, 0.1f/*ニヤ*/, 1000.0f/*ファー*/);
		pDevice->SetTransform(D3DTS_VIEW, MtxView);
		pDevice->SetTransform(D3DTS_PROJECTION, &aMtxProjection);
	}
}

//==========================================================================
// ビュー行列生成
D3DXMATRIX * CCamera::CreateView(void)
{
	D3DXVECTOR3 Eye; // 注視点
	D3DXVECTOR3 At; // カメラ座標
	D3DXVECTOR3 Up; // ベクター

	Eye = this->m_Eye + this->m_Eye2;
	At = this->m_At + this->m_At2;
	Up = this->m_Up;

	// ビュー変換行列 ,(LH = 左手座標 ,LR = 右手座標)
	D3DXMatrixLookAtLH(&this->m_aMtxView, &Eye, &At, &Up);

	return &this->m_aMtxView;
}

//==========================================================================
// 平行処理
void CCamera::CameraMoveXYZ(D3DXVECTOR3 * pVec, D3DXVECTOR3 * pVecRight, const D3DXVECTOR3 * pVecUp, const D3DXVECTOR3 * pVecFront, D3DXVECTOR3 * pOut1, D3DXVECTOR3 * pOut2, const float * pSpeed)
{
	D3DXVec3Cross(pVecRight, pVecUp, pVecFront); // 外積
	D3DXVec3Normalize(pVec, pVec);
	*pOut1 += (*pVec)*(*pSpeed);
	*pOut2 += (*pVec)*(*pSpeed);
}

//==========================================================================
// X軸回転処理
void CCamera::CameraRangX(D3DXVECTOR3 * pDirection, D3DXMATRIX * pRot, D3DXVECTOR3 * pVecRight, const D3DXVECTOR3 * pVecUp, D3DXVECTOR3 * pVecFront, D3DXVECTOR3 * pOut1, D3DXVECTOR3 * pOut2, const float * pRang)
{
	D3DXVec3Cross(pVecRight, pVecUp, pVecFront); // 外積
	D3DXMatrixRotationY(pRot, *pRang); // 回転
	*pDirection = (*pOut1) - (*pOut2); // 向きベクトル
	D3DXVec3TransformNormal(pDirection, pDirection, pRot);
	D3DXVec3TransformNormal(pVecFront, pVecFront, pRot);
	D3DXVec3TransformNormal(pVecRight, pVecRight, pRot);
	*pOut1 = (*pOut2) + (*pDirection);
}

//==========================================================================
// Y軸回転処理
void CCamera::CameraRangY(D3DXVECTOR3 * pDirection, D3DXMATRIX * pRot, D3DXVECTOR3 * pVecRight, const D3DXVECTOR3 * pVecUp, D3DXVECTOR3 * pVecFront, D3DXVECTOR3 * pOut1, D3DXVECTOR3 * pOut2, const float * pRang)
{
	D3DXVec3Cross(pVecRight, pVecUp, pVecFront); // 外積
	D3DXMatrixRotationAxis(pRot, pVecRight, *pRang); // 回転
	*pDirection = (*pOut1) - (*pOut2); // 向きベクトル
	D3DXVec3TransformNormal(pDirection, pDirection, pRot);
	D3DXVec3TransformNormal(pVecFront, pVecFront, pRot);
	D3DXVec3TransformNormal(pVecRight, pVecRight, pRot);
	*pOut1 = (*pOut2) + (*pDirection);
}

//==========================================================================
// 視点変更 戻り値,視点とカメラの距離
float CCamera::ViewPos(D3DXVECTOR3 * pVec, D3DXVECTOR3 * pVecRight, const D3DXVECTOR3 * pVecUp, const D3DXVECTOR3 * pVecFront, D3DXVECTOR3 * pOut1, D3DXVECTOR3 * pOut2, const float * pSpeed)
{
	D3DXVec3Cross(pVecRight, pVecUp, pVecFront); // 外積
	D3DXVec3Normalize(pVec, pVec);
	*pOut1 += (*pVec)*(*pSpeed);
	*pOut2;

	float fDistance = powf
	(
		((pOut1->x) - (pOut2->x))*
		((pOut1->x) - (pOut2->x)) +
		((pOut1->y) - (pOut2->y))*
		((pOut1->y) - (pOut2->y)) +
		((pOut1->z) - (pOut2->z))*
		((pOut1->z) - (pOut2->z)),
		0.5f
	);

	return fDistance;
}

//==========================================================================
// 内積
bool CCamera::Restriction(D3DXMATRIX pRot, D3DXVECTOR3 pVecRight, const D3DXVECTOR3 pVecUp, D3DXVECTOR3 pVecFront, const float* pRang)
{
	D3DXVECTOR3 dir = D3DXVECTOR3(0, 1.0f, 0); // 単位ベクトル
	float Limit = 0.75f;

	// ベクトルの座標変換
	D3DXVec3Cross(&pVecRight, &pVecUp, &pVecFront); // 外積
	D3DXMatrixRotationAxis(&pRot, &pVecRight, *pRang); // 回転
	D3DXVec3TransformNormal(&pVecFront, &pVecFront, &pRot);
	float fVec3Dot = atanf(D3DXVec3Dot(&pVecFront, &dir));

	// 内積
	if (-Limit<fVec3Dot && Limit>fVec3Dot) { return true; }

	return false;
}

//==========================================================================
// 視点中心にX軸回転
// Rang = 入れた値が加算されます
void CCamera::RotViewX(float Rang)
{
	D3DXVECTOR3 Direction; // 向き
	D3DXMATRIX RotX; //X回転行列

	// ベクトルの座標変換
	this->CameraRangX(&Direction, &RotX, &this->m_VecRight, &this->m_VecUp, &this->m_VecFront, &this->m_Eye, &this->m_At, &Rang);
}

//==========================================================================
// 視点中心にY軸回転
// Rang = 入れた値が加算されます
void CCamera::RotViewY(float Rang)
{
	D3DXVECTOR3 Direction; // 向き
	D3DXMATRIX RotY; //Y回転行列

	// ベクトルの座標変換
	if (this->Restriction(RotY, this->m_VecRight, this->m_VecUp, this->m_VecFront, &Rang))
	{
		this->CameraRangY(&Direction, &RotY, &this->m_VecRight, &this->m_VecUp, &this->m_VecFront, &this->m_Eye, &this->m_At, &Rang);
	}
}

//==========================================================================
// カメラ中心にX軸回転
// Rang = 入れた値が加算されます
void CCamera::RotCameraX(float Rang)
{
	D3DXVECTOR3 Direction; // 向き
	D3DXMATRIX RotX; //X回転行列

	// ベクトルの座標変換
	this->CameraRangX(&Direction, &RotX, &this->m_VecRight, &this->m_VecUp, &this->m_VecFront, &this->m_At, &this->m_Eye, &Rang);
}

//==========================================================================
// カメラ中心にY軸回転
// Rang = 入れた値が加算されます
void CCamera::RotCameraY(float Rang)
{
	D3DXVECTOR3 Direction; // 向き
	D3DXMATRIX RotY; //Y回転行列

	// ベクトルの座標変換
	if (this->Restriction(RotY, this->m_VecRight, this->m_VecUp, this->m_VecFront, &Rang))
	{
		this->CameraRangY(&Direction, &RotY, &this->m_VecRight, &this->m_VecUp, &this->m_VecFront, &this->m_At, &this->m_Eye, &Rang);
	}
}

//==========================================================================
// X軸平行移動
// Speed = 入れた値が加算されます
void CCamera::MoveX(float Speed)
{
	D3DXVECTOR3 vec = this->m_VecRight;
	vec.y = 0;

	this->CameraMoveXYZ(&vec, &this->m_VecRight, &this->m_VecUp, &this->m_VecFront, &this->m_Eye, &this->m_At, &Speed);
}

//==========================================================================
// Y軸平行移動
// Speed = 入れた値が加算されます
void CCamera::MoveY(float Speed)
{
	D3DXVECTOR3 vec = this->m_VecUp;
	vec.z = 0;
	vec.x = 0;

	this->CameraMoveXYZ(&vec, &this->m_VecRight, &this->m_VecUp, &this->m_VecFront, &this->m_Eye, &this->m_At, &Speed);
}

//==========================================================================
// Z軸平行移動
// Speed = 入れた値が加算されます
void CCamera::MoveZ(float Speed)
{
	D3DXVECTOR3 vec = this->m_VecFront;
	vec.y = 0;

	this->CameraMoveXYZ(&vec, &this->m_VecRight, &this->m_VecUp, &this->m_VecFront, &this->m_Eye, &this->m_At, &Speed);
}

//==========================================================================
// 視点変更 戻り値,視点とカメラの距離
// Distance = 入れた値が加算されます
float CCamera::DistanceFromView(float Distance)
{
	D3DXVECTOR3 vec = this->m_VecFront;

	return this->ViewPos(&vec, &this->m_VecRight, &this->m_VecUp, &this->m_VecFront, &this->m_Eye, &this->m_At, &Distance);
}

//==========================================================================
// ベクターの取得
D3DXVECTOR3 CCamera::GetVECTOR(VectorList List)
{
	switch (List)
	{
	case VectorList::VEYE:
		return this->m_Eye + this->m_Eye2;
		break;
	case VectorList::VAT:
		return this->m_At + this->m_At2;
		break;
	case VectorList::VUP:
		return this->m_Up;
		break;
	case VectorList::VECUP:
		return this->m_VecUp;
		break;
	case VectorList::VECFRONT:
		return this->m_VecFront;
		break;
	case VectorList::VECRIGHT:
		return this->m_VecRight;
		break;
	default:
		break;
	}

	return D3DXVECTOR3(0, 0, 0);
}

//==========================================================================
// カメラY回転情報
float CCamera::GetRestriction(void)
{
	D3DXVECTOR3 dir = D3DXVECTOR3(0, 1.0f, 0); // 単位ベクトル

	return atanf(D3DXVec3Dot(&this->m_VecFront, &dir)); // 内積
}

//==========================================================================
// カメラ座標をセット
// Eye = 注視点
// At = カメラ座標
// Up = ベクター
void CCamera::SetCameraPos(D3DXVECTOR3 * Eye, D3DXVECTOR3 * At, D3DXVECTOR3 * Up)
{
	this->m_Eye = D3DXVECTOR3(0.0f, 2.5f, -2.0f); // 注視点
	this->m_At = D3DXVECTOR3(0.0f, 2.5f, 0.0f); // カメラ座標
	this->m_Up = D3DXVECTOR3(0.0f, 1.0f, 0.0f); // ベクター

	this->m_Eye = *Eye; this->m_At = *At; this->m_Up = *Up;
}

//==========================================================================
// カメラ座標
// At = カメラ座標
void CCamera::SetAt(D3DXVECTOR3 * At)
{
	this->m_At2 = *At;
}

//==========================================================================
// 注視点
// Eye = 注視点
void CCamera::SetEye(D3DXVECTOR3 * Eye)
{
	this->m_Eye2 = *Eye;
}
