//==========================================================================
// デバイス[DXDevice.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "DXDevice.h"

//==========================================================================
// 実体
//==========================================================================
LPDIRECT3D9 CDirectXDevice::m_pD3D; //ダイレクト3Dインターフェース
LPDIRECT3DDEVICE9 CDirectXDevice::m_pD3DDevice;//ダイレクト3Dデバイス
CDirectXDevice::CWindowsSize<int> CDirectXDevice::m_WindowsSize; // ウィンドウサイズ
D3DPRESENT_PARAMETERS CDirectXDevice::m_d3dpp;
D3DDISPLAYMODE CDirectXDevice::m_d3dpm;
std::vector<D3DDISPLAYMODE> CDirectXDevice::m_WindowsModes; // ディスプレイの解像度格納
float CDirectXDevice::m_screenbuffscale;
HWND CDirectXDevice::m_hwnd = nullptr;
int CDirectXDevice::m_winmood = 0;
bool CDirectXDevice::m_GraphicMode = false;

CDirectXDevice::CDirectXDevice()
{
}

CDirectXDevice::~CDirectXDevice()
{
}

//==========================================================================
// デバイスの生成
bool CDirectXDevice::CreateDevice(void)
{
	m_pD3DDevice = nullptr;

	//デバイスオブジェクトを生成
	//[デバイス作成制御]<描画>と<頂点処理>
	if (FAILED(m_pD3D->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, m_hwnd, D3DCREATE_HARDWARE_VERTEXPROCESSING, &m_d3dpp, &m_pD3DDevice)))
	{
		ErrorMessage("デバイスの生成に失敗しました。");
		return true;
	}

	m_d3dpp.hDeviceWindow = m_hwnd;

	// ビューポートの設定
	D3DVIEWPORT9 vp;
	vp.X = 0;
	vp.Y = 0;
	vp.Width = m_d3dpp.BackBufferWidth;
	vp.Height = m_d3dpp.BackBufferHeight;
	vp.MinZ = 0.0f;
	vp.MaxZ = 1.0f;

	if (FAILED(m_pD3DDevice->SetViewport(&vp)))
	{
		ErrorMessage("ビューポートの設定に失敗しました。");
		return true;
	}

	// レンダーステートの設定
	{// この3つでα値が使える
		m_pD3DDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);				// αブレンドを許可
		m_pD3DDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);		// αソースカラーの設定
		m_pD3DDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);	// α
	}

	// テクスチャステージの設定
	{
		m_pD3DDevice->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
		m_pD3DDevice->SetTextureStageState(0, D3DTSS_ALPHAARG2, D3DTA_DIFFUSE);
		m_pD3DDevice->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_MODULATE);
	}

	// サンプラーステートの設定(UV値を変えると増えるようになる)
	{
		// WRAP...		反復する
		// CLAMP...　	引き伸ばす
		// MIRROR...　	鏡状態
		m_pD3DDevice->SetSamplerState(0, D3DSAMP_ADDRESSU, D3DTADDRESS_WRAP);
		m_pD3DDevice->SetSamplerState(0, D3DSAMP_ADDRESSV, D3DTADDRESS_WRAP);
		m_pD3DDevice->SetSamplerState(0, D3DSAMP_ADDRESSW, D3DTADDRESS_WRAP);
	}

	// フィルタリング
	{
		// 画像を滑らかにする
		if (m_GraphicMode == false)
		{
			m_pD3DDevice->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR); // 小さくなった時に白枠
			m_pD3DDevice->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR); // 常に白枠
			m_pD3DDevice->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_LINEAR); // 元のサイズより小さい時綺麗にする
		}
		else if (m_GraphicMode == true)
		{
			m_pD3DDevice->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR); // 小さくなった時に白枠
			m_pD3DDevice->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR); // 常に白枠
			m_pD3DDevice->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_NONE); // 元のサイズより小さい時綺麗にする
		}
	}

	return false;
}

//==========================================================================
// 初期化 失敗時true
bool CDirectXDevice::Init(void)
{
	D3DDISPLAYMODE d3dspMode;
	int num = 0;

	ZeroMemory(&m_d3dpp, sizeof(m_d3dpp));

	// 現在のディスプレイモードを取得
	if (FAILED(m_pD3D->GetAdapterDisplayMode(D3DADAPTER_DEFAULT, &m_d3dpm)))
	{
		return true;
	}

	// 何種類ディスプレイモードあるかを調べる
	num = m_pD3D->GetAdapterModeCount(D3DADAPTER_DEFAULT, m_d3dpm.Format);

	// ディスプレイモードを調べる
	for (int i = 0; i < num; i++)
	{
		if (FAILED(m_pD3D->EnumAdapterModes(D3DADAPTER_DEFAULT, m_d3dpm.Format, i, &d3dspMode)))
		{
			ErrorMessage("ディスプレイモードの検索に失敗しました");
			return true;
		}

		// ディスプレイモードを記憶
		m_WindowsModes.push_back(d3dspMode);
	}

	return false;
}

//==========================================================================
// 解放
void CDirectXDevice::Uninit(void)
{
	//デバイスの開放
	if (m_pD3DDevice != nullptr)
	{
		m_pD3DDevice->Release();
		m_pD3DDevice = nullptr;
	}

	//Direct3Dオブジェクトの開放
	if (m_pD3D != nullptr)
	{
		m_pD3D->Release();
		m_pD3D = nullptr;
	}

	m_WindowsModes.clear();
}

//==========================================================================
// Direct3Dオブジェクトの作成
bool CDirectXDevice::Direct3DCreate(void)
{
	m_pD3D = Direct3DCreate9(D3D_SDK_VERSION);
	if (m_pD3D == nullptr)
	{
		return true;
	}

	return false;
}

bool CDirectXDevice::CreateDisplayMode(void)
{
	// どのディスプレイモードを使うかを決定
	int nCount = 0;
	D3DDISPLAYMODE d3dspMode = D3DDISPLAYMODE();
	CWindowsSize<float> w1percent = CWindowsSize<float>(0.0f, 0.0f);
	CWindowsSize<float> wErrorpercent = CWindowsSize<float>(0.0f, 0.0f);

	m_screenbuffscale = 1.0f;

	if (m_winmood != (int)m_WindowsModes.size())
	{
		//フルスクリーンのとき、リフレッシュレートを変えられる
		d3dspMode = m_WindowsModes[m_winmood];
		m_d3dpp.FullScreen_RefreshRateInHz = D3DPRESENT_RATE_DEFAULT;
		m_d3dpp.Windowed = true; // ウィンドウモード
	}
	else if (m_winmood == (int)m_WindowsModes.size())
	{
		//フルスクリーンのとき、リフレッシュレートを変えられる
		d3dspMode = m_WindowsModes[m_WindowsModes.size() - 1];
		m_d3dpp.FullScreen_RefreshRateInHz = d3dspMode.RefreshRate;
		m_d3dpp.Windowed = false; // ウィンドウモード
	}

	// 推奨ウィンドウサイズの1%のウィンドウサイズを算出
	w1percent.m_Width = (float)((float)1920 / 100.0f);
	w1percent.m_Height = (float)((float)1080 / 100.0f);
	nCount = 0;

	// 動作環境の最大解像度になるまで繰り返す
	for (;;)
	{
		wErrorpercent.m_Width += w1percent.m_Width;
		wErrorpercent.m_Height += w1percent.m_Height;
		nCount++;
		if (d3dspMode.Width <= wErrorpercent.m_Width)
		{
			break;
		}
		if (d3dspMode.Height <= wErrorpercent.m_Height)
		{
			break;
		}
	}

	// 補正値算出
	m_screenbuffscale = nCount *0.01f;

	//デバイスのプレゼンテーションパラメータ(デバイスの設定値)
	m_d3dpp.BackBufferWidth = d3dspMode.Width;		//バックバッファの幅
	m_d3dpp.BackBufferHeight = d3dspMode.Height;	//バックバッファの高さ
	m_d3dpp.BackBufferFormat = d3dspMode.Format;    //バックバッファフォーマット
	m_d3dpp.BackBufferCount = 1;					//バッファの数
	m_d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;		//垂直？
	m_d3dpp.EnableAutoDepthStencil = TRUE;			//3Dの描画に必要
	m_d3dpp.AutoDepthStencilFormat = D3DFMT_D16;	// 16Bit Zバッファ作成
	m_d3dpp.PresentationInterval = D3DPRESENT_INTERVAL_DEFAULT;

	// ウィンドウサイズ確定
	m_WindowsSize.m_Width = d3dspMode.Width;
	m_WindowsSize.m_Height = d3dspMode.Height;

	return true;
}
