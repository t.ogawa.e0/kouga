//==========================================================================
// テクスチャローダー[TextureLoader.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _TextureLoader_H_
#define _TextureLoader_H_

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>
#include <vector>
#include <string.h>
#include "DataType.h"
#include "DXDevice.h"

//==========================================================================
//
// class  : CTextureLoader
// Content: 2Dテクスチャローダー
//
//==========================================================================
class CTextureLoader
{
public:
	//==========================================================================
	//
	// class  : CTexture
	// Content: テクスチャの管理
	//
	//==========================================================================
	class CTexture
	{
	public:
		CTexture();
		~CTexture();
		// サイズのリセット
		void resetsize(void);
		// サイズのセット
		void setsize(CTexvec<int> Input);
		// サイズの取得
		CTexvec<int> *getsize(void) { return &this->m_texparam; }
		// 読み込み
		HRESULT load(void);
		// 解放
		void release(void);
		// tagのセット
		void settag(const char * ptag);
		// tagの取得
		const char * gettag(void) { return this->m_tag; }
		// リソースの取得
		const LPDIRECT3DTEXTURE9 gettex(void) { return this->m_texture; }
		// コピー
		void copy(CTexture * pinp);
	private:
		LPDIRECT3DTEXTURE9 m_texture; // テクスチャポインタ
		CTexvec<int> m_texparam; // テクスチャのサイズ
		CTexvec<int> m_mastersize; // マスターサイズ
		char *m_tag; // タグ
		bool m_Lock; // 鍵
	};
public:
	CTextureLoader();
	~CTextureLoader();
	// 読み込み
	bool init(const char ** Input, int Num);
	// 読み込み
	bool init(const char * Input);
	// 解放
	void release(void);
	// データのゲッター
	CTexture * get(int nID);
	// データ数
	int maxdata(void) { return this->m_numtex; }
private:
	// 生成
	CTexture * create(CTexture *& pinp, const char * Input);
private:
	std::vector<CTexture*> m_texture; // 管理
	int m_numtex = 0; // データ数
};

#endif // !_TextureLoader_H_
