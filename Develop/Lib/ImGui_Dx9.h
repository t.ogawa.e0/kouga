//==========================================================================
// ImGui_Dx9[ImGui_Dx9.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _ImGui_Dx9_h_
#define _ImGui_Dx9_h_

//==========================================================================
// include
//==========================================================================
#if defined(_DEBUG) || defined(DEBUG)
#include "ImGUI_DirectX9\imgui.h"
#include "ImGUI_DirectX9\imgui_impl_dx9.h"
#else
// Define attributes of all API symbols declarations, e.g. for DLL under Windows.
#ifndef IMGUI_API
#define IMGUI_API
#endif
#endif
#include <d3d9.h>
#define DIRECTINPUT_VERSION (0x0800) // DirectInputのバージョン指定
#include <dinput.h>
#include <tchar.h>

//==========================================================================
//
// class  : CImGui_Dx9
// Content: ImGui
//
//==========================================================================
class CImGui_Dx9
{
public:
	CImGui_Dx9();
	~CImGui_Dx9();

	// 初期化
	bool Init(HWND hWnd, LPDIRECT3DDEVICE9 pDevice);

	// 解放
	void Uninit(void);

	// 更新
	void Update(void);

	// 描画
	void Draw(void);

	// Handle loss of D3D9 device
	void DeviceReset(HRESULT result, LPDIRECT3DDEVICE9 pDevice, D3DPRESENT_PARAMETERS * pd3dpp);

	// Process Win32 mouse/keyboard inputs. 
	IMGUI_API LRESULT ImGui_WndProcHandler(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

	// メッセージ
	LRESULT SetMenu(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam, LPDIRECT3DDEVICE9 pDevice, D3DPRESENT_PARAMETERS * pd3dpp);

	// 新しいウィンドウ
	void NewWindow(const char * name, bool key);

	// ウィンドウ終了
	void EndWindow(void);

	// テキスト 英文字限定
#if defined(_DEBUG) || defined(DEBUG)
	template <typename ... Args>
	void Text(const char * text, Args const & ... args);
#else
	template <typename ... Args>
	void Text(const char * text, ...);
#endif
	// 新しいフレーム 一回のみ
	void NewFrame(void);

	// フレーム終わり 一回のみ
	void EndFrame(void);
private:
};

//==========================================================================
// テキスト 英文字限定
#if defined(_DEBUG) || defined(DEBUG)
template<typename ... Args>
inline void CImGui_Dx9::Text(const char * text, Args const & ... args)
{
	ImGui::Text(text, args ...);
	ImGui::Separator();
}
#else
template<typename ...Args>
inline void CImGui_Dx9::Text(const char * text, ...)
{
	text;
}
#endif

#endif // !_ImGui_Dx9_h_
