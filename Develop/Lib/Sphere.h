//==========================================================================
// 球体[Screen.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _sphere_h_
#define _sphere_h_

//==========================================================================
// Include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>
#include <vector>
#include "Template.h"
#include "3DObject.h"
#include "DXDevice.h"
#include "SetRender.h"
#include "DataType.h"
#include "Vertex3D.h"
#include "TextureLoader.h"

//==========================================================================
//
// class  : CSphere
// Content: 球体
//
//==========================================================================
class CSphere : private CSetRender, private VERTEX_3D
{
private:
	//==========================================================================
	//
	// class  : CSphereData
	// Content: 球体のデータ管理
	//
	//==========================================================================
	class CSphereData
	{
	public:
		CSphereData();
		~CSphereData();
		// 解放
		void release(void);
		// クリエイト
		bool CreateSphere(int SubdivisionRatio, LPDIRECT3DDEVICE9 pDevice);
		// コンバート
		void convert(void);
		// マテリアル情報のセット
		void setmat(void);
		// コピー
		void copy(CSphereData* pinp);
	public:
		int m_SubdivisionRatio; // 球体の品質
		D3DMATERIAL9 m_mat; // マテリアルの情報
		LPD3DXMESH m_mesh1; // メッシュ
		LPD3DXMESH m_mesh2; // メッシュ
		LPDIRECT3DVERTEXBUFFER9 m_vertexbuffer; // バッファ
		VERTEX_4* m_pseudo;// 頂点バッファのロック
		bool m_Lock; // 親データかの判定
	private:
		CTemplates m_temp; // テンプレート
	};
public:
	CSphere();
	~CSphere();

	// 初期化
	// Input = 使用するテクスチャのパス
	// SubdivisionRatio = 品質 20あれば十分綺麗な球体になる
	bool Init(const char * ptex, int SubdivisionRatio);

	// 初期化
	// Input = 使用するテクスチャのパス ダブルポインタに対応
	// num = 要素数
	// SubdivisionRatio = 品質 20あれば十分綺麗な球体になる
	bool Init(const char ** ptex, int num, int SubdivisionRatio);

	// 初期化
	// Input = 使用するテクスチャのパス ダブルポインタに対応
	// num = 要素数
	// SubdivisionRatio = 品質 20あれば十分綺麗な球体になる
	bool Init(const char ** ptex, int num, int *SubdivisionRatio);

	// 解放
	void Uninit(void);

	// 描画
	// Input = 座標クラス(C3DObject)をアドレス渡し 
	void Draw(C3DObject * Input);
private:
	// 重複検索
	bool OverlapSearch(CSphereData* matdata, int SubdivisionRatio);
private:
	CTextureLoader m_texture; // テクスチャの格納
	std::vector<CSphereData*> m_matdata; // 管理
	CTemplates m_temp; // テンプレート
};

#endif // !_sphere_h_
