//==========================================================================
// タイマー[Timer.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _Timer_H_
#define _Timer_H_

//==========================================================================
// include
//==========================================================================
#include <Windows.h>

//==========================================================================
//
// class  : CTimer
// Content: タイマー
//
//==========================================================================
class CTimer
{
private:
	// リスト
	class List
	{
	public:
		int m_Count; // カウンタ
		int m_Limit; // 制限
		int m_Defalt; // デフォルトの値
	public:
		// セット
		void Set(int Count, int Limit);
	};
public:
	CTimer();
	~CTimer();

	// 初期化
	// Time = 秒数を入れてください
	// Comma = コンマの値を入れてください 0〜60
	void Init(int Time, int Comma);

	// カウントダウン処理
	bool Countdown(void);

	// カウント処理
	void Count(void);

	// タイマーのゲッター
	int GetTime(void) { return this->m_Time.m_Count; }

	// コンマのゲッター
	int GetComma(void) { return this->m_Comma.m_Count; }
private:
	List m_Comma; // コンマ
	List m_Time; // 秒
};

#endif // !_Timer_H_
