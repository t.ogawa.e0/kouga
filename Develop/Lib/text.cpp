//==========================================================================
// テキスト表示[text.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "text.h"

CText::CText()
{
	this->m_color = CColor<int>(255, 255, 255, 255);
	this->m_pos = CTexvec<int>(0, 0, 0, 0);
	this->m_font = nullptr;
	this->m_fontheight = 0;
	this->m_fontheight_master = 20;
	this->m_Lock = false;
}

CText::~CText()
{
	this->m_temp.Release_(this->m_font);
}

//==========================================================================
// 初期化
// w = 画面の横幅
// h = 画面の高さ
// フォントの種類はデフォルトで Terminal になってます。
void CText::init(int w, int h, const char * font)
{
	LPDIRECT3DDEVICE9 pDevice = CDirectXDevice::GetD3DDevice();

	this->m_color = CColor<int>(255, 255, 255, 255);
	this->m_pos = CTexvec<int>(this->m_pos.x, this->m_pos.y, w, h);
	this->m_fontheight = 0;
	this->m_Lock = false;

	D3DXCreateFont
	(
		pDevice,					// デバイス
		this->m_fontheight_master,	// 文字の高さ
		0,							// 文字の幅
		FW_REGULAR,					// フォントの太さ
		0,							// MIPMAPのレベル
		FALSE,						// イタリックか？
		SHIFTJIS_CHARSET,			// 文字セット
		OUT_DEFAULT_PRECIS,			// 出力精度
		DEFAULT_QUALITY,			// 出力品質
		FIXED_PITCH | FF_SCRIPT,	// フォントピッチとファミリ
		TEXT(font),					// フォント名
		&this->m_font				// Direct3Dフォントへのポインタへのアドレス
	);
}

//==========================================================================
// 解放
void CText::uninit(void)
{
	this->m_temp.Release_(this->m_font);
}

//==========================================================================
// テキスト表示
void CText::text(const char * input, ...)
{
	if (this->m_Lock)
	{
		va_list argp;
		RECT rect = { this->m_pos.x, this->m_pos.y + this->m_fontheight, this->m_pos.w, this->m_pos.h };
		char strBuf[1024] = { 0 };

		va_start(argp, input);
		vsprintf_s(strBuf, this->m_temp.Sizeof_(strBuf), input, argp);
		va_end(argp);
		this->m_font->DrawText(nullptr, strBuf, -1, &rect, DT_LEFT, this->m_color.get());
		this->m_fontheight += this->m_fontheight_master;
	}
}
