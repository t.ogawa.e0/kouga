//==========================================================================
// 2Dオブジェクト[2DObject.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "2DObject.h"

C2DObject::C2DObject()
{
	this->m_Affine = CAffine(0.0f, false);
	this->m_Anim = CAnim(-1, 0, 0, 0, false);
	this->m_Color = CColor<int>(255, 255, 255, 255);
	this->m_Cut = CTexvec<float>(0.0f, 0.0f, 0.0f, 0.0f);
	this->m_Pos = CVector<float>(0.0f, 0.0f, 0.0f, 0.0f);
	this->m_index = -1;
	this->m_Scale = 1.0f;
	this->m_Key = false;
	this->m_SetAnimationCount = false;
	this->m_CentralCoordinates = false;
}

C2DObject::~C2DObject()
{
}

//==========================================================================
// 初期化
// index = テクスチャ番号
void C2DObject::Init(int index)
{
	this->m_Affine = CAffine(0.0f, false);
	this->m_Anim = CAnim(-1, 0, 0, 0, false);
	this->m_Color = CColor<int>(255, 255, 255, 255);
	this->m_Cut = CTexvec<float>(0.0f, 0.0f, 0.0f, 0.0f);
	this->m_Pos = CVector<float>(0.0f, 0.0f, 0.0f, 0.0f);
	this->m_index = index;
	this->m_Scale = 1.0f;
	this->m_Key = false;
	this->m_SetAnimationCount = false;
	this->m_CentralCoordinates = false;
}

//==========================================================================
// アニメーション用初期化
// index = テクスチャ番号
// Frame = 更新フレーム 
// Pattern = アニメーション数
// Direction = 横一列のアニメーション数
void C2DObject::Init(int index, int Frame, int Pattern, int Direction)
{
	this->m_Affine = CAffine(0.0f, false);
	this->m_Anim = CAnim(-1, Frame, Pattern, Direction, true);
	this->m_Color = CColor<int>(255, 255, 255, 255);
	this->m_Cut = CTexvec<float>(0.0f, 0.0f, 0.0f, 0.0f);
	this->m_Pos = CVector<float>(0.0f, 0.0f, 0.0f, 0.0f);
	this->m_index = index;
	this->m_Scale = 1.0f;
	this->m_Key = false;
	this->m_SetAnimationCount = false;
	this->m_CentralCoordinates = false;
}

//==========================================================================
// アニメーションだけの情報セット
// index = テクスチャ番号
// Frame = 更新フレーム 
// Pattern = アニメーション数
// Direction = 横一列のアニメーション数
void C2DObject::SetAnim(int index, int Frame, int Pattern, int Direction)
{
	this->m_index = index;
	this->m_Anim = CAnim(-1, Frame, Pattern, Direction, true);
}

//==========================================================================
// 座標のセット
void C2DObject::SetPos(float x, float y)
{
	this->m_Pos = CVector<float>(x, y, 0.0f, 0.0f);
}

//==========================================================================
// 座標のセット
void C2DObject::SetX(float x)
{
	this->m_Pos.x = x;
}

//==========================================================================
// 座標のセット
void C2DObject::SetY(float y)
{
	this->m_Pos.y = y;
}

//==========================================================================
// 座標の加算
void C2DObject::SetXPlus(float x)
{
	this->m_Pos.x += x;
}

//==========================================================================
// 座標の加算
void C2DObject::SetYPlus(float y)
{
	this->m_Pos.y += y;
}

//==========================================================================
// 座標のセット
void C2DObject::SetPos(CVector<float> vpos)
{
	this->m_Pos = vpos;
}

//==========================================================================
// 座標の加算
void C2DObject::SetPosPlus(CVector<float> vpos)
{
	this->m_Pos += vpos;
}

//==========================================================================
// サイズ
// 入力した値が加算されます
void C2DObject::Scale(float Size)
{
	this->m_Scale += Size;
}

//==========================================================================
// 回転角度
// 入力した値が加算されます
void C2DObject::Angle(float Angle)
{
	this->m_Affine.Key = true;
	this->m_Affine.Angle += Angle;
}

//==========================================================================
// 回転角度リセット
void C2DObject::AngleReset(void)
{
	this->m_Affine.Key = false;
	this->m_Affine.Angle = 0.0f;
}

//==========================================================================
// 色セット
void C2DObject::SetColor(int r, int g, int b, int a)
{
	this->m_Color = CColor<int>(r, g, b, a);
}

//==========================================================================
// 色セット
void C2DObject::SetColor(CColor<int> color)
{
	this->m_Color = color;
}

//==========================================================================
// 無回転バーテックス
const C2DObject::VERTEX_3 * C2DObject::CreateVertex(VERTEX_3 * pPseudo, const CTexvec<int> * ptexsize)
{
	// 無回転の場合
	if (this->m_Affine.Key == false)
	{
		CUv<float> UV;

		this->SetTexturCut(ptexsize);
		this->AnimationPalam();
		this->UV(&UV, ptexsize);

		// 中心座標モード
		if (this->m_CentralCoordinates)
		{
			float fWidht = (this->m_Cut.w / 2)*this->m_Scale;
			float fHeight = (this->m_Cut.h / 2)*this->m_Scale;

			pPseudo[0].pos = D3DXVECTOR4((this->m_Pos.x - fWidht - 0.5f), (this->m_Pos.y - fHeight - 0.5f), 1.0f, 1.0f);
			pPseudo[1].pos = D3DXVECTOR4((this->m_Pos.x + fWidht - 0.5f), (this->m_Pos.y - fHeight - 0.5f), 1.0f, 1.0f);
			pPseudo[2].pos = D3DXVECTOR4((this->m_Pos.x - fWidht - 0.5f), (this->m_Pos.y + fHeight - 0.5f), 1.0f, 1.0f);
			pPseudo[3].pos = D3DXVECTOR4((this->m_Pos.x + fWidht - 0.5f), (this->m_Pos.y + fHeight - 0.5f), 1.0f, 1.0f);
		}
		else // 中心座標モード無効時
		{
			if (this->m_Anim.Key)
			{
				pPseudo[0].pos = D3DXVECTOR4((this->m_Pos.x + 0.0f - 0.5f)*this->m_Scale, (this->m_Pos.y + 0.0f - 0.5f)*this->m_Scale, 1.0f, 1.0f);
				pPseudo[1].pos = D3DXVECTOR4((this->m_Pos.x + this->m_Cut.w - 0.5f)*this->m_Scale, (this->m_Pos.y + 0.0f - 0.5f)*this->m_Scale, 1.0f, 1.0f);
				pPseudo[2].pos = D3DXVECTOR4((this->m_Pos.x + 0.0f - 0.5f)*this->m_Scale, (this->m_Pos.y + this->m_Cut.h - 0.5f)*this->m_Scale, 1.0f, 1.0f);
				pPseudo[3].pos = D3DXVECTOR4((this->m_Pos.x + this->m_Cut.w - 0.5f)*this->m_Scale, (this->m_Pos.y + this->m_Cut.h - 0.5f)*this->m_Scale, 1.0f, 1.0f);
			}
			else
			{
				pPseudo[0].pos = D3DXVECTOR4((this->m_Pos.x - 0.0f - 0.5f)*this->m_Scale, (this->m_Pos.y - 0.0f - 0.5f)*this->m_Scale, 1.0f, 1.0f);
				pPseudo[1].pos = D3DXVECTOR4((this->m_Pos.x + ptexsize->w - 0.5f)*this->m_Scale, (this->m_Pos.y - 0.0f - 0.5f)*this->m_Scale, 1.0f, 1.0f);
				pPseudo[2].pos = D3DXVECTOR4((this->m_Pos.x - 0.0f - 0.5f)*this->m_Scale, (this->m_Pos.y + ptexsize->h - 0.5f)*this->m_Scale, 1.0f, 1.0f);
				pPseudo[3].pos = D3DXVECTOR4((this->m_Pos.x + ptexsize->w - 0.5f)*this->m_Scale, (this->m_Pos.y + ptexsize->h - 0.5f)*this->m_Scale, 1.0f, 1.0f);
			}
		}

		pPseudo[0].color = pPseudo[1].color = pPseudo[2].color = pPseudo[3].color = D3DCOLOR_RGBA(this->m_Color.r, this->m_Color.g, this->m_Color.b, this->m_Color.a);

		pPseudo[0].Tex = D3DXVECTOR2(UV.u0, UV.v0);
		pPseudo[1].Tex = D3DXVECTOR2(UV.u1, UV.v0);
		pPseudo[2].Tex = D3DXVECTOR2(UV.u0, UV.v1);
		pPseudo[3].Tex = D3DXVECTOR2(UV.u1, UV.v1);
	}

	return pPseudo;
}

//==========================================================================
// 回転バーテックス
const C2DObject::VERTEX_3 * C2DObject::CreateVertexAngle(VERTEX_3 * pPseudo, const CTexvec<int> * ptexsize)
{
	// 回転時
	if (this->m_Affine.Key == true)
	{
		CUv<float> UV;

		this->SetTexturCut(ptexsize);
		this->AnimationPalam();
		this->UV(&UV, ptexsize);

		float PosX = this->m_Pos.x;
		float PosY = this->m_Pos.y;

		// 中心座標割り出し
		float APolyX = (-(this->m_Cut.w / 2)* this->m_Scale) + (this->m_Cut.w / 2);
		float APolyX_W = ((this->m_Cut.w - (this->m_Cut.w / 2))*  this->m_Scale) + (this->m_Cut.w / 2);
		float APolyY = (-(this->m_Cut.h / 2)* this->m_Scale) + (this->m_Cut.h / 2);
		float APolyY_H = ((this->m_Cut.h - (this->m_Cut.h / 2))*  this->m_Scale) + (this->m_Cut.h / 2);

		// ずらした分修正
		APolyX -= (this->m_Cut.w / 2);
		APolyX_W -= (this->m_Cut.w / 2);
		APolyY -= (this->m_Cut.h / 2);
		APolyY_H -= (this->m_Cut.h / 2);

		// 中心座標モード無効時
		if (!this->m_CentralCoordinates)
		{
			PosX += (this->m_Cut.w / 2);
			PosY += (this->m_Cut.h / 2);
			PosX *= this->m_Scale;
			PosY *= this->m_Scale;
		}

		pPseudo[0].pos = D3DXVECTOR4((PosX + APolyX*  cosf(this->m_Affine.Angle) - APolyY* sinf(this->m_Affine.Angle) - 0.5f), PosY + APolyX* sinf(this->m_Affine.Angle) + APolyY* cosf(this->m_Affine.Angle) - 0.5f, 1.0f, 1.0f);
		pPseudo[1].pos = D3DXVECTOR4((PosX + APolyX_W* cosf(this->m_Affine.Angle) - APolyY*  sinf(this->m_Affine.Angle) - 0.5f), PosY + APolyX_W* sinf(this->m_Affine.Angle) + APolyY* cosf(this->m_Affine.Angle) - 0.5f, 1.0f, 1.0f);
		pPseudo[2].pos = D3DXVECTOR4((PosX + APolyX*  cosf(this->m_Affine.Angle) - APolyY_H* sinf(this->m_Affine.Angle) - 0.5f), PosY + APolyX*	sinf(this->m_Affine.Angle) + APolyY_H* cosf(this->m_Affine.Angle) - 0.5f, 1.0f, 1.0f);
		pPseudo[3].pos = D3DXVECTOR4((PosX + APolyX_W* cosf(this->m_Affine.Angle) - APolyY_H* sinf(this->m_Affine.Angle) - 0.5f), PosY + APolyX_W* sinf(this->m_Affine.Angle) + APolyY_H* cosf(this->m_Affine.Angle) - 0.5f, 1.0f, 1.0f);

		pPseudo[0].color = pPseudo[1].color = pPseudo[2].color = pPseudo[3].color = D3DCOLOR_RGBA(this->m_Color.r, this->m_Color.g, this->m_Color.b, this->m_Color.a);

		pPseudo[0].Tex = D3DXVECTOR2(UV.u0, UV.v0);
		pPseudo[1].Tex = D3DXVECTOR2(UV.u1, UV.v0);
		pPseudo[2].Tex = D3DXVECTOR2(UV.u0, UV.v1);
		pPseudo[3].Tex = D3DXVECTOR2(UV.u1, UV.v1);
	}

	return pPseudo;
}

//==========================================================================
// アニメーション切り替わり時の判定 切り替え時true 
bool C2DObject::GetPattanNum(void)
{
	if (this->m_Anim.Count == (this->m_Anim.Frame*this->m_Anim.Pattern) - 1)
	{
		this->m_Anim.Count = -1;
		return true;
	}
	return false;
}

//==========================================================================
// アニメーションカウンタのセット
void C2DObject::SetAnimationCount(int nCount)
{
	// アニメーションが有効時
	if (this->m_Anim.Key == true)
	{
		this->m_SetAnimationCount = true;
		this->m_Anim.Count = nCount;
	}
}

//==========================================================================
// UVの生成
void C2DObject::UV(CUv<float> * UV, const CTexvec<int> * ptexsize)
{
	// 左上
	UV->u0 = (float)this->m_Cut.x / ptexsize->w;
	UV->v0 = (float)this->m_Cut.y / ptexsize->h;

	// 左下
	UV->u1 = (float)(this->m_Cut.x + this->m_Cut.w) / ptexsize->w;
	UV->v1 = (float)(this->m_Cut.y + this->m_Cut.h) / ptexsize->h;
}

//==========================================================================
// アニメーションの情報
void C2DObject::AnimationPalam(void)
{
	// アニメーションが有効時
	if (this->m_Anim.Key == true)
	{
		if (this->m_SetAnimationCount)
		{
			int PattanNum = (this->m_Anim.Count / this->m_Anim.Frame) % this->m_Anim.Pattern;	// フレームに１回	パターン数
			int patternV = PattanNum % this->m_Anim.Direction; // 横方向のパターン
			int patternH = PattanNum / this->m_Anim.Direction; // 縦方向のパターン

			this->m_Cut.x = patternV * this->m_Cut.w; // 切り取り座標X
			this->m_Cut.y = patternH * this->m_Cut.h; // 切り取り座標Y
		}
		else
		{
			this->m_Anim.Count++;
			int PattanNum = (this->m_Anim.Count / this->m_Anim.Frame) % this->m_Anim.Pattern;	// フレームに１回	パターン数
			int patternV = PattanNum % this->m_Anim.Direction; // 横方向のパターン
			int patternH = PattanNum / this->m_Anim.Direction; // 縦方向のパターン

			this->m_Cut.x = patternV * this->m_Cut.w; // 切り取り座標X
			this->m_Cut.y = patternH * this->m_Cut.h; // 切り取り座標Y
		}
	}
}

//==========================================================================
// テクスチャの切り取り
void C2DObject::SetTexturCut(const CTexvec<int> * ptexsize)
{
	if (this->m_Cut.w == 0.0f)
	{
		this->m_Cut.w = (float)ptexsize->w;
	}
	if (this->m_Cut.h == 0.0f)
	{
		this->m_Cut.h = (float)ptexsize->h;
	}

	if (this->m_Key == false && this->m_Anim.Key == true)
	{
		int nCount = 0;
		this->m_Cut.w = (float)ptexsize->w / this->m_Anim.Direction;

		// 縦の枚数を検索
		for (int i = 0;; i += this->m_Anim.Direction)
		{
			if (this->m_Anim.Pattern <= i) { break; }
			nCount++;
		}
		this->m_Cut.h = (float)ptexsize->h / nCount;
		this->m_Cut.x = this->m_Cut.y = 0.0f;
		this->m_Key = true;
	}
}
