//==========================================================================
// データクラス[DataType.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _DataType_H_
#define _DataType_H_

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>
#include <cstddef> 
#include <iostream>

//==========================================================================
// Declaration
//==========================================================================

//==========================================================================
//
// class  : CUv
// Content : CUv
// how to use : CUv<float>,CUv<int>,CUv<double>
//
//==========================================================================
template<typename types>
class CUv // UV
{
public:
	CUv() {};
	CUv(types _u0, types _v0, types _u1, types _v1)
	{
		this->u0 = _u0;
		this->v0 = _v0;
		this->u1 = _u1;
		this->v1 = _v1;
	}

	// Member Selection
	const CUv* operator->(void) const noexcept;
	// Member Selection
	CUv* operator->(void) noexcept;

	// Compare
	bool operator<(const CUv&);
	// Compare
	bool operator>(const CUv&);
	// Compare
	bool operator<=(const CUv&);
	// Compare
	bool operator>=(const CUv&);
	// Equality Compare
	bool operator == (const CUv&) const;
	// Equality Compare
	bool operator != (const CUv&) const;

	// Compare
	bool operator<(const types);
	// Compare
	bool operator>(const types);
	// Compare
	bool operator<=(const types);
	// Compare
	bool operator>=(const types);
	// Equality Compare
	bool operator == (const types) const;
	// Equality Compare
	bool operator != (const types) const;

	// Unary Negation/Plus
	CUv operator + (void) const;
	// Unary Negation/Plus
	CUv operator - (void) const;

	// Prefix Increment / Decremrnt
	CUv &operator ++(void);
	// Postfix Increment / Decrement
	CUv operator ++(int);
	// Prefix Increment / Decremrnt
	CUv &operator --(void);
	// Postfix Increment / Decrement
	CUv operator --(int);

	CUv operator +(const CUv &);
	CUv operator -(const CUv &);
	CUv operator *(const CUv &);
	CUv operator /(const CUv &);

	CUv &operator =(const CUv &);
	CUv &operator +=(const CUv &);
	CUv &operator -=(const CUv &);
	CUv &operator *=(const CUv &);
	CUv &operator /=(const CUv &);

	CUv operator + (const types) const;
	CUv operator - (const types) const;
	CUv operator * (const types) const;
	CUv operator / (const types) const;

	CUv &operator =(const types);
	CUv &operator +=(const types);
	CUv &operator -=(const types);
	CUv &operator *=(const types);
	CUv &operator /=(const types);

	// Array Subscript
	const CUv& operator[](std::size_t) const&;
	// Array Subscript
	CUv& operator[](std::size_t) &;
	// Array Subscript
	CUv operator[](std::size_t) && ;
public:
	types u0;
	types v0;
	types u1;
	types v1;
};

//==========================================================================
//
// class  : CTexvec
// Content : CTexvec
// how to use : CTexvec<float>,CTexvec<int>,CTexvec<double>
//
//==========================================================================
template<typename types>
class CTexvec
{
public:
	CTexvec() {};
	CTexvec(types _x, types _y, types _w, types _h)
	{
		this->x = _x;
		this->y = _y;
		this->w = _w;
		this->h = _h;
	}

	// Member Selection
	const CTexvec* operator->(void) const noexcept;
	// Member Selection
	CTexvec* operator->(void) noexcept;

	// Compare
	bool operator<(const CTexvec&);
	// Compare
	bool operator>(const CTexvec&);
	// Compare
	bool operator<=(const CTexvec&);
	// Compare
	bool operator>=(const CTexvec&);
	// Equality Compare
	bool operator == (const CTexvec&) const;
	// Equality Compare
	bool operator != (const CTexvec&) const;

	// Compare
	bool operator<(const types);
	// Compare
	bool operator>(const types);
	// Compare
	bool operator<=(const types);
	// Compare
	bool operator>=(const types);
	// Equality Compare
	bool operator == (const types) const;
	// Equality Compare
	bool operator != (const types) const;

	// Unary Negation/Plus
	CTexvec operator + (void) const;
	// Unary Negation/Plus
	CTexvec operator - (void) const;

	// Prefix Increment / Decremrnt
	CTexvec &operator ++(void);
	// Postfix Increment / Decrement
	CTexvec operator ++(int);
	// Prefix Increment / Decremrnt
	CTexvec &operator --(void);
	// Postfix Increment / Decrement
	CTexvec operator --(int);

	CTexvec operator +(const CTexvec &);
	CTexvec operator -(const CTexvec &);
	CTexvec operator *(const CTexvec &);
	CTexvec operator /(const CTexvec &);

	CTexvec &operator =(const CTexvec &);
	CTexvec &operator +=(const CTexvec &);
	CTexvec &operator -=(const CTexvec &);
	CTexvec &operator *=(const CTexvec &);
	CTexvec &operator /=(const CTexvec &);

	CTexvec operator + (const types) const;
	CTexvec operator - (const types) const;
	CTexvec operator * (const types) const;
	CTexvec operator / (const types) const;

	CTexvec &operator =(const types);
	CTexvec &operator +=(const types);
	CTexvec &operator -=(const types);
	CTexvec &operator *=(const types);
	CTexvec &operator /=(const types);

	// Array Subscript
	const CTexvec& operator[](std::size_t) const&;
	// Array Subscript
	CTexvec& operator[](std::size_t) &;
	// Array Subscript
	CTexvec operator[](std::size_t) && ;
public:
	types x;
	types y;
	types w;
	types h;
};

//==========================================================================
//
// class  : CColor
// Content : CColor
// how to use : CColor<float>,CColor<int>,CColor<double>
//
//==========================================================================
template<typename types>
class CColor
{
public:
	CColor() {};
	CColor(types _r, types _g, types _b, types _a)
	{
		this->r = _r;
		this->g = _g;
		this->b = _b;
		this->a = _a;
	}

	D3DCOLOR get(void) { return D3DCOLOR_RGBA((int)this->r, (int)this->g, (int)this->b, (int)this->a); }

	// Member Selection
	const CColor* operator->(void) const noexcept;
	// Member Selection
	CColor* operator->(void) noexcept;

	// Compare
	bool operator<(const CColor&);
	// Compare
	bool operator>(const CColor&);
	// Compare
	bool operator<=(const CColor&);
	// Compare
	bool operator>=(const CColor&);
	// Equality Compare
	bool operator == (const CColor&) const;
	// Equality Compare
	bool operator != (const CColor&) const;

	// Compare
	bool operator<(const types);
	// Compare
	bool operator>(const types);
	// Compare
	bool operator<=(const types);
	// Compare
	bool operator>=(const types);
	// Equality Compare
	bool operator == (const types) const;
	// Equality Compare
	bool operator != (const types) const;

	// Unary Negation/Plus
	CColor operator + (void) const;
	// Unary Negation/Plus
	CColor operator - (void) const;

	// Prefix Increment / Decremrnt
	CColor &operator ++(void);
	// Postfix Increment / Decrement
	CColor operator ++(int);
	// Prefix Increment / Decremrnt
	CColor &operator --(void);
	// Postfix Increment / Decrement
	CColor operator --(int);

	CColor operator +(const CColor &);
	CColor operator -(const CColor &);
	CColor operator *(const CColor &);
	CColor operator /(const CColor &);

	CColor &operator =(const CColor &);
	CColor &operator +=(const CColor &);
	CColor &operator -=(const CColor &);
	CColor &operator *=(const CColor &);
	CColor &operator /=(const CColor &);

	CColor operator + (const types) const;
	CColor operator - (const types) const;
	CColor operator * (const types) const;
	CColor operator / (const types) const;

	CColor &operator =(const types);
	CColor &operator +=(const types);
	CColor &operator -=(const types);
	CColor &operator *=(const types);
	CColor &operator /=(const types);

	// Array Subscript
	const CColor& operator[](std::size_t) const&;
	// Array Subscript
	CColor& operator[](std::size_t) &;
	// Array Subscript
	CColor operator[](std::size_t) && ;
public:
	types r;
	types g;
	types b;
	types a;
};

//==========================================================================
//
// class  : CVector
// Content : CVector
// how to use : CVector<float>,CVector<int>,CVector<double>
//
//==========================================================================
template<typename types>
class CVector
{
public:
	CVector() {};
	CVector(types _x, types _y, types _z, types _w)
	{
		this->x = _x;
		this->y = _y;
		this->z = _z;
		this->w = _w;
	}

	// Member Selection
	const CVector* operator->(void) const noexcept;
	// Member Selection
	CVector* operator->(void) noexcept;

	// Compare
	bool operator<(const CVector&);
	// Compare
	bool operator>(const CVector&);
	// Compare
	bool operator<=(const CVector&);
	// Compare
	bool operator>=(const CVector&);
	// Equality Compare
	bool operator == (const CVector&) const;
	// Equality Compare
	bool operator != (const CVector&) const;

	// Compare
	bool operator<(const types);
	// Compare
	bool operator>(const types);
	// Compare
	bool operator<=(const types);
	// Compare
	bool operator>=(const types);
	// Equality Compare
	bool operator == (const types) const;
	// Equality Compare
	bool operator != (const types) const;

	// Unary Negation/Plus
	CVector operator + (void) const;
	// Unary Negation/Plus
	CVector operator - (void) const;

	// Prefix Increment / Decremrnt
	CVector &operator ++(void);
	// Postfix Increment / Decrement
	CVector operator ++(int);
	// Prefix Increment / Decremrnt
	CVector &operator --(void);
	// Postfix Increment / Decrement
	CVector operator --(int);

	CVector operator +(const CVector &);
	CVector operator -(const CVector &);
	CVector operator *(const CVector &);
	CVector operator /(const CVector &);

	CVector &operator =(const CVector &);
	CVector &operator +=(const CVector &);
	CVector &operator -=(const CVector &);
	CVector &operator *=(const CVector &);
	CVector &operator /=(const CVector &);

	CVector operator + (const types) const;
	CVector operator - (const types) const;
	CVector operator * (const types) const;
	CVector operator / (const types) const;

	CVector &operator =(const types);
	CVector &operator +=(const types);
	CVector &operator -=(const types);
	CVector &operator *=(const types);
	CVector &operator /=(const types);

	// Array Subscript
	const CVector& operator[](std::size_t n) const&;
	// Array Subscript
	CVector& operator[](std::size_t) &;
	// Array Subscript
	CVector operator[](std::size_t) && ;
public:
	types x;
	types y;
	types z;
	types w;
};

//==========================================================================
// Definition
//==========================================================================

//==========================================================================
//
// class  : CUv
// Content : CUv
// how to use : CUv<float>,CUv<int>,CUv<double>
//
//==========================================================================

// Member Selection
template<typename types>
inline const CUv<types> * CUv<types>::operator->(void) const noexcept
{
	return this;
}

// Member Selection
template<typename types>
inline CUv<types> * CUv<types>::operator->(void) noexcept
{
	return this;
}

// Compare
template<typename types>
inline bool CUv<types>::operator<(const CUv & v)
{
	if (this->u0 < v.u0 && this->v0 < v.v0 && this->u1 < v.u1 && this->v1 < v.v1)
	{
		return true;
	}
	return false;
}

// Compare
template<typename types>
inline bool CUv<types>::operator>(const CUv & v)
{
	if (this->u0 > v.u0 && this->v0 > v.v0 && this->u1 > v.u1 && this->v1 > v.v1)
	{
		return true;
	}
	return false;
}

// Compare
template<typename types>
inline bool CUv<types>::operator<=(const CUv & v)
{
	if ((this->u0 <= v.u0) || (this->v0 <= v.v0) || (this->u1 <= v.u1) || (this->v1 <= v.v1))
	{
		return true;
	}
	return false;
}

// Compare
template<typename types>
inline bool CUv<types>::operator>=(const CUv & v)
{
	if ((this->u0 >= v.u0) || (this->v0 >= v.v0) || (this->u1 >= v.u1) || (this->v1 >= v.v1))
	{
		return true;
	}
	return false;
}

// Equality Compare
template<typename types>
inline bool CUv<types>::operator==(const CUv & v) const
{
	if ((this->u0 == v.u0) || (this->v0 == v.v0) || (this->u1 == v.u1) || (this->v1 == v.v1))
	{
		return true;
	}
	return false;
}

// Equality Compare
template<typename types>
inline bool CUv<types>::operator!=(const CUv & v) const
{
	if ((this->u0 != v.u0) || (this->v0 != v.v0) || (this->u1 != v.u1) || (this->v1 != v.v1))
	{
		return true;
	}
	return false;
}

// Compare
template<typename types>
inline bool CUv<types>::operator<(const types t)
{
	if (this->u0 < t && this->v0 < t && this->u1 < t && this->v1 < t)
	{
		return true;
	}
	return false;
}

// Compare
template<typename types>
inline bool CUv<types>::operator>(const types t)
{
	if (this->u0 > t && this->v0 > t && this->u1 > t && this->v1 > t)
	{
		return true;
	}
	return false;
}

// Compare
template<typename types>
inline bool CUv<types>::operator<=(const types t)
{
	if ((this->u0 <= t) || (this->v0 <= t) || (this->u1 <= t) || (this->v1 <= t))
	{
		return true;
	}
	return false;
}

// Compare
template<typename types>
inline bool CUv<types>::operator>=(const types t)
{
	if ((this->u0 >= t) || (this->v0 >= t) || (this->u1 >= t) || (this->v1 >= t))
	{
		return true;
	}
	return false;
}

// Equality Compare
template<typename types>
inline bool CUv<types>::operator==(const types t) const
{
	if ((this->u0 == t) || (this->v0 == t) || (this->u1 == t) || (this->v1 == t))
	{
		return true;
	}
	return false;
}

// Equality Compare
template<typename types>
inline bool CUv<types>::operator!=(const types t) const
{
	if ((this->u0 != t) || (this->v0 != t) || (this->u1 != t) || (this->v1 != t))
	{
		return true;
	}
	return false;
}

// Unary Negation/Plus
template<typename types>
inline CUv<types> CUv<types>::operator+(void) const
{
	return *this;
}

// Unary Negation/Plus
template<typename types>
inline CUv<types> CUv<types>::operator-(void) const
{
	return *this;
}

// Postfix Increment / Decrement
template<typename types>
inline CUv<types> & CUv<types>::operator++(void)
{
	this->u0++;
	this->v0++;
	this->u1++;
	this->v1++;
	return *this;
}

// Postfix Increment / Decrement
template<typename types>
inline CUv<types> CUv<types>::operator++(int)
{
	CUv<types> out = *this;
	this->u0++;
	this->v0++;
	this->u1++;
	this->v1++;
	return out;
}

// Postfix Increment / Decrement
template<typename types>
inline CUv<types> & CUv<types>::operator--(void)
{
	this->u0--;
	this->v0--;
	this->u1--;
	this->v1--;
	return *this;
}

// Postfix Increment / Decrement
template<typename types>
inline CUv<types> CUv<types>::operator--(int)
{
	CUv<types> out = *this;
	this->u0--;
	this->v0--;
	this->u1--;
	this->v1--;
	return out;
}

template<typename types>
inline CUv<types> CUv<types>::operator+(const CUv & v)
{
	CUv<types> out;
	out.u0 = this->u0 + v.u0;
	out.v0 = this->v0 + v.v0;
	out.u1 = this->u1 + v.u1;
	out.v1 = this->v1 + v.v1;
	return out;
}

template<typename types>
inline CUv<types> CUv<types>::operator-(const CUv & v)
{
	CUv<types> out;
	out.u0 = this->u0 - v.u0;
	out.v0 = this->v0 - v.v0;
	out.u1 = this->u1 - v.u1;
	out.v1 = this->v1 - v.v1;
	return out;
}

template<typename types>
inline CUv<types> CUv<types>::operator*(const CUv & v)
{
	CUv<types> out;
	out.u0 = this->u0 * v.u0;
	out.v0 = this->v0 * v.v0;
	out.u1 = this->u1 * v.u1;
	out.v1 = this->v1 * v.v1;
	return out;
}

template<typename types>
inline CUv<types> CUv<types>::operator/(const CUv & v)
{
	CUv<types> out;
	out.u0 = this->u0 / v.u0;
	out.v0 = this->v0 / v.v0;
	out.u1 = this->u1 / v.u1;
	out.v1 = this->v1 / v.v1;
	return out;
}

template<typename types>
inline CUv<types> & CUv<types>::operator=(const CUv & v)
{
	this->u0 = v.u0;
	this->v0 = v.v0;
	this->u1 = v.u1;
	this->v1 = v.v1;
	return *this;
}

template<typename types>
inline CUv<types> & CUv<types>::operator+=(const CUv & v)
{
	this->u0 += v.u0;
	this->v0 += v.v0;
	this->u1 += v.u1;
	this->v1 += v.v1;
	return *this;
}

template<typename types>
inline CUv<types> & CUv<types>::operator-=(const CUv & v)
{
	this->u0 -= v.u0;
	this->v0 -= v.v0;
	this->u1 -= v.u1;
	this->v1 -= v.v1;
	return *this;
}

template<typename types>
inline CUv<types> & CUv<types>::operator*=(const CUv & v)
{
	this->u0 *= v.u0;
	this->v0 *= v.v0;
	this->u1 *= v.u1;
	this->v1 *= v.v1;
	return *this;
}

template<typename types>
inline CUv<types> & CUv<types>::operator/=(const CUv & v)
{
	this->u0 /= v.u0;
	this->v0 /= v.v0;
	this->u1 /= v.u1;
	this->v1 /= v.v1;
	return *this;
}

template<typename types>
inline CUv<types> CUv<types>::operator+(const types t) const
{
	return CUv(this->u0 + t, this->v0 + t, this->u1 + t, this->v1 + t);
}

template<typename types>
inline CUv<types> CUv<types>::operator-(const types t) const
{
	return CUv(this->u0 - t, this->v0 - t, this->u1 - t, this->v1 - t);
}

template<typename types>
inline CUv<types> CUv<types>::operator*(const types t) const
{
	return CUv(this->u0 * t, this->v0 * t, this->u1 * t, this->v1 * t);
}

template<typename types>
inline CUv<types> CUv<types>::operator/(const types t) const
{
	types tInv = (types)1;
	tInv = tInv / t;
	return CUv(this->u0 * tInv, this->v0 * tInv, this->u1 * tInv, this->v1 * tInv);
}

template<typename types>
inline CUv<types> & CUv<types>::operator=(const types t)
{
	this->u0 = t;
	this->v0 = t;
	this->u1 = t;
	this->v1 = t;
	return *this;
}

template<typename types>
inline CUv<types> & CUv<types>::operator+=(const types t)
{
	this->u0 += t;
	this->v0 += t;
	this->u1 += t;
	this->v1 += t;
	return *this;
}

template<typename types>
inline CUv<types> & CUv<types>::operator-=(const types t)
{
	this->u0 -= t;
	this->v0 -= t;
	this->u1 -= t;
	this->v1 -= t;
	return *this;
}

template<typename types>
inline CUv<types> & CUv<types>::operator*=(const types t)
{
	this->u0 *= t;
	this->v0 *= t;
	this->u1 *= t;
	this->v1 *= t;
	return *this;
}

template<typename types>
inline CUv<types> & CUv<types>::operator/=(const types t)
{
	this->u0 /= t;
	this->v0 /= t;
	this->u1 /= t;
	this->v1 /= t;
	return *this;
}

// Array Subscript
template<typename types>
inline const CUv<types> & CUv<types>::operator[](std::size_t n) const &
{
	return this[n];
}

// Array Subscript
template<typename types>
inline CUv<types> & CUv<types>::operator[](std::size_t n) &
{
	return this[n];
}

// Array Subscript
template<typename types>
inline CUv<types> CUv<types>::operator[](std::size_t n) &&
{
	return std::move(this[n]);
}

//==========================================================================
//
// class  : CTexvec
// Content : CTexvec
// how to use : CTexvec<float>,CTexvec<int>,CTexvec<double>
//
//==========================================================================

// Member Selection
template<typename types>
inline const CTexvec<types> * CTexvec<types>::operator->(void) const noexcept
{
	return this;
}

// Member Selection
template<typename types>
inline CTexvec<types> * CTexvec<types>::operator->(void) noexcept
{
	return this;
}

// Compare
template<typename types>
inline bool CTexvec<types>::operator<(const CTexvec & v)
{
	if (this->x < v.x && this->y < v.y && this->w < v.w && this->h < v.h)
	{
		return true;
	}
	return false;
}

// Compare
template<typename types>
inline bool CTexvec<types>::operator>(const CTexvec & v)
{
	if (this->x > v.x && this->y > v.y && this->w > v.w && this->h > v.h)
	{
		return true;
	}
	return false;
}

// Compare
template<typename types>
inline bool CTexvec<types>::operator<=(const CTexvec & v)
{
	if (this->x <= v.x && this->y <= v.y && this->w <= v.w && this->h <= v.h)
	{
		return true;
	}
	return false;
}

// Compare
template<typename types>
inline bool CTexvec<types>::operator>=(const CTexvec & v)
{
	if (this->x >= v.x && this->y >= v.y && this->w >= v.w && this->h >= v.h)
	{
		return true;
	}
	return false;
}

// Equality Compare
template<typename types>
inline bool CTexvec<types>::operator==(const CTexvec & v) const
{
	if ((this->x == v.x) || (this->y == v.y) || (this->w == v.w) || (this->h == v.h))
	{
		return true;
	}
	return false;
}

// Equality Compare
template<typename types>
inline bool CTexvec<types>::operator!=(const CTexvec & v) const
{
	if ((this->x != v.x) || (this->y != v.y) || (this->w != v.w) || (this->h != v.h))
	{
		return true;
	}
	return false;
}

// Compare
template<typename types>
inline bool CTexvec<types>::operator<(const types t)
{
	if (this->x < t && this->y < t && this->w < t && this->h < t)
	{
		return true;
	}
	return false;
}

// Compare
template<typename types>
inline bool CTexvec<types>::operator>(const types t)
{
	if (this->x > t && this->y > t && this->w > t && this->h > t)
	{
		return true;
	}
	return false;
}

// Compare
template<typename types>
inline bool CTexvec<types>::operator<=(const types t)
{
	if (this->x <= t && this->y <= t && this->w <= t && this->h <= t)
	{
		return true;
	}
	return false;
}

// Compare
template<typename types>
inline bool CTexvec<types>::operator>=(const types t)
{
	if (this->x >= t && this->y >= t && this->w >= t && this->h >= t)
	{
		return true;
	}
	return false;
}

// Equality Compare
template<typename types>
inline bool CTexvec<types>::operator==(const types t) const
{
	if ((this->x == t) || (this->y == t) || (this->w == t) || (this->h == t))
	{
		return true;
	}
	return false;
}

// Equality Compare
template<typename types>
inline bool CTexvec<types>::operator!=(const types t) const
{
	if ((this->x != t) || (this->y != t) || (this->w != t) || (this->h != t))
	{
		return true;
	}
	return false;
}

// Unary Negation/Plus
template<typename types>
inline CTexvec<types> CTexvec<types>::operator+(void) const
{
	return *this;
}

// Unary Negation/Plus
template<typename types>
inline CTexvec<types> CTexvec<types>::operator-(void) const
{
	return *this;
}

// Postfix Increment / Decrement
template<typename types>
inline CTexvec<types> & CTexvec<types>::operator++(void)
{
	this->x++;
	this->y++;
	this->w++;
	this->h++;
	return *this;
}

// Postfix Increment / Decrement
template<typename types>
inline CTexvec<types> CTexvec<types>::operator++(int)
{
	CTexvec<types> out = *this;
	this->x++;
	this->y++;
	this->w++;
	this->h++;
	return out;
}

// Postfix Increment / Decrement
template<typename types>
inline CTexvec<types> & CTexvec<types>::operator--(void)
{
	this->x--;
	this->y--;
	this->w--;
	this->h--;
	return *this;
}

// Postfix Increment / Decrement
template<typename types>
inline CTexvec<types> CTexvec<types>::operator--(int)
{
	CTexvec<types> out = *this;
	this->x--;
	this->y--;
	this->w--;
	this->h--;
	return out;
}

template<typename types>
inline CTexvec<types> CTexvec<types>::operator+(const CTexvec & v)
{
	CTexvec<types> out;
	out.x = this->x + v.x;
	out.y = this->y + v.y;
	out.w = this->w + v.w;
	out.h = this->h + v.h;
	return out;
}

template<typename types>
inline CTexvec<types> CTexvec<types>::operator-(const CTexvec & v)
{
	CTexvec<types> out;
	out.x = this->x - v.x;
	out.y = this->y - v.y;
	out.w = this->w - v.w;
	out.h = this->h - v.h;
	return out;
}

template<typename types>
inline CTexvec<types> CTexvec<types>::operator*(const CTexvec & v)
{
	CTexvec<types> out;
	out.x = this->x * v.x;
	out.y = this->y * v.y;
	out.w = this->w * v.w;
	out.h = this->h * v.h;
	return out;
}

template<typename types>
inline CTexvec<types> CTexvec<types>::operator/(const CTexvec & v)
{
	CTexvec<types> out;
	out.x = this->x / v.x;
	out.y = this->y / v.y;
	out.w = this->w / v.w;
	out.h = this->h / v.h;
	return out;
}

template<typename types>
inline CTexvec<types> & CTexvec<types>::operator=(const CTexvec & v)
{
	this->x = v.x;
	this->y = v.y;
	this->w = v.w;
	this->h = v.h;
	return *this;
}

template<typename types>
inline CTexvec<types> & CTexvec<types>::operator+=(const CTexvec & v)
{
	this->x += v.x;
	this->y += v.y;
	this->w += v.w;
	this->h += v.h;
	return *this;
}

template<typename types>
inline CTexvec<types> & CTexvec<types>::operator-=(const CTexvec & v)
{
	this->x -= v.x;
	this->y -= v.y;
	this->w -= v.w;
	this->h -= v.h;
	return *this;
}

template<typename types>
inline CTexvec<types> & CTexvec<types>::operator*=(const CTexvec & v)
{
	this->x *= v.x;
	this->y *= v.y;
	this->w *= v.w;
	this->h *= v.h;
	return *this;
}

template<typename types>
inline CTexvec<types> & CTexvec<types>::operator/=(const CTexvec & v)
{
	this->x /= v.x;
	this->y /= v.y;
	this->w /= v.w;
	this->h /= v.h;
	return *this;
}

template<typename types>
inline CTexvec<types> CTexvec<types>::operator+(const types t) const
{
	return CTexvec(this->x + t, this->y + t, this->w + t, this->h + t);
}

template<typename types>
inline CTexvec<types> CTexvec<types>::operator-(const types t) const
{
	return CTexvec(this->x - t, this->y - t, this->w - t, this->h - t);
}

template<typename types>
inline CTexvec<types> CTexvec<types>::operator*(const types t) const
{
	return CTexvec(this->x * t, this->y * t, this->w * t, this->h * t);
}

template<typename types>
inline CTexvec<types> CTexvec<types>::operator/(const types t) const
{
	types tInv = (types)1;
	tInv = tInv / t;
	return CTexvec(this->x * tInv, this->y * tInv, this->w * tInv, this->h * tInv);
}

template<typename types>
inline CTexvec<types> & CTexvec<types>::operator=(const types t)
{
	this->x = t;
	this->y = t;
	this->w = t;
	this->h = t;
	return *this;
}

template<typename types>
inline CTexvec<types> & CTexvec<types>::operator+=(const types t)
{
	this->x += t;
	this->y += t;
	this->w += t;
	this->h += t;
	return *this;
}

template<typename types>
inline CTexvec<types> & CTexvec<types>::operator-=(const types t)
{
	this->x -= t;
	this->y -= t;
	this->w -= t;
	this->h -= t;
	return *this;
}

template<typename types>
inline CTexvec<types> & CTexvec<types>::operator*=(const types t)
{
	this->x *= t;
	this->y *= t;
	this->w *= t;
	this->h *= t;
	return *this;
}

template<typename types>
inline CTexvec<types> & CTexvec<types>::operator/=(const types t)
{
	this->x /= t;
	this->y /= t;
	this->w /= t;
	this->h /= t;
	return *this;
}

// Array Subscript
template<typename types>
inline const CTexvec<types> & CTexvec<types>::operator[](std::size_t n) const &
{
	return this[n];
}

// Array Subscript
template<typename types>
inline CTexvec<types> & CTexvec<types>::operator[](std::size_t n) &
{
	return this[n];
}

// Array Subscript
template<typename types>
inline CTexvec<types> CTexvec<types>::operator[](std::size_t n) &&
{
	return std::move(this[n]);
}

//==========================================================================
//
// class  : CColor
// Content : CColor
// how to use : CColor<float>,CColor<int>,CColor<double>
//
//==========================================================================

// Member Selection
template<typename types>
inline const CColor<types> * CColor<types>::operator->(void) const noexcept
{
	return this;
}

// Member Selection
template<typename types>
inline CColor<types> * CColor<types>::operator->(void) noexcept
{
	return this;
}

// Compare
template<typename types>
inline bool CColor<types>::operator<(const CColor & v)
{
	if (this->r < v.r && this->g < v.g && this->b < v.b && this->a < v.a)
	{
		return true;
	}
	return false;
}

// Compare
template<typename types>
inline bool CColor<types>::operator>(const CColor & v)
{
	if (this->r > v.r && this->g > v.g && this->b > v.b && this->a > v.a)
	{
		return true;
	}
	return false;
}

// Compare
template<typename types>
inline bool CColor<types>::operator<=(const CColor & v)
{
	if (this->r <= v.r && this->g <= v.g && this->b <= v.b && this->a <= v.a)
	{
		return true;
	}
	return false;
}

// Compare
template<typename types>
inline bool CColor<types>::operator>=(const CColor & v)
{
	if (this->r >= v.r && this->g >= v.g && this->b >= v.b && this->a >= v.a)
	{
		return true;
	}
	return false;
}

// Equality Compare
template<typename types>
inline bool CColor<types>::operator==(const CColor & v) const
{
	if ((this->r == v.r) || (this->g == v.g) || (this->b == v.b) || (this->a == v.a))
	{
		return true;
	}
	return false;
}

// Equality Compare
template<typename types>
inline bool CColor<types>::operator!=(const CColor & v) const
{
	if ((this->r != v.r) || (this->g != v.g) || (this->b != v.b) || (this->a != v.a))
	{
		return true;
	}
	return false;
}

// Compare
template<typename types>
inline bool CColor<types>::operator<(const types t)
{
	if (this->r < t && this->g < t && this->b < t && this->a < t)
	{
		return true;
	}
	return false;
}

// Compare
template<typename types>
inline bool CColor<types>::operator>(const types t)
{
	if (this->r > t && this->g > t && this->b > t && this->a > t)
	{
		return true;
	}
	return false;
}

// Compare
template<typename types>
inline bool CColor<types>::operator<=(const types t)
{
	if (this->r <= t && this->g <= t && this->b <= t && this->a <= t)
	{
		return true;
	}
	return false;
}

// Compare
template<typename types>
inline bool CColor<types>::operator>=(const types t)
{
	if (this->r >= t && this->g >= t && this->b >= t && this->a >= t)
	{
		return true;
	}
	return false;
}

// Equality Compare
template<typename types>
inline bool CColor<types>::operator==(const types t) const
{
	if ((this->r == t) || (this->g == t) || (this->b == t) || (this->a == t))
	{
		return true;
	}
	return false;
}

// Equality Compare
template<typename types>
inline bool CColor<types>::operator!=(const types t) const
{
	if ((this->r != t) || (this->g != t) || (this->b != t) || (this->a != t))
	{
		return true;
	}
	return false;
}

// Unary Negation/Plus
template<typename types>
inline CColor<types> CColor<types>::operator+(void) const
{
	return *this;
}

// Unary Negation/Plus
template<typename types>
inline CColor<types> CColor<types>::operator-(void) const
{
	return *this;
}

// Postfix Increment / Decrement
template<typename types>
inline CColor<types> & CColor<types>::operator++(void)
{
	this->r++;
	this->g++;
	this->b++;
	this->a++;
	return *this;
}

// Postfix Increment / Decrement
template<typename types>
inline CColor<types> CColor<types>::operator++(int)
{
	CColor<types> out = *this;
	this->r++;
	this->g++;
	this->b++;
	this->a++;
	return out;
}

// Postfix Increment / Decrement
template<typename types>
inline CColor<types> & CColor<types>::operator--(void)
{
	this->r--;
	this->g--;
	this->b--;
	this->a--;
	return *this;
}

// Postfix Increment / Decrement
template<typename types>
inline CColor<types> CColor<types>::operator--(int)
{
	CColor<types> out = *this;
	this->r--;
	this->g--;
	this->b--;
	this->a--;
	return out;
}

template<typename types>
inline CColor<types> CColor<types>::operator+(const CColor & v)
{
	CColor<types> out;
	out.r = this->r + v.r;
	out.g = this->g + v.g;
	out.b = this->b + v.b;
	out.a = this->a + v.a;
	return out;
}

template<typename types>
inline CColor<types> CColor<types>::operator-(const CColor & v)
{
	CColor<types> out;
	out.r = this->r - v.r;
	out.g = this->g - v.g;
	out.b = this->b - v.b;
	out.a = this->a - v.a;
	return out;
}

template<typename types>
inline CColor<types> CColor<types>::operator*(const CColor & v)
{
	CColor<types> out;
	out.r = this->r * v.r;
	out.g = this->g * v.g;
	out.b = this->b * v.b;
	out.a = this->a * v.a;
	return out;
}

template<typename types>
inline CColor<types> CColor<types>::operator/(const CColor & v)
{
	CColor<types> out;
	out.r = this->r / v.r;
	out.g = this->g / v.g;
	out.b = this->b / v.b;
	out.a = this->a / v.a;
	return out;
}

template<typename types>
inline CColor<types> & CColor<types>::operator=(const CColor & v)
{
	this->r = v.r;
	this->g = v.g;
	this->b = v.b;
	this->a = v.a;
	return *this;
}

template<typename types>
inline CColor<types> & CColor<types>::operator+=(const CColor & v)
{
	this->r += v.r;
	this->g += v.g;
	this->b += v.b;
	this->a += v.a;
	return *this;
}

template<typename types>
inline CColor<types> & CColor<types>::operator-=(const CColor & v)
{
	this->r -= v.r;
	this->g -= v.g;
	this->b -= v.b;
	this->a -= v.a;
	return *this;
}

template<typename types>
inline CColor<types> & CColor<types>::operator*=(const CColor & v)
{
	this->r *= v.r;
	this->g *= v.g;
	this->b *= v.b;
	this->a *= v.a;
	return *this;
}

template<typename types>
inline CColor<types> & CColor<types>::operator/=(const CColor & v)
{
	this->r /= v.r;
	this->g /= v.g;
	this->b /= v.b;
	this->a /= v.a;
	return *this;
}

template<typename types>
inline CColor<types> CColor<types>::operator+(const types t) const
{
	return CColor(this->r + t, this->g + t, this->b + t, this->a + t);
}

template<typename types>
inline CColor<types> CColor<types>::operator-(const types t) const
{
	return CColor(this->r - t, this->g - t, this->b - t, this->a - t);
}

template<typename types>
inline CColor<types> CColor<types>::operator*(const types t) const
{
	return CColor(this->r * t, this->g * t, this->b * t, this->a * t);
}

template<typename types>
inline CColor<types> CColor<types>::operator/(const types t) const
{
	types tInv = (types)1;
	tInv = tInv / t;
	return CColor(this->r * tInv, this->g * tInv, this->b * tInv, this->a * tInv);
}

template<typename types>
inline CColor<types> & CColor<types>::operator=(const types t)
{
	this->r = t;
	this->g = t;
	this->b = t;
	this->a = t;
	return *this;
}

template<typename types>
inline CColor<types> & CColor<types>::operator+=(const types t)
{
	this->r += t;
	this->g += t;
	this->b += t;
	this->a += t;
	return *this;
}

template<typename types>
inline CColor<types> & CColor<types>::operator-=(const types t)
{
	this->r -= t;
	this->g -= t;
	this->b -= t;
	this->a -= t;
	return *this;
}

template<typename types>
inline CColor<types> & CColor<types>::operator*=(const types t)
{
	this->r *= t;
	this->g *= t;
	this->b *= t;
	this->a *= t;
	return *this;
}

template<typename types>
inline CColor<types> & CColor<types>::operator/=(const types t)
{
	this->r /= t;
	this->g /= t;
	this->b /= t;
	this->a /= t;
	return *this;
}

// Array Subscript
template<typename types>
inline const CColor<types> & CColor<types>::operator[](std::size_t n) const &
{
	return this[n];
}

// Array Subscript
template<typename types>
inline CColor<types> & CColor<types>::operator[](std::size_t n) &
{
	return this[n];
}

// Array Subscript
template<typename types>
inline CColor<types> CColor<types>::operator[](std::size_t n) &&
{
	return std::move(this[n]);
}

//==========================================================================
//
// class  : CVector
// Content : CVector
// how to use : CVector<float>,CVector<int>,CVector<double>
//
//==========================================================================

// Member Selection
template<typename types>
inline const CVector<types> * CVector<types>::operator->(void) const noexcept
{
	return this;
}

// Member Selection
template<typename types>
inline CVector<types> * CVector<types>::operator->(void) noexcept
{
	return this;
}

// Compare
template<typename types>
inline bool CVector<types>::operator<(const CVector & v)
{
	if (this->x < v.x && this->y < v.y && this->z < v.z && this->w < v.w)
	{
		return true;
	}
	return false;
}

// Compare
template<typename types>
inline bool CVector<types>::operator>(const CVector & v)
{
	if (this->x > v.x && this->y > v.y && this->z > v.z && this->w > v.w)
	{
		return true;
	}
	return false;
}

// Compare
template<typename types>
inline bool CVector<types>::operator<=(const CVector & v)
{
	if (this->x <= v.x && this->y <= v.y && this->z <= v.z && this->w <= v.w)
	{
		return true;
	}
	return false;
}

// Compare
template<typename types>
inline bool CVector<types>::operator>=(const CVector & v)
{
	if (this->x >= v.x && this->y >= v.y && this->z >= v.z && this->w >= v.w)
	{
		return true;
	}
	return false;
}

// Equality Compare
template<typename types>
inline bool CVector<types>::operator==(const CVector & v) const
{
	if ((this->x == v.x) || (this->y == v.y) || (this->z == v.z) || (this->w == v.w))
	{
		return true;
	}
	return false;
}

// Equality Compare
template<typename types>
inline bool CVector<types>::operator!=(const CVector & v) const
{
	if ((this->x != v.x) || (this->y != v.y) || (this->z != v.z) || (this->w != v.w))
	{
		return true;
	}
	return false;
}

// Compare
template<typename types>
inline bool CVector<types>::operator<(const types t)
{
	if (this->x < t && this->y < t && this->z < t && this->w < t)
	{
		return true;
	}
	return false;
}

// Compare
template<typename types>
inline bool CVector<types>::operator>(const types t)
{
	if (this->x > t && this->y > t && this->z > t && this->w > t)
	{
		return true;
	}
	return false;
}

// Compare
template<typename types>
inline bool CVector<types>::operator<=(const types t)
{
	if (this->x <= t && this->y <= t && this->z <= t && this->w <= t)
	{
		return true;
	}
	return false;
}

// Compare
template<typename types>
inline bool CVector<types>::operator>=(const types t)
{
	if (this->x >= t && this->y >= t && this->z >= t && this->w >= t)
	{
		return true;
	}
	return false;
}

// Equality Compare
template<typename types>
inline bool CVector<types>::operator==(const types t) const
{
	if ((this->x == t) || (this->y == t) || (this->z == t) || (this->w == t))
	{
		return true;
	}
	return false;
}

// Equality Compare
template<typename types>
inline bool CVector<types>::operator!=(const types t) const
{
	if ((this->x != t) || (this->y != t) || (this->z != t) || (this->w != t))
	{
		return true;
	}
	return false;
}

// Unary Negation/Plus
template<typename types>
inline CVector<types> CVector<types>::operator+(void) const
{
	return *this;
}

// Unary Negation/Plus
template<typename types>
inline CVector<types> CVector<types>::operator-(void) const
{
	return *this;
}

// Postfix Increment / Decrement
template<typename types>
inline CVector<types> & CVector<types>::operator++(void)
{
	this->x++;
	this->y++;
	this->z++;
	this->w++;
	return *this;
}

// Postfix Increment / Decrement
template<typename types>
inline CVector<types> CVector<types>::operator++(int)
{
	CVector<types> out = *this;
	this->x++;
	this->y++;
	this->z++;
	this->w++;
	return out;
}

// Postfix Increment / Decrement
template<typename types>
inline CVector<types> & CVector<types>::operator--(void)
{
	this->x--;
	this->y--;
	this->z--;
	this->w--;
	return *this;
}

// Postfix Increment / Decrement
template<typename types>
inline CVector<types> CVector<types>::operator--(int)
{
	CVector<types> out = *this;
	this->x--;
	this->y--;
	this->z--;
	this->w--;
	return out;
}

template<typename types>
inline CVector<types> CVector<types>::operator+(const CVector & v)
{
	CVector<types> out;
	out.x = this->x + v.x;
	out.y = this->y + v.y;
	out.z = this->z + v.z;
	out.w = this->w + v.w;
	return out;
}

template<typename types>
inline CVector<types> CVector<types>::operator-(const CVector & v)
{
	CVector<types> out;
	out.x = this->x - v.x;
	out.y = this->y - v.y;
	out.z = this->z - v.z;
	out.w = this->w - v.w;
	return out;
}

template<typename types>
inline CVector<types> CVector<types>::operator*(const CVector & v)
{
	CVector<types> out;
	out.x = this->x * v.x;
	out.y = this->y * v.y;
	out.z = this->z * v.z;
	out.w = this->w * v.w;
	return out;
}

template<typename types>
inline CVector<types> CVector<types>::operator/(const CVector & v)
{
	CVector<types> out;
	out.x = this->x / v.x;
	out.y = this->y / v.y;
	out.z = this->z / v.z;
	out.w = this->w / v.w;
	return out;
}

template<typename types>
inline CVector<types> & CVector<types>::operator=(const CVector & v)
{
	this->x = v.x;
	this->y = v.y;
	this->z = v.z;
	this->w = v.w;
	return *this;
}

template<typename types>
inline CVector<types> & CVector<types>::operator+=(const CVector & v)
{
	this->x += v.x;
	this->y += v.y;
	this->z += v.z;
	this->w += v.w;
	return *this;
}

template<typename types>
inline CVector<types> & CVector<types>::operator-=(const CVector & v)
{
	this->x -= v.x;
	this->y -= v.y;
	this->z -= v.z;
	this->w -= v.w;
	return *this;
}

template<typename types>
inline CVector<types> & CVector<types>::operator*=(const CVector & v)
{
	this->x *= v.x;
	this->y *= v.y;
	this->z *= v.z;
	this->w *= v.w;
	return *this;
}

template<typename types>
inline CVector<types> & CVector<types>::operator/=(const CVector & v)
{
	this->x /= v.x;
	this->y /= v.y;
	this->z /= v.z;
	this->w /= v.w;
	return *this;
}

template<typename types>
inline CVector<types> CVector<types>::operator+(const types t) const
{
	return CVector(this->x + t, this->y + t, this->z + t, this->w + t);
}

template<typename types>
inline CVector<types> CVector<types>::operator-(const types t) const
{
	return CVector(this->x - t, this->y - t, this->z - t, this->w - t);
}

template<typename types>
inline CVector<types> CVector<types>::operator*(const types t) const
{
	return CVector(this->x * t, this->y * t, this->z * t, this->w * t);
}

template<typename types>
inline CVector<types> CVector<types>::operator/(const types t) const
{
	types tInv = (types)1;
	tInv = tInv / t;
	return CVector(this->x * tInv, this->y * tInv, this->z * tInv, this->w * tInv);
}

template<typename types>
inline CVector<types> & CVector<types>::operator=(const types t)
{
	this->x = t;
	this->y = t;
	this->z = t;
	this->w = t;
	return *this;
}

template<typename types>
inline CVector<types> & CVector<types>::operator+=(const types t)
{
	this->x += t;
	this->y += t;
	this->z += t;
	this->w += t;
	return *this;
}

template<typename types>
inline CVector<types> & CVector<types>::operator-=(const types t)
{
	this->x -= t;
	this->y -= t;
	this->z -= t;
	this->w -= t;
	return *this;
}

template<typename types>
inline CVector<types> & CVector<types>::operator*=(const types t)
{
	this->x *= t;
	this->y *= t;
	this->z *= t;
	this->w *= t;
	return *this;
}

template<typename types>
inline CVector<types> & CVector<types>::operator/=(const types t)
{
	this->x /= t;
	this->y /= t;
	this->z /= t;
	this->w /= t;
	return *this;
}

// Array Subscript
template<typename types>
inline const CVector<types> & CVector<types>::operator[](std::size_t n) const &
{
	return this[n];
}

// Array Subscript
template<typename types>
inline CVector<types> & CVector<types>::operator[](std::size_t n) &
{
	return this[n];
}

// Array Subscript
template<typename types>
inline CVector<types> CVector<types>::operator[](std::size_t n) &&
{
	return std::move(this[n]);
}

#endif // !_DataType_H_
