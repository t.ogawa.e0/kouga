//==========================================================================
// 当たり判定[HitDetermination.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "HitDetermination.h"

CHitDetermination::CHitDetermination()
{
}

CHitDetermination::~CHitDetermination()
{
}

//==========================================================================
// 球当たり判定 当たった場合true
bool CHitDetermination::Ball(C3DObject * TargetA, C3DObject * TargetB, float Scale)
{
	if (this->Distance(TargetA, TargetB)<Scale) { return true; }

	return false;
}

//==========================================================================
// シンプルな当たり判定 当たった場合true
bool CHitDetermination::Simple(C3DObject * TargetA, C3DObject * TargetB, float Scale)
{
	if (this->Sinple2(&TargetA->Info.pos.z, &TargetB->Info.pos.z, &m_Null, Scale)) { return true; }
	if (this->Sinple2(&TargetA->Info.pos.x, &TargetB->Info.pos.x, &m_Null, Scale)) { return true; }

	return false;
}

//==========================================================================
// 距離の算出
float CHitDetermination::Distance(C3DObject * TargetA, C3DObject * TargetB)
{
	float fDistance = powf
	(
		((TargetB->Info.pos.x) - (TargetA->Info.pos.x))*
		((TargetB->Info.pos.x) - (TargetA->Info.pos.x)) +
		((TargetB->Info.pos.y) - (TargetA->Info.pos.y))*
		((TargetB->Info.pos.y) - (TargetA->Info.pos.y)) +
		((TargetB->Info.pos.z) - (TargetA->Info.pos.z))*
		((TargetB->Info.pos.z) - (TargetA->Info.pos.z)),
		0.5f
	);

	return fDistance;
}

//==========================================================================
// シンプルな当たり判定の内部
bool CHitDetermination::Sinple2(float * TargetA, float * TargetB, const float * TargetC, float Scale)
{
	if (*TargetC < *TargetA)
	{
		if (*TargetC < *TargetB)
		{
			if (*TargetB < (*TargetA)*Scale)
			{
				return true;
			}
		}
	}
	else if (*TargetA < *TargetC)
	{
		if (*TargetB < *TargetC)
		{
			if ((*TargetA)*Scale < *TargetB)
			{
				return true;
			}
		}
	}

	return false;
}
