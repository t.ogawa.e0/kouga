//==========================================================================
// DXデバッグ[DXDebug.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "DXDebug.h"

//==========================================================================
// 実体
//==========================================================================
#if defined(_DEBUG) || defined(DEBUG)
int CDirectXDebug::m_FontPosX;
int CDirectXDebug::m_FontPosY;
LPD3DXFONT CDirectXDebug::m_pFont;
int CDirectXDebug::m_Width;
int CDirectXDebug::m_Height;
D3DCOLOR CDirectXDebug::m_Color;
int CDirectXDebug::m_FontSize;
#endif

CDirectXDebug::CDirectXDebug()
{
}

CDirectXDebug::~CDirectXDebug()
{
}

//==========================================================================
// 初期化
void CDirectXDebug::Init(int Width, int Height, LPDIRECT3DDEVICE9 pDevice)
{
#if defined(_DEBUG) || defined(DEBUG)
	static bool bLock = false;

	if (bLock == false)
	{
		D3DXCreateFont
		(
			pDevice,				// デバイス
			20,						// 文字の高さ
			0,						// 文字の幅
			FW_REGULAR,				// フォントの太さ
			0,						// MIPMAPのレベル
			FALSE,					// イタリックか？
			SHIFTJIS_CHARSET,		// 文字セット
			OUT_DEFAULT_PRECIS,		// 出力精度
			DEFAULT_QUALITY,		// 出力品質
			FIXED_PITCH | FF_SCRIPT,// フォントピッチとファミリ
			TEXT("Terminal"),		// フォント名
			&m_pFont				// Direct3Dフォントへのポインタへのアドレス
		);
		m_FontSize = 18;
		bLock = true;
		m_Width = Width;
		m_Height = Height;
	}
#else
	Decoy(&Width);
	Decoy(&Height);
	Decoy(&pDevice);
#endif
}

//==========================================================================
// 解放
void CDirectXDebug::Uninit(void)
{
#if defined(_DEBUG) || defined(DEBUG)
	static bool bLock = false;

	if (bLock == false)
	{
		Release(m_pFont);
		bLock = true;
	}
#endif
}

//==========================================================================
// 座標セット
void CDirectXDebug::PosAndColor(int x, int y, D3DCOLOR Color)
{
#if defined(_DEBUG) || defined(DEBUG)
	m_FontPosX = x;
	m_FontPosY = y;
	m_Color = Color;
#else
	Decoy(&x);
	Decoy(&y);
	Decoy(&Color);
#endif
}

//==========================================================================
// 当たり判定の有効範囲視覚化
void CDirectXDebug::HitRound(C3DObject * pPos, int NumLine, float Scale, LPDIRECT3DDEVICE9 pDevice)
{
#if defined(_DEBUG) || defined(DEBUG)
	CVertex *RoundX = nullptr;
	CVertex *RoundY = nullptr;
	float C_R = D3DX_PI * 2 / NumLine;	// 円周率
	D3DXMATRIX aMtxWorld; // ワールド行列
	C3DObject pos = *pPos;

	// メモリの確保
	RoundX = new CVertex[(NumLine + 1)];
	RoundY = new CVertex[(NumLine + 1)];

	// 円の生成
	for (int i = 0; i < (NumLine + 1); i++)
	{
		RoundX[i].pos.x = ((0.0f + cosf(C_R * i)));
		RoundX[i].pos.y = 0.0f;
		RoundX[i].pos.z = ((0.0f + sinf(C_R * i)));
		RoundX[i].color = D3DCOLOR_RGBA(255, 0, 0, 255);

		RoundY[i].pos.x = 0.0f;
		RoundY[i].pos.y = ((0.0f + cosf(C_R * i)));
		RoundY[i].pos.z = ((0.0f + sinf(C_R * i)));
		RoundY[i].color = D3DCOLOR_RGBA(0, 255, 0, 255);
	}

	pos.Info.sca = D3DXVECTOR3(Scale, Scale, Scale);

	// FVFの設定
	pDevice->SetFVF(D3DFVF_XYZ | D3DFVF_DIFFUSE);

	// ライト設定
	pDevice->SetRenderState(D3DRS_LIGHTING, FALSE);

	// サイズ
	pDevice->SetTransform(D3DTS_WORLD, pos.CreateMtxWorld1(&aMtxWorld));
	pDevice->SetTexture(0, nullptr);

	// 描画
	pDevice->DrawPrimitiveUP(D3DPT_LINESTRIP, NumLine, RoundX, sizeof(CVertex));
	pDevice->DrawPrimitiveUP(D3DPT_LINESTRIP, NumLine, RoundY, sizeof(CVertex));

	// メモリの解放
	delete[]RoundX;
	RoundX = nullptr;
	delete[]RoundY;
	RoundY = nullptr;
#else
	pPos;
	NumLine;
	Scale;
	pDevice;
#endif
}

//==========================================================================
// 文字描画
void CDirectXDebug::DPrintf(const char * pFormat, ...)
{
#if defined(_DEBUG) || defined(DEBUG)
	va_list argp;
	RECT rect = { m_FontPosX, m_FontPosY, m_Width, m_Height };
	char strBuf[m_Bite];

	va_start(argp, pFormat);
	vsprintf_s(strBuf, m_Bite, pFormat, argp);
	va_end(argp);
	m_pFont->DrawText(nullptr, strBuf, -1, &rect, DT_LEFT, m_Color);
#else
	Decoy(&pFormat);
#endif
}
