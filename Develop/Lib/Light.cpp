//==========================================================================
// ライト[Light.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Light.h"

CLight::CLight()
{
}

CLight::~CLight()
{
}

//==========================================================================
// 初期化
// aVecDir = 光のベクトルを入れるところ
void CLight::Init(D3DXVECTOR3 aVecDir)
{
	LPDIRECT3DDEVICE9 pDevice = CDirectXDevice::GetD3DDevice();

	// ライトの設定
	ZeroMemory(&this->m_aLight, sizeof(this->m_aLight));
	this->m_aLight.Type = D3DLIGHT_DIRECTIONAL; // ディレクショナルライト
	D3DXVec3Normalize((D3DXVECTOR3*)&this->m_aLight.Direction, &aVecDir); // 正規化
	this->m_aLight.Diffuse.r = 1.0f; // 色
	this->m_aLight.Diffuse.g = 1.0f; // 色
	this->m_aLight.Diffuse.b = 1.0f; // 色
	this->m_aLight.Diffuse.a = 1.0f; // 色
	this->m_aLight.Ambient.r = 0.3f; // アンビエントライト
	this->m_aLight.Ambient.g = 0.3f; // アンビエントライト
	this->m_aLight.Ambient.b = 0.3f; // アンビエントライト
	this->m_aLight.Ambient.a = 0.3f; // アンビエントライト

	// ライトの設定
	pDevice->SetLight(0, &this->m_aLight); // ライトの種類
	pDevice->LightEnable(0, TRUE); // 使用の許可

	// グローバルアンビエントの設定
	pDevice->SetRenderState(D3DRS_AMBIENT, D3DXCOLOR(0.3f, 0.3f, 0.3f, 1.f));
}
