//==========================================================================
// カメラ[Camera.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _Camera_H_
#define _Camera_H_

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>

//==========================================================================
//
// class  : CCamera
// Content: カメラ
//
//==========================================================================
class CCamera // カメラ
{
public:
	enum VectorList
	{
		VEYE = 0, // 注視点
		VAT, // カメラ座標
		VUP, // ベクター
		VECUP, // 上ベクトル
		VECFRONT, // 前ベクトル
		VECRIGHT, //  右ベクトル
	};
private:
	// 平行処理
	void CameraMoveXYZ(D3DXVECTOR3 *pVec, D3DXVECTOR3 *pVecRight, const D3DXVECTOR3 *pVecUp, const D3DXVECTOR3 *pVecFront, D3DXVECTOR3 *pOut1, D3DXVECTOR3 *pOut2, const float *pSpeed);

	// X軸回転処理
	void CameraRangX(D3DXVECTOR3 *pDirection, D3DXMATRIX *pRot, D3DXVECTOR3 *pVecRight, const D3DXVECTOR3 *pVecUp, D3DXVECTOR3 *pVecFront, D3DXVECTOR3 *pOut1, D3DXVECTOR3 *pOut2, const float *pRang);

	// Y軸回転処理
	void CameraRangY(D3DXVECTOR3 *pDirection, D3DXMATRIX *pRot, D3DXVECTOR3 *pVecRight, const D3DXVECTOR3 *pVecUp, D3DXVECTOR3 *pVecFront, D3DXVECTOR3 *pOut1, D3DXVECTOR3 *pOut2, const float *pRang);

	// 視点変更 戻り値,視点とカメラの距離
	float ViewPos(D3DXVECTOR3 *pVec, D3DXVECTOR3 *pVecRight, const D3DXVECTOR3 *pVecUp, const D3DXVECTOR3 *pVecFront, D3DXVECTOR3 *pOut1, D3DXVECTOR3 *pOut2, const float *pSpeed);

	// 内積
	bool Restriction(D3DXMATRIX pRot, D3DXVECTOR3 pVecRight, const D3DXVECTOR3 pVecUp, D3DXVECTOR3 pVecFront, const float* pRang);
public:
	CCamera();
	~CCamera();

	// 初期化
	void Init(void);

	// 初期化
	// pEye = 注視点
	// pAt = カメラ座標
	void Init(D3DXVECTOR3 * pEye, D3DXVECTOR3 * pAt);

	// 解放
	void Uninit(void);

	// 更新
	// この関数は一か所に設置すれば全て自動で処理されます
	// MtxView = ビュー行列を入れてください
	// fWidth = 画面の幅を入れてください
	// fHeight = 画面の高さを入れてください
	// pDevice = デバイスを入れてください
	static void Update(D3DXMATRIX * MtxView, int fWidth, int fHeight, LPDIRECT3DDEVICE9 pDevice);

	// ビュー行列生成
	D3DXMATRIX *CreateView(void);

	// 視点中心にX軸回転
	// Rang = 入れた値が加算されます
	void RotViewX(float Rang);

	// 視点中心にY軸回転
	// Rang = 入れた値が加算されます
	void RotViewY(float Rang);

	// カメラ中心にX軸回転
	// Rang = 入れた値が加算されます
	void RotCameraX(float Rang);

	// カメラ中心にY軸回転
	// Rang = 入れた値が加算されます
	void RotCameraY(float Rang);

	// X軸平行移動
	// Speed = 入れた値が加算されます
	void MoveX(float Speed);

	// Y軸平行移動
	// Speed = 入れた値が加算されます
	void MoveY(float Speed);

	// Z軸平行移動
	// Speed = 入れた値が加算されます
	void MoveZ(float Speed);

	// 視点変更 戻り値,視点とカメラの距離
	// Distance = 入れた値が加算されます
	float DistanceFromView(float Distance);

	// ベクターの取得
	// List = カメラベクトルの種類
	D3DXVECTOR3 GetVECTOR(VectorList List);

	// カメラY回転情報
	float GetRestriction(void);

	// カメラ座標をセット
	// Eye = 注視点
	// At = カメラ座標
	// Up = ベクター
	void SetCameraPos(D3DXVECTOR3 * Eye, D3DXVECTOR3 * At, D3DXVECTOR3 * Up);

	// カメラ座標
	// At = カメラ座標
	void SetAt(D3DXVECTOR3 * At);

	// 注視点
	// Eye = 注視点
	void SetEye(D3DXVECTOR3 * Eye);
private:
	D3DXMATRIX m_aMtxView; // ビュー行列
	D3DXVECTOR3 m_Eye; // 注視点
	D3DXVECTOR3 m_At; // カメラ座標
	D3DXVECTOR3 m_Eye2; // 注視点
	D3DXVECTOR3 m_At2; // カメラ座標
	D3DXVECTOR3 m_Up; // ベクター
	D3DXVECTOR3 m_VecUp; // 上ベクトル
	D3DXVECTOR3 m_VecFront; // 前ベクトル
	D3DXVECTOR3 m_VecRight; //  右ベクトル
};

#endif // !_Camera_H_
