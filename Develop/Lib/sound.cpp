//==========================================================================
// サウンド[sound.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "sound.h"

//==========================================================================
// 実体
//==========================================================================
IXAudio2 *CSoundDevice::m_pXAudio2; // XAudio2オブジェクトへのインターフェイス
IXAudio2MasteringVoice *CSoundDevice::m_pMasteringVoice; // マスターボイス
HWND CSoundDevice::m_hWnd = nullptr; // ウィンドハンドル

CSoundDevice::CSoundDevice()
{
}

CSoundDevice::~CSoundDevice()
{
}

//==========================================================================
// 初期化
HRESULT CSoundDevice::Init(HWND hWnd)
{
	m_pXAudio2 = nullptr;
	m_pMasteringVoice = nullptr;
	m_hWnd = hWnd;

	// COMライブラリの初期化
	CoInitializeEx(nullptr, COINIT_MULTITHREADED);

	// XAudio2オブジェクトの作成
	if (FAILED(XAudio2Create(&m_pXAudio2, 0)))
	{
		MessageBox(hWnd, "XAudio2オブジェクトの作成に失敗！", "警告！", MB_ICONWARNING);

		// COMライブラリの終了処理
		CoUninitialize();

		return E_FAIL;
	}

	// マスターボイスの生成
	if (FAILED(m_pXAudio2->CreateMasteringVoice(&m_pMasteringVoice)))
	{
		MessageBox(hWnd, "マスターボイスの生成に失敗", "警告！", MB_ICONWARNING);

		// XAudio2オブジェクトの開放

		Release(m_pXAudio2);

		// COMライブラリの終了処理
		CoUninitialize();

		return E_FAIL;
	}

	return S_OK;
}

//==========================================================================
// 解放
void CSoundDevice::Uninit(void)
{
	// マスターボイスの破棄
	DestroyVoice();

	// XAudio2オブジェクトの開放
	Release(m_pXAudio2);

	// COMライブラリの終了処理
	CoUninitialize();
}

//==========================================================================
// ボイスデータの破棄
void CSoundDevice::DestroyVoice(void)
{
	if (m_pMasteringVoice != nullptr)
	{
		m_pMasteringVoice->DestroyVoice();
		m_pMasteringVoice = nullptr;
	}
}

CSound::CSound()
{
	this->m_pSound = nullptr;
}

CSound::~CSound()
{
}

//==========================================================================
// 初期化
HRESULT CSound::Init(const SoundLabel *lnk, int size)
{
	HANDLE hFile;
	DWORD dwChunkSize = 0;
	DWORD dwChunkPosition = 0;
	DWORD dwFiletype;
	WAVEFORMATEXTENSIBLE wfx;
	XAUDIO2_BUFFER buffer;
	CParam *pSound = nullptr;
	HWND phWnd = CSoundDevice::GetWnd();
	IXAudio2 * pXAudio2 = CSoundDevice::GetAudio2();

	this->m_pSound = nullptr;
	this->m_NumSound = size;

	// メモリの初期化
	this->New(this->m_pSound, size);

	for (int i = 0; i < this->m_NumSound; i++)
	{
		// バッファのクリア
		memset(&wfx, 0, sizeof(WAVEFORMATEXTENSIBLE));
		memset(&buffer, 0, sizeof(XAUDIO2_BUFFER));

		pSound = &this->m_pSound[i];

		// サウンドデータファイルの生成
		hFile = CreateFile(lnk[i].m_pFilename, GENERIC_READ, FILE_SHARE_READ, nullptr, OPEN_EXISTING, 0, nullptr);
		if (hFile == INVALID_HANDLE_VALUE)
		{
			MessageBox(phWnd, "サウンドデータファイルの生成に失敗(1)", "警告", MB_ICONWARNING);
			return HRESULT_FROM_WIN32(GetLastError());
		}

		// ファイルポインタを先頭に移動
		if (SetFilePointer(hFile, 0, nullptr, FILE_BEGIN) == INVALID_SET_FILE_POINTER)
		{
			MessageBox(phWnd, "サウンドデータファイルの生成に失敗(2)", "警告", MB_ICONWARNING);
			return HRESULT_FROM_WIN32(GetLastError());
		}

		// WAVEファイルのチェック
		if (FAILED(this->CheckChunk(hFile, 'FFIR', &dwChunkSize, &dwChunkPosition)))
		{
			MessageBox(phWnd, "WAVEファイルのチェックに失敗(1)", "警告", MB_ICONWARNING);
			return S_FALSE;
		}

		if (FAILED(this->ReadChunkData(hFile, &dwFiletype, sizeof(DWORD), dwChunkPosition)))
		{
			MessageBox(phWnd, "WAVEファイルのチェックに失敗(2)", "警告", MB_ICONWARNING);
			return S_FALSE;
		}

		if (dwFiletype != 'EVAW')
		{
			MessageBox(phWnd, "WAVEファイルのチェックに失敗(3)", "警告", MB_ICONWARNING);
			return S_FALSE;
		}

		// フォーマットチェック
		if (FAILED(this->CheckChunk(hFile, ' tmf', &dwChunkSize, &dwChunkPosition)))
		{
			MessageBox(phWnd, "フォーマットチェックに失敗(1)", "警告", MB_ICONWARNING);
			return S_FALSE;
		}

		if (FAILED(this->ReadChunkData(hFile, &wfx, dwChunkSize, dwChunkPosition)))
		{
			MessageBox(phWnd, "フォーマットチェックに失敗(2)", "警告", MB_ICONWARNING);
			return S_FALSE;
		}

		// オーディオデータ読み込み
		if (FAILED(this->CheckChunk(hFile, 'atad', &pSound->m_SizeAudio, &dwChunkPosition)))
		{
			MessageBox(phWnd, "オーディオデータ読み込みに失敗(1)", "警告", MB_ICONWARNING);
			return S_FALSE;
		}

		// メモリ確保
		this->New(pSound->m_pDataAudio, pSound->m_SizeAudio);

		if (FAILED(this->ReadChunkData(hFile, pSound->m_pDataAudio, pSound->m_SizeAudio, dwChunkPosition)))
		{
			MessageBox(phWnd, "オーディオデータ読み込みに失敗(2)", "警告", MB_ICONWARNING);
			return S_FALSE;
		}

		// ソースボイスの生成
		if (FAILED(pXAudio2->CreateSourceVoice(&pSound->m_pSourceVoice, &(wfx.Format))))
		{
			MessageBox(phWnd, "ソースボイスの生成に失敗！", "警告！", MB_ICONWARNING);
			return S_FALSE;
		}

		// バッファの値設定
		memset(&buffer, 0, sizeof(XAUDIO2_BUFFER));
		buffer.AudioBytes = pSound->m_SizeAudio;
		buffer.pAudioData = pSound->m_pDataAudio;
		buffer.Flags = XAUDIO2_END_OF_STREAM;
		buffer.LoopCount = lnk[i].m_CntLoop;

		// カウント記憶
		pSound->m_CntLoop = lnk[i].m_CntLoop;

		// ボリューム記憶
		pSound->m_Volume = lnk[i].m_Volume;

		// オーディオバッファの登録
		pSound->m_pSourceVoice->SubmitSourceBuffer(&buffer);
	}

return S_OK;
}

//==========================================================================
// 解放
void CSound::Uninit(void)
{
	CParam *pSound = nullptr;

	// 一時停止
	for (int i = 0; i < this->m_NumSound; i++)
	{
		pSound = &this->m_pSound[i];

		if (pSound->m_pSourceVoice)
		{
			this->Stop(i);

			// ソースボイスの破棄
			pSound->DestroyVoice();

			// オーディオデータの開放
			this->Delete(pSound->m_pDataAudio);
		}
	}

	// データを0に
	this->m_NumSound = 0;
	
	// メモリの解放
	this->Delete(this->m_pSound);
}

//==========================================================================
// 再生
HRESULT CSound::Play(int label)
{
	XAUDIO2_VOICE_STATE xa2state;
	XAUDIO2_BUFFER buffer;
	CParam *pSound = &this->m_pSound[label];

	// バッファの値設定
	memset(&buffer, 0, sizeof(XAUDIO2_BUFFER));
	buffer.AudioBytes = pSound->m_SizeAudio;
	buffer.pAudioData = pSound->m_pDataAudio;
	buffer.Flags = XAUDIO2_END_OF_STREAM;
	buffer.LoopCount = pSound->m_CntLoop;

	// 状態取得
	pSound->m_pSourceVoice->GetState(&xa2state);

	if (xa2state.BuffersQueued != 0)
	{// 再生中
	 // 一時停止
		pSound->m_pSourceVoice->Stop(0);

		// オーディオバッファの削除
		pSound->m_pSourceVoice->FlushSourceBuffers();
	}

	// オーディオバッファの登録
	pSound->m_pSourceVoice->SubmitSourceBuffer(&buffer);

	// 再生
	pSound->m_pSourceVoice->Start(0);

	return S_OK;
}

//==========================================================================
// 特定のサウンド停止
void CSound::Stop(int label)
{
	XAUDIO2_VOICE_STATE xa2state;
	CParam *pSound = &this->m_pSound[label];

	// 状態取得
	pSound->m_pSourceVoice->GetState(&xa2state);
	if (xa2state.BuffersQueued != 0)
	{// 再生中
	 // 一時停止
		pSound->m_pSourceVoice->Stop(0);

		// オーディオバッファの削除
		pSound->m_pSourceVoice->FlushSourceBuffers();
	}
}

//==========================================================================
// 格納してある全てのサウンド停止
void CSound::Stop(void)
{
	XAUDIO2_VOICE_STATE xa2state;
	CParam *pSound = nullptr;

	// 一時停止
	for (int i = 0; i < this->m_NumSound; i++)
	{
		pSound = &this->m_pSound[i];
		pSound->m_pSourceVoice->GetState(&xa2state);
		if (xa2state.BuffersQueued != 0)
		{// 再生中
		 // 一時停止
			pSound->m_pSourceVoice->Stop(0);

			// オーディオバッファの削除
			pSound->m_pSourceVoice->FlushSourceBuffers();
		}
	}
}

//==========================================================================
// 特定のサウンドのボリューム設定
// volume 最大=1.0f
// volume 無音=0.0f
void CSound::Volume(int label, float volume)
{
	CParam *pSound = &this->m_pSound[label];

	pSound->m_Volume = volume;
}

//==========================================================================
// 全てのサウンドのボリューム設定
// volume 最大=1.0f
// volume 無音=0.0f
void CSound::Volume(float volume)
{
	CParam *pSound = nullptr;

	for (int i = 0; i < this->m_NumSound; i++)
	{
		pSound= &this->m_pSound[i];

		pSound->m_Volume = volume;
	}
}

//==========================================================================
// 全てのサウンドのボリューム自動設定
void CSound::Volume(void)
{
	CParam *pSound = nullptr;

	for (int i = 0; i < this->m_NumSound; i++)
	{
		pSound = &this->m_pSound[i];

		if (pSound->m_CntLoop == 0)
		{
			pSound->m_Volume = 1.0f;
		}
		else if (pSound->m_CntLoop == -1)
		{
			pSound->m_Volume = 0.5f;
		}
	}
}

//==========================================================================
// ボイスデータの破棄
void CSound::CParam::DestroyVoice(void)
{
	if (this->m_pSourceVoice != nullptr)
	{
		this->m_pSourceVoice->DestroyVoice();
		this->m_pSourceVoice = nullptr;
	}
}

//==========================================================================
// チャンクのチェック
HRESULT CSound::CheckChunk(HANDLE hFile, DWORD format, DWORD * pChunkSize, DWORD * pChunkDataPosition)
{
	HRESULT hr = S_OK;
	DWORD dwRead;
	DWORD dwChunkType;
	DWORD dwChunkDataSize;
	DWORD dwRIFFDataSize = 0;
	DWORD dwFileType;
	DWORD dwBytesRead = 0;
	DWORD dwOffset = 0;

	// ファイルポインタを先頭に移動
	if (SetFilePointer(hFile, 0, nullptr, FILE_BEGIN) == INVALID_SET_FILE_POINTER)
	{
		return HRESULT_FROM_WIN32(GetLastError());
	}

	while (hr == S_OK)
	{
		// チャンクの読み込み
		if (ReadFile(hFile, &dwChunkType, sizeof(DWORD), &dwRead, nullptr) == 0)
		{
			hr = HRESULT_FROM_WIN32(GetLastError());
		}

		// チャンクデータの読み込み
		if (ReadFile(hFile, &dwChunkDataSize, sizeof(DWORD), &dwRead, nullptr) == 0)
		{
			hr = HRESULT_FROM_WIN32(GetLastError());
		}

		switch (dwChunkType)
		{
		case 'FFIR':
			dwRIFFDataSize = dwChunkDataSize;
			dwChunkDataSize = 4;

			// ファイルタイプの読み込み
			if (ReadFile(hFile, &dwFileType, sizeof(DWORD), &dwRead, nullptr) == 0)
			{
				hr = HRESULT_FROM_WIN32(GetLastError());
			}
			break;
		default:
			// ファイルポインタをチャンクデータ分移動
			if (SetFilePointer(hFile, dwChunkDataSize, nullptr, FILE_CURRENT) == INVALID_SET_FILE_POINTER)
			{
				return HRESULT_FROM_WIN32(GetLastError());
			}
		}

		dwOffset += sizeof(DWORD) * 2;
		if (dwChunkType == format)
		{
			*pChunkSize = dwChunkDataSize;
			*pChunkDataPosition = dwOffset;

			return S_OK;
		}

		dwOffset += dwChunkDataSize;
		if (dwBytesRead >= dwRIFFDataSize)
		{
			return S_FALSE;
		}
	}

	return S_OK;
}

//==========================================================================
// チャンクデータの読み込み
HRESULT CSound::ReadChunkData(HANDLE hFile, void * pBuffer, DWORD dwBuffersize, DWORD dwBufferoffset)
{
	DWORD dwRead;

	// ファイルポインタを指定位置まで移動
	if (SetFilePointer(hFile, dwBufferoffset, nullptr, FILE_BEGIN) == INVALID_SET_FILE_POINTER)
	{
		return HRESULT_FROM_WIN32(GetLastError());
	}

	// データの読み込み
	if (ReadFile(hFile, pBuffer, dwBuffersize, &dwRead, nullptr) == 0)
	{
		return HRESULT_FROM_WIN32(GetLastError());
	}

	return S_OK;
}
