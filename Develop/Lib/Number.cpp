//==========================================================================
// 数字処理[Number.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Number.h"

CNumber::CNumber()
{
}

CNumber::~CNumber()
{
}

//==========================================================================
// 初期化
// Digit 桁
// LeftAlignment 左寄せするかしないか
// Zero ゼロ描画判定
void CNumber::Init(int Digit, bool LeftAlignment, bool Zero)
{
	int MaxScore = this->m_Pusyu_Score;

	for (int i = 1; i < Digit; i++)
	{
		MaxScore *= this->m_Pusyu_Score;
	}
	MaxScore--;

	this->m_list = CList(Digit, MaxScore, Zero, LeftAlignment);
}

//==========================================================================
// 解放
void CNumber::Uninit(void)
{
}

//==========================================================================
// 更新
void CNumber::Update(void)
{
}

//==========================================================================
// 描画
float CNumber::Draw(C2DPolygon * pPoly, C2DObject * pPos, int NumBer)
{
	C2DObject Pos = *pPos;
	float fWidht = (float)pPoly->GetTexSize(Pos.getindex())->w / Pos.GetAnimParam()->Direction;
	int nDigit = this->m_list.m_Digit;

	//左詰め対応
	if (this->m_list.m_LeftAlignment == true)
	{
		int Set = NumBer;

		nDigit = 1;
		for (;;)
		{
			Set /= this->m_Pusyu_Score;
			if (Set == 0)
			{
				break;
			}
			nDigit++;
			if (nDigit == this->m_list.m_Digit)
			{
				break;
			}
		}
	}

	NumBer = this->Min(this->m_list.m_Max, NumBer);

	for (int i = nDigit - 1; i >= 0; i--)
	{
		// 1234567
		Pos.SetAnimationCount(NumBer%this->m_Pusyu_Score);

		// 123456
		Pos.SetX(pPos->GetPos()->x + i*fWidht);

		pPoly->Draw(&Pos);

		NumBer /= this->m_Pusyu_Score;

		// 12345.6
		if (!this->m_list.m_Zero && NumBer == 0)
		{
			break;
		}
	}

	return (pPos->GetPos()->x + nDigit*fWidht);
}
