//==========================================================================
// 影[Shadow.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _Shadow_H_
#define _Shadow_H_

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>
#include "Rectangle.h"
#include "Vertex3D.h"
#include "SetRender.h"
#include "3DObject.h"
#include "Template.h"
#include "DataType.h"
#include "TextureLoader.h"
#include "Create.h"
#include "DXDevice.h"

//==========================================================================
//
// class  : CShadow
// Content: 影
//
//==========================================================================
class CShadow : private CRectangle, private VERTEX_3D, private CSetRender, private CCreate
{
private:
	static constexpr float VCRCT = 0.5f;
	static constexpr int DefaltColor = 80;
	static constexpr int ColorBuf = 8;
public:
	CShadow();
	~CShadow();

	// 初期化 失敗時true
	// Input = 使用するテクスチャのパス
	bool Init(const char * Input);

	// 解放
	void Uninit(void);

	// 描画
	// 影をつけたい対象のC3DObject
	void Draw(const C3DObject * pInput);
private:
	// 頂点バッファの生成
	void VertexBuffer(VERTEX_4 * Output, CUv<float> * uv);
private:
	VERTEX_4* m_pPseudo;// 頂点バッファのロック
	CTextureLoader m_texture; // テクスチャの格納
	LPDIRECT3DVERTEXBUFFER9 m_pVertexBuffer;	// バッファ
	LPDIRECT3DINDEXBUFFER9 m_pIndexBuffer; // インデックスバッファ
	CTemplates m_temp;
	bool m_Lock;
};

#endif // !_Shadow_H_
