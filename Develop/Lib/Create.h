//==========================================================================
// クリエイト処理[Create.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _Create_H_
#define _Create_H_

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>
#include "DXDevice.h"

//==========================================================================
//
// class  : CCreate
// Content: クリエイト 
//
//==========================================================================
class CCreate
{
protected:
	// バーテックスバッファの生成
	bool CreateVertexBuffer(UINT Length, DWORD Usage, DWORD FVF, D3DPOOL Pool, IDirect3DVertexBuffer9** ppVertexBuffer, HANDLE* pSharedHandle);
	// インデックスバッファの生成
	bool CreateIndexBuffer(UINT Length, DWORD Usage, D3DFORMAT Format, D3DPOOL Pool, IDirect3DIndexBuffer9** ppIndexBuffer, HANDLE* pSharedHandle);
};
#endif // !_Create_H_
