//==========================================================================
// DXデバッグ[DXDebug.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _DXDebug_H_
#define _DXDebug_H_

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <stdio.h>
#include <d3d9.h>
#include <d3dx9.h>
#include "Template.h"
#include "3DObject.h"

//==========================================================================
//
// class  : CDirectXDebug
// Content: デバッグ 
//
//==========================================================================
class CDirectXDebug : private CTemplate
{
private:
	static constexpr int m_Bite = 512; // バッファ
private:
	class CVertex
	{
	public:
		D3DXVECTOR3 pos; // 座標変換が必要
		D3DCOLOR color; // ポリゴンの色
	};
public:
	CDirectXDebug();
	~CDirectXDebug();

	// 初期化
	static void Init(int Width, int Height, LPDIRECT3DDEVICE9 pDevice);

	// 解放
	static void Uninit(void);

	// 座標セット
	static void PosAndColor(int x, int y, D3DCOLOR Color = D3DCOLOR_RGBA(255, 255, 255, 255));

	// 当たり判定の有効範囲視覚化
	static void HitRound(C3DObject * pPos, int NumLine, float Scale, LPDIRECT3DDEVICE9 pDevice);

	// 文字描画
	static void DPrintf(const char* pFormat, ...);

	// フォントサイズ
	static int GetFontSize(void) { return m_FontSize; }
private:
	static int m_FontPosX; // 表示位置X
	static int m_FontPosY; // 表示位置Y
	static LPD3DXFONT m_pFont; // フォント
	static int m_Width; // 幅
	static int m_Height; // 高さ
	static D3DCOLOR m_Color; // カラー
	static int m_FontSize; // フォントサイズ
};

#endif // !_DXDebug_H_
