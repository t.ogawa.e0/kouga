//==========================================================================
// テキスト表示[text.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _text_h_
#define _text_h_

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <stdio.h>
#include <d3d9.h>
#include <d3dx9.h>
#include "Template.h"
#include "DataType.h"
#include "DXDevice.h"

//==========================================================================
//
// class  : CText
// Content: テキストクラス 
//
//==========================================================================
class CText
{
public:
	CText();
	~CText();

	// 初期化
	// w = 画面の横幅
	// h = 画面の高さ
	// フォントの種類はデフォルトで Terminal になってます。
	void init(int w, int h) { this->init(w, h, "Terminal"); }

	// 初期化
	// w = 画面の横幅
	// h = 画面の高さ
	// font = フォントの種類
	void init(int w, int h, const char * font);

	// 解放
	void uninit(void);

	// 色
	void color(int r, int g, int b, int a) { this->m_color = CColor<int>(r, g, b, a); }

	// 色
	void color(CColor<int> input) { this->m_color = input; }

	// 座標
	void pos(int x, int y) { this->m_pos = CTexvec<int>(x, y, this->m_pos.w, this->m_pos.h); }

	// テキスト表示
	void text(const char* input, ...);

	// テキストの始まりに必ずセットしよう。
	void textnew(void) { this->m_Lock = true; this->m_fontheight = 0; }

	// テキストの終わりに必ずセットしよう。
	void textend(void) { this->m_Lock = false; }

	// 文字のサイズ
	void size(int input) { this->m_fontheight_master = input; }
private:
	CTemplates m_temp; // テンプレート
	CColor<int> m_color; // 色
	CTexvec<int> m_pos; // 座標
	LPD3DXFONT m_font; // フォント
	int m_fontheight; // 文字の高さ
	int m_fontheight_master; // 文字の高さ
	bool m_Lock; // 描画のロック
};

#endif // !_text_h_
