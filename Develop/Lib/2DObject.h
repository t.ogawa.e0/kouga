//==========================================================================
// 2Dオブジェクト[2DObject.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _2DObject_H_
#define _2DObject_H_

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>
#include "Vertex3D.h"
#include "DataType.h"

//==========================================================================
//
// class  : C2DObject
// Content: 2Dオブジェクト オブジェクトを動かすためのもの
//
//==========================================================================
class C2DObject : private VERTEX_3D
{
private:
	//==========================================================================
	//
	// class  : CAnim
	// Content: アニメーション情報
	//
	//==========================================================================
	class CAnim
	{
	public:
		CAnim() { this->Count = 0;this->Frame = 0;this->Pattern = 0;this->Direction = 0;this->Key = false; };
		CAnim(int count, int frame, int pattern, int direction, bool key)
		{
			this->Count = count;
			this->Frame = frame;
			this->Pattern = pattern;
			this->Direction = direction;
			this->Key = key;
		}
		~CAnim() {}
	public:
		int Count; // アニメーションカウンタ
		int Frame; // 更新タイミング
		int Pattern; // アニメーションのパターン数
		int Direction; // 一行のアニメーション数
		bool Key; // アニメーションの有無
	};

	//==========================================================================
	//
	// class  : CAffine
	// Content: 回転情報
	//
	//==========================================================================
	class CAffine
	{
	public:
		CAffine() { this->Angle = 0.0f;this->Key = false; };
		CAffine(float ang, bool key)
		{
			this->Angle = ang;
			this->Key = key;
		}
		~CAffine() {}
	public:
		float Angle; // 回転情報
		bool Key; // 回転の有無
	};

	//==========================================================================
	//
	// struct  : ANGLE
	// Content: 回転のテンプレート
	//
	//==========================================================================
	struct ANGLE
	{
		static constexpr float	_ANGLE_000 = 0.00000000f; // 0度
		static constexpr float	_ANGLE_045 = 0.785398185f; // 45度
		static constexpr float	_ANGLE_090 = 1.57079637f; // 90度
		static constexpr float	_ANGLE_135 = 2.35619450f; // 135度
		static constexpr float	_ANGLE_180 = 3.14159274f; // 180度
		static constexpr float	_ANGLE_225 = -2.35619450f; // 225度
		static constexpr float	_ANGLE_270 = -1.57079637f; // 270度
		static constexpr float	_ANGLE_315 = -0.785398185f; // 315度

	}m_ANGLE;
public:
	C2DObject();
	~C2DObject();

	// 初期化
	// index = テクスチャ番号
	void Init(int index);

	// アニメーション用初期化
	// index = テクスチャ番号
	// Frame = 更新フレーム 
	// Pattern = アニメーション数
	// Direction = 横一列のアニメーション数
	void Init(int index, int Frame, int Pattern, int Direction);

	// アニメーションだけの情報セット
	// index = テクスチャ番号
	// Frame = 更新フレーム 
	// Pattern = アニメーション数
	// Direction = 横一列のアニメーション数
	void SetAnim(int index, int Frame, int Pattern, int Direction);

	// 座標のセット
	void SetPos(float x, float y);
	// 座標のセット
	void SetX(float x);
	// 座標のセット
	void SetY(float y);
	// 座標の加算
	void SetXPlus(float x);
	// 座標の加算
	void SetYPlus(float y);

	// 座標
	void SetPos(CVector<float> vpos);
	// 座標の加算
	void SetPosPlus(CVector<float> vpos);

	// 中心座標モード
	// mood = true で有効化
	void SetCentralCoordinatesMood(bool mood) { this->m_CentralCoordinates = mood; }
	// 座標のゲッター
	CVector<float> *GetPos(void) { return &this->m_Pos; }

	// サイズ
	// 入力した値が加算されます
	void Scale(float Size);

	// スケールのゲッター
	float GetScale(void) { return this->m_Scale; }

	// 回転角度
	// 入力した値が加算されます
	void Angle(float Angle);

	// 回転角度リセット
	void AngleReset(void);

	// 色セット
	void SetColor(int r, int g, int b, int a);

	// 色セット
	void SetColor(CColor<int> color);

	// 無回転バーテックス
	// 使用禁止
	const VERTEX_3 * CreateVertex(VERTEX_3 * pPseudo, const CTexvec<int> * ptexsize);

	// 回転バーテックス
	// 使用禁止
	const VERTEX_3 * CreateVertexAngle(VERTEX_3 * pPseudo, const CTexvec<int> * ptexsize);

	// アニメーション切り替わり時の判定 切り替え時true 
	bool GetPattanNum(void);

	// 角度テンプレート
	ANGLE GetAngle(void) { return this->m_ANGLE; }

	// アニメーションカウンタのセット
	void SetAnimationCount(int nCount);

	// アニメーション情報のゲッター
	CAnim * GetAnimParam(void) { return &this->m_Anim; }
	
	// インデックス情報の取得
	int getindex(void) { return this->m_index; }
private:
	// UVの生成
	void UV(CUv<float> * UV, const CTexvec<int> * ptexsize);

	// アニメーションの情報
	void AnimationPalam(void);

	// テクスチャの切り取り
	void SetTexturCut(const CTexvec<int> * ptexsize);
private:
	CTexvec<float> m_Cut; // テクスチャカット位置
	CVector<float> m_Pos; // ポリゴンの座標
	CColor<int> m_Color; // ポリゴンの色
	CAnim m_Anim; // アニメーション
	CAffine m_Affine; // アフィン変換
	float m_Scale; // スケール
	bool m_Key; // キー
	bool m_SetAnimationCount; // アニメーションカウンタがセットされているフラグ
	bool m_CentralCoordinates; // 中心座標モード
	int m_index; // テクスチャ番号
};

#endif // !_2DObject_H_
