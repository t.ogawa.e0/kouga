//==========================================================================
// ビルボード[Billboard.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _Billboard_H_
#define _Billboard_H_

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>
#include <vector>
#include "Vertex3D.h"
#include "Rectangle.h"
#include "SetRender.h"
#include "3DObject.h"
#include "Template.h"
#include "DataType.h"
#include "TextureLoader.h"
#include "Create.h"
#include "DXDevice.h"

//==========================================================================
//
// class  : CBillboard
// Content: ビルボード
//
//==========================================================================
class CBillboard : private VERTEX_3D, private CRectangle, private CSetRender, private CCreate
{
private:
	static constexpr float VCRCT = 0.5f;
public:
	//==========================================================================
	//
	// class  : PlateList
	// Content: 板ポリの種類
	//
	//==========================================================================
	enum class PlateList
	{
		Up, // 上向き
		Vertical //　縦
	};
private:
	//==========================================================================
	//
	// class  : CAnimation
	// Content: テクスチャ情報
	//
	//==========================================================================
	class CAnimation
	{
	public:
		CAnimation() {};
		CAnimation(int frame, int pattern, int direction, CTexvec<float> cut, CTexvec<int> texsize)
		{
			this->Frame = frame;
			this->Pattern = pattern;
			this->Direction = direction;
			this->m_Cut = cut;
			this->m_TexSize = texsize;
		}
		~CAnimation() {}
	public:
		int Frame; // 更新タイミング
		int Pattern; // アニメーションのパターン数
		int Direction; // 一行のアニメーション数
		CTexvec<float> m_Cut; // 切り取り情報
		CTexvec<int> m_TexSize; // テクスチャのサイズ
	};

	//==========================================================================
	//
	// class  : CBuffer
	// Content: バッファ
	//
	//==========================================================================
	class CBuffer
	{
	public:
		CBuffer();
		~CBuffer();
		// 解放
		void release(void);
	public:
		LPDIRECT3DVERTEXBUFFER9 m_pVertexBuffer; // バッファ
		LPDIRECT3DINDEXBUFFER9 m_pIndexBuffer; // インデックスバッファ
		PlateList m_plate; // 板ポリの種類
		VERTEX_4* m_pPseudo;// 頂点バッファのロック
	private:
		CTemplates m_tenp; // テンプレート
	};

	//==========================================================================
	//
	// class  : CAnimationParam
	// Content: アニメーション情報
	//
	//==========================================================================
	class CAnimationParam
	{
	public:
		CAnimationParam();
		~CAnimationParam();
	public:
		CAnimation *m_pAnimation; // アニメーション情報
		CBuffer *m_buffer; // バッファ
		CUv<float> m_uv; // UV格納
		bool m_key; // オリジナルかどうかの判定
	};
public:
	//==========================================================================
	//
	// class  : CAnimationDataCase
	// Content: 複数アニメーション情報ボックス
	//
	//==========================================================================
	class CAnimationDataCase
	{
	public:
		const char *m_texname[4]; // 四方向テクスチャ格納
		int m_numdata; // データ数
		int m_frame; // 更新フレーム
		int m_pattern; // アニメーション数
		int m_direction; // 横一列のアニメーション数
		PlateList m_plate; // 板ポリの種類
		bool m_centermood; // 中心モード
	};
	//==========================================================================
	//
	// class  : CAnimationData
	// Content: 複数アニメーション登録用
	//
	//==========================================================================
	class CAnimationData
	{
	public:
		CAnimationDataCase m_list;
	};
public:
	CBillboard();
	~CBillboard();

	// 初期化 失敗時true
	// Input = 使うテクスチャのパス
	// plate = 板ポリの種類
	// bCenter = 中心座標を下の真ん中にします
	bool Init(const char* Input, PlateList plate, bool bCenter = true);

	// アニメーション用初期化 失敗時true
	// Input = 使うテクスチャのパス
	// UpdateFrame = 更新フレーム
	// Pattern = アニメーション数
	// Direction = 横一列のアニメーション数
	// plate = 板ポリの種類
	// bCenter = 中心座標を下の真ん中にします
	bool Init(const char* Input, int UpdateFrame, int Pattern, int Direction, PlateList plate, bool bCenter = true);

	// 初期化 失敗時true
	// pTex = 使うテクスチャのパス ダブルポインタ用
	// numdata = 要素数
	// plate = 板ポリの種類
	// bCenter = 中心座標を下の真ん中にします
	bool Init(const char** Input, int numdata, PlateList plate, bool bCenter = true);

	// アニメーション用初期化 失敗時true
	// Input = 使うテクスチャのパス ダブルポインタ用
	// numdata = 要素数
	// UpdateFrame = 更新フレーム
	// Pattern = アニメーション数
	// Direction = 横一列のアニメーション数
	// plate = 板ポリの種類
	// bCenter = 中心座標を下の真ん中にします
	bool Init(const char** Input, int numdata, int UpdateFrame, int Pattern, int Direction, PlateList plate, bool bCenter = true);

	// 解放
	void Uninit(void);

	// アニメーション更新
	// AnimationCount = アニメーション用変数を入れるところ
	void UpdateAnimation(int * AnimationCount);

	// 描画
	// pInput = C3DObjectのアドレス渡し
	void Draw(C3DObject * pInput) { this->BaseDraw(pInput, nullptr, nullptr, nullptr); }

	// 描画
	// pInput = C3DObjectのアドレス渡し
	// AnimationCount = アニメーション用変数を入れるところ
	void Draw(C3DObject * pInput, int * AnimationCount) { this->BaseDraw(pInput, nullptr, AnimationCount, nullptr); }

	// 描画
	// pInput = C3DObjectのアドレス渡し
	// MtxView = ビュー行列を入れるところ
	void Draw(C3DObject * pInput, D3DXMATRIX * MtxView) { this->BaseDraw(pInput, MtxView, nullptr, nullptr); }

	// 描画
	// pInput = C3DObjectのアドレス渡し
	// bADD = 加算合成するかの判定
	void Draw(C3DObject * pInput, bool bADD) { this->BaseDraw(pInput, nullptr, nullptr, &bADD); }

	// 描画
	// pInput = C3DObjectのアドレス渡し
	// AnimationCount = アニメーション用変数を入れるところ
	// bADD = 加算合成するかの判定
	void Draw(C3DObject * pInput, int * AnimationCount, bool bADD) { this->BaseDraw(pInput, nullptr, AnimationCount, &bADD); }

	// 描画
	// pInput = C3DObjectのアドレス渡し
	// MtxView = ビュー行列を入れるところ
	// bADD = 加算合成するかの判定
	void Draw(C3DObject * pInput, D3DXMATRIX * MtxView, bool bADD) { this->BaseDraw(pInput, MtxView, nullptr, &bADD); }

	// 描画
	// pInput = C3DObjectのアドレス渡し
	// MtxView = ビュー行列を入れるところ
	// AnimationCount = アニメーション用変数を入れるところ
	void Draw(C3DObject * pInput, D3DXMATRIX * MtxView, int * AnimationCount) { this->BaseDraw(pInput, MtxView, AnimationCount, nullptr); }

	// 描画
	// pInput = C3DObjectのアドレス渡し
	// MtxView = ビュー行列を入れるところ
	// AnimationCount = アニメーション用変数を入れるところ
	// bADD = 加算合成するかの判定
	void Draw(C3DObject * pInput, D3DXMATRIX * MtxView, int * AnimationCount, bool bADD) { this->BaseDraw(pInput, MtxView, AnimationCount, &bADD); }

	// アニメーション切り替わり時の判定 切り替え時true 
	// index = 判定路調べたい板ポリの番号
	// AnimationCount = アニメーション用変数を入れるところ
	bool GetPattanNum(int index, int * AnimationCount);

	// アニメーション用のカウンタの初期化 引数はカウンタ用変数
	// AnimationCount = アニメーション用変数を入れるところ
	int *AnimationCountInit(int * AnimationCount);
private:
	// ベースとなる描画
	void BaseDraw(C3DObject * pInput = nullptr, D3DXMATRIX * MtxView = nullptr, int * AnimationCount = nullptr, bool *bADD = nullptr);

	// 頂点バッファの生成
	void CreateVertex(VERTEX_4 * Output, CAnimationParam *ptexture, bool bCenter);

	// アニメーション情報のセット
	void AnimationPalam(CAnimationParam *ptexture, int * AnimationCount);

	// UVの生成
	void UV(CAnimationParam *ptexture);

	// 重複検索
	bool OverlapSearch(CAnimationParam * panimparam, PlateList plate);

	// バッファの生成
	bool CreateBuffer(CAnimationParam * panimparam, PlateList plate, bool bCenter);

	// アニメーション情報の生成
	void CreateAnimation(CAnimationParam * panimparam, int UpdateFrame, int Pattern, int Direction);
private:
	std::vector<CAnimationParam*> m_texture; // 管理
	CTextureLoader m_tex; // テクスチャの格納
	CTemplates m_tenp; // テンプレート
};

#endif // !_Billboard_H_
