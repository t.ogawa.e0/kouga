//==========================================================================
// ��`[Rectangle.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _Rectangle_H_
#define _Rectangle_H_

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>

//==========================================================================
//
// class  : CRectangle
// Content: Index
//
//==========================================================================
class CRectangle
{
protected:
	static constexpr int m_NumDfaltIndex = 6;
protected:
	CRectangle();
	~CRectangle();

	// Index
	void RectangleIndex(WORD * Output, int NumRectangle);
};

#endif // !_Rectangle_H_
