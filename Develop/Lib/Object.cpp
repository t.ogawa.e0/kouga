//==========================================================================
// オブジェクト管理[Object.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Object.h"

//==========================================================================
// 実体
//==========================================================================
std::list<CObject*> CObject::m_object; // オブジェクト格納

//==========================================================================
// コンストラクタ
CObject::CObject()
{
	m_object.push_back(this);
}

CObject::~CObject()
{
}

//==========================================================================
// 登録済みオブジェクトの初期化
bool CObject::InitAll(void)
{
	for (auto itr = m_object.begin(); itr != m_object.end(); ++itr)
	{
		if ((*itr)->Init()) { return true; }
	}

	return false;
}

//==========================================================================
// 登録済みオブジェクトの解放
void CObject::UninitAll(void)
{
	for (auto itr = m_object.begin(); itr != m_object.end(); ++itr)
	{
		(*itr)->Uninit();
		delete (*itr);
	}
	m_object.clear();
}

//==========================================================================
// 登録済みオブジェクトの更新
void CObject::UpdateAll(void)
{
	for (auto itr = m_object.begin(); itr != m_object.end(); ++itr)
	{
		(*itr)->Update();
	}
}

//==========================================================================
// 登録済みオブジェクトの描画
void CObject::DrawAll(void)
{
	for (auto itr = m_object.begin(); itr != m_object.end(); ++itr)
	{
		(*itr)->Draw();
	}
}
