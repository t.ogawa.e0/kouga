//==========================================================================
// グリッド[Grid.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _Grid_H_
#define _Grid_H_

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>
#include "Vertex3D.h"
#include "Template.h"
#include "DXDevice.h"

//==========================================================================
//
// class  : CGrid
// Content: グリッド
//
//==========================================================================
class CGrid : private VERTEX_3D
{
public:
	CGrid();
	~CGrid();

	// 初期化
	// scale = グリッドの表示範囲の指定
	void Init(int scale);

	// 解放
	void Uninit(void);

	// 描画
	void Draw(void);
private:
	VERTEX_2 *m_pos; // 頂点の作成
	CTemplates m_temp; // テンプレート
	int m_num; // 線の本数管理
	int m_scale; // サイズの記録
};

#endif // !_Grid_H_
