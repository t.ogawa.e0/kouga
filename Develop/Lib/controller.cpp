//==========================================================================
// コントローラー[Controller.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "controller.h"

//==========================================================================
// 実体
//==========================================================================
LPDIRECTINPUT8 CController::m_pDInput; // DirectInputオブジェクト
LPDIRECTINPUTDEVICE8 CController::m_pDIDevice; // 入力デバイス(コントローラー)へのポインタ
DIJOYSTATE CController::m_State; // 入力情報ワーク
BYTE CController::m_StateTrigger[(int)sizeof(DIJOYSTATE::rgbButtons)]; // トリガー情報ワーク
BYTE CController::m_StateRelease[(int)sizeof(DIJOYSTATE::rgbButtons)]; // リリース情報ワーク
BYTE CController::m_StateRepeat[(int)sizeof(DIJOYSTATE::rgbButtons)]; // リピート情報ワーク
int CController::m_StateRepeatCnt[(int)sizeof(DIJOYSTATE::rgbButtons)]; // リピートカウンタ

BYTE CController::m_POVState[m_MaxPOV]; // POV入力情報ワーク
BYTE CController::m_POVTrigger[m_MaxPOV]; // トリガー情報ワーク
BYTE CController::m_POVRelease[m_MaxPOV]; // リリース情報ワーク
BYTE CController::m_POVRepeat[m_MaxPOV]; // リピート情報ワーク
int CController::m_POVRepeatCnt[m_MaxPOV]; // リピートカウンタ

CController::CController()
{
}

CController::~CController()
{
}

//-----------------------------------------------------------------
// コントローラーの初期化 DirectInput初期化
bool CController::Init(HINSTANCE hInstance, HWND hWnd)
{
	HRESULT hr;
	DIDEVCAPS diDevCaps; // デバイス機能

	m_pDInput = nullptr;
	m_pDIDevice = nullptr;

	if (FAILED(DirectInput8Create(hInstance, DIRECTINPUT_VERSION, IID_IDirectInput8, (void**)&m_pDInput, nullptr)))
	{
		MessageBox(hWnd, "DirectInputオブジェクトが作れませんでした", "警告", MB_OK);
		return true;
	}

	hr = m_pDInput->EnumDevices(DI8DEVCLASS_GAMECTRL, EnumJoysticksCallback, nullptr, DIEDFL_FORCEFEEDBACK | DIEDFL_ATTACHEDONLY);
	if (FAILED(hr) || m_pDIDevice == nullptr)
	{
		m_pDInput->EnumDevices(DI8DEVCLASS_GAMECTRL, EnumJoysticksCallback, nullptr, DIEDFL_ATTACHEDONLY);
	}

	if (m_pDIDevice != nullptr)
	{
		if (FAILED(m_pDIDevice->SetDataFormat(&c_dfDIJoystick)))
		{
			MessageBox(hWnd, "コントローラーの初期化に失敗.", "警告", MB_OK);
			return true;
		}

		if (FAILED(m_pDIDevice->SetCooperativeLevel(hWnd, DISCL_EXCLUSIVE | DISCL_FOREGROUND)))
		{
			MessageBox(hWnd, "協調モードを設定できません", "警告", MB_OK);
			return true;
		}

		diDevCaps.dwSize = sizeof(DIDEVCAPS);
		if (FAILED(m_pDIDevice->GetCapabilities(&diDevCaps)))
		{
			MessageBox(hWnd, "コントローラー機能を作成できません", "警告", MB_OK);
			return true;
		}

		if (FAILED(m_pDIDevice->EnumObjects(EnumAxesCallback, (void*)hWnd, DIDFT_AXIS)))
		{
			MessageBox(hWnd, "プロパティを設定できません", "警告", MB_OK);
			return true;
		}

		if (FAILED(m_pDIDevice->Poll()))
		{
			hr = m_pDIDevice->Acquire();
			while (hr == DIERR_INPUTLOST)
			{
				hr = m_pDIDevice->Acquire();
			}
		}
	}

	return false;
}

//-----------------------------------------------------------------
// コントローラーの更新
bool CController::Update(void)
{
	DIJOYSTATE aState;
	BYTE aPOVState[m_MaxPOV];

	if (m_pDIDevice != nullptr)
	{
		if (SUCCEEDED(m_pDIDevice->GetDeviceState(sizeof(aState), &aState)))
		{
			// ボタン
			for (int i = 0; i < (int)sizeof(DIJOYSTATE::rgbButtons); i++)
			{
				// トリガー・リリース情報を生成
				m_StateTrigger[i] = (m_State.rgbButtons[i] ^ aState.rgbButtons[i]) & aState.rgbButtons[i];
				m_StateRelease[i] = (m_State.rgbButtons[i] ^ aState.rgbButtons[i]) & m_State.rgbButtons[i];

				// リピート情報を生成
				if (aState.rgbButtons[i])
				{
					if (m_StateRepeatCnt[i] < m_LimitCounter)
					{
						m_StateRepeatCnt[i]++;
						if (m_StateRepeatCnt[i] == 1 || m_StateRepeatCnt[i] >= m_LimitCounter)
						{// 押し始めた最初のフレーム、または一定時間経過したらリピート情報ON
							m_StateRepeat[i] = aState.rgbButtons[i];
						}
						else
						{
							m_StateRepeat[i] = 0;
						}
					}
				}
				else
				{
					m_StateRepeatCnt[i] = 0;
					m_StateRepeat[i] = 0;
				}
				// プレス情報を保存
				m_State.rgbButtons[i] = aState.rgbButtons[i];
			}

			// 方向キー
			for (int i = 0; i < m_MaxPOV; i++)
			{
				// 入力情報生成
				if (((aState.rgdwPOV[0] / (DWORD)4500) + (DWORD)1) == (DWORD)(i + 1))
				{
					aPOVState[i] = 0x80;
				}
				else
				{
					aPOVState[i] = 0x00;
				}

				// トリガー・リリース情報を生成
				m_POVTrigger[i] = (m_POVState[i] ^ aPOVState[i]) & aPOVState[i];
				m_POVRelease[i] = (m_POVState[i] ^ aPOVState[i]) & m_POVState[i];

				// リピート情報を生成
				if (aPOVState[i])
				{
					if (m_POVRepeatCnt[i] < m_LimitCounter)
					{
						m_POVRepeatCnt[i]++;
						if (m_POVRepeatCnt[i] == 1 || m_POVRepeatCnt[i] >= m_LimitCounter)
						{// 押し始めた最初のフレーム、または一定時間経過したらリピート情報ON
							m_POVRepeat[i] = aPOVState[i];
						}
						else
						{
							m_POVRepeat[i] = 0x00;
						}
					}
				}
				else
				{
					m_POVRepeatCnt[i] = 0x00;
					m_POVRepeat[i] = 0x00;
				}
				// プレス情報を保存
				m_POVState[i] = aPOVState[i];
			}

			m_State.lX = aState.lX;
			m_State.lY = aState.lY;
			m_State.lZ = aState.lZ;
			m_State.lRx = aState.lRx;
			m_State.lRy = aState.lRy;
			m_State.lRz = aState.lRz;
		}
		else
		{
			// アクセス権を取得
			m_pDIDevice->Acquire();
		}
	}

	return false;
}

//-----------------------------------------------------------------
// コントローラー解放 DirectInput解放
void CController::Uninit(void)
{
	if (m_pDIDevice != nullptr)
	{
		m_pDIDevice->Unacquire();
		m_pDIDevice->Release();
		m_pDIDevice = nullptr;
	}

	if (m_pDInput != nullptr)
	{
		m_pDInput->Release();
		m_pDInput = nullptr;
	}
}

//------------------------------------------------------------------------------
// ジョイスティックのコールバック
BOOL CALLBACK CController::EnumJoysticksCallback(const DIDEVICEINSTANCE *pdidInstance, void *pContext)
{
	UNREFERENCED_PARAMETER(pContext);

	if (FAILED(m_pDInput->CreateDevice(pdidInstance->guidInstance, &m_pDIDevice, nullptr)))
	{
		return DIENUM_CONTINUE;
	}

	return DIENUM_STOP;
}

//------------------------------------------------------------------------------
// 軸のコールバック
BOOL CALLBACK CController::EnumAxesCallback(const DIDEVICEOBJECTINSTANCE *pdidoi, void *pContext)
{
	DIPROPRANGE diprg;

	UNREFERENCED_PARAMETER(pContext);

	diprg.diph.dwSize = sizeof(DIPROPRANGE);
	diprg.diph.dwHeaderSize = sizeof(DIPROPHEADER);
	diprg.diph.dwHow = DIPH_BYID;
	diprg.diph.dwObj = pdidoi->dwType;
	diprg.lMin = 0 - 90;
	diprg.lMax = 0 + 90;

	if (FAILED(m_pDIDevice->SetProperty(DIPROP_RANGE, &diprg.diph)))
	{
		return DIENUM_STOP;
	}

	return DIENUM_CONTINUE;
}

//-----------------------------------------------------------------
// PS4 左スティック
CController::CStick CController::LeftStick(void)
{
	return Stick(m_State.lX, m_State.lY);
}

//-----------------------------------------------------------------
// PS4 右スティック
CController::CStick CController::RightStick(void)
{
	return Stick(m_State.lZ, m_State.lRz);
}

//-----------------------------------------------------------------
// PS4 L2
LONG CController::L2(void)
{
	if (m_pDIDevice == nullptr)
	{
		return (LONG)0;
	}

	return m_State.lRx;
}

//-----------------------------------------------------------------
// PS4 R2
LONG CController::R2(void)
{
	if (m_pDIDevice == nullptr)
	{
		return (LONG)0;
	}

	return m_State.lRy;
}

//-----------------------------------------------------------------
// PS4 方向キー
bool CController::DirectionKey(CDirecyionkey Key)
{
	int Num = 0;

	if (m_pDIDevice == nullptr)
	{
		return false;
	}

	if ((int)(sizeof(m_State.rgdwPOV) / 4) < Num)
	{
		return false;
	}

	if ((LONG)4294967295 == m_State.rgdwPOV[Num])
	{
		return false;
	}

	if ((CDirecyionkey)((m_State.rgdwPOV[Num] / (LONG)4500) + (LONG)1) == Key)
	{
		return true;
	}

	return false;
}

LONG CController::Adjustment(LONG Set)
{
	LONG Stick;

	if (Set >= (LONG)50 || -(LONG)50 >= Set)
	{
		Stick = Set;
	}
	else
	{
		Stick = (LONG)0;
	}

	return Stick;
}

CController::CStick CController::Stick(LONG Stick1, LONG Stick2)
{
	CStick Stick;

	if (m_pDIDevice != nullptr)
	{
		Stick.m_LeftRight = Adjustment(Stick1);
		Stick.m_UpUnder = Adjustment(Stick2);
	}
	else
	{
		Stick.m_LeftRight = (LONG)0;
		Stick.m_UpUnder = (LONG)0;
	}

	return Stick;
}

//==========================================================================
// プレス
bool CController::CButton::Press(Ckey key)
{
	if (m_pDIDevice == nullptr)
	{
		return false;
	}

	return (m_State.rgbButtons[(int)key] & 0x80) ? true : false;
}

//==========================================================================
// トリガー
bool CController::CButton::Trigger(Ckey key)
{
	if (m_pDIDevice == nullptr)
	{
		return false;
	}

	return (m_StateTrigger[(int)key] & 0x80) ? true : false;
}

//==========================================================================
// リピート
bool CController::CButton::Repeat(Ckey key)
{
	if (m_pDIDevice == nullptr)
	{
		return false;
	}

	return (m_StateRelease[(int)key] & 0x80) ? true : false;
}

//==========================================================================
// リリ−ス
bool CController::CButton::Release(Ckey key)
{
	if (m_pDIDevice == nullptr)
	{
		return false;
	}

	return (m_StateRelease[(int)key] & 0x80) ? true : false;
}

//==========================================================================
// プレス
bool CController::CDirection::Press(Ckey key)
{
	if (m_pDIDevice == nullptr)
	{
		return false;
	}

	return (m_POVState[(int)key] & 0x80) ? true : false;
}

//==========================================================================
// トリガー
bool CController::CDirection::Trigger(Ckey key)
{
	if (m_pDIDevice == nullptr)
	{
		return false;
	}

	return (m_POVTrigger[(int)key] & 0x80) ? true : false;
}

//==========================================================================
// リピート
bool CController::CDirection::Repeat(Ckey key)
{
	if (m_pDIDevice == nullptr)
	{
		return false;
	}

	return (m_POVRepeat[(int)key] & 0x80) ? true : false;
}

//==========================================================================
// リリ−ス
bool CController::CDirection::Release(Ckey key)
{
	if (m_pDIDevice == nullptr)
	{
		return false;
	}

	return (m_POVRelease[(int)key] & 0x80) ? true : false;
}
