//==========================================================================
// 当たり判定[HitDetermination.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _HitDetermination_H_
#define _HitDetermination_H_

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>
#include "3DObject.h"

//==========================================================================
//
// class  : CHitDetermination
// Content: 当たり判定専用クラス
//
//==========================================================================
class CHitDetermination
{
private:
	const float m_Null = 0.0f;
public:
	CHitDetermination();
	~CHitDetermination();

	// 球当たり判定 当たった場合true
	bool Ball(C3DObject * TargetA, C3DObject * TargetB, float Scale);

	// シンプルな当たり判定 当たった場合true
	bool Simple(C3DObject * TargetA, C3DObject * TargetB, float Scale);

	// 距離の算出
	float Distance(C3DObject * TargetA, C3DObject * TargetB);
private:
	// シンプルな当たり判定の内部 
	bool Sinple2(float * TargetA, float * TargetB, const float * TargetC, float Scale);
};

#endif // !_HitDetermination_H_
