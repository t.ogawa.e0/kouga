//==========================================================================
// デバイス[DXDevice.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _DXDevice_H_
#define _DXDevice_H_

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>
#include <vector>

//==========================================================================
//
// class  : CDirectXDevice
// Content: デバイス 
//
//==========================================================================
class CDirectXDevice
{
public:
	//==========================================================================
	//
	// class  : CWindowsSize
	// Content: ウィンドウサイズ 
	//
	//==========================================================================
	template<typename types>
	class CWindowsSize
	{
	public:
		CWindowsSize() { this->m_Width = 0;this->m_Height = 0; }
		CWindowsSize(types Width, types Height) { this->m_Width = Width; this->m_Height = Height; }
		~CWindowsSize() {}
	public:
		types m_Width;
		types m_Height;
	};
public:
	CDirectXDevice();
	~CDirectXDevice();

	// デバイスの生成
	static bool CreateDevice(void);

	// 初期化 失敗時true
	static bool Init(void);

	// 解放
	static void Uninit(void);

	// デバイスの習得
	static LPDIRECT3DDEVICE9 GetD3DDevice(void) { return m_pD3DDevice; };

	// デバイスの習得
	static D3DPRESENT_PARAMETERS Getd3dpp(void) { return m_d3dpp; };

	// ウィンドウサイズ
	static CWindowsSize<int> GetWindowsSize(void) { return m_WindowsSize; }

	// Direct3Dオブジェクトの作成
	static bool Direct3DCreate(void);

	// ウィンドウハンドルの記録
	static void SetHwnd(HWND hWnd) { m_hwnd = hWnd; }

	// 画質モードの選択
	static void SetGraphicMode(bool mood) { m_GraphicMode = mood; }

	// 選択された画質モードのゲッター
	static bool GetGraphicMode(void) { return m_GraphicMode; }

	// ウィンドウハンドルのゲッター
	static HWND GetHwnd(void) { return m_hwnd; }

	// ウィンドウモードの取得
	static D3DDISPLAYMODE GetWindowMode(int serect) { return m_WindowsModes[serect]; }

	// ウィンドウモードの数の取得
	static int GetNumWindowMode(void) { return m_WindowsModes.size(); }

	// ウィンドウモードのセット
	static void setwinmoad(int serect) { m_winmood = serect; }

	// 使用可能なディスプレイモードの検索
	static bool CreateDisplayMode(void);

	// スクリーンバッファ誤差修正
	static float screenbuffscale(void) { return m_screenbuffscale; }

	// エラーメッセージ
	template <typename ... Args>
	static void ErrorMessage(const char * text, Args const & ... args);
private:
	static LPDIRECT3D9 m_pD3D; //ダイレクト3Dインターフェース
	static LPDIRECT3DDEVICE9 m_pD3DDevice;//ダイレクト3Dデバイス
	static CWindowsSize<int> m_WindowsSize; // ウィンドウサイズ
	static D3DPRESENT_PARAMETERS m_d3dpp;
	static D3DDISPLAYMODE m_d3dpm;
	static HWND m_hwnd; // ウィンドウハンドル
	static int m_winmood; // ウィンドウモード
	static std::vector<D3DDISPLAYMODE> m_WindowsModes; // ディスプレイの解像度格納
	static float m_screenbuffscale; // 推奨サイズとの描画サイズの誤差
	static bool m_GraphicMode; // 画質モード
};

//==========================================================================
// エラーメッセージ
template<typename ...Args>
inline void CDirectXDevice::ErrorMessage(const char * text, Args const & ...args)
{
	char pfon[1024] = { 0 };

	sprintf(pfon, text, args ...);

	MessageBox(m_hwnd, pfon, "ERROR", MB_OK | MB_ICONEXCLAMATION | MB_ICONWARNING);
}

#endif // !_DXDevice_H_

