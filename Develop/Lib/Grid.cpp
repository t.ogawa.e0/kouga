//==========================================================================
// グリッド[Grid.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Grid.h"

CGrid::CGrid()
{
	this->m_pos = nullptr;
	this->m_num = 0;
	this->m_scale = 0;
}

CGrid::~CGrid()
{
	this->m_temp.Delete_(this->m_pos);
}

//==========================================================================
// 初期化
// scale = グリッドの表示範囲の指定
void CGrid::Init(int scale)
{
	this->m_scale = scale; // サイズの記録
	this->m_num = 4; // 十字を作るための線
	float X = (float)this->m_scale; // サイズの記録
	float Z = (float)this->m_scale; // サイズの記録

	// サイズ分の外枠の線を追加
	for (int i = 0; i < this->m_scale; i++)
	{
		this->m_num += 8; 
	}

	this->m_pos = nullptr;
	
	this->m_temp.New_(this->m_pos, this->m_num);

	int nNumLine = (int)(this->m_num / 2);

	for (int i = 0; i < nNumLine; i += 2, X--, Z--)
	{
		this->m_pos[i].pos = D3DXVECTOR3(1.0f * X, 0.0f, (float)this->m_scale); // x座標に線
		this->m_pos[i + 1].pos = D3DXVECTOR3(1.0f * X, 0.0f, -(float)this->m_scale); // x座標に線
		this->m_pos[nNumLine + i].pos = D3DXVECTOR3((float)this->m_scale, 0.0f, 1.0f * Z); // z座標に線
		this->m_pos[nNumLine + i + 1].pos = D3DXVECTOR3(-(float)this->m_scale, 0.0f, 1.0f * Z); // z座標に線

		this->m_pos[i].color = D3DCOLOR_RGBA(255, 255, 255, 255);
		this->m_pos[i + 1].color = D3DCOLOR_RGBA(255, 255, 255, 255);
		this->m_pos[nNumLine + i].color = D3DCOLOR_RGBA(255, 255, 255, 255);
		this->m_pos[nNumLine + i + 1].color = D3DCOLOR_RGBA(255, 255, 255, 255);

		if (i == (int)((float)this->m_scale * 2))
		{
			this->m_pos[i].color = D3DCOLOR_RGBA(255, 0, 0, 255);
			this->m_pos[i + 1].color = D3DCOLOR_RGBA(255, 0, 0, 255);
			this->m_pos[nNumLine + i].color = D3DCOLOR_RGBA(255, 0, 0, 255);
			this->m_pos[nNumLine + i + 1].color = D3DCOLOR_RGBA(255, 0, 0, 255);
		}
	}
}

//==========================================================================
// 解放
void CGrid::Uninit(void)
{
	this->m_temp.Delete_(this->m_pos);
}

//==========================================================================
// 描画
void CGrid::Draw(void)
{
	LPDIRECT3DDEVICE9 pDevice = CDirectXDevice::GetD3DDevice();
	D3DXMATRIX m_MtxWorld; // ワールド行列

	D3DXMatrixIdentity(&m_MtxWorld);

	// ライト設定
	pDevice->SetRenderState(D3DRS_LIGHTING, FALSE);

	// 各種行列の設定
	pDevice->SetTransform(D3DTS_WORLD, &m_MtxWorld);

	// FVFの設定
	pDevice->SetFVF(FVF_VERTEX_2);

	pDevice->SetTexture(0, nullptr);
	pDevice->DrawPrimitiveUP(D3DPT_LINELIST, (int)(this->m_num / 2), this->m_pos, sizeof(VERTEX_2));
}
