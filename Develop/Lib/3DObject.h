//==========================================================================
// オブジェクト[3DObject.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _3DObject_H_
#define _3DObject_H_

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>
#include "DataType.h"

//==========================================================================
//
// class  : C3DObject
// Content: オブジェクト
//
//==========================================================================
class C3DObject
{
private:
	//==========================================================================
	//
	// class  : CMatLook
	// Content: 視覚情報
	//
	//==========================================================================
	class CMatLook
	{
	public:
		D3DXVECTOR3 Eye; // 視点
		D3DXVECTOR3 At; // 座標
		D3DXVECTOR3 Up; // ベクター
	};

	//==========================================================================
	//
	// class  : CVec3
	// Content: ベクトル
	//
	//==========================================================================
	class CVec3
	{
	public:
		D3DXVECTOR3 Up; // 上ベクトル
		D3DXVECTOR3 Front; // 前ベクトル
		D3DXVECTOR3 Right; //  右ベクトル
	};

	//==========================================================================
	//
	// class  : CObjInfo
	// Content: オブジェクトの情報
	//
	//==========================================================================
	class CObjInfo
	{
	public:
		D3DXVECTOR3 pos; // 平行
		D3DXVECTOR3 rot; // 回転
		D3DXVECTOR3 sca; // スケール
	};

	//==========================================================================
	//
	// class  : CVertex
	// Content: 向きベクトル情報
	//
	//==========================================================================
	class CVertex
	{
	public:
		D3DXVECTOR3 pos; // 座標変換が必要
		D3DCOLOR color; // ポリゴンの色
	};
private:
	//==========================================================================
	//
	// struct : ANGLE
	// Content: 回転情報のテンプレート
	//
	//==========================================================================
	struct ANGLE
	{
		static constexpr float	_ANGLE_000 = 0.00000000f; // 0度
		static constexpr float	_ANGLE_045 = 0.785398185f; // 45度
		static constexpr float	_ANGLE_090 = 1.57079637f; // 90度
		static constexpr float	_ANGLE_135 = 2.35619450f; // 135度
		static constexpr float	_ANGLE_180 = 3.14159274f; // 180度
		static constexpr float	_ANGLE_225 = -2.35619450f; // 225度
		static constexpr float	_ANGLE_270 = -1.57079637f; // 270度
		static constexpr float	_ANGLE_315 = -0.785398185f; // 315度

	}m_ANGLE;
public:
	C3DObject();
	~C3DObject();
	// 初期化
	// index = 使用するオブジェクトの番号
	void Init(int index);

	// 解放
	void Uninit(void);

	// 描画
	// NumLine = 線の細かさ
	// pDevice = デバイスをセットしてください
	void Draw(int NumLine, LPDIRECT3DDEVICE9 pDevice);

	// X軸回転
	// rot = 入力した値が加算されます
	void RotX(float rot);

	// Y軸回転
	// rot = 入力した値が加算されます
	void RotY(float rot);

	// 平行移動
	// speed = 入力した値が加算されます
	void MoveZ(float speed);

	// 平行移動
	// speed = 入力した値が加算されます
	void MoveX(float speed);

	// 平行移動
	// speed = 入力した値が加算されます
	void MoveY(float speed);

	// スケール
	// speed = 入力した値が加算されます
	void Scale(float speed);

	// ベクトルを使った行列
	const D3DXMATRIX *CreateMtxWorld1(D3DXMATRIX * pInOut);

	// 直接値を入れる行列
	const D3DXMATRIX *CreateMtxWorld2(D3DXMATRIX * pInOut);

	// ベクトルを使ったカメラの方向を向かせる行列
	const D3DXMATRIX *CreateMtxWorld1(D3DXMATRIX * pInOut, D3DXMATRIX *pMtxView);

	// 直接値を入れるカメラの方向を向かせる行列
	const D3DXMATRIX *CreateMtxWorld2(D3DXMATRIX * pInOut, D3DXMATRIX *pMtxView);

	// 角度テンプレート
	ANGLE GetAngle(void) { return this->m_ANGLE; }

	// ラジコン回転
	// vecRight = 向きベクトル
	// speed = 入力した値が加算されます
	void RadioControl(D3DXVECTOR3 vecRight, float speed);

	// 前ベクトル
	// Input = 向きベクトル
	void SetVecFront(D3DXVECTOR3 Input);

	// 上ベクトル
	// Input = 向きベクトル
	void SetVecUp(D3DXVECTOR3 Input);

	// 右ベクトル
	// Input = 向きベクトル
	void SetVecRight(D3DXVECTOR3 Input);

	// 対象の方向に向かせる
	// Input = ターゲットのC3DObject
	void LockOn(const C3DObject * Input);

	// インデックス情報の取得 
	int getindex(void) { return m_index; }

	// インデックスの変更
	void setindex(int nindex) { this->m_index = nindex; }
private:
	// 回転の制限
	bool Restriction(D3DXMATRIX pRot, const float * pRang);

	// 回転行列
	const D3DXMATRIX* CalcLookAtMatrix(D3DXMATRIX* pOut);

	// 回転行列
	const D3DXMATRIX* CalcLookAtMatrixAxisFix(D3DXMATRIX* pOut);
public:
	CMatLook Look; // 視覚情報
	CVec3 Vec; // ベクトル
	CObjInfo Info; // オブジェクトの情報
	CColor<int> * Collar = nullptr; // 色
private:
	int m_index; // データ
	CVertex *m_RoundX;
	CVertex *m_RoundY;
	CVertex *m_lineX;
	CVertex *m_lineY;
	CVertex *m_lineZ;
};

#endif // !_3DObject_H_
