//==========================================================================
// モデルデータ[Model.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _Model_H_
#define _Model_H_

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>
#include <vector>
#include "3DObject.h"
#include "Template.h"
#include "DXDevice.h" 
#include "TextureLoader.h"

//==========================================================================
//
// class  : CXmodel
// Content: Xモデル 
//
//==========================================================================
class CXmodel
{
public:
	// マテリアルライトの設定
	enum class LightMoad
	{
		System, // システム設定
		Material // マテリアル設定
	};
private:
	//==========================================================================
	//
	// class  : CXdata
	// Content: Xデータ管理 
	//
	//==========================================================================
	class CXdata
	{
	public:
		CXdata();
		~CXdata();
		// tagのセット
		void settag(const char * ptag);
		// tagの取得
		const char * gettag(void) { return this->m_tag; }
		// コピー
		void copy(CXdata * pinp);
		// 読み込み
		HRESULT load(LightMoad mode, LPDIRECT3DDEVICE9 pDevice);
		// 解放
		void release(void);
		// 描画
		void draw(LPDIRECT3DDEVICE9 pDevice);
	private:
		// 読み込み
		HRESULT read(LPD3DXBUFFER * pAdjacency, LPD3DXBUFFER * pMaterialBuffer, LPDIRECT3DDEVICE9 pDevice);
		// 最適化
		HRESULT optimisation(LPD3DXBUFFER pAdjacency);
		// 頂点の宣言
		HRESULT declaration(D3DVERTEXELEMENT9 *pElements);
		// 複製
		HRESULT replication(D3DVERTEXELEMENT9 *pElements, LPD3DXMESH * pTempMesh, LPDIRECT3DDEVICE9 pDevice);
		// テクスチャの読み込み
		const LPSTR loadtexture(const LPD3DXMATERIAL Input);
		// マテリアルの設定
		void materialsetting(const LPD3DXMATERIAL Input);
		// マテリアルの設定 システム設定
		void materialsetting_system(D3DMATERIAL9 *pMatMesh);
		// マテリアルの設定 マテリアル素材設定
		void materialsetting_material(D3DMATERIAL9 *pMatMesh, const D3DMATERIAL9 *Input);
	private:
		LightMoad m_MaterialMood; // マテリアルのモード
		LPD3DXMESH m_Mesh; // メッシュデータ
		DWORD m_NumMaterial; // マテリアル数
		D3DMATERIAL9 *m_MaterialMesh; // マテリアル情報
		CTextureLoader m_texture; // テクスチャの格納
		char * m_tag; // タグ
		bool m_Lock; // オリジナルデータ判定
		CTemplates m_temp;
	};
public:
	CXmodel();
	~CXmodel();

	// 初期化
	// Input = 使用するテクスチャのパス
	// mode = マテリアルのライティングの設定
	bool Init(const char * Input, LightMoad mode = LightMoad::System);

	// 初期化
	// Input = 使用するテクスチャのパス ダブルポインタに対応
	// num = 要素数
	// mode = マテリアルのライティングの設定
	bool Init(const char ** Input, int num, LightMoad mode = LightMoad::System);

	// 解放
	void Uninit(void);

	// 描画
	// Input = 座標クラス(C3DObject)をアドレス渡し 
	void Draw(C3DObject * Input);

	// モデルデータのパラメーターゲッター
	// Input = (C3DObject)アドレス渡し 
	CXdata* GetMdelDataParam(C3DObject * Input) { return m_data[Input->getindex()]; }
private:
	// 生成
	CXdata * create(CXdata *& pinp, const char * Input);
private:
	std::vector<CXdata*> m_data; // モデルデータ管理
};

#endif // !_Model_H_
