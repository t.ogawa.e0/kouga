//==========================================================================
// マウスヘッダ[mouse.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _MOUSE_H_ 
#define _MOUSE_H_

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#define DIRECTINPUT_VERSION (0x0800) // DirectInputのバージョン指定
#include<dinput.h>
#pragma comment(lib,"dinput8.lib")
#pragma comment(lib,"dxguid.lib")

//==========================================================================
//
// class  : CMouse
// Content: マウス
//
//==========================================================================
class CMouse
{
private:
	class CSpeed
	{
	public:
		LONG m_lX; // X軸への速度
		LONG m_lY; // Y軸への速度
		LONG m_lZ; // Z軸への速度
	};
public:
	// 有効ボタン
	enum class ButtonKey
	{
		Left = 0,	// 左クリック
		Right,	// 右クリック
		Wheel,	// ホイール
	};
public:
	CMouse();
	~CMouse();
	// 初期化
	static bool Init(HINSTANCE hInstance, HWND hWnd);

	// 終了処理
	static void Uninit(void);

	// 更新処理
	static void Update(void);

	// Button Cast(int)
	static ButtonKey KeyCast(int cast) { return (ButtonKey)cast; };

	// プレス
	static bool Press(ButtonKey key);

	// トリガー
	static bool Trigger(ButtonKey key);

	// リピート
	static bool Repeat(ButtonKey key);

	// リリ−ス
	static bool Release(ButtonKey key);

	// マウスの速度
	static CSpeed Speed(void);

	// カーソルの座標
	static POINT WIN32Cursor(void);

	// 左クリック
	static SHORT WIN32LeftClick(void);

	// 右クリック
	static SHORT WIN32RightClick(void);

	// マウスホイールホールド
	static SHORT WIN32WheelHold(void);
private:
	static BOOL CALLBACK EnumAxesCallback(const DIDEVICEOBJECTINSTANCE*, VOID*);
private: // DirectInput
	static LPDIRECTINPUT8 m_pDInput; // DirectInputオブジェクト
	static LPDIRECTINPUTDEVICE8 m_pDIDevice; // 入力デバイス(コントローラー)へのポインタ
	static DIMOUSESTATE2 m_State; // 入力情報ワーク
	static BYTE m_StateTrigger[(int)sizeof(DIMOUSESTATE2::rgbButtons)];	// トリガー情報ワーク
	static BYTE m_StateRelease[(int)sizeof(DIMOUSESTATE2::rgbButtons)]; // リリース情報ワーク
	static BYTE m_StateRepeat[(int)sizeof(DIMOUSESTATE2::rgbButtons)]; // リピート情報ワーク
	static int m_StateRepeatCnt[(int)sizeof(DIMOUSESTATE2::rgbButtons)]; // リピートカウンタ
private:
	static constexpr int m_LimitCounter = 20;
private: // WindowsAPI
	static POINT m_mousePos;
	static HWND m_hWnd;
};

#endif // !_MOUSE_H_
