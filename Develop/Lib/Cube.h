//==========================================================================
// キューブ[Cube.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _Cube_H_
#define _Cube_H_

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>
#include "Rectangle.h"
#include "Vertex3D.h"
#include "3DObject.h"
#include "Template.h"
#include "DataType.h"
#include "TextureLoader.h"
#include "DXDevice.h"
#include "Create.h"

//==========================================================================
//
// class  : CCube
// Content: キューブ 
//
//==========================================================================
class CCube : private CRectangle, private VERTEX_3D, private CCreate
{
private:
	static constexpr int nBite = 256; // バイト数
	static constexpr int nMaxIndex = 36; // 
	static constexpr int nNumVertex = 24; // 頂点数
	static constexpr int nNumTriangle = 12; // 三角形の数
	static constexpr float VCRCT = 0.5f;
private:
	struct DrawType
	{
		enum List
		{
			MtxWorld1,
			MtxWorld2,
		};
	};

	// テクスチャ情報格納
	class TEXTURE
	{
	public:
		int Direction1; // 一行の画像数
		int Direction2; // 一列の画像数
		int Pattern; // 面の数
		CTexvec<float> texsize; // テクスチャのサイズ
		CTexvec<float> texcutsize; // テクスチャの切り取りサイズ
	};
public:
	typedef DrawType::List DrawTypeName;
public:
	CCube();
	~CCube();

	// 初期化 失敗時true
	// Input = 使用するテクスチャのパス ダブルポインタに対応
	// size = 要素数 
	bool Init(const char** Input, int size);

	// 初期化 失敗時true
	// Input = 使用するテクスチャのパス
	bool Init(const char* Input);

	// 解放
	void Uninit(void);

	// 描画
	// Input = C3DObjectのアドレス渡し
	// DrawType = ワールド行列の種類の指定をします
	void Draw(C3DObject * Input, DrawTypeName DrawType = DrawTypeName::MtxWorld2);
private:
	// キューブ生成
	void SetCube(VERTEX_4 * Output, const CUv<float> * _this);

	// UV生成
	void SetUV(const TEXTURE * Input, CUv<float> * Output, const int nNum);
private: // 格納
	CTextureLoader m_texture; // テクスチャの格納
	D3DMATERIAL9 m_aMat; // マテリアルの情報
	LPDIRECT3DVERTEXBUFFER9 m_pVertexBuffer; // バッファ
	LPDIRECT3DINDEXBUFFER9 m_pIndexBuffer; // インデックスバッファ
	CTemplates m_temp; // テンプレート
	bool m_Lock; // バッファロック
};


#endif // !_Cube_H_
