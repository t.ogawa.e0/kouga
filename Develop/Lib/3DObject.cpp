//==========================================================================
// オブジェクト[3DObject.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "3DObject.h"

C3DObject::C3DObject()
{
	this->Collar = nullptr;
}

C3DObject::~C3DObject()
{
	if (this->Collar != nullptr)
	{
		delete[]this->Collar;
		this->Collar = nullptr;
	}
}

//==========================================================================
// 初期化
// index = 使用するオブジェクトの番号
void C3DObject::Init(int index)
{
	this->m_index = index;

	this->Info.pos = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	this->Info.rot = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	this->Info.sca = D3DXVECTOR3(1.0f, 1.0f, 1.0f);

	this->Look.Eye = D3DXVECTOR3(0.0f, 0.0f, -2.0f); // 視点
	this->Look.At = D3DXVECTOR3(0.0f, 0.0f, 0.0f); // 座標
	this->Look.Up = D3DXVECTOR3(0.0f, 1.0f, 0.0f); // ベクター

	this->Vec.Up = D3DXVECTOR3(0, 1, 0); // 上ベクトル
	this->Vec.Front = D3DXVECTOR3(0, 0, 1); // 前ベクトル
	this->Vec.Right = D3DXVECTOR3(1, 0, 0); //  右ベクトル

	// ベクトルの正規化
	D3DXVec3Normalize(&this->Vec.Front, &this->Vec.Front);
	D3DXVec3Normalize(&this->Vec.Up, &this->Vec.Up);
	D3DXVec3Normalize(&this->Vec.Right, &this->Vec.Right);
}

//==========================================================================
// 解放
void C3DObject::Uninit(void)
{
	if (this->Collar != nullptr)
	{
		delete[]this->Collar;
		this->Collar = nullptr;
	}
}

//==========================================================================
// 描画
// NumLine = 線の細かさ
// pDevice = デバイスをセットしてください
void C3DObject::Draw(int NumLine, LPDIRECT3DDEVICE9 pDevice)
{
#if defined(_DEBUG) || defined(DEBUG)
	// 向きベクトル情報
	const float fPosZ[] = { 1.0f,-1.0f,-0.8f,-0.8f ,-1.0f };
	const float fPosX[] = { 0.0f,0.0f,0.1f,-0.1f,0.0f };
	const int nNumSize = sizeof(fPosZ) / sizeof(float);

	float C_R = D3DX_PI * 2 / NumLine;	// 円周率
	D3DXMATRIX aMtxWorld; // ワールド行列

	// メモリの確保
	this->m_RoundX = new CVertex[(NumLine + 1)];
	this->m_RoundY = new CVertex[(NumLine + 1)];
	this->m_lineX = new CVertex[nNumSize];
	this->m_lineY = new CVertex[nNumSize];
	this->m_lineZ = new CVertex[nNumSize];

	// 円の生成
	for (int i = 0; i < (NumLine + 1); i++)
	{
		this->m_RoundX[i].pos.x = ((0.0f + cosf(C_R * i)));
		this->m_RoundX[i].pos.y = 0.0f;
		this->m_RoundX[i].pos.z = ((0.0f + sinf(C_R * i)));
		this->m_RoundX[i].color = D3DCOLOR_RGBA(255, 0, 0, 255);

		this->m_RoundY[i].pos.x = 0.0f;
		this->m_RoundY[i].pos.y = ((0.0f + cosf(C_R * i)));
		this->m_RoundY[i].pos.z = ((0.0f + sinf(C_R * i)));
		this->m_RoundY[i].color = D3DCOLOR_RGBA(0, 255, 0, 255);
	}

	// 矢印の生成
	for (int i = 0; i < nNumSize; i++)
	{
		this->m_lineX[i].pos.z = fPosX[i];
		this->m_lineX[i].pos.x = -fPosZ[i];
		this->m_lineX[i].pos.y = 0.0f;
		this->m_lineX[i].color = D3DCOLOR_RGBA(255, 0, 0, 255);

		this->m_lineY[i].pos.z = fPosX[i];
		this->m_lineY[i].pos.x = 0.0f;
		this->m_lineY[i].pos.y = -fPosZ[i];
		this->m_lineY[i].color = D3DCOLOR_RGBA(0, 255, 0, 255);

		this->m_lineZ[i].pos.z = fPosZ[i];
		this->m_lineZ[i].pos.x = fPosX[i];
		this->m_lineZ[i].pos.y = 0.0f;
		this->m_lineZ[i].color = D3DCOLOR_RGBA(0, 0, 255, 255);
	}

	// FVFの設定
	pDevice->SetFVF(D3DFVF_XYZ | D3DFVF_DIFFUSE);

	// ライト設定
	pDevice->SetRenderState(D3DRS_LIGHTING, FALSE);

	// サイズ
	pDevice->SetTransform(D3DTS_WORLD, this->CreateMtxWorld1(&aMtxWorld));
	pDevice->SetTexture(0, nullptr);

	// 描画
	pDevice->DrawPrimitiveUP(D3DPT_LINESTRIP, NumLine, this->m_RoundX, sizeof(CVertex));
	pDevice->DrawPrimitiveUP(D3DPT_LINESTRIP, NumLine, this->m_RoundY, sizeof(CVertex));
	pDevice->DrawPrimitiveUP(D3DPT_LINESTRIP, (nNumSize - 1), this->m_lineZ, sizeof(CVertex));
	pDevice->DrawPrimitiveUP(D3DPT_LINESTRIP, (nNumSize - 1), this->m_lineX, sizeof(CVertex));
	pDevice->DrawPrimitiveUP(D3DPT_LINESTRIP, (nNumSize - 1), this->m_lineY, sizeof(CVertex));

	// メモリの解放
	delete[]this->m_RoundX;
	this->m_RoundX = nullptr;
	delete[]this->m_RoundY;
	this->m_RoundY = nullptr;
	delete[]this->m_lineZ;
	this->m_lineZ = nullptr;
	delete[]this->m_lineX;
	this->m_lineX = nullptr;
	delete[]this->m_lineY;
	this->m_lineY = nullptr;
#else
	NumLine;
	pDevice;
#endif
}

//==========================================================================
// X軸回転
// rot = 入力した値が加算されます
void C3DObject::RotX(float rot)
{
	D3DXMATRIX RotX; //回転行列
	D3DXVECTOR3 Direction;

	D3DXMatrixRotationY(&RotX, rot); // 回転
	D3DXVec3Cross(&this->Vec.Right, &this->Vec.Up, &this->Vec.Front); // 外積
	Direction = (this->Look.Eye) - (this->Look.At); // 向きベクトル	
	D3DXVec3TransformNormal(&Direction, &Direction, &RotX);
	D3DXVec3TransformNormal(&this->Vec.Front, &this->Vec.Front, &RotX);
	D3DXVec3TransformNormal(&this->Vec.Right, &this->Vec.Right, &RotX);
	this->Look.Eye = (this->Look.At) + (Direction);
}

//==========================================================================
// Y軸回転
// rot = 入力した値が加算されます
void C3DObject::RotY(float rot)
{
	D3DXMATRIX RotY; //回転行列
	D3DXVECTOR3 Direction;

	if (this->Restriction(RotY, &rot))
	{
		D3DXVec3Cross(&this->Vec.Right, &this->Vec.Up, &this->Vec.Front); // 外積
		D3DXMatrixRotationAxis(&RotY, &this->Vec.Right, rot); // 回転
		Direction = (this->Look.Eye) - (this->Look.At); // 向きベクトル
		D3DXVec3TransformNormal(&Direction, &Direction, &RotY);
		D3DXVec3TransformNormal(&this->Vec.Front, &this->Vec.Front, &RotY);
		D3DXVec3TransformNormal(&this->Vec.Right, &this->Vec.Right, &RotY);
		this->Look.Eye = (this->Look.At) + (Direction);
	}
}

//==========================================================================
// 平行移動
// speed = 入力した値が加算されます
void C3DObject::MoveZ(float speed)
{
	D3DXVec3Normalize(&this->Vec.Front, &this->Vec.Front);
	this->Info.pos += this->Vec.Front*speed;
}

//==========================================================================
// 平行移動
// speed = 入力した値が加算されます
void C3DObject::MoveX(float speed)
{
	D3DXVec3Normalize(&this->Vec.Right, &this->Vec.Right);
	this->Info.pos += this->Vec.Right*speed;
}

//==========================================================================
// 平行移動
// speed = 入力した値が加算されます
void C3DObject::MoveY(float speed)
{
	D3DXVec3Normalize(&this->Vec.Up, &this->Vec.Up);
	this->Info.pos += this->Vec.Up*speed;
}

//==========================================================================
// スケール
void C3DObject::Scale(float speed)
{
	this->Info.sca += D3DXVECTOR3(speed, speed, speed);
}

//==========================================================================
// ベクトルを使った行列
const D3DXMATRIX * C3DObject::CreateMtxWorld1(D3DXMATRIX * pInOut)
{
	D3DXMATRIX aMtxRot;
	D3DXMATRIX aMtxTra;
	D3DXMATRIX aMtxSca;

	// 平行
	D3DXMatrixTranslation(&aMtxTra, this->Info.pos.x, this->Info.pos.y, this->Info.pos.z);

	// スケール
	D3DXMatrixScaling(&aMtxSca, this->Info.sca.x, this->Info.sca.y, this->Info.sca.z);

	// 回転
	this->CalcLookAtMatrix(&aMtxRot);

	D3DXMatrixIdentity(pInOut);

	// 平行の合成
	D3DXMatrixMultiply(pInOut, &aMtxTra, pInOut);

	// スケールの合成
	D3DXMatrixMultiply(pInOut, &aMtxSca, pInOut);

	// 回転の合成
	D3DXMatrixMultiply(pInOut, &aMtxRot, pInOut);

	return pInOut;
}

//==========================================================================
// 直接値を入れる行列
const D3DXMATRIX * C3DObject::CreateMtxWorld2(D3DXMATRIX * pInOut)
{
	D3DXMATRIX aMtxRot;
	D3DXMATRIX aMtxTra;
	D3DXMATRIX aMtxSca;

	// 平行
	D3DXMatrixTranslation(&aMtxTra, this->Info.pos.x, this->Info.pos.y, this->Info.pos.z);

	// スケール
	D3DXMatrixScaling(&aMtxSca, this->Info.sca.x, this->Info.sca.y, this->Info.sca.z);

	// 回転
	D3DXMatrixRotationYawPitchRoll(&aMtxRot, this->Info.rot.y, this->Info.rot.x, this->Info.rot.z);

	D3DXMatrixIdentity(pInOut);

	// 平行の合成
	D3DXMatrixMultiply(pInOut, &aMtxTra, pInOut);

	// スケールの合成
	D3DXMatrixMultiply(pInOut, &aMtxSca, pInOut);

	// 回転の合成
	D3DXMatrixMultiply(pInOut, &aMtxRot, pInOut);

	return pInOut;
}

//==========================================================================
// ベクトルを使ったカメラの方向を向かせる行列
const D3DXMATRIX * C3DObject::CreateMtxWorld1(D3DXMATRIX * pInOut, D3DXMATRIX *pMtxView)
{
	D3DXMATRIX MtxView = *pMtxView;
	this->CreateMtxWorld1(pInOut);

	// 各種行列の設定
	D3DXMatrixTranspose(&MtxView, &MtxView);
	MtxView._14 = 0.0f;
	MtxView._24 = 0.0f;
	MtxView._34 = 0.0f;

	D3DXMatrixMultiply(pInOut, &MtxView, pInOut); // 行列の合成

	return pInOut;
}

//==========================================================================
// 直接値を入れるカメラの方向を向かせる行列
const D3DXMATRIX * C3DObject::CreateMtxWorld2(D3DXMATRIX * pInOut, D3DXMATRIX * pMtxView)
{
	D3DXMATRIX MtxView = *pMtxView;
	this->CreateMtxWorld2(pInOut);

	// 各種行列の設定
	D3DXMatrixTranspose(&MtxView, &MtxView);
	MtxView._14 = 0.0f;
	MtxView._24 = 0.0f;
	MtxView._34 = 0.0f;

	D3DXMatrixMultiply(pInOut, &MtxView, pInOut); // 行列の合成

	return pInOut;
}

//==========================================================================
// ラジコン回転
// vecRight = 向きベクトル
// speed = 入力した値が加算されます
void C3DObject::RadioControl(D3DXVECTOR3 vecRight, float speed)
{
	D3DXVECTOR3 vecCross;

	D3DXVec3Cross(&vecCross, &this->Vec.Front, &vecRight);
	this->RotX((vecCross.y < 0.0f ? -0.01f : 0.01f)*speed);
}

//==========================================================================
// 前ベクトル
// Input = 向きベクトル
void C3DObject::SetVecFront(D3DXVECTOR3 Input)
{
	this->Vec.Front = Input;
}

//==========================================================================
// 上ベクトル
// Input = 向きベクトル
void C3DObject::SetVecUp(D3DXVECTOR3 Input)
{
	this->Vec.Up = Input;
}

//==========================================================================
//  右ベクトル
// Input = 向きベクトル
void C3DObject::SetVecRight(D3DXVECTOR3 Input)
{
	this->Vec.Right = Input;
}

//==========================================================================
// 対象の方向に向かせる
// Input = ターゲットのC3DObject
void C3DObject::LockOn(const C3DObject * Input)
{
	D3DXVECTOR3 VecTor = D3DXVECTOR3(0, 0, 0);

	VecTor = Input->Info.pos - this->Info.pos;
	D3DXVec3Normalize(&VecTor, &VecTor);
	this->Vec.Front = VecTor;
	this->Look.Eye = -VecTor;
}

//==========================================================================
// 回転の制限
bool C3DObject::Restriction(D3DXMATRIX pRot, const float * pRang)
{
	D3DXVECTOR3 dir = D3DXVECTOR3(0, 1.0f, 0); // 単位ベクトル
	float Limit = 0.75f;
	D3DXVECTOR3 pVecRight, pVecUp, pVecFront;

	pVecRight = this->Vec.Right;
	pVecUp = this->Vec.Up;
	pVecFront = this->Vec.Front;

	// ベクトルの座標変換
	D3DXVec3Cross(&pVecRight, &pVecUp, &pVecFront); // 外積
	D3DXMatrixRotationAxis(&pRot, &pVecRight, *pRang); // 回転
	D3DXVec3TransformNormal(&pVecFront, &pVecFront, &pRot);
	float fVec3Dot = atanf(D3DXVec3Dot(&pVecFront, &dir));

	// 内積
	if (-Limit<fVec3Dot && Limit>fVec3Dot) { return true; }

	return false;
}

//==========================================================================
// 回転行列
const D3DXMATRIX* C3DObject::CalcLookAtMatrix(D3DXMATRIX* pOut)
{
	D3DXVECTOR3 X, Y, Z;

	Z = this->Look.Eye - this->Look.At;
	D3DXVec3Normalize(&Z, &Z);
	D3DXVec3Cross(&X, D3DXVec3Normalize(&Y, &this->Look.Up), &Z);
	D3DXVec3Normalize(&X, &X);
	D3DXVec3Normalize(&Y, D3DXVec3Cross(&Y, &Z, &X));

	pOut->_11 = X.x; pOut->_12 = X.y; pOut->_13 = X.z; pOut->_14 = 0;
	pOut->_21 = Y.x; pOut->_22 = Y.y; pOut->_23 = Y.z; pOut->_24 = 0;
	pOut->_31 = Z.x; pOut->_32 = Z.y; pOut->_33 = Z.z; pOut->_34 = 0;
	pOut->_41 = 0.0f; pOut->_42 = 0.0f; pOut->_43 = 0.0f; pOut->_44 = 1.0f;

	return pOut;
}

//==========================================================================
// 回転行列
const D3DXMATRIX* C3DObject::CalcLookAtMatrixAxisFix(D3DXMATRIX* pOut)
{
	D3DXVECTOR3 X, Y, Z, D;
	D = this->Look.Eye - this->Look.At;
	D3DXVec3Normalize(&D, &D);
	D3DXVec3Cross(&X, D3DXVec3Normalize(&Y, &this->Look.Up), &D);
	D3DXVec3Normalize(&X, &X);
	D3DXVec3Normalize(&Z, D3DXVec3Cross(&Z, &X, &Y));

	pOut->_11 = X.x; pOut->_12 = X.y; pOut->_13 = X.z; pOut->_14 = 0;
	pOut->_21 = Y.x; pOut->_22 = Y.y; pOut->_23 = Y.z; pOut->_24 = 0;
	pOut->_31 = Z.x; pOut->_32 = Z.y; pOut->_33 = Z.z; pOut->_34 = 0;
	pOut->_41 = 0.0f; pOut->_42 = 0.0f; pOut->_43 = 0.0f; pOut->_44 = 1.0f;

	return pOut;
}