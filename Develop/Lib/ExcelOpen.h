//==========================================================================
// Excelオープン[ExcelOpen.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _ExcelOpen_H_
#define _ExcelOpen_H_

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <stdio.h>
#include <d3d9.h>
#include <d3dx9.h>
#include "DXDevice.h"

//==========================================================================
//
// class  : CExcelOpen
// Content: ExcelOpen
//
//==========================================================================
class CExcelOpen
{
private:
	static constexpr int m_MaxData = 256;
protected:
	class FILELINK1
	{
	public:
		char m_Data[m_MaxData] = { 0 }; // リンク格納
	};

	class FILELINK2
	{
	public:
		char m_Data[m_MaxData] = { 0 }; // リンク格納
		int m_NumMeshX = 0; // メッシュX
		int m_NumMeshZ = 0; // メッシュZ
	};
protected:
	CExcelOpen();
	~CExcelOpen();

	// ファイルパスのみ読み込み
	// 戻り値 bool
	bool OpenFile(const char* pFile, FILELINK1*& pOutput, int * Numoutput);

	// ファイルパス &面の数読み込み
	// 戻り値 bool
	bool OpenFile(const char* pFile, FILELINK2*& pOutput, int * Numoutput);
};

#endif // !_ExcelOpen_H_
