#ifndef _time_H_
#define _time_H_

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

class CTime : private CTemplate, public CObject
{
public:
	CTime();
	~CTime();
	// 初期化
	bool Init(void);
	// 解放
	void Uninit(void);
	// 更新
	void Update(void);
	// 描画
	void Draw(void);
private:
	 int m_GetTime; // 時間記憶
	 bool m_TimeUpKey;
	 C2DObject *m_pos; // 座標
	 C2DPolygon *m_poly; // ポリゴン
	 CTimer *m_Time; // タイム
	 CNumber * m_NumBer; // 数字の処理
};

#endif // !_time_H_
