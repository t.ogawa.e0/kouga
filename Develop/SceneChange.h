//==========================================================================
// シーン遷移[SceneChange.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _SCENECHANGE_H_
#define _SCENECHANGE_H_

//==========================================================================
// Include
//==========================================================================
#include <Windows.h>
#include "dxlib.h"

//==========================================================================
//
// class  : CBaseScene
// Content: ベースとなる継承用クラス
//
//==========================================================================
class CBaseScene
{
public:
	// 初期化
	virtual bool Init(void) = 0;
	// 解放
	virtual void Uninit(void) = 0;
	// 更新
	virtual void Update(void) = 0;
	// 描画
	virtual void Draw(void) = 0;
};

//==========================================================================
//
// class  : CSceneManager
// Content: 全てのシーンの管理
//
//==========================================================================
class CSceneManager
{
public:
	// シーンのリスト
	enum class SceneName
	{
		NOME = -1, // 無し
		Title, // タイトル
		Home, // ホーム
		Game, // ゲーム
		Result, // リザルト
		Screen_Saver, // スクリーンセーバー
		Practice, // チュートリアル
		Load, // ロード画面
		Default, // default
	};
public:
	CSceneManager();
	~CSceneManager();
	// シーンの切り替え
	bool ChangeScene(SceneName Name);
	// 解放
	void Uninit(void);
	// 更新
	void Update(void);
	// 描画
	void Draw(void);
private:
	// 初期化
	bool Init(void);
private:
	CBaseScene* m_pScene; // 今のシーン
};


#endif // !_SCENECHANGE_H_
