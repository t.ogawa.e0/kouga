//==========================================================================
// システムウィンドウ[SystemWindow.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _SystemWindow_H_
#define _SystemWindow_H_

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <stdio.h>
#include <vector>
#include "dxlib.h"
#include "resource.h"

//==========================================================================
// マクロ定義
//==========================================================================
#define WINDOW_STYLE (WS_OVERLAPPEDWINDOW& ~(WS_THICKFRAME | WS_MAXIMIZEBOX | WS_MINIMIZEBOX))
#define ID_BUTTON000 (10000)	//ボタンの認識ID
#define ID_BUTTON001 (10001)	//ボタンの認識ID
#define ID_BUTTON002 (10002)	//ボタンの認識ID

//==========================================================================
//
// class  : CSystemWindow
// Content: システムウィンドウ
//
//==========================================================================
class CSystemWindow
{
private:
	const char *Class_Name = "Config";
	const char *Window_Name = "Config";
	static constexpr int m_MainWidth = 502;
	static constexpr int m_MainHeight = 500;
private:
	class CWinSize
	{
	public:
		CWinSize() {};
		CWinSize(int _w, int _h)
		{
			this->w = _w;
			this->h = _h;
		}
	public:
		int w;
		int h;
	};
private:
	class CData
	{
	public:
		CData() {};
		CData(int serect, bool graphicmode, bool key)
		{
			this->m_graphicmode = graphicmode;
			this->m_serect = serect;
			this->m_key = key;
		}
	public:
		int m_serect;
		bool m_graphicmode;
		bool m_key;
	};
public:
	CSystemWindow();
	~CSystemWindow();
	// ウィンドウ生成
	void Window(WNDCLASSEX *wcex, HINSTANCE hInstance);
	// 更新
	void WindowUpdate(WNDCLASSEX *wcex, int nCmdShow);

	CData GetData(void) { return m_data; }

	void setwindata(int w_, int h_);
private:
	static void newtex(void);
	static void deletetex(void) { m_WindowMode.clear(); }
	// ボタン
	static void Button(HWND hWnd, LPARAM *lParam, int posx, int posy);
	// 設定
	static void Config(HDC * hDC, CTexvec<int> * vpos);
	// テキスト描画
	static void Text(HWND hWnd, HDC * hDC, LPCTSTR lpszStr, int posx, int posy);
	// イメージデータ表示場所
	static void ImgeData(HDC * hDC, CTexvec<int> * vpos);
	// テクスチャ読み込み
	static void LoadTex(LPARAM * lp);
	// テクスチャ描画
	static void DrawTex(HWND hWnd, HDC * hDC, CTexvec<int> * vpos);
	// ウインドウプロシージャ
	static LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
private:
	static HWND m_hWnd;
	static HWND m_combo;
	static HDC m_BackBufferDC;
	static HBITMAP m_BackBufferBMP;
	static HWND m_hWndButton10000;
	static HWND m_hWndButton10001;
	static HWND m_check;

	//ビットマップ
	static HBITMAP m_hBitmap;
	static BITMAP m_Bitmap;

	static CData m_data;

	static std::vector<CWinSize> m_WindowMode; // ディスプレイの解像度格納
};
#endif // !_SystemWindow_H_
