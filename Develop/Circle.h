//==========================================================================
// 召喚サークル[Circle.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _Circle_H_
#define _Circle_H_

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "SummonsKey.h"

class CCircle : public CObject
{
private:
	static constexpr float m_MoveSpeed = 0.2f; // 移動速度?
	static constexpr int m_Updateframe = 60; // 移動速度?
public:
	CCircle();
	~CCircle();
	// 初期化
	bool Init(void);
	// 解放
	void Uninit(void);
	// 更新
	void Update(void);
	// 描画
	void Draw(void);
	// ゲッター
	static C3DObject * Get(void) { return m_pos; }
	// 召喚キーセット
	static void setsummonkey(CSummonsKey::KeyList input);
private:
	// 移動
	void Move(void);
	// 前進
	CVector<float> Advance(void);
	// 後進
	CVector<float> Backward(void);
	// 右
	CVector<float> Right(void);
	// 左
	CVector<float> Left(void);
	// 右前進
	CVector<float> AdvanceRight(void);
	// 左前進
	CVector<float> AdvanceLeft(void);
	// 右後進
	CVector<float> BackwardRight(void);
	// 左後進
	CVector<float> BackwardLeft(void);
private:
	CBillboard * m_poly;
	C3DObject * m_movepos;
	int m_count;
	int m_count2;
	bool m_drawserect;
	static C3DObject * m_pos;
	static CSummonsKey::KeyList m_Inputkey;
	static int m_serect;
	static int m_serectdrawcount;
	CHitDetermination m_hit;
	CTemplates m_temp;
};

#endif // !_Circle_H_
