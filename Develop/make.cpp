//==========================================================================
// 制作用シーン[make.h]
// author: tatsuya ogawa
//==========================================================================
#include "make.h"
#include "test.h"

//==========================================================================
// 実体
//==========================================================================
CCamera CMake::m_camera;

CMake::CMake()
{
}

CMake::~CMake()
{
}

bool CMake::Init(void)
{
	D3DXVECTOR3 Eye = D3DXVECTOR3(0.0f, 3.0f, -10.0f); // 注視点
	D3DXVECTOR3 At = D3DXVECTOR3(0.0f, 3.0f, 0.0f); // カメラ座標

	this->m_camera.Init(&Eye, &At);

	this->m_light.Init({ 0, -1, 1 });

	CTest * ptest;

	CObject::NewObject(ptest);

	return CObject::InitAll();
}

void CMake::Uninit(void)
{
	this->m_camera.Uninit();
	CObject::UninitAll();
}

void CMake::Update(void)
{
	if (CKeyboard::Press(CKeyboard::KeyList::KEY_UP))
	{
		this->m_camera.DistanceFromView(0.05f);
	}
	else if (CKeyboard::Press(CKeyboard::KeyList::KEY_DOWN))
	{
		this->m_camera.DistanceFromView(-0.05f);
	}

	if (CKeyboard::Press(CKeyboard::KeyList::KEY_LEFT))
	{
		this->m_camera.RotViewX(0.05f);
	}
	else if (CKeyboard::Press(CKeyboard::KeyList::KEY_RIGHT))
	{
		this->m_camera.RotViewX(-0.05f);
	}
	else
	{
		this->m_camera.RotViewX(0.0025f);
	}
	CObject::UpdateAll();
}

void CMake::Draw(void)
{
	CObject::DrawAll();
}
