//==========================================================================
// �����L�[[SummonsKey.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _SummonsKey_H_
#define _SummonsKey_H_

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

class CSummonsKey : private CTemplate, public CObject
{
public:
	enum class KeyList
	{
		start = -1,
		RightButton1,	// �� �V�J�N
		RightButton2,	// �w �o�c
		RightButton3,	// �Z �}��
		RightButton4,	// �� �T���J�N
		max,
	};
public:
	static constexpr float m_chargelimit = 42.0f;
public:
	CSummonsKey();
	~CSummonsKey();
	// ������
	bool Init(void);
	// ���
	void Uninit(void);
	// �X�V
	void Update(void);
	// �`��
	void Draw(void);

	static void CSummonsKey::SummonsSetkey(KeyList key)
	{
		m_summon = key;
	}

	static bool CSummonsKey::SummonsGetkey(KeyList key)
	{
		return m_key[(int)key];
	}

private:
	C2DObject *m_pos;
	C2DPolygon *m_poly;
	int m_numdata;

	C2DObject *m_pos2;
	C2DPolygon *m_poly2;
	int m_numdata2;

	static KeyList m_summon;
	static bool m_key[(int)KeyList::max];
	
	float m_charge[(int)KeyList::max];

	CImGui_Dx9 m_Imgui;
};

#endif // !_SummonsKey_H_
