//==========================================================================
// タイトルロゴ[titlelogo.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _titlelogo_h_
#define _titlelogo_h_

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

//==========================================================================
//
// class  : CTitlelogo
// Content: タイトルロゴ
//
//==========================================================================
class CTitlelogo : public CObject
{
public:
	CTitlelogo();
	~CTitlelogo();
	// 初期化
	bool Init(void);
	// 解放
	void Uninit(void);
	// 更新
	void Update(void);
	// 描画
	void Draw(void);

	static bool getkey(void) { return m_key; }
private:
	C2DPolygon * m_poly; // ぽりごん
	C2DObject * m_pos; // 座標
	C2DObject * m_fadepos; // フェード座標
	int m_fadeα; // α値
	int m_logoα; // α値
	int m_frame;
	CImGui_Dx9 m_Imgui;
	CTemplates m_temp;
	static bool m_key;
};

#endif // !_titlelogo_h_
