//==========================================================================
// 失敗演出[failureUI.h]
// author : 
//==========================================================================
#ifndef _failureUI_h_
#define _failureUI_h_

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

//==========================================================================
//
// class  : CFailureUI
// Content: 失敗演出
//
//==========================================================================
class CFailureUI : private CTemplate, public CObject
{
public:
	CFailureUI();
	~CFailureUI();
	// 初期化
	bool Init(void);
	// 解放
	void Uninit(void);
	// 更新
	void Update(void);
	// 描画
	void Draw(void);
	// 処理のアクティブ化
	static void ActiveKey(void) { m_key = true; }
private:
	C2DObject *m_pos; // 座標
	C2DObject *m_masterpos; // マスター座標
	C2DPolygon *m_poly; // ポリゴン
	int m_numdata; // データ数
	int m_AnimCount; //フレームカウント変数
	bool m_key2;
	static bool m_key; // 鍵
};

#endif // !_failureUI_h_
