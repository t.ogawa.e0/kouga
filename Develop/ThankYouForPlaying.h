//==========================================================================
// ThankYouForPlaying[ThankYouForPlaying.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _ThankYouForPlaying_H_
#define _ThankYouForPlaying_H_

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

//==========================================================================
//
// class  : CThankYouForPlaying
// Content: ThankYouForPlaying
//
//==========================================================================
class CThankYouForPlaying : private CTemplate, public CObject
{
public:
	CThankYouForPlaying();
	~CThankYouForPlaying();
	// 初期化
	bool Init(void);
	// 解放
	void Uninit(void);
	// 更新
	void Update(void);
	// 描画
	void Draw(void);

	static void set(void) { m_key = true; }
private:
	 C2DObject * m_Pos; // 座標
	 C2DPolygon * m_Poly; // ポリゴン
	 int m_Num; // 枚数カウント
	 int m_αcount; // α値カウンタ
	 int m_Count; // カウンタ

	 static bool m_key;
};

#endif // !_ThankYouForPlaying_H_
