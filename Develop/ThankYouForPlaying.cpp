//==========================================================================
// ThankYouForPlaying[ThankYouForPlaying.cpp]
// author: tatuya ogawa
//==========================================================================
#include "ThankYouForPlaying.h"
#include "Screen.h"

//==========================================================================
// ����
//==========================================================================
bool CThankYouForPlaying::m_key;

CThankYouForPlaying::CThankYouForPlaying()
{
	this->m_Pos = nullptr;
	this->m_Poly = nullptr;
	this->m_Num = 0;
	this->m_��count = 0;
	this->m_Count = 0;
}

CThankYouForPlaying::~CThankYouForPlaying()
{
}

//==========================================================================
// ������
bool CThankYouForPlaying::Init(void)
{
	const char * pTexLink[] =
	{
		"resource/texture/ColorTex.DDS",
		"resource/texture/result/ThankYouForPlaying.DDS",
	};

	this->m_Num = (int)this->Sizeof(pTexLink);
	this->New(this->m_Pos, this->m_Num);
	this->New(this->m_Poly);

	for (int i = 0; i < this->m_Num; i++)
	{
		this->m_Pos[i].Init(i);
		this->m_Pos[i].SetColor(0, 0, 0, 0);
	}

	this->m_Pos[1].SetCentralCoordinatesMood(true);
	this->m_Pos[1].SetX((float)(CDirectXDevice::GetWindowsSize().m_Width / 2));
	this->m_Pos[1].SetY((float)(CDirectXDevice::GetWindowsSize().m_Height / 2));

	if (this->m_Poly->Init(pTexLink, this->m_Num, true))
	{
		return true;
	}

	this->m_Poly->SetTexSize(this->m_Pos[0].getindex(), CDirectXDevice::GetWindowsSize().m_Width, CDirectXDevice::GetWindowsSize().m_Height);

	this->m_��count = 0;
	this->m_Count = 0;
	this->m_key = false;

	return false;
}

//==========================================================================
// ���
void CThankYouForPlaying::Uninit(void)
{
	this->m_Poly->Uninit();

	this->Delete(this->m_Pos);
	this->Delete(this->m_Poly);

	this->m_Num = 0;
	this->m_��count = 0;
	this->m_Count = 0;
}

//==========================================================================
// �X�V
void CThankYouForPlaying::Update(void)
{
	if (this->m_key)
	{
		this->m_��count += 5;
		if (255 <= this->m_��count)
		{
			this->m_��count = 255;
			if (60<this->m_Count)
			{
				// ���̃V�[���w��
				CScreen::screenchange(CScreen::scenelist_t::Title);
			}
			this->m_Count++;
		}
		this->m_Pos[1].SetColor(255, 255, 255, this->m_��count);
		this->m_Pos[0].SetColor(0, 0, 0, this->m_��count);
	}
}

//==========================================================================
// �`��
void CThankYouForPlaying::Draw(void)
{
	for (int i = 0; i < this->m_Num; i++)
	{
		this->m_Poly->Draw(&this->m_Pos[i]);
	}
}
