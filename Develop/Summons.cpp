//==========================================================================
// 召喚[Summons.cpp]
// author: tatuya ogawa
//==========================================================================
#include "Summons.h"
#include "UserCamera.h"
#include "ExplosionEffect.h"
#include "Base.h"

CSummons::CSummons()
{
	this->m_pStartAddress = nullptr;
	this->m_NumData = 0;
	this->m_IDCount = 0;
}

CSummons::~CSummons()
{
}

//==========================================================================
// 初期化
bool CSummons::Init(void)
{
	// 各parameter初期化
	this->m_pStartAddress = nullptr;
	this->m_NumData = 0;
	this->m_IDCount = 0;

	return false;
}

bool CSummons::TexInit(CBillboard::CAnimationData ** pInput, int num)
{
	this->m_NumCharacter = num;

	// メモリ確保
	this->New(this->m_poly, this->m_NumCharacter);

	for (int i = 0; i < this->m_NumCharacter; i++)
	{
		this->New(this->m_poly[i], this->m_animpattern);

		for (int s = 0; s < this->m_animpattern; s++)
		{
			if (this->m_poly[i][s].Init(
				pInput[i][s].m_list.m_texname,
				pInput[i][s].m_list.m_numdata,
				pInput[i][s].m_list.m_frame,
				pInput[i][s].m_list.m_pattern,
				pInput[i][s].m_list.m_direction,
				pInput[i][s].m_list.m_plate,
				pInput[i][s].m_list.m_centermood
			))
			{
				return true;
			}
		}
	}

	return false;
}

//==========================================================================
// 解放
void CSummons::Uninit(void)
{
	CData *pData = this->m_pStartAddress; // 初期アドレスを記憶
	CData *pNextDelete = nullptr; // 次のアドレス

	// アドレスがnullではない限り繰り返す
	for (;;)
	{
		// アドレスがnullだった場合終了
		if (pData == nullptr)
		{
			break;
		}

		// 次のアドレスを記憶
		pNextDelete = pData->m_pNext;

		// メモリ解放
		this->Delete(pData);
		this->m_NumData--;

		pData = pNextDelete;

		// データ数が0の時
		if (this->m_NumData == 0)
		{
			this->m_pStartAddress = nullptr;
			this->m_IDCount = 0;
		}
	}
}

void CSummons::TexUninit(void)
{
	// 解放処理
	if (this->m_poly != nullptr)
	{
		// キャラクターの数解放
		for (int i = 0; i < this->m_NumCharacter; i++)
		{
			// セットされているアニメーション数解放
			for (int s = 0; s < this->m_animpattern; s++)
			{
				this->m_poly[i][s].Uninit();
			}
			this->Delete(this->m_poly[i]);
			this->m_poly[i] = nullptr;
		}
		this->Delete(this->m_poly);
		this->m_poly = nullptr;
	}
}

//==========================================================================
// 更新
void CSummons::Update(void)
{
	// 先頭アドレスを記憶
	CData *pData = this->m_pStartAddress;

	// アドレスがnullではない限り繰り返す
	for (;;)
	{
		pData = this->LifeManagement(pData);

		// アドレスがnullだった場合終了
		if (pData == nullptr)
		{
			break;
		}

		this->m_poly[(int)pData->m_Parameter.GetType()][pData->m_Parameter.m_motionselect].UpdateAnimation(&pData->m_Parameter.m_animcount[(int)pData->m_Parameter.m_motionselect]);

		// 解放キー
		this->ReleaseKeySet(pData);

		// 古い座標に格納
		pData->m_PosData.m_OldPos = pData->m_PosData.m_Pos;

		// 次のアドレスを記憶
		pData = pData->m_pNext;
	}
}

//==========================================================================
// 描画
void CSummons::Draw(void)
{
	// 先頭アドレスを記憶
	CData *pData = this->m_pStartAddress;

	// アドレスがnullではない限り繰り返す
	for (;;)
	{
		// アドレスがnullだった場合終了
		if (pData == nullptr)
		{
			break;
		}

		// 描画
		pData->m_PosData.m_Pos.Draw(10, CDirectXDevice::GetD3DDevice());

		// 視野描画
		CDirectXDebug::HitRound(&pData->m_PosData.m_Pos, 20, pData->m_Parameter.GetStatus()->m_SearchScale, CDirectXDevice::GetD3DDevice());

		// 仮置き
		this->m_poly[(int)pData->m_Parameter.GetType()][pData->m_Parameter.m_motionselect].Draw(&pData->m_PosData.m_Pos, CUserCamera::GetView(), &pData->m_Parameter.m_animcount[(int)pData->m_Parameter.m_motionselect], false);

		// 次のアドレスを記憶
		pData = pData->m_pNext;
	}
}

//==========================================================================
// 生成
// 第一引数 生成座標
// 第二引数 パラメータ
void CSummons::Create(const C3DObject * Input, CParam * Param)
{
	CData *pMain = nullptr; // 生成時のアドレス
	CData *pData = nullptr; // 次のアドレス
	this->m_NumData++;
	this->m_IDCount++;

	// メモリを確保
	this->New(pMain);
	pMain->m_pNext = nullptr;
	pMain->m_pBack = nullptr;
	pMain->m_ID = this->m_IDCount;
	pMain->m_PosData.m_Pos = *Input;
	pMain->m_PosData.m_OldPos = *Input;
	pMain->m_Target.m_Pos = nullptr;
	pMain->m_Target.m_Parameter = nullptr;
	pMain->m_Release = false;
	for (int i = 0; i < pMain->m_MaxCase; i++)
	{
		pMain->m_Hate[i].m_Pos = nullptr;
		pMain->m_Hate[i].m_Parameter = nullptr;
	}
	pMain->m_Parameter = *Param;

	// 先頭アドレスがなかった場合
	if (this->m_pStartAddress == nullptr)
	{
		this->m_pStartAddress = pMain;
	}
	else if (this->m_pStartAddress != nullptr)
	{
		// 先頭アドレスを記憶
		pData = this->m_pStartAddress;

		// アドレスがnullではない限り検索を繰り返す
		for (;;)
		{
			// 次のアドレスがnullの場合
			if (pData->m_pNext == nullptr)
			{
				break;
			}

			// 検索をするアドレスを記憶
			pData = pData->m_pNext;
		}

		// 次のアドレスを記憶
		pData->m_pNext = pMain;

		// ひとつ前のアドレスに記憶
		pMain->m_pBack = pData;
	}
}

//==========================================================================
// 特定のデータ消去
// 引数 管理ID
void CSummons::PinpointDelete(const int * ID)
{
	CData *pData = this->m_pStartAddress; // 先頭アドレスを記憶
	CData *pBack = nullptr; // 前のデータ
	CData *pNext = nullptr; // 次のデータ

	// 解放対象が見つかるまで検索を続ける
	for (;;)
	{
		// アドレスが無ければ終了
		if (pData == nullptr)
		{
			break;
		}

		// 検索IDとヒット
		if (pData->m_ID == (*ID))
		{
			// 先頭アドレスと検索IDが同じ場合先頭アドレスを書き換える
			if (this->m_pStartAddress->m_ID == pData->m_ID)
			{
				this->m_pStartAddress = pData->m_pNext;
			}

			pBack = pData->m_pBack; // ひとつ前のアドレスを記憶
			pNext = pData->m_pNext; // 次のアドレスを記憶

			// アドレスがある場合次のアドレスをつなぎなおす
			if (pBack != nullptr)
			{
				pBack->m_pNext = pNext;
			}

			// アドレスがある場合ひとつ前のアドレスをつなぎなおす
			if (pNext != nullptr)
			{
				pNext->m_pBack = pBack;
			}

			// メモリ解放
			this->Delete(pData);
			this->m_NumData--;

			// データ数が0の時
			if (this->m_NumData == 0)
			{
				this->m_pStartAddress = nullptr;
				this->m_IDCount = 0;
			}

			break; // 終了
		}

		// 次のアドレスを記憶
		pData = pData->m_pNext;
	}
}

//==========================================================================
// ゲッター データがない場合null
CSummons::CData * CSummons::GetData(const int * Count)
{
	CData *pData = this->m_pStartAddress; // 先頭アドレスを記憶

	// カウンタの回数繰り返す
	for (int i = 0;i != (*Count); i++)
	{
		// アドレスが無ければ終了
		if (pData == nullptr)
		{
			break;
		}

		// 次のアドレスを記憶
		pData = pData->m_pNext;
	}

	return pData;
}

//==========================================================================
// ターゲットをセット
// 戻り値 : 誰をターゲットしたか
CSummons::CData * CSummons::SetTarget(CSummons::CData * pTarget, CSummons::CData * pData)
{
	CSummons::CData * p = nullptr;

	// ターゲットとヘイト先の解放キーがセットされていない場合
	if ((pData->m_Release == false) && (pTarget->m_Release == false))
	{
		// タイプ事にヘイトに入れる数を分岐させる
		for (int i = 0; i < (int)pTarget->m_Parameter.GetType(); i++)
		{
			// ターゲットのアドレスが登録済みアドレスと同じ場合
			if (&pTarget->m_PosData.m_Pos == pData->m_Target.m_Pos)
			{
				break;
			}

			// ヘイト先に自分のアドレスが存在するとき
			if (pTarget->m_Hate[i].m_Pos == &pData->m_PosData.m_Pos)
			{
				break;
			}

			// お互いのアドレスがnullのとき
			if ((pTarget->m_Hate[i].m_Pos == nullptr) && (pData->m_Target.m_Pos == nullptr))
			{
				// ヘイトを格納
				pTarget->m_Hate[i].m_Pos = &pData->m_PosData.m_Pos;

				// ヘイトしたアドレスのパラメーターをセット
				pTarget->m_Hate[i].m_Parameter = &pData->m_Parameter;

				// ターゲット情報セット
				pData->m_Target.m_Pos = &pTarget->m_PosData.m_Pos;

				// ターゲットのパラメーターのセット
				pData->m_Target.m_Parameter = &pTarget->m_Parameter;

				// 判定用戻り値
				p = pTarget;
				break;
			}
		}
	}

	return p;
}

//==========================================================================
// 解放キーのセット
void CSummons::ReleaseKeySet(CData * pData)
{
	// ターゲットの座標がある
	if (pData->m_Target.m_Pos != nullptr)
	{
		// ターゲットのライフが0以下の場合
		if (pData->m_Target.m_Parameter->GetStatus()->m_Life <= 0.0f)
		{
			// アドレスを消す
			pData->m_Target.m_Pos = nullptr;
			pData->m_Target.m_Parameter = nullptr;
		}
	}

	// 有効領域の数のみ回す
	for (int i = 0; i < (int)pData->m_Parameter.GetType(); i++)
	{
		// 座標が存在
		if (pData->m_Hate[i].m_Pos != nullptr)
		{
			// ヘイトしてきているアドレスのライフが0以下の場合
			if (pData->m_Hate[i].m_Parameter->GetStatus()->m_Life <= 0.0f)
			{
				// アドレスを消す
				pData->m_Hate[i].m_Pos = nullptr;
				pData->m_Hate[i].m_Parameter = nullptr;
			}
		}
	}

	// 自身のライフが0の場合解放キーをセット
	if (pData->m_Parameter.GetStatus()->m_Life <= 0.0f)
	{
		CExplosionEffect::create(&pData->m_PosData.m_Pos);
		if (pData->m_Parameter.objtype == ObjTypes::enemy)
		{
			CBase::setscore(50);
		}

		pData->m_Release = true;
	}
}

//==========================================================================
// ライフ管理
CSummons::CData * CSummons::LifeManagement(CData * pData)
{
	CData *pBack = nullptr; // 前のデータ
	CData *pNext = nullptr; // 次のデータ

	if (pData != nullptr)
	{
		// 解放キーが有効の場合
		if (pData->m_Release == true)
		{
			// 先頭アドレスと検索IDが同じ場合先頭アドレスを書き換える
			if (this->m_pStartAddress->m_ID == pData->m_ID)
			{
				this->m_pStartAddress = pData->m_pNext;
			}

			pBack = pData->m_pBack; // ひとつ前のアドレスを記憶
			pNext = pData->m_pNext; // 次のアドレスを記憶

			// アドレスがある場合次のアドレスをつなぎなおす
			if (pBack != nullptr)
			{
				pBack->m_pNext = pNext;
			}

			// アドレスがある場合ひとつ前のアドレスをつなぎなおす
			if (pNext != nullptr)
			{
				pNext->m_pBack = pBack;
			}

			// メモリ解放
			this->Delete(pData);
			this->m_NumData--;

			// データ数が0の時
			if (this->m_NumData == 0)
			{
				this->m_pStartAddress = nullptr;
				this->m_IDCount = 0;
			}

			pData = pNext;
		}
	}

	return pData;
}

//==========================================================================
// パラメータセット
void CSummons::CParam::SetParam(float Life, float Attack, float Spped, float SearchScale, int AttackFrame, TypeList Type, ObjTypes _objtype)
{
	this->m_Status.m_Attack = Attack;
	this->m_Status.m_Life = Life;
	this->m_Status.m_Spped = Spped;
	this->m_Status.m_SearchScale = SearchScale;
	this->m_Type = Type;
	this->m_AttackFrame = AttackFrame;
	this->m_animcount[0] = -1;
	this->m_animcount[1] = -1;
	this->m_motionselect = 0;
	this->m_lock = false;
	this->objtype = _objtype;
}

//==========================================================================
// 攻撃判定
// 攻撃時 true 非攻撃時 false
bool CSummons::CParam::AttackFrameCount(void)
{
	this->m_AttackFrameCount++;
	if (this->m_AttackFrame <= this->m_AttackFrameCount)
	{
		this->m_AttackFrameCount = -1;
		return true;
	}

	return false;
}
