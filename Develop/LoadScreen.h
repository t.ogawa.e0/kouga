//==========================================================================
// ロードスクリーン[LoadScreen.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _LoadScreen_H_
#define _LoadScreen_H_

#include "dxlib.h"

//==========================================================================
//
// class  : CLoadScreen
// Content: ロードスクリーン
//
//==========================================================================
class CLoadScreen : private CTemplate
{
private:
	// パラメータ
	class CParam
	{
	public:
		int m_a; // α
		bool m_Change; // change
	};
public:
	CLoadScreen();
	~CLoadScreen();

	// 初期化
	bool Init(void);
	// 解放
	void Uninit(void);
	// 更新
	void Update(void);
	// 描画
	void Draw(void);
private:
	// αチェンジ
	void Change(int Speed);
private:
	C2DObject * m_posback; // 座標
	C2DPolygon * m_polyback; // ポリゴン

	C2DObject * m_posload; // 座標
	C2DPolygon * m_polyload; // ポリゴン
	CParam * m_paramload; // パラメータ

	C2DObject * m_poslink; // 座標
	C2DPolygon * m_polylink; // ポリゴン
};

#endif // !_LoadScreen_H_
