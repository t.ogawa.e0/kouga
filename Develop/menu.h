//==========================================================================
// メニュー[menu.h]
// author: keita yanagidate
//==========================================================================
#ifndef _menu_h_
#define _menu_h_

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

//==========================================================================
//
// class  : CMenu
// Content: メニュー
//
// author: keita yanagidate
//
//==========================================================================
class CMenu : public CObject
{
public:
	CMenu();
	~CMenu();
	// 初期化
	bool Init(void);
	// 解放
	void Uninit(void);
	// 更新
	void Update(void);
	// 描画
	void Draw(void);

	static bool getmenukey(void) { return m_statickey; }

	static int GetMenu(void) { return m_menu; }

	static void OpenSound(void);
private:
	C2DPolygon * m_poly; // ポリゴン
	C2DObject * m_pos; // 座標
	int *m_ncollarα; // UIのα値
	bool *m_keylock; // キー操作のロック
	int m_NumData; // データ数
	float m_fCount; // カーソル動作用エネルギー
	bool m_lock; // 選択ロック
	int m_Count; // フェードカウンタ

	CImGui_Dx9 m_Imgui; // imgui

	static int m_menu; // メニュー
	static bool m_statickey; // メニュー表示キー

	C2DPolygon * m_backpoly; //  背景のポリゴン
	C2DObject * m_backpos; //  背景の座標
	CTemplates m_temp;
	int m_backcollarα; // 背景のα値

	static bool m_soundkey;

	static CSound m_sound;
};

#endif // !_menu_h_
