//==========================================================================
// �i���֎~[NoEntry.cpp]
// author: tatuya ogawa
//==========================================================================
#include "NoEntry.h"

C3DObject *CNoEntry::m_vpos = nullptr;

CNoEntry::CNoEntry()
{
	this->m_poly = nullptr;
	this->m_pos = nullptr;
	this->m_numdata = 0;
}

CNoEntry::~CNoEntry()
{
}

//==========================================================================
// ������
bool CNoEntry::Init(void)
{
	float fscale = 20.0f;
	int nCount = 0;

	this->m_numdata = (int)4 * 2;

	this->New(this->m_poly);
	this->New(this->m_pos, this->m_numdata);
	this->New(this->m_vpos, this->m_snumdata);

	for (int i = 0; i < this->m_numdata; i++)
	{
		this->m_pos[i].Init(i);
		this->m_pos[i].Scale(fscale);
	}

	this->m_pos[4].Init(0);
	this->m_pos[4].Scale(fscale);

	this->m_pos[5].Init(1);
	this->m_pos[5].Scale(fscale);

	this->m_pos[6].Init(2);
	this->m_pos[6].Scale(fscale);

	this->m_pos[7].Init(3);
	this->m_pos[7].Scale(fscale);

	if (this->m_poly->Init("resource/texture/Game/object/shimenawa2.DDS", this->m_WallZ, this->m_WallY)) { return true; }
	if (this->m_poly->Init("resource/texture/Game/object/shimenawa2.DDS", this->m_WallX, this->m_WallY)) { return true; }
	if (this->m_poly->Init("resource/texture/Game/object/shimenawa2.DDS", this->m_WallX, this->m_WallY)) { return true; }
	if (this->m_poly->Init("resource/texture/Game/object/shimenawa2.DDS", this->m_WallZ, this->m_WallY)) { return true; }

	nCount = -1;

	//==========================================================================
	//==========================================================================
	// ��
	nCount++;
	this->m_pos[nCount].Info.rot.x = this->m_pos[nCount].GetAngle()._ANGLE_270;
	this->m_pos[nCount].Info.pos.z = this->m_poly->GetMeshData(nCount).NumMeshX*0.5f;

	this->m_vpos[nCount] = this->m_pos[nCount];

	this->m_pos[4].Info.rot.x = this->m_pos[4].GetAngle()._ANGLE_090;
	this->m_pos[4].Info.pos.z = this->m_poly->GetMeshData(nCount).NumMeshX*0.5f;

	//==========================================================================
	//==========================================================================
	// �E
	nCount++;
	this->m_pos[nCount].Info.rot.x = this->m_pos[nCount].GetAngle()._ANGLE_270;
	this->m_pos[nCount].Info.rot.y = this->m_pos[nCount].GetAngle()._ANGLE_270;
	this->m_pos[nCount].Info.pos.z = 0.0f;
	this->m_pos[nCount].Info.pos.x = this->m_poly->GetMeshData(nCount).NumMeshX*0.5f;

	this->m_vpos[nCount] = this->m_pos[nCount];

	this->m_pos[5].Info.rot.x = this->m_pos[nCount].GetAngle()._ANGLE_090;
	this->m_pos[5].Info.rot.y = this->m_pos[nCount].GetAngle()._ANGLE_270;
	this->m_pos[5].Info.pos.z = 0.0f;
	this->m_pos[5].Info.pos.x = this->m_poly->GetMeshData(nCount).NumMeshX*0.5f;

	//==========================================================================
	//==========================================================================
	// ��
	nCount++;
	this->m_pos[nCount].Info.rot.x = this->m_pos[nCount].GetAngle()._ANGLE_270;
	this->m_pos[nCount].Info.rot.y = this->m_pos[nCount].GetAngle()._ANGLE_270;
	this->m_pos[nCount].Info.pos.z = 0.0f;
	this->m_pos[nCount].Info.pos.x = -this->m_poly->GetMeshData(nCount).NumMeshX*0.5f;

	this->m_vpos[nCount] = this->m_pos[nCount];

	this->m_pos[6].Info.rot.x = this->m_pos[nCount].GetAngle()._ANGLE_090;
	this->m_pos[6].Info.rot.y = this->m_pos[nCount].GetAngle()._ANGLE_270;
	this->m_pos[6].Info.pos.z = 0.0f;
	this->m_pos[6].Info.pos.x = -this->m_poly->GetMeshData(nCount).NumMeshX*0.5f;

	//==========================================================================
	//==========================================================================
	// ��O
	nCount++;
	this->m_pos[nCount].Info.rot.x = this->m_pos[nCount].GetAngle()._ANGLE_090;
	this->m_pos[nCount].Info.pos.z = -this->m_poly->GetMeshData(nCount).NumMeshX*0.5f;

	this->m_vpos[nCount] = this->m_pos[nCount];

	this->m_pos[7].Info.rot.x = this->m_pos[nCount].GetAngle()._ANGLE_270;
	this->m_pos[7].Info.pos.z = -this->m_poly->GetMeshData(nCount).NumMeshX*0.5f;

	return false;
}

//==========================================================================
// ���
void CNoEntry::Uninit(void)
{
	this->m_poly->Uninit();

	this->Delete(this->m_poly);
	this->Delete(this->m_pos);
	this->Delete(this->m_vpos);
	this->m_numdata = 0;
}

//==========================================================================
// �X�V
void CNoEntry::Update(void)
{
}

//==========================================================================
// �`��
void CNoEntry::Draw(void)
{
	for (int i = 0; i < this->m_numdata; i++)
	{
		this->m_pos[i].Draw(10, CDirectXDevice::GetD3DDevice());
		this->m_poly->Draw(&this->m_pos[i]);
	}
}
