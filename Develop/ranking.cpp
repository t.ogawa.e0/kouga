//==========================================================================
// ランキング[ranking.cpp]
// author : tatuya ogawa
//==========================================================================
#include "ranking.h"
#include "titlelogo.h"

bool CRanking::m_OpenKey = false;
bool CRanking::m_EndKey = false;
bool CRanking::m_OpenKey2 = false;

CRanking::CRanking()
{
	this->m_NumData = 0;
	this->m_Pos = nullptr;
	this->m_Poly = nullptr;
	this->m_MasterPos = nullptr;
	this->m_data = nullptr;
	this->m_NotData = false;
	this->m_MoveKey = false;
	this->m_initKey = false;
}

CRanking::~CRanking()
{
}

//==========================================================================
// 初期化
bool CRanking::Init(void)
{
	float fscale = 0.7f;
	this->m_EndKey = false;
	this->m_OpenKey = false;
	this->m_OpenKey2 = false;

	if (this->loadtex(fscale)) { return true; }
	this->initpos(fscale);

	return false;
}

//==========================================================================
// 解放
void CRanking::Uninit(void)
{
	this->m_Poly->Uninit();

	this->m_temp.Delete_(this->m_MasterPos);
	this->m_temp.Delete_(this->m_Pos);
	this->m_temp.Delete_(this->m_Poly);
	this->m_temp.Delete_(this->m_data);
	this->m_NumData = 0;
	this->m_NotData = false;
	this->m_MoveKey = false;
	this->m_initKey = false;
	this->m_OpenKey = false;
	this->m_EndKey = false;
	this->m_OpenKey2 = false;
}

//==========================================================================
// 更新
void CRanking::Update(void)
{
	CVector<float> vspeed = CVector<float>(0.0f, 0.0f, 0.0f, 0.0f);

	this->m_ImGui.NewWindow("Ranking", true);
#if defined(_DEBUG) || defined(DEBUG)
	const char * nametexlist1[] =
	{
		"0","1","2","3","4","5","6","7","8","9",
	};
	const char * nametexlist2[] =
	{
		"A","B","C","D","E","F","G","H","I","J",
		"K","L","M","N","O","P","Q","R","S",
		"T","U","V","W","X","Y","Z"," ",
	};
	const char * nametexlist3[] =
	{
		".","_",
	};

	const char **clist[] =
	{
		nametexlist1,
		nametexlist2,
		nametexlist3,
	};
	int ndaata[10] = { 0 };
	int ntexID[10] = { 0 };

	for (int i = 0; i < this->m_NumData; i++)
	{
		this->m_data[i].m_rank;
		this->m_data[i].m_score;
		for (int s = 0; s < 10; s++)
		{
			ndaata[s] = this->m_data[i].m_name.m_namedata[s].m_key;
			ntexID[s] = this->m_data[i].m_name.m_namedata[s].m_textureID;
		}
		this->m_ImGui.Text
		(
			" lank  : %d \n score : %d \n TexID : %d %d %d %d %d %d %d %d %d %d  \n Name  : %d %d %d %d %d %d %d %d %d %d \n Name  : %s%s%s%s%s%s%s%s%s%s",
			this->m_data[i].m_rank,
			this->m_data[i].m_score,
			ntexID[0],
			ntexID[1],
			ntexID[2],
			ntexID[3],
			ntexID[4],
			ntexID[5],
			ntexID[6],
			ntexID[7],
			ntexID[8],
			ntexID[9],
			ndaata[0],
			ndaata[1],
			ndaata[2],
			ndaata[3],
			ndaata[4],
			ndaata[5],
			ndaata[6],
			ndaata[7],
			ndaata[8],
			ndaata[9],
			clist[ntexID[0]][ndaata[0]],
			clist[ntexID[1]][ndaata[1]],
			clist[ntexID[2]][ndaata[2]],
			clist[ntexID[3]][ndaata[3]],
			clist[ntexID[4]][ndaata[4]],
			clist[ntexID[5]][ndaata[5]],
			clist[ntexID[6]][ndaata[6]],
			clist[ntexID[7]][ndaata[7]],
			clist[ntexID[8]][ndaata[8]],
			clist[ntexID[9]][ndaata[9]]
		);
	}
#endif
	if (this->m_OpenKey2)
	{
		if (CTitlelogo::getkey())
		{
			this->m_temp.Bool_(&this->m_OpenKey);
			for (int i = 0; i < this->m_NumData; i++)
			{
				this->m_Pos[i] = this->m_MasterPos[i];
			}
		}
		this->m_OpenKey2 = false;
	}

	// ロックが解除されているとき
	if (this->m_OpenKey)
	{
		for (int i = 0; i < this->m_NumData; i++)
		{
			vspeed = CVector<float>(0.0f, 0.0f, 0.0f, 0.0f);
			if (CKeyboard::Press(CKeyboard::KeyList::KEY_UP) || CController::CDirection::Press(CController::CDirection::Ckey::LeftButtonUp))
			{
				vspeed += CVector<float>(0.0f, -5.0f, 0.0f, 0.0f);
			}
			else if (CKeyboard::Press(CKeyboard::KeyList::KEY_DOWN) || CController::CDirection::Press(CController::CDirection::Ckey::LeftButtonUnder))
			{
				vspeed += CVector<float>(0.0f, 5.0f, 0.0f, 0.0f);
			}

			this->m_Pos[i].m_scorpos.SetPosPlus(vspeed);
			this->m_Pos[i].m_rankpos.SetPosPlus(vspeed);
			this->m_Pos[i].m_scorfontpos.SetPosPlus(vspeed);
			this->m_Pos[i].m_namefontpos.SetPosPlus(vspeed);
			this->m_Pos[i].m_rankfontpos.SetPosPlus(vspeed);
			for (int s = 0; s < this->m_Pos[i].m_numdata; s++)
			{
				this->m_Pos[i].m_namepos[s].SetPosPlus(vspeed);
				this->m_Pos[i].m_namepos[s].SetAnimationCount(this->m_data[i].m_name.m_namedata[s].m_key);
			}

			this->m_Pos[i].m_rankpos.SetAnimationCount(this->m_data[i].m_rank);
			for (int s = 0; s < this->m_Pos[i].m_numdata; s++)
			{
				// アニメーション結果の入力
				this->m_Pos[i].m_namepos[s].SetAnimationCount(this->m_data[i].m_name.m_namedata[s].m_key);
			}
		}
	}

	this->m_ImGui.EndWindow();
}

//==========================================================================
// 描画
void CRanking::Draw(void)
{
	if (this->m_OpenKey)
	{
		for (int i = 0; i < this->m_NumData; i++)
		{
			this->m_Poly->Draw(&this->m_Pos[i].m_rankfontpos); //描画
			this->m_Pos[i].m_ranknumber.Draw(this->m_Poly, &this->m_Pos[i].m_rankpos, this->m_data[i].m_rank);

			this->m_Poly->Draw(&this->m_Pos[i].m_scorfontpos); //描画
			this->m_Pos[i].m_scornumber.Draw(this->m_Poly, &this->m_Pos[i].m_scorpos, this->m_data[i].m_score);

			this->m_Poly->Draw(&this->m_Pos[i].m_namefontpos); //描画
			for (int s = 0; s < this->m_Pos[i].m_numdata; s++)
			{
				this->m_Poly->Draw(&this->m_Pos[i].m_namepos[s]); //描画
			}
		}
	}
}

void CRanking::startRanking(void)
{
	if (CKeyboard::Trigger(CKeyboard::KeyList::KEY_R) || CController::CButton::Trigger(CController::CButton::Ckey::R1Button))
	{
		m_OpenKey2 = true;
	}
}

void CRanking::EndRanking(void)
{
	m_OpenKey = false;
}

void CRanking::EndKey(void)
{
	if (CKeyboard::Trigger(CKeyboard::KeyList::KEY_RETURN) || CController::CButton::Trigger(CController::CButton::Ckey::RightButton3))
	{
		m_OpenKey = false;
	}
}

//==========================================================================
// テクスチャ読み込み
bool CRanking::loadtex(float fscale)
{
	this->m_temp.New_(this->m_Poly);

	const char* TexLink[] =
	{
		"resource/texture/numbertex.DDS",
		"resource/texture/namefontcase/keyboardfontlist.DDS",
		"resource/texture/namefontcase/period_underbar.DDS",
		"resource/texture/RankFont.DDS",
		"resource/texture/namefont.DDS",
		"resource/texture/ScoreFont.DDS",
	};

	if (this->m_Poly->Init(TexLink, this->m_temp.Sizeof_(TexLink), true))
	{
		return true;
	}

	for (int i = 0; i < (int)this->m_temp.Sizeof_(TexLink); i++)
	{
		this->m_Poly->SetTexScale(i, fscale);
	}

	return false;
}

//==========================================================================
// 座標の初期化
void CRanking::initpos(float fscale)
{
	CVector<float> vpos = CVector<float>(0.0f, 0.0f, 0.0f, 0.0f);
	CVector<float> vspeed = CVector<float>(0.0f, 0.0f, 0.0f, 0.0f);

	this->m_temp.Delete_(this->m_MasterPos);
	this->m_temp.Delete_(this->m_Pos);

	this->m_NumData = 0;
	float fpos2 = 0.0f;
	float fposX = 0.0f;
	float fMove = 70.0f;

	this->m_MoveKey = false;
	this->m_OpenKey = false;

	fpos2 = 0.0f;
	fposX = (float)CDirectXDevice::GetWindowsSize().m_Width * 0.47f;
	vpos.y = (float)CDirectXDevice::GetWindowsSize().m_Height * 0.25f;
	fMove = 70.0f;
	this->m_ranling.Load(this->m_data, this->m_ranling.m_FileName, &this->m_NumData);

	// メモリ確保
	this->m_temp.New_(this->m_Pos, this->m_NumData);
	this->m_temp.New_(this->m_MasterPos, this->m_NumData);

	for (int i = 0; i < this->m_NumData; i++)
	{
		vpos.x = fposX;
		fpos2 = (fMove * 5);
		fpos2 = fpos2*fscale;
		fpos2 = fpos2*CDirectXDevice::screenbuffscale();
		vpos.x -= fpos2;

		fpos2 = fMove*fscale;
		fpos2 = fpos2*CDirectXDevice::screenbuffscale();

		this->m_Pos[i].m_rankfontpos.Init(3);
		this->m_Pos[i].m_namefontpos.Init(4);
		this->m_Pos[i].m_scorfontpos.Init(5);
		this->m_Pos[i].m_scorpos.Init(0, 1, 10, 10);
		this->m_Pos[i].m_rankpos.Init(0, 1, 10, 10);
		this->m_Pos[i].m_ranknumber.Init(4, true, true);
		this->m_Pos[i].m_scornumber.Init(9, true, true);

		this->m_Pos[i].m_scorfontpos.SetCentralCoordinatesMood(true);
		this->m_Pos[i].m_rankfontpos.SetCentralCoordinatesMood(true);
		this->m_Pos[i].m_namefontpos.SetCentralCoordinatesMood(true);
		this->m_Pos[i].m_rankpos.SetCentralCoordinatesMood(true);
		this->m_Pos[i].m_scorpos.SetCentralCoordinatesMood(true);

		this->m_Pos[i].m_rankfontpos.SetPos(vpos);
		this->m_Pos[i].m_rankpos.SetX(vpos.x*this->m_Pos[i].m_scaleposX);
		this->m_Pos[i].m_rankpos.SetY(vpos.y);

		vpos.y += fpos2*this->m_Pos[i].m_scaleposY;
		this->m_Pos[i].m_scorfontpos.SetPos(vpos);
		this->m_Pos[i].m_scorpos.SetX(vpos.x*this->m_Pos[i].m_scaleposX);
		this->m_Pos[i].m_scorpos.SetY(vpos.y);

		vpos.y += fpos2*this->m_Pos[i].m_scaleposY;
		this->m_Pos[i].m_namefontpos.SetPos(vpos);

		vpos.x = (vpos.x*this->m_Pos[i].m_scaleposX);
		for (int s = 0; s < this->m_Pos[i].m_numdata; s++)
		{
			// 初期化
			this->m_Pos[i].m_namepos[s].Init
			(
				this->m_data[i].m_name.m_namedata[s].m_textureID,
				1,
				this->m_data[i].m_name.m_namedata[s].Pattern,
				this->m_data[i].m_name.m_namedata[s].Direction
			);
			this->m_Pos[i].m_namepos[s].SetCentralCoordinatesMood(true);

			this->m_Pos[i].m_namepos[s].SetPos(vpos);

			vpos.x += fpos2;
		}
		vpos.y += fpos2*this->m_scaleposY;

		this->m_MasterPos[i] = this->m_Pos[i];
	}
}
