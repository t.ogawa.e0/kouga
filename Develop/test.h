//==========================================================================
// テストプログラム[test.h]
// author: tatsuya ogawa
//==========================================================================

#ifndef _test_h_
#define _test_h_

/*
このヘッダは、各クラスの扱い方を説明するためのテストプログラムです。
ここで解説されるクラスは以下の通りです。

**** 描画系座標系クラスの扱い方 ****
変数宣言をする感覚で使用可能です。

**** 座標系クラス ****
C3DObject	3Dオブジェクトの座標を扱うクラスです。
C2DObject	2Dオブジェクトの座標を扱うクラスです。

**** 描画系クラス ****
CXmodel		.x形式のモデルデータを管理、描画を行えるクラスです。
CSphere		球体の管理、描画を行えるクラスです。
CMesh		メッシュの管理、描画が行えるクラスです。
CBillboard	ビルボードの管理、描画が行えるクラスです。
CCube		キューブの管理、描画が行えるクラスです。
C2DPolygon	2D画像の管理、描画が行えるクラスです。

**** グリッド ****
CGrid		グリッドの管理、描画が行えるクラスです。

**** クラスの管理クラス **** 
CObject		このクラスを継承することにより、初期化、解放、描画、更新が簡単に行えるようになります。

**** 座標系クラスと描画系クラスの関係性 ****
描画系クラスでオブジェクトを描画させるには、
座標の管理を行っている座標系クラスと併用しなければいけません。
各描画クラスのDraw関数の引数に[C3DObject]又は[C2DObject]をアドレス渡しをすれば描画ができるように、
全ての描画系クラスは設計されています。

**** 併用しなければならないもの ****
**** 2D系 ****
[C2DObject]×[C2DPolygon]

**** 3D系 ****
[C3DObject]×[CXmodel]
[C3DObject]×[CSphere]
[C3DObject]×[CMesh]
[C3DObject]×[CBillboard]
[C3DObject]×[CCube]

**** 単体で描画可能なもの ****
[CGrid]

**** CTemplates とは何者？ ****
面倒な記述が必要な動的メモリ確保、解放、要素数の計算などを簡単にできるようにしたヘルパー関数が入っています！

**** CTemplate とは何者？ ****
CTemplates と中身はまったく同じですが、継承して使用するタイプです。
*/

//==========================================================================
// include
//==========================================================================
#include "dxlib.h" // 全ライブラリのincludeヘッダです。これだけincludeすれば全て使えます。
#include "UserCamera.h" // ユーザーカメラヘッダ。これをincludeするとユーザーが現在使用しているカメラのビュー行列を取得できるようになります。

//==========================================================================
//
// class  : C3DText
// Content: テキストの描画
//
//==========================================================================
class C3DText
{
public:
	C3DText();
	~C3DText();

	// 初期化
	bool Init(void);

	// 解放
	void Uninit(void);

	// 更新
	void Update(void);

	// 描画
	void Draw(void);
private:
	CText m_text; // テキスト
};

//==========================================================================
//
// class  : C3DXmodel
// Content: Xモデルの描画
//
//==========================================================================
class C3DXmodel
{
public:
	C3DXmodel();
	~C3DXmodel();
	// 初期化
	bool Init(void);

	// 解放
	void Uninit(void);

	// 更新
	void Update(void);

	// 描画
	void Draw(void);
private:
	CXmodel m_xmodel; // xモデルの管理
	C3DObject m_pos; // 座標の管理
};

//==========================================================================
//
// class  : C3DSphere
// Content: 球体の描画
//
//==========================================================================
class C3DSphere
{
public:
	C3DSphere();
	~C3DSphere();
	// 初期化
	bool Init(void);

	// 解放
	void Uninit(void);

	// 更新
	void Update(void);

	// 描画
	void Draw(void);
private:
	CSphere m_sphere; // 球体の管理
	C3DObject m_pos; // 座標
};

//==========================================================================
//
// class  : C3DMesh
// Content: メッシュの描画
//
//==========================================================================
class C3DMesh
{
public:
	C3DMesh();
	~C3DMesh();
	// 初期化
	bool Init(void);

	// 解放
	void Uninit(void);

	// 更新
	void Update(void);

	// 描画
	void Draw(void);
private:
	CMesh m_mesh; // メッシュの管理
	C3DObject m_pos[2]; // 座標
	CTemplates m_temp; // テンプレート
};

//==========================================================================
//
// class  : C3DBillboard
// Content: ビルボードの描画
//
//==========================================================================
class C3DBillboard
{
public:
	C3DBillboard();
	~C3DBillboard();
	// 初期化
	bool Init(void);

	// 解放
	void Uninit(void);

	// 更新
	void Update(void);

	// 描画
	void Draw(void);
private:
	int m_animcount; // アニメーションのカウンタ
	CBillboard m_bill; // ビルボード
	C3DObject m_pos[2]; // 座標
	CTemplates m_temp; // テンプレート
};

//==========================================================================
//
// class  : C3DCubeData
// Content: キューブの描画
//
//==========================================================================
class C3DCubeData
{
public:
	C3DCubeData();
	~C3DCubeData();
	// 初期化
	bool Init(void);

	// 解放
	void Uninit(void);

	// 更新
	void Update(void);

	// 描画
	void Draw(void);
private:
	CCube m_cube; // キューブ
	C3DObject m_pos[2]; // 座標管理
	CTemplates m_temp; // テンプレート
};

//==========================================================================
//
// class  : C3DGridData
// Content: グリッド描画
//
//==========================================================================
class C3DGridData
{
public:
	C3DGridData();
	~C3DGridData();
	// 初期化
	bool Init(void);

	// 解放
	void Uninit(void);

	// 更新
	void Update(void);

	// 描画
	void Draw(void);
private:
	CGrid m_grid; // グリッド
};

//==========================================================================
//
// class  : C2DAnimData
// Content: 2Dアニメーション描画
//
//==========================================================================
class C2DAnimData
{
public:
	C2DAnimData();
	~C2DAnimData();
	// 初期化
	bool Init(void);

	// 解放
	void Uninit(void);

	// 更新
	void Update(void);

	// 描画
	void Draw(void);
private:
	C2DObject m_2DObjpos; // 2D座標管理
	C2DPolygon m_2DObjpoly; // 2Dオブジェクト
	int m_animcount; // アニメーションカウンタ
};

//==========================================================================
//
// class  : C2DData
// Content: 2D描画
//
//==========================================================================
class C2DData
{
public:
	C2DData();
	~C2DData();
	// 初期化
	bool Init(void);

	// 解放
	void Uninit(void);

	// 更新
	void Update(void);

	// 描画
	void Draw(void);
private:
	C2DObject m_2DObjpos; // 座標
	C2DPolygon m_2DObjpoly; // 2Dオブジェクト
};

//==========================================================================
//
// class  : CTest
// Content: テスト用
//
//==========================================================================
// CObject クラスにこのクラスを認識させるための継承
class CTest : public CObject
{
public:
	CTest();
	~CTest();
	// 初期化
	bool Init(void);

	// 解放
	void Uninit(void);

	// 更新
	void Update(void);

	// 描画
	void Draw(void);
private:
	// 管理を楽にしたいがためにここに全部入れてます(あまりよろしくない)
	C2DData m_2dobj; 
	C2DAnimData m_2danimobj;
	C3DGridData m_3dgrid;
	C3DCubeData m_3dcube;
	C3DBillboard m_3dbill;
	C3DMesh m_3dmesh;
	C3DSphere m_3dsphere;
	C3DXmodel m_3dxmodel;
	C3DText m_dxtext;
};

#endif // !_test_h_
