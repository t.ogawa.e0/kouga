//==========================================================================
// ベースキャンプ[Base.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _Base_H_
#define _Base_H_

//==========================================================================
// Include
//==========================================================================
#include "dxlib.h"
#include "menu.h"

//==========================================================================
//
// class  : CBase
// Content: ベースキャンプ
//
//==========================================================================
class CBase : public CObject
{
private:
	// ストック
	class CStock
	{
	private:
		static constexpr int m_CreateLimit = 180;
		static constexpr int m_ReleaseFlameLimit = 30;
	public:
		// 生成上限指定
		void Init(int nLimit);
		// 生成
		void Create(void);
		// 生成した数分排出
		// 排出時true 非排出時 false
		bool Release(void);
	private:
		int m_ReleaseFlame;
		int m_Limit;
		int m_Num;
		int m_CreateCount;
	};

	class CParam
	{
	public:
		// 初期化
		void Init(C3DObject * pPos, float SearchScale, int bDurableValue);
		// 更新
		void Update(void);
		// 座標のゲッター
		C3DObject *GetPos(void) { return &this->m_Pos; }
	private:
		// ベース
		void Base(void);
	public:
		C3DObject m_Pos; // 座標
		CStock m_Stock; // ストック
		float m_SearchScale; // 索敵範囲
		int m_DurableValue; // 耐久値
		bool m_Existence; // 存在
		int m_select;
		CHitDetermination m_hit;
	};
public:
	class CData
	{
	public:
		int m_createlimit;
		float m_searchscale;
		int m_life;
	};
public:
	typedef CParam BaseParam;
public:
	CBase();
	~CBase();
	// 初期化
	bool Init(void);
	// 解放
	void Uninit(void);
	// 更新
	void Update(void);
	// 描画
	void Draw(void);

	static void setscore(int scot) { m_scor += scot; }

	static int getscor(void) { return (int)(m_scor*(CMenu::GetMenu() + 1)); }

	static int getmaxbase(void) { return m_MaxBase; }
	static CParam* baseparam(int num) { return &m_BaseData[num]; }
private:
	void createtrap(void);
private:
	CBillboard * m_poly;
	static CParam *m_BaseData; // 座標
	static int m_MaxBase; // ベースの総数
	bool m_key;
	static int m_scor;
	CTemplates m_temp;
};

#endif // !_Base_H_
