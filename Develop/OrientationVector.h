//==========================================================================
// 向きベクトル[OrientationVector.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _OrientationVector_H_
#define _OrientationVector_H_

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

//==========================================================================
//
// class  : COrientationVector
// Content: 向きベクトル
//
//==========================================================================
class COrientationVector
{
protected:
	// 前進
	bool Advance(D3DXVECTOR3 vec);
	// 後進
	bool Backward(D3DXVECTOR3 vec);
	// 右
	bool Right(D3DXVECTOR3 vec);
	// 左
	bool Left(D3DXVECTOR3 vec);
	// 右前進
	bool AdvanceRight(D3DXVECTOR3 vec);
	// 左前進
	bool AdvanceLeft(D3DXVECTOR3 vec);
	// 右後進
	bool BackwardRight(D3DXVECTOR3 vec);
	// 左後進
	bool BackwardLeft(D3DXVECTOR3 vec);
};

#endif // !_OrientationVector_H_
