//==========================================================================
// タイトル[title.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _TITLE_H_
#define _TITLE_H_

#include"SceneChange.h"
#include "dxlib.h"

//==========================================================================
//
// class  : CTitleScene
// Content: タイトルシーン
//
//==========================================================================
class CTitleScene : public CBaseScene
{
public:
	CTitleScene();
	~CTitleScene();
	// 初期化
	bool Init(void);
	// 解放
	void Uninit(void);
	// 更新
	void Update(void);
	// 描画
	void Draw(void);
private:
};

#endif // !_TITLE_H_
