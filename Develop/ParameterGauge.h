//==========================================================================
// パラメーターのゲージ[ParameterGauge.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _ParameterGauge_H_
#define _ParameterGauge_H_

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <stdio.h>

//==========================================================================
//
// class  : CParameterGauge
// Content: パラメーターのゲージ処理
//
//==========================================================================
class CParameterGauge
{
private:
	class CParam
	{
	public:
		float m_Percent; // 1%のゲージの幅
		float m_PhysicalFitnessPercent; // 1%のパラメータ
		float m_CurrentGaugeLength; // 現在のゲージ長さ
		float m_MaximumGaugeLength; // 最大ゲージ長さ
		int m_Limit; // ゲージの幅上限
		int m_SetPercent; // パラメータの上限
	};
public:
	CParameterGauge();
	~CParameterGauge();
	// ゲージの初期化
	void Init(float MaxParam, float ParamGauge);
	// ゲージの更新
	void Update(float CurrentParam);
	// データのゲッター
	CParam * GetData(void) { return &this->m_Param; }
private:
	CParam m_Param; // パラメータ
};

#endif // !_ParameterGauge_H_
