//==========================================================================
// ランキング[ranking.h]
// author : tatsuya ogawa
//==========================================================================
#ifndef _Ranking_H_
#define _Ranking_H_

//==========================================================================
// Include
//==========================================================================
#include "dxlib.h"
#include "GameData.h"

//==========================================================================
//
// class  : CRanking
// Content: ランキング
//
//==========================================================================
class CRanking : public CObject
{
private:
	static constexpr float m_scaleposY = 2.0f;
private:
	class CRankPos
	{
	public:
		static constexpr int m_numdata = 10;
		static constexpr float m_scaleposX = 1.45f;
		static constexpr float m_scaleposY = 1.5f;
	public:
		C2DObject m_namepos[m_numdata];
		C2DObject m_scorpos;
		C2DObject m_rankpos;

		C2DObject m_scorfontpos;
		C2DObject m_namefontpos;
		C2DObject m_rankfontpos;

		CNumber m_ranknumber;
		CNumber m_scornumber;
	};
public:
	CRanking();
	~CRanking();
	// 初期化
	bool Init(void);
	// 解放
	void Uninit(void);
	// 更新
	void Update(void);
	// 描画
	void Draw(void);

	static void startRanking(void);

	static void EndRanking(void);

	static void EndKey(void);
private:
	// テクスチャ読み込み
	bool loadtex(float fscale);
	// 座標の初期化
	void initpos(float fscale);
private:
	CRankPos *m_Pos;
	CRankPos *m_MasterPos;
	C2DPolygon *m_Poly; //描画

	CGameData::CData *m_data;

	CImGui_Dx9 m_ImGui;
	CTemplates m_temp;
	CGameData m_ranling;

	static bool m_OpenKey;

	static bool m_OpenKey2;

	static bool m_EndKey;

	bool m_MoveKey;

	bool m_initKey;

	int m_NumData;
	bool m_NotData;
};
#endif //!_Ranking_H_