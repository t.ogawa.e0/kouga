//==========================================================================
// フィールド[field.h]
// author: keita yanagidate
//==========================================================================
#ifndef _FIELD_H_
#define _FIELD_H_

//==========================================================================
// Include
//==========================================================================
#include "dxlib.h"

//==========================================================================
//
// class  : CFIELD
// Content: フィールド
//
//==========================================================================
class CFIELD : private CTemplate, public CObject
{
public:
	CFIELD();
	~CFIELD();
	// 初期化
	bool Init(void);
	// 解放
	void Uninit(void);
	// 更新
	void Update(void);
	// 描画
	void Draw(void);
private:
	CMesh *m_MeshField; // フィールドデータ
	C3DObject *m_Pos; // 座標
};

#endif // !_FIELD_H_#pragma once

