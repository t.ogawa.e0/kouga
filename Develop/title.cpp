//==========================================================================
// タイトル[title.cpp]
// author: tatuya ogawa
//==========================================================================
#include "title.h"
#include "teamlogo.h"
#include "titlelogo.h"
#include "titlebackground.h"
#include "menu.h"
#include "ranking.h"

CTitleScene::CTitleScene()
{
}

CTitleScene::~CTitleScene()
{
}

//==========================================================================
// 初期化
bool CTitleScene::Init(void)
{
	CLight light;
	CTeamlogo * plogo;
	CTitlelogo * ptitlelogo;
	CTitlebackground * titlebackground;
	CRanking * rank;

	CMenu * menu;

	light.Init({ -1,-1,0 });

	CObject::NewObject(titlebackground);
	CObject::NewObject(ptitlelogo);
	CObject::NewObject(menu);
	CObject::NewObject(rank);
	CObject::NewObject(plogo);

	return CObject::InitAll();
}

//==========================================================================
// 解放
void CTitleScene::Uninit(void)
{
	CObject::UninitAll();
}

//==========================================================================
// 更新
void CTitleScene::Update(void)
{
	CObject::UpdateAll();
}

//==========================================================================
// 描画
void CTitleScene::Draw(void)
{
	CObject::DrawAll();
}
