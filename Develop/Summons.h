//==========================================================================
// 召喚[Summons.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _SUMMONS_H_
#define _SUMMONS_H_

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

//==========================================================================
//
// class  : CSummons
// Content: 召喚
//
//==========================================================================
class CSummons : private CTemplate
{
private:
	static constexpr int m_animpattern = 2;
public:
	// タイプリスト
	enum class TypeList
	{
		Trap = 0, // トラップ
		Spped, // スピード
		Attacker, // 攻撃特化
		Defender, // 防御特化
	};
	// タイプリスト
	enum class ObjTypes
	{
		enemy,
		player
	};
private:
	// パラメーター
	class CParam
	{
	private:
		// ステータス
		class CStatus
		{
		public:
			float m_Life; // HP
			float m_Attack; // 攻撃力
			float m_Spped; // 速度
			float m_SearchScale; // 索敵範囲
		};
	public:
		// パラメータセット
		void SetParam(float Life, float Attack, float Spped, float SearchScale, int AttackFrame, TypeList Type, ObjTypes _objtype);
		// 攻撃判定
		// 攻撃時 true 非攻撃時 false
		bool AttackFrameCount(void);
		// ステータスのゲッター
		CStatus *GetStatus(void) { return &this->m_Status; }
		// タイプのゲッター
		TypeList GetType(void) { return this->m_Type; }
	private:
		int m_AttackFrameCount = -1; // 攻撃フレーム
		TypeList m_Type; // タイプ
		CStatus m_Status; // ステータス
		int m_AttackFrame; // 攻撃間隔
	public:
		int m_animcount[2]; // アニメーションのカウンタ
		int m_motionselect; // モーション選択
		bool m_lock;
		ObjTypes objtype;
	};
	// 箱
	class CCase
	{
	public:
		C3DObject * m_Pos; // ターゲット
		CParam *m_Parameter; // パラメーター
	};
	// 座標
	class CPosData
	{
	public:
		C3DObject m_Pos; // 座標
		C3DObject m_OldPos; // 古い座標
	};
	// データ
	class CData
	{
	public:
		static const int m_MaxCase = (int)TypeList::Defender;
	public:
		int m_ID; // 管理ID 
		CData *m_pNext; // 次のアドレス
		CData *m_pBack; // 前のアドレス
		CParam m_Parameter; // パラメーター
		CPosData m_PosData; // 座標データ
		CCase m_Target; // ターゲット
		CCase m_Hate[m_MaxCase]; // ヘイトケース
		bool m_Release; // 解放キー
	};
public:
	typedef CSummons::CPosData Pos_t;// 座標
	typedef CSummons::CParam Param_t;// パラメーター
	typedef CSummons::CData Data_t;// データ
public:
	CSummons();
	~CSummons();

	// 初期化
	bool Init(void);

	// 初期化
	bool TexInit(CBillboard::CAnimationData **pInput, int num);
	// 解放
	void Uninit(void);
	// 解放
	void TexUninit(void);
	// 更新
	void Update(void);
	// 描画
	void Draw(void);

	// 生成
	// 第一引数 生成座標
	// 第二引数 パラメータ
	void Create(const C3DObject *Input, CParam * Param);

	// 特定のデータ消去
	// 引数 管理ID
	void PinpointDelete(const int * ID);

	// ゲッター データがない場合null
	CData *GetData(const int *Count);
	// ターゲットをセット
	// 戻り値 : 誰をターゲットしたか
	CData * SetTarget(CSummons::CData * pTarget, CSummons::CData * pTargetB);
private:
	// 解放キーのセット
	void ReleaseKeySet(CData * pData);
	// ライフ管理
	CData * LifeManagement(CData * pData);
private:
	CBillboard **m_poly = nullptr;
	CData *m_pStartAddress = nullptr; // 先頭アドレス
	int m_NumData; // データ数
	int m_IDCount; // IDカウンタ
	int m_NumCharacter;
};

#endif // !_SUMMONS_H_
