//==========================================================================
// スクリーン[Screen.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _Screen_H_
#define _Screen_H_

//==========================================================================
// Include
//==========================================================================
#include "dxlib.h"
#include "SceneChange.h"
#include "LoadScreen.h"
#include "UserCamera.h"
#include "Fade.h"
#include "debug.h"

//==========================================================================
//
// class  : CScreen
// Content: スクリーン
//
//==========================================================================
class CScreen : private CSetRender, private CSetSampler, private CTemplate
{
public:
	typedef CSceneManager::SceneName scenelist_t;
public:
	CScreen();
	~CScreen();
	// 初期化
	bool Init(HINSTANCE hInstance, HWND hWnd);
	// 解放
	void Uninit(void);
	// 更新
	bool Update(void);
	// 描画
	void Draw(void);
	// シーン変更キー
	static void screenchange(scenelist_t Name);
private:
	// スクリーンの切り替え
	bool change(scenelist_t Name);
	// 最初の初期化
	bool startinit(void);
	// 最初の初期化時の描画
	void startdraw(void);
	// 最初の初期化データの破棄
	void startdatarelease(void);
	// スクリーンの初期化
	bool screeninit(void);
	// フェード
	void fade(void);

	// 描画モード
	void Fillmode(LPDIRECT3DDEVICE9 pDevice);

	// フィルタリング
	void Fittering(LPDIRECT3DDEVICE9 pDevice);

	// 更新の停止
	bool Updatestop(void);
private:
	CFade m_fade; // フェード
	CLoadScreen m_load; // ロード画面
	CSceneManager m_scene; // スクリーンセット
	CUserCamera m_camera; // カメラ
	CDebug m_debug; // デバッグ
	CImGui_Dx9 m_ImGui;
	bool m_debugkey; // デバッグキー
	bool m_start; // 初期化フラグ
	bool m_change; // 切り替えフラグ
	int m_count; // カウンタ
	bool m_bFillmode;
	bool m_bFittering;
	bool m_bUpdatestop;
	bool m_bstartinitdata; // 初期時格納データを設定する場合このフラグをtrueに
	bool m_bstartdraw;

	CText m_text; // テキスト

	static bool m_screenchange;
	static scenelist_t m_name; // name
};

#endif // !_Screen_H_
