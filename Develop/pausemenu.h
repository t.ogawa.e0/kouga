//==========================================================================
// ポーズのメニュー[pausemenu.h]
// author : 
//==========================================================================
#ifndef _pausemenu_h_
#define _pausemenu_h_

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

//==========================================================================
//
// class  : CPausemenu
// Content: ポーズのメニュー
//
//==========================================================================
class CPausemenu : private CTemplate
{
public:
	CPausemenu();
	~CPausemenu();
	// 初期化
	bool Init(void);
	// 解放
	void Uninit(void);
	// 更新
	void Update(void);
	// 描画
	void Draw(void);
private:
	C2DObject *m_pos; // 座標
	C2DObject *m_masterpos; // マスター座標
	C2DPolygon *m_poly; // ポリゴン
	int m_MENU;
	int m_numdata; // データ数
	CImGui_Dx9 m_imgui;
public:
	static bool m_key; // 鍵
};

#endif // !_pausemenu_h_
