//==========================================================================
// ゲームカメラ[GameCamera.cpp]
// author: keita yanagidate
//==========================================================================
#include "GameCamera.h"

//==========================================================================
// 実体
//==========================================================================
CCamera *CGameCamera::m_Camera; // カメラ

CGameCamera::CGameCamera()
{
	this->m_CameraPos = nullptr;
}

CGameCamera::~CGameCamera()
{

}

//==========================================================================
// 初期化
bool CGameCamera::Init(void)
{
	C3DObject Eye;
	C3DObject At;

	// メモリ確保
	this->New(this->m_Camera);
	this->New(this->m_CameraPos);

	// カメラ座標設定
	this->m_CameraPos->Init(0);
	this->m_CameraPos->MoveZ(-20);
	this->m_CameraPos->MoveY(20);

	// 視点座標設定
	Eye = *this->m_CameraPos;
	Eye.MoveY(4);
	At = Eye;
	Eye.MoveZ(-5);

	// カメラ本体の座標設定
	this->m_Camera->Init();
	this->m_Camera->Init(&Eye.Info.pos, &At.Info.pos);
	this->m_Camera->RotViewX(-0.7f);
	this->m_Camera->RotViewY(1.1f);

	// カメラ座標設定
	this->m_CameraPos->MoveZ(5);
	this->m_CameraPos->MoveX(40);

	return false;
}

//==========================================================================
// 解放
void CGameCamera::Uninit(void)
{
	this->m_Camera->Uninit();
	this->Delete(this->m_Camera);
	this->Delete(this->m_CameraPos);
}

//==========================================================================
// 更新
void CGameCamera::Update(void)
{
	// カメラ固定化
	this->m_Camera->SetEye(&this->m_CameraPos->Info.pos);
	this->m_Camera->SetAt(&this->m_CameraPos->Info.pos);
}

//==========================================================================
// 描画
void CGameCamera::Draw(void)
{

}
