//==========================================================================
// プレイヤー[Player.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _Player_H_
#define _Player_H_

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "Arrow.h"

//==========================================================================
//
// class  : CPlayer
// Content: プレイヤー
//
//==========================================================================
class CPlayer : private CTemplate, public CObject
{
private:
	static constexpr float m_MoveSpeed = 0.2f; // 移動速度?
private:
	enum class CMotion
	{
		taiki,
		houkou,
		kuchiyose,
		dame,
		max
	};
private:
	// パラメーター
	class CParam
	{
	private:
		static constexpr int m_MaxHP = 2000;
		static constexpr int m_MaxMP = 1000;
	private:
		class CData
		{
		public:
			int m_MP; //プレイヤーMP
			int m_HP; //プレイヤーHP
			bool m_Existence; // 存在
		};
	public:
		// 初期化
		void Init(void);
		// 更新
		void Update(void);
		// ダメージ
		void Damage(int Damage);
		// MP減少
		void DecreaseMp(int Mp);
		// リカバリー
		void RecoveryHp(int Hp);
		// リカバリー
		void RecoveryMp(int Mp);
		// ゲッター
		CData Get(void) { return this->m_Data; }
		void SetExistence(bool bExistence) { this->m_Data.m_Existence = bExistence; }
	private:
		CData m_Data; // データ
		CImGui_Dx9 m_ImGui;
	};
public:
	CPlayer();
	~CPlayer();
	// 初期化
	bool Init(void);
	// 解放
	void Uninit(void);
	// 更新
	void Update(void);
	// 描画
	void Draw(void);
	// 拠点索敵
	void SearchBase(void);

	void SetCamera(void);

	void MoveCamera(void);

	// 座標のゲッター
	static C3DObject * GetPos(void) { return m_Pos; }

	static CParam *GetParam(void) { return m_Param; }

	static D3DXMATRIX * GetCamera(void) { return m_Camera.CreateView(); }

	static void damagemotionstart(void) { m_damagekey = true; }

	static bool InitTex(void);
	static void UninitTex(void);
private:
	// 前進
	CVector<float> Advance(void);
	// 後進
	CVector<float> Backward(void);
	// 右
	CVector<float> Right(void);
	// 左
	CVector<float> Left(void);
	// 右前進
	CVector<float> AdvanceRight(void);
	// 左前進
	CVector<float> AdvanceLeft(void);
	// 右後進
	CVector<float> BackwardRight(void);
	// 左後進
	CVector<float> BackwardLeft(void);
	// 召喚
	void Summons(void);
	// 移動
	void Move(void);
	// 攻撃モーション
	void attackmotion(void);
	// ダメージモーション
	void damagemotion(void);
private:
	CArrow * m_Arrow; // 矢印
	CMotion m_motionselect;
	static CBillboard *m_poly; // 待機ポリゴン
	CImGui_Dx9 m_ImGui;

	int m_Count;
	int *m_AnumCount;
	int m_animselect;
	int m_oldanimselect;
	static int m_numdata;
	bool m_attakkey;
	C3DObject *m_OldPos; // 座標

	static bool m_damagekey;
	static C3DObject *m_Pos; // 座標
	static CParam *m_Param; // パラメーター
	static CCamera m_Camera; // カメラ

	CHitDetermination m_hit;
};

#endif // !_Player_H_
