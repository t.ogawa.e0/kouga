//==========================================================================
// プレイヤー[Player.cpp]
// author: tatuya ogawa
//==========================================================================
#include "Player.h"
#include "Ally_AI.h"
#include "Base.h"
#include "UserCamera.h"
#include "SummonsKey.h"
#include "Circle.h"
#include "Summonseffect.h"
#include "NoEntry.h"
#include "startUI.h"
#include "OrientationVector.h"

//==========================================================================
// 実体
//==========================================================================
C3DObject* CPlayer::m_Pos;
CPlayer::CParam *CPlayer::m_Param; // パラメーター
CCamera CPlayer::m_Camera; // カメラ
bool CPlayer::m_damagekey = false;
CBillboard *CPlayer::m_poly = nullptr;
int CPlayer::m_numdata = 0;

 CPlayer::CPlayer()
{
	 this->m_Arrow = nullptr;
	 this->m_Count = 0;
	 this->m_AnumCount = nullptr;
	 this->m_animselect = 0;
	 this->m_oldanimselect = 0;
	 this->m_motionselect = CMotion::max;
	 this->m_attakkey = false;
	 this->m_OldPos = nullptr;
}

CPlayer::~CPlayer()
{
}

//==========================================================================
// 初期化
bool CPlayer::Init(void)
{
	this->New(this->m_AnumCount, this->m_numdata);
	this->New(this->m_Arrow);
	this->New(this->m_Pos);
	this->New(this->m_OldPos);
	this->New(this->m_Param);

	// 初期化
	this->m_Pos->Init(0);
	this->SetCamera();

	this->m_Pos->MoveY(0.5f);

	// プレイヤーの座標変更
	this->m_Pos->MoveZ(-60);

	this->m_Pos->Scale(5.0f);

	// パラメーター初期化
	this->m_Param->Init();


	if (this->m_Arrow->Init("resource/texture/Game/UI/mokuteki_houkou.DDS"))
	{
		return true;
	}

	for (int i = 0; i < this->m_numdata; i++)
	{
		this->m_poly->AnimationCountInit(&this->m_AnumCount[i]);
	}

	this->m_Count = 0;
	this->m_animselect = 0;
	this->m_oldanimselect = -1;
	this->m_motionselect = CMotion::max;
	this->m_attakkey = false;
	this->m_damagekey = false;

	*this->m_OldPos = *this->m_Pos;

	// 初期化
	return false;
}

//==========================================================================
// 解放
void CPlayer::Uninit(void)
{
	this->m_Arrow->Uninit();

	//	メモリ解放
	this->Delete(this->m_AnumCount);
	this->Delete(this->m_Arrow);
	this->Delete(this->m_Pos);
	this->Delete(this->m_OldPos);
	this->Delete(this->m_Param);
}

//==========================================================================
// 更新
void CPlayer::Update(void)
{
	this->m_ImGui.NewWindow("Player", true);

	if (this->m_Param->Get().m_Existence)
	{
		this->Move();
		this->Summons();
		this->SearchBase();

		this->attackmotion();
		this->damagemotion();

		this->m_Count++;
		if (this->m_Count == 50)
		{
			this->m_Count = -1;
			this->m_Param->RecoveryMp(50);
		}
		this->m_Param->Update();
		this->MoveCamera();

		this->m_poly[(int)this->m_motionselect].UpdateAnimation(&this->m_AnumCount[(int)this->m_motionselect]);
	}

	this->m_ImGui.EndWindow();
}

//==========================================================================
// 描画
void CPlayer::Draw(void)
{
	if (this->m_Param->Get().m_Existence)
	{
		this->m_Pos->Draw(10, CDirectXDevice::GetD3DDevice());
		this->m_Arrow->Draw();

		this->m_poly[(int)this->m_motionselect].Draw(this->m_Pos, CUserCamera::GetView(), &this->m_AnumCount[(int)this->m_motionselect], false);
	}
}

//==========================================================================
// 拠点索敵
void CPlayer::SearchBase(void)
{
	D3DXVECTOR3 VecTor = D3DXVECTOR3(0, 0, 0);

	float fDistance = 1000000000.0f;
	CBase::BaseParam * basepos = nullptr;
	CBase::BaseParam * basetarget = nullptr;
	C3DObject pos = *this->m_Pos;
	int nBaseID = 0;

	// 最短の拠点の算出
	for (int i = 0; i < CBase::getmaxbase(); i++)
	{
		basepos = CBase::baseparam(i);
		if (basepos->m_Existence)
		{
			float fDistance2 = this->m_hit.Distance(&basepos->m_Pos, &pos);
			if (fDistance2<fDistance)
			{
				basetarget = basepos;
				fDistance = fDistance2;
				VecTor = pos.Info.pos - basepos->m_Pos.Info.pos;
				VecTor.y = 0;
				D3DXVec3Normalize(&VecTor, &VecTor);
				pos.Vec.Front = VecTor;
				pos.Look.Eye = VecTor;
				nBaseID = i;
			}
		}
	}
	pos.Scale(5);

	this->m_Arrow->Update(&pos);

	this->m_ImGui.Text("BaseID %d", nBaseID);
}

void CPlayer::SetCamera(void)
{
	C3DObject Eye = *m_Pos;
	C3DObject At = *m_Pos;

	this->m_Camera.Init();

	At = Eye;
	Eye.MoveZ(-20);
	this->m_Camera.Init(&Eye.Info.pos, &At.Info.pos);
	this->m_Camera.RotViewX(-0.76f);
	this->m_Camera.RotViewY(0.8f);
}

void CPlayer::MoveCamera(void)
{
	// 各カメラ情報に座標データをセット
	this->m_Camera.SetEye(&this->m_Pos->Info.pos);
	this->m_Camera.SetAt(&this->m_Pos->Info.pos);
}

//==========================================================================
// 召喚
void CPlayer::Summons(void)
{
#if defined(_DEBUG) || defined(DEBUG)
	static int count = 0;
#else
	int count = 0;
	count;
#endif
	if (!this->m_attakkey && !this->m_damagekey)
	{
		if (CKeyboard::Trigger(CKeyboard::KeyList::KEY_UP) || CController::CButton::Trigger(CController::CButton::Ckey::RightButton4))
		{
			if (CSummonsKey::SummonsGetkey(CSummonsKey::KeyList::RightButton4))
			{
				C3DObject Pos;
				CSummonsKey::SummonsSetkey(CSummonsKey::KeyList::RightButton4);
				CCircle::setsummonkey(CSummonsKey::KeyList::RightButton4);
				Pos.Init(0);
				Pos.Info.pos = CCircle::Get()->Info.pos;
				Pos.Info.pos.y = 1.4f;
				Pos.setindex((int)CSummons::TypeList::Attacker);
				Pos.Scale(5.0f);
				CAlly_AI::Summons(&Pos, CSummons::TypeList::Attacker);
				CSummonseffect::create(CCircle::Get());
				count++;
				this->m_Param->DecreaseMp(20);
				this->m_attakkey = true;
			}
		}

		if (CKeyboard::Trigger(CKeyboard::KeyList::KEY_DOWN) || CController::CButton::Trigger(CController::CButton::Ckey::RightButton2))
		{
			if (CSummonsKey::SummonsGetkey(CSummonsKey::KeyList::RightButton2))
			{
				C3DObject Pos;
				D3DXVECTOR3 vpos1;
				D3DXVECTOR3 vpos2;

				CSummonsKey::SummonsSetkey(CSummonsKey::KeyList::RightButton2);
				CCircle::setsummonkey(CSummonsKey::KeyList::RightButton2);

				// 座標入れ替え
				Pos.Init(0);
				Pos.Info.pos = this->m_Pos->Info.pos;
				Pos.Info.pos.y = 1.4f;
				Pos.setindex((int)CSummons::TypeList::Defender);
				Pos.Scale(5.0f);

				CAlly_AI::Summons(&Pos, CSummons::TypeList::Defender);
				CSummonseffect::create(&Pos);

				this->m_Pos->Info.pos.x = CCircle::Get()->Info.pos.x;
				this->m_Pos->Info.pos.z = CCircle::Get()->Info.pos.z;
				CSummonseffect::create(this->m_Pos);

				count++;
				this->m_Param->DecreaseMp(20);
				this->m_attakkey = true;
			}
		}

		if (CKeyboard::Trigger(CKeyboard::KeyList::KEY_LEFT) || CController::CButton::Trigger(CController::CButton::Ckey::RightButton1))
		{
			if (CSummonsKey::SummonsGetkey(CSummonsKey::KeyList::RightButton1))
			{
				C3DObject Pos;
				CSummonsKey::SummonsSetkey(CSummonsKey::KeyList::RightButton1);
				CCircle::setsummonkey(CSummonsKey::KeyList::RightButton1);
				CSummonseffect::create(CCircle::Get());
				Pos.Init(0);
				Pos.Info.pos = CCircle::Get()->Info.pos;
				Pos.Info.pos.y = 1.4f;
				Pos.setindex((int)CSummons::TypeList::Trap);
				Pos.Scale(5.0f);
				CAlly_AI::Summons(&Pos, CSummons::TypeList::Trap);
				CSummonseffect::create(CCircle::Get());
				count++;
				this->m_Param->DecreaseMp(10);
				this->m_attakkey = true;
			}
		}

		if (CKeyboard::Trigger(CKeyboard::KeyList::KEY_RIGHT) || CController::CButton::Trigger(CController::CButton::Ckey::RightButton3))
		{
			if (CSummonsKey::SummonsGetkey(CSummonsKey::KeyList::RightButton3))
			{
				C3DObject Pos;
				CSummonsKey::SummonsSetkey(CSummonsKey::KeyList::RightButton3);
				CCircle::setsummonkey(CSummonsKey::KeyList::RightButton3);
				Pos.Init(0);
				Pos.Info.pos = CCircle::Get()->Info.pos;
				Pos.Info.pos.y = 1.4f;
				Pos.setindex((int)CSummons::TypeList::Spped);
				Pos.Scale(5.0f);
				CAlly_AI::Summons(&Pos, CSummons::TypeList::Spped);
				CSummonseffect::create(CCircle::Get());
				count++;
				this->m_Param->DecreaseMp(10);
				this->m_attakkey = true;
			}
		}
	}

	this->m_ImGui.Text("TotalSummons %d", count);
}

bool CPlayer::InitTex(void)
{
	//==========================================================================
	// プレイヤー
	//==========================================================================

	// 待機//31
	CBillboard::CAnimationDataCase texclass1 =
	{
		"resource/texture/Game/Player/NINJA/taiki/ushiro.DDS",
		"resource/texture/Game/Player/NINJA/taiki/hidari.DDS",
		"resource/texture/Game/Player/NINJA/taiki/mae.DDS",
		"resource/texture/Game/Player/NINJA/taiki/migi.DDS",
		4, 2, 31, 31,CBillboard::PlateList::Vertical, false
	};

	// 歩行//8
	CBillboard::CAnimationDataCase texclass2 =
	{
		"resource/texture/Game/Player/NINJA/houkou/ushiro.DDS",
		"resource/texture/Game/Player/NINJA/houkou/hidari.DDS",
		"resource/texture/Game/Player/NINJA/houkou/mae.DDS",
		"resource/texture/Game/Player/NINJA/houkou/migi.DDS",
		4, 2, 8, 8,CBillboard::PlateList::Vertical, false
	};

	// 口寄せ//11 召喚
	CBillboard::CAnimationDataCase texclass3 =
	{
		"resource/texture/Game/Player/NINJA/kuchiyose/ushiro.DDS",
		"resource/texture/Game/Player/NINJA/kuchiyose/hidari.DDS",
		"resource/texture/Game/Player/NINJA/kuchiyose/mae.DDS",
		"resource/texture/Game/Player/NINJA/kuchiyose/migi.DDS",
		4, 2, 11, 11,CBillboard::PlateList::Vertical, false
	};

	// ダメージ//9
	CBillboard::CAnimationDataCase texclass4 =
	{
		"resource/texture/Game/Player/NINJA/dame/ushiro.DDS",
		"resource/texture/Game/Player/NINJA/dame/hidari.DDS",
		"resource/texture/Game/Player/NINJA/dame/mae.DDS",
		"resource/texture/Game/Player/NINJA/dame/migi.DDS",
		4, 2, 9, 9,CBillboard::PlateList::Vertical, false
	};

	CBillboard::CAnimationData ptexdata[] =
	{
		texclass1,
		texclass2,
		texclass3,
		texclass4,
	};

	// メモリ確保

	m_numdata = Sizeof(ptexdata);
	New(m_poly, m_numdata);
	for (int i = 0; i < m_numdata; i++)
	{
		if (m_poly[i].Init(
			ptexdata[i].m_list.m_texname,
			ptexdata[i].m_list.m_numdata,
			ptexdata[i].m_list.m_frame,
			ptexdata[i].m_list.m_pattern,
			ptexdata[i].m_list.m_direction,
			ptexdata[i].m_list.m_plate,
			ptexdata[i].m_list.m_centermood
		))
		{
			return true;
		}
	}
	return false;
}

void CPlayer::UninitTex(void)
{
	// 解放
	for (int i = 0; i < m_numdata; i++)
	{
		m_poly[i].Uninit();
	}
	m_numdata = 0;
	Delete(m_poly);
}

//==========================================================================
// 前進
CVector<float> CPlayer::Advance(void)
{
	// 歩きモーションに切り替え
	if (this->m_motionselect != CMotion::houkou)
	{
		this->m_motionselect = CMotion::houkou;
		this->m_AnumCount[(int)this->m_motionselect] = 0;
	}

	this->m_animselect = 0;
	this->m_oldanimselect = this->m_animselect;
	this->m_Pos->SetVecFront({ -1,0,1 });

	return CVector<float>(0.0f, -1.3f, 0.0f, 0.0f);
}

//==========================================================================
// 後進
CVector<float> CPlayer::Backward(void)
{
	// 歩きモーションに切り替え
	if (this->m_motionselect != CMotion::houkou)
	{
		this->m_motionselect = CMotion::houkou;
		this->m_AnumCount[(int)this->m_motionselect] = 0;
	}

	this->m_animselect = 2;
	this->m_oldanimselect = this->m_animselect;
	this->m_Pos->SetVecFront({ 1,0,-1 });

	return CVector<float>(0.0f, -1.3f, 0.0f, 0.0f);
}

//==========================================================================
// 右
CVector<float> CPlayer::Right(void)
{
	// 歩きモーションに切り替え
	if (this->m_motionselect != CMotion::houkou)
	{
		this->m_motionselect = CMotion::houkou;
		this->m_AnumCount[(int)this->m_motionselect] = 0;
	}

	this->m_animselect = 3;
	this->m_oldanimselect = this->m_animselect;
	this->m_Pos->SetVecFront({ 1,0,1 });

	return CVector<float>(0.0f, -1.3f, 0.0f, 0.0f);
}

//==========================================================================
// 左
CVector<float> CPlayer::Left(void)
{
	// 歩きモーションに切り替え
	if (this->m_motionselect != CMotion::houkou)
	{
		this->m_motionselect = CMotion::houkou;
		this->m_AnumCount[(int)this->m_motionselect] = 0;
	}

	this->m_animselect = 1;
	this->m_oldanimselect = this->m_animselect;
	this->m_Pos->SetVecFront({ -1,0,-1 });

	return CVector<float>(0.0f, -1.3f, 0.0f, 0.0f);
}

//==========================================================================
// 右前進
CVector<float> CPlayer::AdvanceRight(void)
{
	// 歩きモーションに切り替え
	if (this->m_motionselect != CMotion::houkou)
	{
		this->m_motionselect = CMotion::houkou;
		this->m_AnumCount[(int)this->m_motionselect] = 0;
	}

	if (this->m_oldanimselect == 0)
	{
		this->m_animselect = 0;
	}
	else if (this->m_oldanimselect == 3)
	{
		this->m_animselect = 3;
	}
	else
	{
		this->m_animselect = 0;
	}
	this->m_Pos->SetVecFront({ 0,0,1 });

	return CVector<float>(0.0f, -1.3f, 0.0f, 0.0f);
}

//==========================================================================
// 左前進
CVector<float> CPlayer::AdvanceLeft(void)
{
	// 歩きモーションに切り替え
	if (this->m_motionselect != CMotion::houkou)
	{
		this->m_motionselect = CMotion::houkou;
		this->m_AnumCount[(int)this->m_motionselect] = 0;
	}

	if (this->m_oldanimselect == 0)
	{
		this->m_animselect = 0;
	}
	else if (this->m_oldanimselect == 1)
	{
		this->m_animselect = 1;
	}
	else
	{
		this->m_animselect = 0;
	}
	this->m_Pos->SetVecFront({ -1,0,0 });

	return CVector<float>(0.0f, -1.3f, 0.0f, 0.0f);
}

//==========================================================================
// 右後進
CVector<float> CPlayer::BackwardRight(void)
{
	// 歩きモーションに切り替え
	if (this->m_motionselect != CMotion::houkou)
	{
		this->m_motionselect = CMotion::houkou;
		this->m_AnumCount[(int)this->m_motionselect] = 0;
	}

	if (this->m_oldanimselect == 2)
	{
		this->m_animselect = 2;
	}
	else if (this->m_oldanimselect == 3)
	{
		this->m_animselect = 3;
	}
	else
	{
		this->m_animselect = 2;
	}
	this->m_Pos->SetVecFront({ 1,0,0 });

	return CVector<float>(0.0f, -1.3f, 0.0f, 0.0f);
}

//==========================================================================
// 左後進
CVector<float> CPlayer::BackwardLeft(void)
{
	// 歩きモーションに切り替え
	if (this->m_motionselect != CMotion::houkou)
	{
		this->m_motionselect = CMotion::houkou;
		this->m_AnumCount[(int)this->m_motionselect] = 0;
	}

	if (this->m_oldanimselect == 2)
	{
		this->m_animselect = 2;
	}
	else if (this->m_oldanimselect == 1)
	{
		this->m_animselect = 1;
	}
	else
	{
		this->m_animselect = 2;
	}
	this->m_Pos->SetVecFront({ 0,0,-1 });

	return CVector<float>(0.0f, -1.3f, 0.0f, 0.0f);
}

//==========================================================================
// 移動
void CPlayer::Move(void)
{
	this->m_ImGui.Text("move X %.2f", (float)CController::LeftStick().m_LeftRight);
	this->m_ImGui.Text("move Y %.2f", (float)CController::LeftStick().m_UpUnder);
	if (!this->m_attakkey && !this->m_damagekey)
	{
		this->m_OldPos->Info = this->m_Pos->Info;
		this->m_OldPos->Look = this->m_Pos->Look;
		this->m_OldPos->Vec = this->m_Pos->Vec;

		// ↑
		if ((float)CController::LeftStick().m_LeftRight == 0.0f && (float)CController::LeftStick().m_UpUnder <= -1.0f)
		{
			this->Advance();
			if (CStartUI::GetKey())
			{
				this->m_Pos->MoveZ(this->m_MoveSpeed);
			}
		}
		// ↓
		else if ((float)CController::LeftStick().m_LeftRight == 0.0f && 1.0f <= (float)CController::LeftStick().m_UpUnder)
		{
			this->Backward();
			if (CStartUI::GetKey())
			{
				this->m_Pos->MoveZ(this->m_MoveSpeed);
			}
		}
		// →
		else if (1.0f <= (float)CController::LeftStick().m_LeftRight && (float)CController::LeftStick().m_UpUnder == 0.0f)
		{
			this->Right();
			if (CStartUI::GetKey())
			{
				this->m_Pos->MoveZ(this->m_MoveSpeed);
			}
		}
		// ←
		else if ((float)CController::LeftStick().m_LeftRight <= -1.0f && (float)CController::LeftStick().m_UpUnder == 0.0f)
		{
			this->Left();
			if (CStartUI::GetKey())
			{
				this->m_Pos->MoveZ(this->m_MoveSpeed);
			}
		}
		// 右上
		else if (1.0f <= (float)CController::LeftStick().m_LeftRight && (float)CController::LeftStick().m_UpUnder <= -1.0f)
		{
			this->AdvanceRight();
			if (CStartUI::GetKey())
			{
				this->m_Pos->MoveZ(this->m_MoveSpeed);
			}
		}
		// 左上
		else if ((float)CController::LeftStick().m_LeftRight <= -1.0f && (float)CController::LeftStick().m_UpUnder <= -1.0f)
		{
			this->AdvanceLeft();
			if (CStartUI::GetKey())
			{
				this->m_Pos->MoveZ(this->m_MoveSpeed);
			}
		}
		// 右下
		else if (1.0f <= (float)CController::LeftStick().m_LeftRight && 1.0f <= (float)CController::LeftStick().m_UpUnder)
		{
			this->BackwardRight();
			if (CStartUI::GetKey())
			{
				this->m_Pos->MoveZ(this->m_MoveSpeed);
			}
		}
		// 左下
		else if ((float)CController::LeftStick().m_LeftRight <= -1.0f && 1.0f <= (float)CController::LeftStick().m_UpUnder)
		{
			this->BackwardLeft();
			if (CStartUI::GetKey())
			{
				this->m_Pos->MoveZ(this->m_MoveSpeed);
			}
		}
		else if (CKeyboard::Press(CKeyboard::KeyList::KEY_W) && CKeyboard::Press(CKeyboard::KeyList::KEY_A))
		{
			this->AdvanceLeft();
			if (CStartUI::GetKey())
			{
				this->m_Pos->MoveZ(this->m_MoveSpeed);
			}
		}
		else if (CKeyboard::Press(CKeyboard::KeyList::KEY_W) && CKeyboard::Press(CKeyboard::KeyList::KEY_D))
		{
			this->AdvanceRight();
			if (CStartUI::GetKey())
			{
				this->m_Pos->MoveZ(this->m_MoveSpeed);
			}
		}
		else if (CKeyboard::Press(CKeyboard::KeyList::KEY_S) && CKeyboard::Press(CKeyboard::KeyList::KEY_A))
		{
			this->BackwardLeft();
			if (CStartUI::GetKey())
			{
				this->m_Pos->MoveZ(this->m_MoveSpeed);
			}
		}
		else if (CKeyboard::Press(CKeyboard::KeyList::KEY_S) && CKeyboard::Press(CKeyboard::KeyList::KEY_D))
		{
			this->BackwardRight();
			if (CStartUI::GetKey())
			{
				this->m_Pos->MoveZ(this->m_MoveSpeed);
			}
		}
		else if (CKeyboard::Press(CKeyboard::KeyList::KEY_W))
		{
			this->Advance();
			if (CStartUI::GetKey())
			{
				this->m_Pos->MoveZ(this->m_MoveSpeed);
			}
		}
		else if (CKeyboard::Press(CKeyboard::KeyList::KEY_A))
		{
			this->Left();
			if (CStartUI::GetKey())
			{
				this->m_Pos->MoveZ(this->m_MoveSpeed);
			}
		}
		else if (CKeyboard::Press(CKeyboard::KeyList::KEY_S))
		{
			this->Backward();
			if (CStartUI::GetKey())
			{
				this->m_Pos->MoveZ(this->m_MoveSpeed);
			}
		}
		else if (CKeyboard::Press(CKeyboard::KeyList::KEY_D))
		{
			this->Right();
			if (CStartUI::GetKey())
			{
				this->m_Pos->MoveZ(this->m_MoveSpeed);
			}
		}
		else
		{
			if (this->m_motionselect != CMotion::taiki)
			{
				this->m_motionselect = CMotion::taiki;
				this->m_AnumCount[(int)this->m_motionselect] = 0;
			}
		}
	}

	// 当たり判定処理
	for (int i = 0; i < CNoEntry::m_snumdata; i++)
	{
		if (this->m_hit.Simple(CPlayer::GetPos(), CNoEntry::GetPos(i), 1.05f))
		{
			// 前回の座標を今の座標にコピー
			this->m_Pos->Info = this->m_OldPos->Info;
			this->m_Pos->Look = this->m_OldPos->Look;
			this->m_Pos->Vec = this->m_OldPos->Vec;
			break;
		}
	}

	this->m_Pos->setindex(this->m_animselect);

	this->m_ImGui.Text("Pos X=%.2f Y=%.2f , Z=%.2f", this->m_Pos->Info.pos.x, this->m_Pos->Info.pos.y, this->m_Pos->Info.pos.z);
}

//==========================================================================
// 攻撃モーション
void CPlayer::attackmotion(void)
{
	if (this->m_attakkey)
	{
		if (this->m_motionselect != CMotion::kuchiyose)
		{
			// 前回のモーションフレーム初期化
			this->m_AnumCount[(int)this->m_motionselect] = -1;

			this->m_motionselect = CMotion::kuchiyose;

			// 現在のモーションフレーム初期化
			this->m_AnumCount[(int)this->m_motionselect] = -1;
		}

		if (this->m_poly[(int)this->m_motionselect].GetPattanNum(this->m_Pos->getindex(), &this->m_AnumCount[(int)this->m_motionselect]))
		{
			this->m_motionselect = CMotion::taiki;
			this->m_attakkey = false;
		}
	}
}

//==========================================================================
// ダメージモーション
void CPlayer::damagemotion(void)
{
	if (this->m_damagekey)
	{
		if (this->m_motionselect != CMotion::dame)
		{
			// 前回のモーションフレーム初期化
			this->m_AnumCount[(int)this->m_motionselect] = -1;

			this->m_motionselect = CMotion::dame;

			// 現在のモーションフレーム初期化
			this->m_AnumCount[(int)this->m_motionselect] = -1;

			this->m_attakkey = false;
		}

		if (this->m_poly[(int)this->m_motionselect].GetPattanNum(this->m_Pos->getindex(), &this->m_AnumCount[(int)this->m_motionselect]))
		{
			this->m_motionselect = CMotion::taiki;
			this->m_damagekey = false;
		}
	}
}

//==========================================================================
// 初期化
void CPlayer::CParam::Init(void)
{
	this->m_Data.m_HP = this->m_MaxHP;
	this->m_Data.m_MP = this->m_MaxMP;
	this->m_Data.m_Existence = true;
}

//==========================================================================
// ダメージ
void CPlayer::CParam::Damage(int Damage)
{
	this->m_Data.m_HP -= Damage;

	if (this->m_Data.m_HP < 0)
	{
		this->m_Data.m_HP = 0;
		this->m_Data.m_Existence = false;
	}
}

//==========================================================================
// 更新
void CPlayer::CParam::Update(void)
{
	this->m_ImGui.Text("HP %d", this->m_Data.m_HP);
	this->m_ImGui.Text("MP %d", this->m_Data.m_MP);
}

//==========================================================================
// MP減少
void CPlayer::CParam::DecreaseMp(int Mp)
{
	this->m_Data.m_MP -= Mp;

	if (this->m_Data.m_MP < 0)
	{
		this->m_Data.m_MP = 0;
	}
}

//==========================================================================
// リカバリー
void CPlayer::CParam::RecoveryHp(int Hp)
{
	this->m_Data.m_HP += Hp;

	if (this->m_MaxHP < this->m_Data.m_HP)
	{
		this->m_Data.m_HP = this->m_MaxHP;
	}
}

//==========================================================================
// リカバリー
void CPlayer::CParam::RecoveryMp(int Mp)
{
	this->m_Data.m_MP += Mp;

	if (this->m_MaxMP < this->m_Data.m_MP)
	{
		this->m_Data.m_MP = this->m_MaxMP;
	}
}
