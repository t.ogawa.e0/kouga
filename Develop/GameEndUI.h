//==========================================================================
// ゲーム終了演出[GameEndUI.h]
// author : 
//==========================================================================
#ifndef _GameEndUI_h_
#define _GameEndUI_h_

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

//==========================================================================
//
// class  : CGameEndUI
// Content: ゲーム終了演出
//
//==========================================================================
class CGameEndUI : private CTemplate, public CObject
{
public:
	CGameEndUI();
	~CGameEndUI();
	// 初期化
	bool Init(void);
	// 解放
	void Uninit(void);
	// 更新
	void Update(void);
	// 描画
	void Draw(void);
	// 処理のアクティブ化
	static void ActiveKey(void) { m_key = true; }
private:
	C2DObject *m_pos; // 座標
	C2DObject *m_masterpos; // マスター座標
	C2DPolygon *m_poly; // ポリゴン
	int m_numdata; // データ数
	int m_AnimCount; //フレームカウント変数
	int m_alpha;	//α値管理変数
	bool m_key2; // 鍵
public:
	static bool m_key; // 鍵
};

#endif // !_GameEndUI_h_
