#include "time.h"
#include "GameEndUI.h"
#include "startUI.h"

CTime::CTime()
{
	this->m_TimeUpKey = false;
	this->m_GetTime = 0;
	this->m_poly = nullptr;
	this->m_pos = nullptr;
	this->m_Time = nullptr;
	this->m_NumBer = nullptr;
}

CTime::~CTime()
{
}

bool CTime::Init(void)
{
	this->New(this->m_poly);
	this->New(this->m_pos);
	this->New(this->m_Time);
	this->New(this->m_NumBer);

	if (this->m_poly->Init("resource/texture/numbertex.DDS", true))
	{
		return true;
	}

	this->m_poly->SetTexScale(0, 0.7f);

	this->m_pos->Init(0);
	this->m_Time->Init(180, 60);

	this->m_GetTime = this->m_Time->GetTime();

	this->m_TimeUpKey = false;

	this->m_pos->Init(0, 1, 10, 10);

	this->m_pos->SetX(25 * CDirectXDevice::screenbuffscale());
	this->m_pos->SetY(70 * CDirectXDevice::screenbuffscale());

	this->m_NumBer->Init(6, true, false);

	return false;
}

void CTime::Uninit(void)
{
	this->m_poly->Uninit();

	this->Delete(this->m_poly);
	this->Delete(this->m_pos);
	this->Delete(this->m_Time);
	this->Delete(this->m_NumBer);
}

void CTime::Update(void)
{
	if (CStartUI::GetKey())
	{
		if (this->m_Time->Countdown())
		{
			this->m_TimeUpKey = true;
		}
		this->m_GetTime = this->m_Time->GetTime();
		if (this->m_TimeUpKey)
		{
			CGameEndUI::ActiveKey();
		}
	}
}

void CTime::Draw(void)
{
	this->m_NumBer->Draw(this->m_poly, this->m_pos, this->m_Time->GetTime());
}
