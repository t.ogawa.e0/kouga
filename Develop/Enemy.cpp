//==========================================================================
// エネミー[Enemy.cpp]
// author: tatuya ogawa
//==========================================================================
#include "Enemy.h"
#include "Ally_AI.h"
#include "Player.h"
#include "ExplosionEffect.h"
#include "failureUI.h"
#include "menu.h"

//==========================================================================
// 実体
//==========================================================================
CSummons * CEnemy::m_Summons;

CEnemy::CEnemy()
{
}

CEnemy::~CEnemy()
{
}

//==========================================================================
// 初期化
bool CEnemy::Init(void)
{
	//==========================================================================
	// スピードアタッカー 
	//==========================================================================

	// スピードアタッカー 移動
	CBillboard::CAnimationDataCase trap_idou =
	{
		"resource/texture/Game/Enemy/ARI/taiki/ushiro.DDS",
		"resource/texture/Game/Enemy/ARI/taiki/hidari.DDS",
		"resource/texture/Game/Enemy/ARI/taiki/mae.DDS",
		"resource/texture/Game/Enemy/ARI/taiki/migi.DDS",
		4, 2, 9, 9,CBillboard::PlateList::Vertical, false
	};

	// スピードアタッカー 攻撃
	CBillboard::CAnimationDataCase trap_kougeki =
	{
		"resource/texture/Game/Enemy/ARI/kougeki/ushiro.DDS",
		"resource/texture/Game/Enemy/ARI/kougeki/hidari.DDS",
		"resource/texture/Game/Enemy/ARI/kougeki/mae.DDS",
		"resource/texture/Game/Enemy/ARI/kougeki/migi.DDS",
		4, 2, 17, 17,CBillboard::PlateList::Vertical, false
	};

	// スピードアタッカー 
	CBillboard::CAnimationData trap[] =
	{
		trap_idou,
		trap_kougeki,
	};

	//==========================================================================
	// スピードアタッカー 
	//==========================================================================

	// スピードアタッカー 移動
	CBillboard::CAnimationDataCase speedattacker_idou =
	{
		"resource/texture/Game/Enemy/TOMBO/idou/ushiro.DDS",
		"resource/texture/Game/Enemy/TOMBO/idou/hidari.DDS",
		"resource/texture/Game/Enemy/TOMBO/idou/mae.DDS",
		"resource/texture/Game/Enemy/TOMBO/idou/migi.DDS",
		4, 2, 4, 4,CBillboard::PlateList::Vertical, false
	};

	// スピードアタッカー 攻撃
	CBillboard::CAnimationDataCase speedattacker_kougeki =
	{
		"resource/texture/Game/Enemy/TOMBO/kougeki/ushiro.DDS",
		"resource/texture/Game/Enemy/TOMBO/kougeki/hidari.DDS",
		"resource/texture/Game/Enemy/TOMBO/kougeki/mae.DDS",
		"resource/texture/Game/Enemy/TOMBO/kougeki/migi.DDS",
		4, 2, 16, 16,CBillboard::PlateList::Vertical, false
	};

	// スピードアタッカー 
	CBillboard::CAnimationData speedattacker[] =
	{
		speedattacker_idou,
		speedattacker_kougeki,
	};

	//==========================================================================
	// メインアタッカー
	//==========================================================================

	// メインアタッカー 移動
	CBillboard::CAnimationDataCase mainattacker_idou =
	{
		"resource/texture/Game/Enemy/BEE/idou/ushiro.DDS",
		"resource/texture/Game/Enemy/BEE/idou/hidari.DDS",
		"resource/texture/Game/Enemy/BEE/idou/mae.DDS",
		"resource/texture/Game/Enemy/BEE/idou/migi.DDS",
		4, 2, 2, 2,CBillboard::PlateList::Vertical, false
	};

	// メインアタッカー 攻撃
	CBillboard::CAnimationDataCase mainattacker_kougeki =
	{
		"resource/texture/Game/Enemy/BEE/kougeki/ushiro.DDS",
		"resource/texture/Game/Enemy/BEE/kougeki/hidari.DDS",
		"resource/texture/Game/Enemy/BEE/kougeki/mae.DDS",
		"resource/texture/Game/Enemy/BEE/kougeki/migi.DDS",
		4, 2, 16, 16,CBillboard::PlateList::Vertical, false
	};

	// メインアタッカー
	CBillboard::CAnimationData mainattacker[] =
	{
		mainattacker_idou,
		mainattacker_kougeki,
	};

	//==========================================================================
	// タンク
	//==========================================================================

	// 移動 待機
	CBillboard::CAnimationDataCase tank_taiki =
	{
		"resource/texture/Game/Enemy/SPIDER/idou/ushiro.DDS",
		"resource/texture/Game/Enemy/SPIDER/idou/hidari.DDS",
		"resource/texture/Game/Enemy/SPIDER/idou/mae.DDS",
		"resource/texture/Game/Enemy/SPIDER/idou/migi.DDS",
		4, 2, 9, 9,CBillboard::PlateList::Vertical, false
	};

	// タンク 攻撃
	CBillboard::CAnimationDataCase tank_kougeki =
	{
		"resource/texture/Game/Enemy/SPIDER/kougeki/ushiro.DDS",
		"resource/texture/Game/Enemy/SPIDER/kougeki/hidari.DDS",
		"resource/texture/Game/Enemy/SPIDER/kougeki/mae.DDS",
		"resource/texture/Game/Enemy/SPIDER/kougeki/migi.DDS",
		4, 2, 10, 10,CBillboard::PlateList::Vertical, false
	};

	// タンク
	CBillboard::CAnimationData tank[] =
	{
		tank_taiki,
		tank_kougeki,
	};

	//==========================================================================
	// キャラセット
	//==========================================================================

	// キャラクターセット
	CBillboard::CAnimationData * pcharacter[] =
	{
		trap,
		speedattacker,
		mainattacker,
		tank,
	};

	// メモリ確保
	this->m_temp.New_(this->m_Summons);

	// 初期化
	this->m_Summons->Init();
	return this->m_Summons->TexInit(pcharacter, this->m_temp.Sizeof_(pcharacter));
}

//==========================================================================
// 解放
void CEnemy::Uninit(void)
{
	// 解放
	this->m_Summons->Uninit();
	this->m_Summons->TexUninit();

	// メモリ解放
	this->m_temp.Delete_(this->m_Summons);
}

//==========================================================================
// 更新
void CEnemy::Update(void)
{
	CSummons::Data_t * penemy = nullptr;
	int nNum = 0;

	this->m_ImGui.NewWindow("Enemy", true);

	this->m_Summons->Update();

	for (int i = 0;;i++)
	{
		penemy = this->m_Summons->GetData(&i);
		if (penemy == nullptr)
		{
			break;
		}

		this->Rockoon(penemy);
		if (!this->Attack(penemy))
		{
			this->Move(penemy);
		}

		nNum = i;

		this->m_ImGui.Text
		(
			"ID = %d TypeID = %d \n Life = %.2f \n Attack = %.2f \n Spped = %.2f \n SearchScale = %.2f",
			penemy->m_ID,
			(int)penemy->m_Parameter.GetType(),
			penemy->m_Parameter.GetStatus()->m_Life,
			penemy->m_Parameter.GetStatus()->m_Attack,
			penemy->m_Parameter.GetStatus()->m_Spped,
			penemy->m_Parameter.GetStatus()->m_SearchScale
		);

		if (penemy->m_Parameter.GetType() == CSummons::TypeList::Trap)
		{
			penemy->m_Parameter.m_motionselect = (int)Motionpattern::idou;
			penemy->m_Parameter.m_animcount[0] = 9 - 1;
			penemy->m_Parameter.m_animcount[1] = 17 - 1;
		}
	}

	this->m_ImGui.Text("Release AI %d", nNum);

	this->m_ImGui.EndWindow();
}

//==========================================================================
// 描画
void CEnemy::Draw(void)
{
	this->m_Summons->Draw();
}

//==========================================================================
// 召喚
void CEnemy::Summons(C3DObject * pPos, CSummons::TypeList Type)
{
	CSummons::Param_t param;

	switch (CMenu::GetMenu())
	{
	case 0:
		// 召喚リスト
		switch (Type)
		{
		case CSummons::TypeList::Trap:
			param.SetParam(1.0f, 2147343647.0f, 0.0f, 5.0f, 1, Type, CSummons::ObjTypes::enemy);
			break;
		case CSummons::TypeList::Spped:
			param.SetParam(200.0f, 15, 0.3f, 10.0f, 20, Type, CSummons::ObjTypes::enemy);
			break;
		case CSummons::TypeList::Attacker:
			param.SetParam(400.0f, 20.0f, 0.1f, 15.0f, 30, Type, CSummons::ObjTypes::enemy);
			break;
		case CSummons::TypeList::Defender:
			param.SetParam(600.0f, 10.0f, 0.05f, 20.0f, 60, Type, CSummons::ObjTypes::enemy);
			break;
		default:
			param.SetParam(1.0f, 2147343647.0f, 0.0f, 5.0f, 1, Type, CSummons::ObjTypes::enemy);
			break;
		}
		break;
	case 1:
		// 召喚リスト
		switch (Type)
		{
		case CSummons::TypeList::Trap:
			param.SetParam(1.0f, 2147343647.0f, 0.0f, 3.0f, 1, Type, CSummons::ObjTypes::enemy);
			break;
		case CSummons::TypeList::Spped:
			param.SetParam(200.0f, 15, 0.3f, 7.0f, 20, Type, CSummons::ObjTypes::enemy);
			break;
		case CSummons::TypeList::Attacker:
			param.SetParam(400.0f, 20.0f, 0.1f, 11.0f, 30, Type, CSummons::ObjTypes::enemy);
			break;
		case CSummons::TypeList::Defender:
			param.SetParam(600.0f, 10.0f, 0.05f, 16.0f, 60, Type, CSummons::ObjTypes::enemy);
			break;
		default:
			param.SetParam(1.0f, 2147343647.0f, 0.0f, 3.0f, 1, Type, CSummons::ObjTypes::enemy);
			break;
		}
		break;
	case 2:
		// 召喚リスト
		switch (Type)
		{
		case CSummons::TypeList::Trap:
			param.SetParam(1.0f, 2147343647.0f, 0.0f, 1.0f, 1, Type, CSummons::ObjTypes::enemy);
			break;
		case CSummons::TypeList::Spped:
			param.SetParam(200.0f, 15, 0.3f, 5.0f, 20, Type, CSummons::ObjTypes::enemy);
			break;
		case CSummons::TypeList::Attacker:
			param.SetParam(400.0f, 20.0f, 0.1f, 8.0f, 30, Type, CSummons::ObjTypes::enemy);
			break;
		case CSummons::TypeList::Defender:
			param.SetParam(600.0f, 10.0f, 0.05f, 11.0f, 60, Type, CSummons::ObjTypes::enemy);
			break;
		default:
			param.SetParam(1.0f, 2147343647.0f, 0.0f, 1.0f, 1, Type, CSummons::ObjTypes::enemy);
			break;
		}
		break;
	default:
		// 召喚リスト
		switch (Type)
		{
		case CSummons::TypeList::Trap:
			param.SetParam(1.0f, 2147343647.0f, 0.0f, 5.0f, 1, Type, CSummons::ObjTypes::enemy);
			break;
		case CSummons::TypeList::Spped:
			param.SetParam(200.0f, 15, 0.3f, 10.0f, 20, Type, CSummons::ObjTypes::enemy);
			break;
		case CSummons::TypeList::Attacker:
			param.SetParam(400.0f, 20.0f, 0.1f, 15.0f, 30, Type, CSummons::ObjTypes::enemy);
			break;
		case CSummons::TypeList::Defender:
			param.SetParam(600.0f, 10.0f, 0.05f, 20.0f, 60, Type, CSummons::ObjTypes::enemy);
			break;
		default:
			param.SetParam(1.0f, 2147343647.0f, 0.0f, 5.0f, 1, Type, CSummons::ObjTypes::enemy);
			break;
		}
		break;
	}

	// 召喚
	m_Summons->Create(pPos, &param);
}

//==========================================================================
// ゲッタ
CSummons::Data_t * CEnemy::GetData(int num)
{
	CSummons::Data_t * p = m_Summons->GetData(&num);

	if (p == nullptr)
	{
		return nullptr;
	}

	return p;
}

//==========================================================================
// ロックオン
void CEnemy::Rockoon(CSummons::Data_t * pData)
{
	CSummons::Data_t * pPos = nullptr;
	
	for (int i = 0; ; i++)
	{
		// アドレスを記憶
		pPos = CAlly_AI::GetData(i);

		if (pPos == nullptr)
		{
			break;
		}

		// ロックオン可能範囲
		if (this->m_hit.Ball(&pData->m_PosData.m_Pos, &pPos->m_PosData.m_Pos, pData->m_Parameter.GetStatus()->m_SearchScale))
		{
			// ロックオンできたら終了
			if (this->m_Summons->SetTarget(pPos, pData) != nullptr)
			{
				break;
			}
		}
	}
}

//==========================================================================
// AI処理
void CEnemy::Move(CSummons::Data_t * pData)
{
	// 追従処理は、対象の座標から動かす対象の座標を引いて出てきたベクトルを使用する
	// ターゲット座標がある場合
	if (pData->m_Target.m_Pos != nullptr)
	{
		D3DXVECTOR3 VecTor = D3DXVECTOR3(0, 0, 0);

		VecTor = pData->m_Target.m_Pos->Info.pos - pData->m_PosData.m_Pos.Info.pos;
		D3DXVec3Normalize(&VecTor, &VecTor);
		pData->m_PosData.m_Pos.Vec.Front = VecTor;
		pData->m_PosData.m_Pos.Look.Eye = -VecTor;
		pData->m_PosData.m_Pos.MoveZ(pData->m_Parameter.GetStatus()->m_Spped);

		// 移動
		this->SelectAnimation(pData, Motionpattern::idou);

		if (this->m_hit.Ball(&pData->m_PosData.m_Pos, pData->m_Target.m_Pos, 1.5f))
		{
			pData->m_PosData.m_Pos.MoveZ(-(pData->m_Parameter.GetStatus()->m_Spped));
		}
	}
	// ターゲット座標がない場合
	else if (pData->m_Target.m_Pos == nullptr)
	{
		if (CPlayer::GetParam()->Get().m_Existence)
		{
			D3DXVECTOR3 VecTor = D3DXVECTOR3(0, 0, 0);

			VecTor = CPlayer::GetPos()->Info.pos - pData->m_PosData.m_Pos.Info.pos;
			D3DXVec3Normalize(&VecTor, &VecTor);
			pData->m_PosData.m_Pos.Vec.Front = VecTor;
			pData->m_PosData.m_Pos.Look.Eye = -VecTor;
			pData->m_PosData.m_Pos.MoveZ(pData->m_Parameter.GetStatus()->m_Spped);

			// 攻撃有効範囲
			if (this->m_hit.Ball(CPlayer::GetPos(), &pData->m_PosData.m_Pos, 2.0f))
			{
				if (pData->m_Parameter.AttackFrameCount())
				{
					// トラップと拠点は反応しない
					if (pData->m_Parameter.GetType() != CSummons::TypeList::Trap)
					{
						CPlayer::GetParam()->Damage((int)(pData->m_Parameter.GetStatus()->m_Attack + 0.5f));
						CPlayer::damagemotionstart();
						if (CPlayer::GetParam()->Get().m_HP <= 0)
						{
							CExplosionEffect::create(CPlayer::GetPos());
							CExplosionEffect::create(CPlayer::GetPos());
							CExplosionEffect::create(CPlayer::GetPos());
							CFailureUI::ActiveKey();
						}
						this->SelectAnimation(pData, Motionpattern::kougeki);
					}
				}
			}
			else
			{
				// 移動
				this->SelectAnimation(pData, Motionpattern::idou);
			}
		}
	}
}

bool CEnemy::Attack(CSummons::Data_t * pData)
{
	if (pData->m_Target.m_Pos != nullptr)
	{
		// 攻撃有効範囲
		if (this->m_hit.Ball(pData->m_Target.m_Pos, &pData->m_PosData.m_Pos, 2.0f))
		{
			// 攻撃フレーム
			if (pData->m_Parameter.AttackFrameCount())
			{
				// トラップと拠点は反応しない
				if (pData->m_Parameter.GetType() == CSummons::TypeList::Trap)
				{
					pData->m_Target.m_Parameter->GetStatus()->m_Life -= pData->m_Parameter.GetStatus()->m_Attack;
					pData->m_Parameter.GetStatus()->m_Life = -1;
				}
				else
				{
					// ターゲットに攻撃
					pData->m_Target.m_Parameter->GetStatus()->m_Life -= pData->m_Parameter.GetStatus()->m_Attack;
				}
			}
			this->SelectAnimation(pData, Motionpattern::kougeki);
			return true;
		}
	}
	return false;
}

//==========================================================================
// 向きアニメーション
void CEnemy::SelectAnimation(CSummons::Data_t * pData, Motionpattern motion)
{
	// モーション切り替え
	if (pData->m_Parameter.m_motionselect != (int)motion)
	{
		pData->m_Parameter.m_animcount[pData->m_Parameter.m_motionselect] = 0;
		pData->m_Parameter.m_motionselect = (int)motion;
		pData->m_Parameter.m_animcount[pData->m_Parameter.m_motionselect] = 0;
	}

	// 前進
	if (this->Advance(pData->m_PosData.m_Pos.Vec.Front))
	{
		pData->m_PosData.m_Pos.setindex(0);
	}
	// 後進
	if (this->Backward(pData->m_PosData.m_Pos.Vec.Front))
	{
		pData->m_PosData.m_Pos.setindex(2);
	}
	// 右
	if (this->Right(pData->m_PosData.m_Pos.Vec.Front))
	{
		pData->m_PosData.m_Pos.setindex(3);
	}
	// 左
	if (this->Left(pData->m_PosData.m_Pos.Vec.Front))
	{
		pData->m_PosData.m_Pos.setindex(1);
	}
}
