//==========================================================================
// スタート演出[startUI.h]
// author : 
//==========================================================================
#ifndef _startUI_h_
#define _startUI_h_

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

//==========================================================================
//
// class  : CStartUI
// Content: スタート演出
//
//==========================================================================
class CStartUI : private CTemplate, public CObject
{
public:
	CStartUI();
	~CStartUI();
	// 初期化
	bool Init(void);
	// 解放
	void Uninit(void);
	// 更新
	void Update(void);
	// 描画
	void Draw(void);

	static bool GetKey(void) { return m_key; }
private:
	C2DObject *m_pos; // 座標
	C2DObject *m_masterpos; // マスター座標
	C2DPolygon *m_poly; // ポリゴン
	int m_numdata; // データ数
	int m_AnimCount; //フレームカウント変数
	bool m_key2; // 鍵
	int m_Count;
	static bool m_key; // 鍵
};

#endif // !_startUI_h_
