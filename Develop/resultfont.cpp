//==========================================================================
// resultフォント処理[resultfont.cpp]
// author: joja taiyo
//==========================================================================
#include "resultfont.h"

//==========================================================================
// 初期化
bool CResultfont::Init()
{
	this->New(this->m_Pos);
	this->New(this->m_Poly);

	if (this->m_Poly->Init("resource/texture/Resultフォント.DDS", true))
	{
		return true;
	}

	this->m_Pos->Init(0);
	this->m_Pos->SetCentralCoordinatesMood(true);
	this->m_Pos->SetX((float)(CDirectXDevice::GetWindowsSize().m_Width / 2));
	this->m_Pos->SetY((float)(this->m_Poly->GetTexSize(this->m_Pos->getindex())->h / 2.0f));

	return false;
}

//==========================================================================
// 解放
void CResultfont::Uninit()
{
	this->m_Poly->Uninit();

	this->Delete(this->m_Pos);
	this->Delete(this->m_Poly);
}

//==========================================================================
// 更新
void CResultfont::Update()
{
}

//==========================================================================
// 描画
void CResultfont::Draw()
{
	this->m_Poly->Draw(this->m_Pos);
}
