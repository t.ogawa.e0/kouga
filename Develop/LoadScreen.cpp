//==========================================================================
// ロードスクリーン[LoadScreen.cpp]
// author: tatuya ogawa
//==========================================================================
#include "LoadScreen.h"

CLoadScreen::CLoadScreen()
{
	this->m_posback = nullptr;
	this->m_polyback = nullptr;

	this->m_posload = nullptr;
	this->m_polyload = nullptr;
	this->m_paramload = nullptr;

	this->m_poslink = nullptr;
	this->m_polylink = nullptr;
}

CLoadScreen::~CLoadScreen()
{
}

//==========================================================================
// 初期化
bool CLoadScreen::Init(void)
{
	CTexvec<int> vwinsize = CTexvec<int>(0, 0, CDirectXDevice::GetWindowsSize().m_Width, CDirectXDevice::GetWindowsSize().m_Height);
	CVector<float> vpos = CVector<float>(0, 0, 0, 0);
	CTexvec<int> *ptexsize = nullptr;

	this->New(this->m_posback);
	this->New(this->m_polyback);

	this->New(this->m_posload);
	this->New(this->m_polyload);
	this->New(this->m_paramload);

	this->New(this->m_poslink);
	this->New(this->m_polylink);

	// テクスチャの確保
	if (this->m_polyback->Init()) { return true; }
	if (this->m_polyload->Init("resource/texture/NowLoading.DDS")) { return true; }
	if (this->m_polylink->Init("resource/texture/LoadTex.DDS")) { return true; }

	//==========================================================================
	// 背景
	*this->m_polyback->GetTexSize(0) = vwinsize;
	this->m_posback->Init(0);
	this->m_posback->SetColor(0, 0, 0, 255);

	//==========================================================================
	// リング
	ptexsize = this->m_polylink->GetTexSize(0);
	CTexvec<float> texsize = CTexvec<float>(0, 0, (float)ptexsize->w, (float)ptexsize->h);
	texsize *= 0.1f;
	*ptexsize = CTexvec<int>(0, 0, (int)texsize.w, (int)texsize.h);

	vpos = CVector<float>((float)vwinsize.w - (ptexsize->w*0.5f), (float)vwinsize.h - (ptexsize->h*0.5f), 0, 0);
	this->m_poslink->Init(0);
	this->m_poslink->SetCentralCoordinatesMood(true);
	this->m_poslink->SetColor(255, 255, 0, 255);
	this->m_poslink->SetPos(vpos);

	//==========================================================================
	// ロードフォント
	ptexsize = this->m_polyload->GetTexSize(0);
	texsize = CTexvec<float>(0, 0, (float)ptexsize->w, (float)ptexsize->h);
	texsize *= 0.1f;
	*ptexsize = CTexvec<int>(0, 0, (int)texsize.w, (int)texsize.h);

	this->m_paramload->m_a = 255;
	this->m_paramload->m_Change = false;
	this->m_paramload->m_a = 100;

	this->m_posload->Init(0);
	this->m_posload->SetCentralCoordinatesMood(true);
	this->m_posload->SetColor(255, 255, 0, this->m_paramload->m_a);
	this->m_posload->SetPos(vpos);


	return false;
}

//==========================================================================
// 解放
void CLoadScreen::Uninit(void)
{
	// テクスチャ解放
	this->m_polyback->Uninit();
	this->m_polyload->Uninit();
	this->m_polylink->Uninit();
	
	this->Delete(this->m_posback);
	this->Delete(this->m_polyback);

	this->Delete(this->m_posload);
	this->Delete(this->m_polyload);
	this->Delete(this->m_paramload);

	this->Delete(this->m_poslink);
	this->Delete(this->m_polylink);
}

//==========================================================================
// 更新
void CLoadScreen::Update(void)
{
	this->m_poslink->Angle(0.05f);

	this->Change(5);
	this->m_posload->SetColor(255, 255, 0, this->m_paramload->m_a);
}

//==========================================================================
// 描画
void CLoadScreen::Draw(void)
{
	this->m_polyback->Draw(this->m_posback);
	this->m_polyload->Draw(this->m_posload);
	this->m_polylink->Draw(this->m_poslink);
}

//==========================================================================
// αチェンジ
void CLoadScreen::Change(int Speed)
{
	// チェンジフラグ
	if (this->m_paramload->m_Change)
	{
		this->m_paramload->m_a -= Speed;
		if (this->m_paramload->m_a <= 0)
		{
			this->m_paramload->m_a = 0;
			this->m_paramload->m_Change = false;
		}
	}
	else
	{
		this->m_paramload->m_a += Speed;
		if (255 <= this->m_paramload->m_a)
		{
			this->m_paramload->m_a = 255;
			this->m_paramload->m_Change = true;
		}
	}
}
