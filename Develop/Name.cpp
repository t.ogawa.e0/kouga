//==========================================================================
// ネーム入力[Name.cpp]
// author : tatuya ogawa
//==========================================================================
#include "Name.h"
#include "keyboard.h"

//==========================================================================
// 実体
//==========================================================================
bool CName::m_OpenKey;
bool CName::m_EndKey;

CName::CName()
{
	this->m_MasterPos = nullptr;
	this->m_Pos = nullptr;
	this->m_Poly = nullptr;
	this->m_MoveKey = false;
	this->m_EndMove = 0;
	this->m_NumData = 0;
	this->m_acount = 0;
}

CName::~CName()
{
}

//==========================================================================
// 初期化
bool CName::Init(void)
{
	float fscale = 0.7f;
	float fpos2 = 0.0f;
	float fpos = 0.0f;
	float fMove = 70.0f;
	const char* TexLink[] =
	{
		"resource/texture/numbertex.DDS",
		"resource/texture/namefontcase/keyboardfontlist.DDS",
		"resource/texture/namefontcase/period_underbar.DDS",
	};

	fpos = (float)CDirectXDevice::GetWindowsSize().m_Width / 2;
	fpos2 = (fMove * 5);
	fpos2 = fpos2*fscale;
	fpos -= fpos2;

	fpos2 = fMove*fscale;
	fpos2 = fpos2*CDirectXDevice::screenbuffscale();

	// メモリ確保
	this->m_NumData = CNameKeyboard::GetNameLimit();
	this->New(this->m_Pos, this->m_NumData);
	this->New(this->m_MasterPos, this->m_NumData);
	this->New(this->m_Poly);

	if (this->m_Poly->Init(TexLink, this->Sizeof(TexLink), true))
	{
		return true;
	}

	for (int i = 0; i < (int)this->Sizeof(TexLink); i++)
	{
		this->m_Poly->SetTexScale(i, fscale);
	}

	for (int i = 0; i < this->m_NumData; i++)
	{
		this->m_Pos[i].Init(0);

		this->m_Pos[i].SetCentralCoordinatesMood(true);

		this->m_Pos[i].SetX(fpos);
		this->m_Pos[i].SetY((float)CDirectXDevice::GetWindowsSize().m_Height * 0.25f);

		fpos += fpos2;

		this->m_MasterPos[i] = this->m_Pos[i];
	}

	for (;;)
	{
		if (CDirectXDevice::GetWindowsSize().m_Width + 400.0f <= this->m_Pos[0].GetPos()->x)
		{
			break;
		}

		for (int i = 0; i < this->m_NumData; i++)
		{
			this->m_Pos[i].SetXPlus(50.0f);
		}
	}

	this->m_acount = 0;
	this->m_OpenKey = false;
	this->m_EndKey = false;
	this->m_MoveKey = false;
	this->m_EndMove = 0;

	return false;
}

//==========================================================================
// 解放
void CName::Uninit(void)
{
	// 解放
	this->m_Poly->Uninit();

	// メモリ解放
	this->Delete(this->m_Pos);
	this->Delete(this->m_Poly);
	this->Delete(this->m_MasterPos);

	this->m_NumData = 0;
	this->m_acount = 0;
	this->m_OpenKey = false;
	this->m_EndMove = false;
	this->m_MoveKey = false;
	this->m_EndMove = 0;
}

//==========================================================================
// 更新
void CName::Update(void)
{
	if (this->m_OpenKey)
	{
		if (this->m_MoveKey)
		{
			// 入力可能上限に達していない限り繰り返す
			if (CNameKeyboard::GetNameLimit() != CNameKeyboard::GetName().m_namecount)
			{
				CVector<float> vPos;

				for (int i = 0; i < this->m_NumData; i++)
				{
					// 座標格納
					vPos = *this->m_Pos[i].GetPos();

					// 初期化
					this->m_Pos[i].Init
					(
						CNameKeyboard::GetName().m_namedata[i].m_textureID,
						1,
						CNameKeyboard::GetName().m_namedata[i].Pattern,
						CNameKeyboard::GetName().m_namedata[i].Direction
					);
					this->m_Pos[i].SetCentralCoordinatesMood(true);
					this->m_Pos[i].SetPos(vPos);

					// アニメーション結果の入力
					this->m_Pos[i].SetAnimationCount(CNameKeyboard::GetName().m_namedata[i].m_key);
				}

				// 現在入力対象になっている場所の座標格納
				vPos = *this->m_Pos[CNameKeyboard::GetName().m_namecount].GetPos();

				// 初期化
				this->m_Pos[CNameKeyboard::GetName().m_namecount].Init
				(
					CKeyboardTextureTemplate::m_namefontlist[CNameKeyboard::GetName().m_Serect].m_textureID,
					1,
					CKeyboardTextureTemplate::m_namefontlist[CNameKeyboard::GetName().m_Serect].Pattern,
					CKeyboardTextureTemplate::m_namefontlist[CNameKeyboard::GetName().m_Serect].Direction
				);
				this->m_Pos[CNameKeyboard::GetName().m_namecount].SetCentralCoordinatesMood(true);
				this->m_Pos[CNameKeyboard::GetName().m_namecount].SetPos(vPos);

				// アニメーション結果の入力
				this->m_Pos[CNameKeyboard::GetName().m_namecount].SetAnimationCount(CKeyboardTextureTemplate::m_namefontlist[CNameKeyboard::GetName().m_Serect].m_key);
			}

			// 必ず毎フレーム色をセット
			for (int i = 0; i < this->m_NumData; i++)
			{
				this->m_Pos[i].SetColor(255, 255, 255, 255);
			}

			// 入力可能上限に達していない限り点滅処理を繰り返す
			if (CNameKeyboard::GetNameLimit() != CNameKeyboard::GetName().m_namecount)
			{
				CColor<int> ccolor = CColor<int>(255, 255, 255, 255);
				if (0 < this->m_acount)
				{
					ccolor = CColor<int>(255, 255, 255, 255);
				}

				if (30 < this->m_acount)
				{
					ccolor = CColor<int>(255, 255, 255, 0);
				}

				this->m_Pos[CNameKeyboard::GetName().m_namecount].SetColor(ccolor);
				this->m_acount++;

				if (60 < this->m_acount)
				{
					this->m_acount = 0;
				}
			}
		}
		else
		{
			CVector<float> vPos;

			for (int i = 0; i < this->m_NumData; i++)
			{
				// 座標格納
				vPos = *this->m_Pos[i].GetPos();

				// 初期化
				this->m_Pos[i].Init
				(
					CNameKeyboard::GetName().m_namedata[i].m_textureID,
					1,
					CNameKeyboard::GetName().m_namedata[i].Pattern,
					CNameKeyboard::GetName().m_namedata[i].Direction
				);
				this->m_Pos[i].SetCentralCoordinatesMood(true);
				*this->m_Pos[i].GetPos() = vPos;

				// アニメーション結果の入力
				this->m_Pos[i].SetAnimationCount(CNameKeyboard::GetName().m_namedata[i].m_key);

				this->m_Pos[i].SetXPlus(-50.0f);
			}

			if (this->m_Pos[0].GetPos()->x <= this->m_MasterPos[0].GetPos()->x)
			{
				for (int i = 0; i < this->m_NumData; i++)
				{
					// 座標格納
					vPos = *this->m_MasterPos[i].GetPos();

					// 初期化
					this->m_MasterPos[i].Init
					(
						CNameKeyboard::GetName().m_namedata[i].m_textureID,
						1,
						CNameKeyboard::GetName().m_namedata[i].Pattern,
						CNameKeyboard::GetName().m_namedata[i].Direction
					);
					this->m_MasterPos[i].SetCentralCoordinatesMood(true);
					this->m_MasterPos[i].SetPos(vPos);

					// アニメーション結果の入力
					this->m_MasterPos[i].SetAnimationCount(CNameKeyboard::GetName().m_namedata[i].m_key);

					this->m_MasterPos[i].SetXPlus(-50.0f);

					this->m_Pos[i] = this->m_MasterPos[i];
				}
				this->m_MoveKey = true;
			}
		}

		if (this->m_EndKey)
		{
			this->m_EndMove++;
			if (this->m_EndMove<120)
			{
				for (int i = 0; i < this->m_NumData; i++)
				{
					this->m_Pos[i].SetXPlus(-50.0f);
				}
			}
		}
	}
}

//==========================================================================
// 描画
void CName::Draw(void)
{
	if (this->m_OpenKey&&this->m_MoveKey)
	{
		if (this->m_EndMove<120)
		{
			for (int i = 0; i < this->m_NumData; i++)
			{
				this->m_Poly->Draw(&this->m_Pos[i]); //描画
			}
		}
	}
}
