//==========================================================================
// 成功演出[successUI.cpp]
// author : 
//==========================================================================
#include "successUI.h"
#include "Screen.h"

//==========================================================================
// 実体化
//==========================================================================
bool CSuccessUI::m_key = false;

CSuccessUI::CSuccessUI()
{
	this->m_masterpos = nullptr;
	this->m_poly = nullptr;
	this->m_pos = nullptr;
	this->m_numdata = 0;
	this->m_AnimCount = 0;
	this->m_alpha = 255;
	this->m_key2 = true;
}

CSuccessUI::~CSuccessUI()
{
}

//==========================================================================
// 初期化
bool CSuccessUI::Init(void)
{
	// テクスチャのパス
	const char * pTexList[]=
	{
		"./resource/texture/Game/UI/seikou.DDS",	//任務成功テクスチャ
	};

	float fUIScale = 1.0f; // UIスケール調整用

	// データ数記録
	this->m_numdata = this->Sizeof(pTexList);

	// 動的メモリ確保 確保領域1
	this->New(this->m_masterpos, this->m_numdata);
	this->New(this->m_pos, this->m_numdata);
	this->New(this->m_poly);

	// 非アクティブ化
	this->m_key = false;

	// テクスチャの初期化
	if (this->m_poly->Init(pTexList, this->m_numdata, true))
	{
		return true;
	}

	// テクスチャのサイズ補正
	for (int i = 0; i < this->m_numdata; i++)
	{
		this->m_poly->SetTexScale(i, fUIScale);
	}

	// 初期化処理
	for (int i = 0; i < this->m_numdata; i++)
	{
		this->m_pos[i].Init(0);
	}

	// ここにUIの座標設定
	m_pos->SetCentralCoordinatesMood(true);
	m_pos->SetX((float)(CDirectXDevice::GetWindowsSize().m_Width / 2));
	m_pos->SetY((float)(CDirectXDevice::GetWindowsSize().m_Height / 2));
	m_pos->Scale(-1.0f);

	// データのコピー
	for (int i = 0; i < this->m_numdata; i++)
	{
		this->m_masterpos[i] = this->m_pos[i];
	}

	this->m_key2 = false;

	return false;
}

//==========================================================================
// 解放
void CSuccessUI::Uninit(void)
{
	// マテリアルの解放
	this->m_poly->Uninit();

	// メモリ解放
	this->Delete(this->m_masterpos);
	this->Delete(this->m_poly);
	this->Delete(this->m_pos);

	// 非アクティブ化
	this->m_key = false;
	this->m_numdata = 0;
	this->m_key2 = true;
}

//==========================================================================
// 更新
void CSuccessUI::Update(void)
{
	// アクティブな時のみ処理
	if (this->m_key)
	{
		this->m_key2 = true;
		if (m_pos->GetScale() <= 1.0f)	//テクスチャが100%になるまで
		{
			m_pos->Scale(0.05f);		//１フレーム+5%し続ける
		}
		if (m_pos->GetScale() >= 1.0f)	//テクスチャが100%になったら
		{
			m_AnimCount++;				//フレームカウント開始
		}
		if (m_AnimCount >= 30)			//0.5秒後
		{
			m_pos->Scale(0.1f);			//１フレーム+10%し続け
			m_alpha -= 8;				//α値をマイナス
			m_pos->SetColor(255, 255, 255, m_alpha);
		}
		if (m_alpha <= 0) //α値が0になったら
		{
			this->m_key2 = false;
			this->m_key = false;
			CScreen::screenchange(CScreen::scenelist_t::Result);
		}
	}
}

//==========================================================================
// 描画
void CSuccessUI::Draw(void)
{
	// アクティブな時のみ処理
	if (this->m_key2)
	{
		// 描画処理
		for (int i = 0; i < this->m_numdata; i++)
		{
			this->m_poly->Draw(&this->m_pos[i]);
		}
	}
}
