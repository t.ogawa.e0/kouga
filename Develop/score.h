//==========================================================================
// scoreフォント処理[score.h]
// author: joja taiyo
//==========================================================================
#ifndef _SCORE_H_
#define _SCORE_H_

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

//==========================================================================
//
// class  : score
// Content: スコア管理、数字描画
//
//==========================================================================
class CScore :private CTemplate, public CObject
{
private:
	static constexpr int m_LimitFlame = 120;
public:
	CScore();
	~CScore();
	// 初期化
	bool Init(void);
	// 解放
	void Uninit(void);
	// 更新
	void Update(void);
	// 描画
	void Draw(void);

	static int Get(void) { return m_Score; }
private:
	C2DPolygon *m_ScoerPoly;
	C2DPolygon *m_ScoerFontPoly;
	C2DObject *m_ScoerPos;
	C2DObject *m_ScoerFontPos;
	CNumber *m_Num;
	static int m_Score;
	int m_Flame;

	bool m_movekey;

	CImGui_Dx9 m_Imgui;
};


#endif	//_SCORE_H_
