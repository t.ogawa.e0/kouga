//==========================================================================
// エネミー[Enemy.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _Enemy_H_
#define _Enemy_H_

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "Summons.h"
#include "OrientationVector.h"

//==========================================================================
//
// class  : CEnemy
// Content: エネミー
//
//==========================================================================
class CEnemy : public CObject, private COrientationVector
{
public:
	enum class Motionpattern
	{
		idou,
		kougeki,
	};
public:
	CEnemy();
	~CEnemy();
	// 初期化
	bool Init(void);
	// 解放
	void Uninit(void);
	// 更新
	void Update(void);
	// 描画
	void Draw(void);
	// 召喚
	static void Summons(C3DObject * pPos, CSummons::TypeList Type);
	// ゲッタ
	static CSummons::Data_t *GetData(int num);
private:
	// AI処理
	void Move(CSummons::Data_t * pData);
	// 攻撃
	bool Attack(CSummons::Data_t * pData);
	// ロックオン
	void Rockoon(CSummons::Data_t * pData);
	// 向きアニメーション
	void SelectAnimation(CSummons::Data_t * pData, Motionpattern motion);
private:
	static CSummons * m_Summons; // 召喚
	CImGui_Dx9 m_ImGui;
	CHitDetermination m_hit;
	CTemplates m_temp;
};

#endif // !_Enemy_H_
