//==========================================================================
// 味方のAI[Ally_AI.cpp]
// author: tatuya ogawa
//==========================================================================
#include "Ally_AI.h"
#include "Enemy.h"
#include "Base.h"
#include "menu.h"

//==========================================================================
// 実体
//==========================================================================
CSummons * CAlly_AI::m_Summons;
CTemplates CAlly_AI::m_temp;

CAlly_AI::CAlly_AI()
{
}

CAlly_AI::~CAlly_AI()
{
}

//==========================================================================
// 初期化
bool CAlly_AI::Init(void)
{
	return this->m_Summons->Init();
}

//==========================================================================
// 解放
void CAlly_AI::Uninit(void)
{
	// 解放
	this->m_Summons->Uninit();
}

//==========================================================================
// 更新
void CAlly_AI::Update(void)
{
	CSummons::Data_t * pPos = nullptr;
	int nNum = 0;

	this->m_ImGui.NewWindow("Ally AI", true);
	this->m_Summons->Update();

	// nullptrが出るまで回す
	for (int i = 0;;i++)
	{
		pPos = this->m_Summons->GetData(&i);
		if (pPos == nullptr)
		{
			break;
		}

		this->Rockoon(pPos);
		if (!this->Attack(pPos))
		{
			this->Move(pPos);
		}
		nNum = i;

		this->m_ImGui.Text
		(
			"ID = %d TypeID = %d \n Life = %.2f \n Attack = %.2f \n Spped = %.2f \n SearchScale = %.2f",
			pPos->m_ID,
			(int)pPos->m_Parameter.GetType(),
			pPos->m_Parameter.GetStatus()->m_Life,
			pPos->m_Parameter.GetStatus()->m_Attack,
			pPos->m_Parameter.GetStatus()->m_Spped,
			pPos->m_Parameter.GetStatus()->m_SearchScale
		);

		if (pPos->m_Parameter.GetType() == CSummons::TypeList::Trap)
		{
			pPos->m_Parameter.m_animcount[0] = 14 - 1;
			pPos->m_Parameter.m_animcount[1] = 14 - 1;
		}
	}

	this->m_ImGui.Text("Release AI %d", nNum);

	this->m_ImGui.EndWindow();
}

//==========================================================================
// 描画
void CAlly_AI::Draw(void)
{
	this->m_Summons->Draw();
}

//==========================================================================
// 召喚
void CAlly_AI::Summons(C3DObject * pPos, CSummons::TypeList Type)
{
	CSummons::Param_t param;

	if (CMenu::GetMenu())
	{

	}

	switch (CMenu::GetMenu())
	{
	case 0:
		// 召喚リスト
		switch (Type)
		{
		case CSummons::TypeList::Trap:
			param.SetParam(1.0f, 2147343647.0f, 0.0f, 5.0f, 1, Type, CSummons::ObjTypes::player);
			break;
		case CSummons::TypeList::Spped:
			param.SetParam(200.0f, 15, 0.3f, 10.0f, 20, Type, CSummons::ObjTypes::player);
			break;
		case CSummons::TypeList::Attacker:
			param.SetParam(400.0f, 20.0f, 0.1f, 15.0f, 30, Type, CSummons::ObjTypes::player);
			break;
		case CSummons::TypeList::Defender:
			param.SetParam(600.0f, 10.0f, 0.0f, 20.0f, 60, Type, CSummons::ObjTypes::player);
			break;
		default:
			param.SetParam(1.0f, 2147343647.0f, 0.0f, 5.0f, 1, Type, CSummons::ObjTypes::player);
			break;
		}
		break;
	case 1:
		// 召喚リスト
		switch (Type)
		{
		case CSummons::TypeList::Trap:
			param.SetParam(1.0f, 2147343647.0f, 0.0f, 3.0f, 1, Type, CSummons::ObjTypes::player);
			break;
		case CSummons::TypeList::Spped:
			param.SetParam(200.0f, 15, 0.3f, 7.0f, 20, Type, CSummons::ObjTypes::player);
			break;
		case CSummons::TypeList::Attacker:
			param.SetParam(400.0f, 20.0f, 0.1f, 11.0f, 30, Type, CSummons::ObjTypes::player);
			break;
		case CSummons::TypeList::Defender:
			param.SetParam(600.0f, 10.0f, 0.0f, 16.0f, 60, Type, CSummons::ObjTypes::player);
			break;
		default:
			param.SetParam(1.0f, 2147343647.0f, 0.0f, 3.0f, 1, Type, CSummons::ObjTypes::player);
			break;
		}
		break;
	case 2:
		// 召喚リスト
		switch (Type)
		{
		case CSummons::TypeList::Trap:
			param.SetParam(1.0f, 2147343647.0f, 0.0f, 1.0f, 1, Type, CSummons::ObjTypes::player);
			break;
		case CSummons::TypeList::Spped:
			param.SetParam(200.0f, 15, 0.3f, 5.0f, 20, Type, CSummons::ObjTypes::player);
			break;
		case CSummons::TypeList::Attacker:
			param.SetParam(400.0f, 20.0f, 0.1f, 8.0f, 30, Type, CSummons::ObjTypes::player);
			break;
		case CSummons::TypeList::Defender:
			param.SetParam(600.0f, 10.0f, 0.0f, 11.0f, 60, Type, CSummons::ObjTypes::player);
			break;
		default:
			param.SetParam(1.0f, 2147343647.0f, 0.0f, 1.0f, 1, Type, CSummons::ObjTypes::player);
			break;
		}
		break;
	default:
		// 召喚リスト
		switch (Type)
		{
		case CSummons::TypeList::Trap:
			param.SetParam(1.0f, 2147343647.0f, 0.0f, 5.0f, 1, Type, CSummons::ObjTypes::player);
			break;
		case CSummons::TypeList::Spped:
			param.SetParam(200.0f, 15, 0.3f, 10.0f, 20, Type, CSummons::ObjTypes::player);
			break;
		case CSummons::TypeList::Attacker:
			param.SetParam(400.0f, 20.0f, 0.1f, 15.0f, 30, Type, CSummons::ObjTypes::player);
			break;
		case CSummons::TypeList::Defender:
			param.SetParam(600.0f, 10.0f, 0.0f, 20.0f, 60, Type, CSummons::ObjTypes::player);
			break;
		default:
			param.SetParam(1.0f, 2147343647.0f, 0.0f, 5.0f, 1, Type, CSummons::ObjTypes::player);
			break;
		}
		break;
	}

	// 召喚
	m_Summons->Create(pPos, &param);
}

//==========================================================================
// ゲッタ
CSummons::Data_t * CAlly_AI::GetData(int num)
{
	CSummons::Data_t * pPos = m_Summons->GetData(&num);

	if (pPos == nullptr)
	{
		return nullptr;
	}

	return pPos;
}

bool CAlly_AI::InitTex(void)
{
	//==========================================================================
	// トラップ 
	//==========================================================================

	// トラップ 
	CBillboard::CAnimationDataCase trap_moguru =
	{
		"resource/texture/Game/Player/KAME/moguru/ushiro.DDS",
		"resource/texture/Game/Player/KAME/moguru/hidari.DDS",
		"resource/texture/Game/Player/KAME/moguru/mae.DDS",
		"resource/texture/Game/Player/KAME/moguru/migi.DDS",
		4, 2, 14, 14,CBillboard::PlateList::Vertical, false
	};

	CBillboard::CAnimationDataCase trap_moguru2 =
	{
		"resource/texture/Game/Player/KAME/moguru/ushiro.DDS",
		"resource/texture/Game/Player/KAME/moguru/hidari.DDS",
		"resource/texture/Game/Player/KAME/moguru/mae.DDS",
		"resource/texture/Game/Player/KAME/moguru/migi.DDS",
		4, 2, 14, 14,CBillboard::PlateList::Vertical, false
	};

	CBillboard::CAnimationData trap[] =
	{
		trap_moguru,
		trap_moguru2
	};

	//==========================================================================
	// スピードアタッカー 
	//==========================================================================

	// スピードアタッカー 移動
	CBillboard::CAnimationDataCase speedattacker_idou =
	{
		"resource/texture/Game/Player/HEBI/idou/ushiro.DDS",
		"resource/texture/Game/Player/HEBI/idou/hidari.DDS",
		"resource/texture/Game/Player/HEBI/idou/mae.DDS",
		"resource/texture/Game/Player/HEBI/idou/migi.DDS",
		4, 2, 37, 37,CBillboard::PlateList::Vertical, false
	};

	// スピードアタッカー 攻撃
	CBillboard::CAnimationDataCase speedattacker_kougeki =
	{
		"resource/texture/Game/Player/HEBI/kougeki/ushiro.DDS",
		"resource/texture/Game/Player/HEBI/kougeki/hidari.DDS",
		"resource/texture/Game/Player/HEBI/kougeki/mae.DDS",
		"resource/texture/Game/Player/HEBI/kougeki/migi.DDS",
		4, 2, 31, 31,CBillboard::PlateList::Vertical, false
	};

	// スピードアタッカー 
	CBillboard::CAnimationData speedattacker[] =
	{
		speedattacker_idou,
		speedattacker_kougeki,
	};

	//==========================================================================
	// メインアタッカー
	//==========================================================================

	// メインアタッカー 移動
	CBillboard::CAnimationDataCase mainattacker_idou =
	{
		"resource/texture/Game/Player/KAERU/idou/ushiro.DDS",
		"resource/texture/Game/Player/KAERU/idou/hidari.DDS",
		"resource/texture/Game/Player/KAERU/idou/mae.DDS",
		"resource/texture/Game/Player/KAERU/idou/migi.DDS",
		4, 2, 2, 2,CBillboard::PlateList::Vertical, false
	};

	// メインアタッカー 攻撃
	CBillboard::CAnimationDataCase mainattacker_kougeki =
	{
		"resource/texture/Game/Player/KAERU/kougeki/ushiro.DDS",
		"resource/texture/Game/Player/KAERU/kougeki/hidari.DDS",
		"resource/texture/Game/Player/KAERU/kougeki/mae.DDS",
		"resource/texture/Game/Player/KAERU/kougeki/migi.DDS",
		4, 2, 11, 11,CBillboard::PlateList::Vertical, false
	};

	// メインアタッカー
	CBillboard::CAnimationData mainattacker[] =
	{
		mainattacker_idou,
		mainattacker_kougeki,
	};

	//==========================================================================
	// タンク
	//==========================================================================

	// タンク 待機
	CBillboard::CAnimationDataCase tank_taiki =
	{
		"resource/texture/Game/Player/MARUTA/taiki/ushiro.DDS",
		"resource/texture/Game/Player/MARUTA/taiki/hidari.DDS",
		"resource/texture/Game/Player/MARUTA/taiki/mae.DDS",
		"resource/texture/Game/Player/MARUTA/taiki/migi.DDS",
		4, 2, 3, 3,CBillboard::PlateList::Vertical, false
	};

	// タンク 攻撃
	CBillboard::CAnimationDataCase tank_kougeki =
	{
		"resource/texture/Game/Player/MARUTA/kougeki/ushiro.DDS",
		"resource/texture/Game/Player/MARUTA/kougeki/hidari.DDS",
		"resource/texture/Game/Player/MARUTA/kougeki/mae.DDS",
		"resource/texture/Game/Player/MARUTA/kougeki/migi.DDS",
		4, 2, 5, 5,CBillboard::PlateList::Vertical, false
	};

	// タンク
	CBillboard::CAnimationData tank[] =
	{
		tank_taiki,
		tank_kougeki,
	};

	//==========================================================================
	// キャラセット
	//==========================================================================

	// キャラクターセット
	CBillboard::CAnimationData * pcharacter[] =
	{
		trap,
		speedattacker,
		mainattacker,
		tank,
	};

	// メモリ確保
	m_temp.New_(m_Summons);

	// 初期化
	return m_Summons->TexInit(pcharacter, m_temp.Sizeof_(pcharacter));
}

void CAlly_AI::UninitTex(void)
{
	m_Summons->TexUninit();

	// メモリ解放
	m_temp.Delete_(m_Summons);
}

//==========================================================================
// 移動
void CAlly_AI::Move(CSummons::Data_t * pData)
{
	// 追従処理は、対象の座標から動かす対象の座標を引いて出てきたベクトルを使用する
	if (pData->m_Target.m_Pos != nullptr)
	{
		D3DXVECTOR3 VecTor = D3DXVECTOR3(0, 0, 0);

		// 向きベクトル算出
		VecTor = pData->m_Target.m_Pos->Info.pos - pData->m_PosData.m_Pos.Info.pos;
		D3DXVec3Normalize(&VecTor, &VecTor);
		pData->m_PosData.m_Pos.Vec.Front = VecTor;
		pData->m_PosData.m_Pos.Look.Eye = -VecTor;
		pData->m_PosData.m_Pos.MoveZ(pData->m_Parameter.GetStatus()->m_Spped);

		// 移動
		this->SelectAnimation(pData, Motionpattern::idou);

		// 後ろに戻す処理
		if (this->m_hit.Ball(&pData->m_PosData.m_Pos, pData->m_Target.m_Pos, 1.5f))
		{
			pData->m_PosData.m_Pos.MoveZ(-(pData->m_Parameter.GetStatus()->m_Spped));
		}
	}
	else if (pData->m_Target.m_Pos == nullptr)
	{
		D3DXVECTOR3 VecTor = D3DXVECTOR3(0, 0, 0);

		float fDistance = 1000000000.0f;
		bool bKey = false;
		C3DObject * m_Pos = nullptr;
		CBase::BaseParam * basepos = nullptr;
		CBase::BaseParam * basetarget = nullptr;

		// 最短の拠点の算出
		for (int i = 0; i < CBase::getmaxbase(); i++)
		{
			basepos = CBase::baseparam(i);
			if (basepos->m_Existence)
			{
				float fDistance2 = this->m_hit.Distance(&basepos->m_Pos, &pData->m_PosData.m_Pos);
				if (fDistance2<fDistance)
				{
					basetarget = basepos;
					fDistance = fDistance2;
					m_Pos = &basepos->m_Pos;
					bKey = true;
				}
			}
		}

		if (bKey)
		{
			VecTor = m_Pos->Info.pos - pData->m_PosData.m_Pos.Info.pos;
			D3DXVec3Normalize(&VecTor, &VecTor);
			pData->m_PosData.m_Pos.Vec.Front = VecTor;
			pData->m_PosData.m_Pos.Look.Eye = -VecTor;
			pData->m_PosData.m_Pos.MoveZ(pData->m_Parameter.GetStatus()->m_Spped);

			// 後ろに戻す処理
			if (this->m_hit.Ball(&pData->m_PosData.m_Pos, m_Pos, 1.5f))
			{
				pData->m_PosData.m_Pos.MoveZ(-(pData->m_Parameter.GetStatus()->m_Spped));
			}

			// 攻撃有効範囲
			if (this->m_hit.Ball(&basetarget->m_Pos, &pData->m_PosData.m_Pos, 2.0f))
			{
				// 攻撃フレーム
				if (pData->m_Parameter.AttackFrameCount())
				{
					// トラップと拠点は反応しない
					if (pData->m_Parameter.GetType() != CSummons::TypeList::Trap)
					{
						basetarget->m_DurableValue -= (int)pData->m_Parameter.GetStatus()->m_Attack;
						this->SelectAnimation(pData, Motionpattern::kougeki);
					}
				}
			}
			else
			{
				// 移動
				this->SelectAnimation(pData, Motionpattern::idou);
			}
		}
	}
}

//==========================================================================
// 攻撃
bool CAlly_AI::Attack(CSummons::Data_t * pData)
{
	if (pData->m_Target.m_Pos != nullptr)
	{
		// 攻撃有効範囲
		if (this->m_hit.Ball(pData->m_Target.m_Pos, &pData->m_PosData.m_Pos, 2.0f))
		{
			// 攻撃フレーム
			if (pData->m_Parameter.AttackFrameCount())
			{
				// トラップと拠点は反応しない
				if (pData->m_Parameter.GetType() == CSummons::TypeList::Trap)
				{
					pData->m_Target.m_Parameter->GetStatus()->m_Life -= pData->m_Parameter.GetStatus()->m_Attack;
					pData->m_Parameter.GetStatus()->m_Life = -1;
				}
				else
				{
					// ターゲットに攻撃
					pData->m_Target.m_Parameter->GetStatus()->m_Life -= pData->m_Parameter.GetStatus()->m_Attack;
				}
			}
			this->SelectAnimation(pData, Motionpattern::kougeki);
			return true;
		}
	}
	return false;
}

//==========================================================================
// ロックオン
void CAlly_AI::Rockoon(CSummons::Data_t * pData)
{
	CSummons::Data_t * pPos = nullptr;

	for (int i = 0; ; i++)
	{
		// アドレスを記憶
		pPos = CEnemy::GetData(i);

		if (pPos == nullptr)
		{
			break;
		}

		// ロックオン可能範囲
		if (this->m_hit.Ball(&pData->m_PosData.m_Pos, &pPos->m_PosData.m_Pos, pData->m_Parameter.GetStatus()->m_SearchScale))
		{
			// ロックオンできたら終了
			if (this->m_Summons->SetTarget(pPos, pData) != nullptr)
			{
				break;
			}
		}
	}
}

//==========================================================================
// 向きアニメーション
void CAlly_AI::SelectAnimation(CSummons::Data_t * pData, Motionpattern motion)
{
	// モーション切り替え
	if (pData->m_Parameter.m_motionselect != (int)motion)
	{
		pData->m_Parameter.m_animcount[pData->m_Parameter.m_motionselect] = 0;
		pData->m_Parameter.m_motionselect = (int)motion;
		pData->m_Parameter.m_animcount[pData->m_Parameter.m_motionselect] = 0;
	}

	// 前進
	if (this->Advance(pData->m_PosData.m_Pos.Vec.Front))
	{
		pData->m_PosData.m_Pos.setindex(0);
	}
	// 後進
	if (this->Backward(pData->m_PosData.m_Pos.Vec.Front))
	{
		pData->m_PosData.m_Pos.setindex(2);
	}
	// 右
	if (this->Right(pData->m_PosData.m_Pos.Vec.Front))
	{
		pData->m_PosData.m_Pos.setindex(3);
	}
	// 左
	if (this->Left(pData->m_PosData.m_Pos.Vec.Front))
	{
		pData->m_PosData.m_Pos.setindex(1);
	}
}

