//==========================================================================
// フィールド[field.cpp]
// author: keita yanagidate
//==========================================================================
#include "Field.h"

CFIELD::CFIELD()
{
	this->m_MeshField = nullptr;
	this->m_Pos = nullptr;
}

CFIELD::~CFIELD()
{
}

//==========================================================================
// 初期化
bool CFIELD::Init(void)
{
	// メモリ確保
	this->New(this->m_MeshField);
	this->New(this->m_Pos);

	// 初期化
	this->m_Pos->Init(0);
	this->m_Pos->Scale(5.0f);

	// 初期化
	return this->m_MeshField->Init("resource/texture/Field001.DDS", 100, 100);
}

//==========================================================================
// 解放
void CFIELD::Uninit(void)
{
	// 解放
	this->m_MeshField->Uninit();

	// メモリ解放
	this->Delete(this->m_MeshField);
	this->Delete(this->m_Pos);
}

//==========================================================================
// 更新
void CFIELD::Update(void)
{

}

//==========================================================================
// 描画
void CFIELD::Draw(void)
{
	this->m_Pos->Draw(10, CDirectXDevice::GetD3DDevice());
	this->m_MeshField->Draw(this->m_Pos);
}
