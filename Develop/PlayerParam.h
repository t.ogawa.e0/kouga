//==========================================================================
// プレイヤーパラメーター[PlayerParam.h]
// author: keita yanagidate
//==========================================================================
#ifndef _PLAYER_PARAM_H_
#define _PLAYER_PARAM_H_

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "ParameterGauge.h"

//==========================================================================
//
// class  : CPlayerParam
// Content: プレイヤーパラメーター
//
//==========================================================================
class CPlayerParam : private CTemplate, public CObject
{
public:
	CPlayerParam();
	~CPlayerParam();
	// 初期化
	bool Init(void);
	// 終了
	void Uninit(void);
	// 更新
	void Update(void);
	// 描画
	void Draw(void);
private:
	CParameterGauge *m_HpGage; //HPパラメーターゲージ
	CParameterGauge *m_MpGage; //MPパラメーターゲージ
	C2DObject *m_GagePos; //HPゲージ座標
	C2DObject *m_HpPos; //HPゲージ座標
	C2DObject *m_MpPos; //HPゲージ座標
	C2DPolygon *m_Poly; //描画
	int m_num;
};

#endif // !_PLAYER_PARAM_H_#pragma once
