//==========================================================================
// 制作用シーン[make.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _make_h_
#define _make_h_

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "SceneChange.h"

//==========================================================================
//
// class  : CMake
// Content: 制作用
//
//==========================================================================
class CMake : public CBaseScene
{
public:
	CMake();
	~CMake();
	bool Init(void);
	void Uninit(void);
	void Update(void);
	void Draw(void);

	// カメラのビュー行列のアドレス渡し
	static D3DXMATRIX * GetD3DMATRIX(void) { return m_camera.CreateView(); }
private:
	CTemplates m_temp; // template
	CLight m_light; // ライト
	static CCamera m_camera; // カメラ
};

#endif // !_make_H_
