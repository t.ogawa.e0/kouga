//==========================================================================
// フェード[Fade.cpph]
// author: tatuya ogawa
//==========================================================================
#include "Fade.h"

CFade::CFade()
{
}

CFade::~CFade()
{
}

//==========================================================================
// 初期化
bool CFade::Init(void)
{
	CTexvec<int> vwinsize = CTexvec<int>(0, 0, CDirectXDevice::GetWindowsSize().m_Width, CDirectXDevice::GetWindowsSize().m_Height);

	// メモリ確保
	this->New(this->m_Pos);
	this->New(this->m_Param);
	this->New(this->m_Poly);

	// テクスチャの格納
	if (this->m_Poly->Init()) { return true; }

	// 座標の初期化
	this->m_Pos->Init(0);
	this->m_Param->m_Change = false;
	this->m_Param->m_Key = false;
	this->m_Param->m_Draw = true;
	this->m_Param->m_In = true;
	this->m_Param->m_a = 255;

	// テクスチャのサイズ変更
	*this->m_Poly->GetTexSize(this->m_Pos->getindex()) = vwinsize;

	//	α値をセット
	this->m_Pos->SetColor(0, 0, 0, this->m_Param->m_a);

	return false;
}

//==========================================================================
// 解放
void CFade::Uninit(void)
{
	// テクスチャの解放
	this->m_Poly->Uninit();

	// メモリ解放
	this->Delete(this->m_Pos);
	this->Delete(this->m_Param);
	this->Delete(this->m_Poly);
}

//==========================================================================
// 更新
bool CFade::Update(void)
{
	// 処理実行判定
	if (this->m_Param->m_Key)
	{
		// フェードin,aut切り替え
		if (this->m_Param->m_Change)
		{
			this->m_Param->m_a += 5;
			if (255 < this->m_Param->m_a)
			{
				this->m_Param->m_Key = false;
				this->m_Param->m_In = true;
				this->m_Param->m_a = 255;
			}
		}
		else
		{
			this->m_Param->m_a -= 5;
			if (this->m_Param->m_a < 0)
			{
				this->m_Param->m_Key = false;
				this->m_Param->m_Draw = false;
				this->m_Param->m_a = 0;
			}
		}
		this->m_Pos->SetColor(0, 0, 0, this->m_Param->m_a);
	}

	return this->m_Param->m_Draw;
}

//==========================================================================
// 描画
void CFade::Draw(void)
{
	// 描画判定が出ている時のみ
	if (this->m_Param->m_Draw)
	{
		this->m_Poly->Draw(this->m_Pos);
	}
}

//==========================================================================
// フェードイン
void CFade::In(void)
{
	this->m_Param->m_Key = true;
	this->m_Param->m_Change = true;
	this->m_Param->m_Draw = true;
	this->m_Param->m_In = false;
	this->m_Param->m_a = 0;
}

//==========================================================================
// フェードアウト
void CFade::Out(void)
{
	// out用のパラメータの初期化
	this->m_Param->m_Key = true;
	this->m_Param->m_Change = false;
	this->m_Param->m_In = false;
	this->m_Param->m_a = 255;
}

//==========================================================================
// フェードイン終了判定
bool CFade::FeadInEnd(void)
{
	return this->m_Param->m_In; // 判定
}
