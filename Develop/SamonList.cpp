//=============================================================================
//
//		召喚リスト [SamonList.cpp]
//		Author : keita yanagidate
//
//=============================================================================
#include "SamonList.h"

CSamonList::CSamonList()
{
	this->m_Pos = nullptr;
	this->m_Poly = nullptr;
}

CSamonList::~CSamonList()
{
}

//=============================================================================
// 初期化
bool CSamonList::Init(void)
{
	// メモリ確保
	this->New(this->m_Pos, this->m_NumData);
	this->New(this->m_Poly);

	// 初期化
	for (int i = 0; i < this->m_NumData; i++)
	{
		this->m_Pos[i].Init(0);
	}
	
	if (this->m_Poly->Init("resource/texture/ColorTex.DDS"))
	{
		return true;
	}

	this->m_Poly->GetTexSize(this->m_Pos->getindex())->w = 50; //サイズ
	this->m_Poly->GetTexSize(this->m_Pos->getindex())->h = 50; //サイズ

	float PosX = (float)this->m_Poly->GetTexSize(this->m_Pos->getindex())->w;
	float posY = (float)CDirectXDevice::GetWindowsSize().m_Height - (float)this->m_Poly->GetTexSize(this->m_Pos->getindex())->h;

	this->m_Pos[0].SetPos(0.0f, posY); //召喚リスト1座標

	this->m_Pos[1].SetPos(PosX, posY); //召喚リスト2座標

	this->m_Pos[2].SetPos(PosX + PosX, posY); //召喚リスト3座標

	return false;
}

//=============================================================================
// 終了
void CSamonList::Uninit(void)
{
	// 解放
	this->m_Poly->Uninit();

	// メモリ解放
	this->Delete(this->m_Pos);
	this->Delete(this->m_Poly);
}

//=============================================================================
// 更新
void CSamonList::Update(void)
{
	//1を押したら半透明から不透明
	if (CKeyboard::Press(CKeyboard::KeyList::KEY_1)) 
	{
		this->m_Pos[0].SetColor(255, 0, 0, 255);
	}
	else
	{
		this->m_Pos[0].SetColor(255, 0, 0, 128);
	}

	//2を押したら半透明から不透明
	if (CKeyboard::Press(CKeyboard::KeyList::KEY_2))
	{
		this->m_Pos[1].SetColor(0, 0, 255, 255);
	}
	else
	{
		this->m_Pos[1].SetColor(0, 0, 255, 128);
	}

	//3を押したら半透明から不透明
	if (CKeyboard::Press(CKeyboard::KeyList::KEY_3))
	{
		this->m_Pos[2].SetColor(0, 255, 0, 255);
	}
	else
	{
		this->m_Pos[2].SetColor(0, 255, 0, 128);
	}
}

//=============================================================================
// 描画
void CSamonList::Draw(void)
{
	for (int i = 0; i < this->m_NumData; i++)
	{
		this->m_Poly->Draw(&this->m_Pos[i]); //描画
	}
}
