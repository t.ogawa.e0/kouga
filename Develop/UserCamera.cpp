//==========================================================================
// カメラ[UserCamera.cpp]
// author: tatuya ogawa
//==========================================================================
#include "UserCamera.h"
#include "make.h"

#include "titlebackground.h"
#include "Player.h"

#if defined(_DEBUG) || defined(DEBUG)
#endif

//==========================================================================
// 実体
//==========================================================================
D3DXMATRIX *CUserCamera::m_MtxView;

CUserCamera::CUserCamera()
{
	this->m_MtxView = nullptr;
}

CUserCamera::~CUserCamera()
{
}

void CUserCamera::Update(void)
{
	switch (this->m_Name)
	{
	case CSceneManager::SceneName::Title:
		this->Title();
		break;
	case CSceneManager::SceneName::Home:
		this->Home();
		break;
	case CSceneManager::SceneName::Game:
		this->Game();
		break;
	case CSceneManager::SceneName::Result:
		this->Result();
		break;
	case CSceneManager::SceneName::Screen_Saver:
		this->Screen_Saver();
		break;
	case CSceneManager::SceneName::Practice:
		this->Practice();
		break;
	case CSceneManager::SceneName::Load:
		this->Loat();
		break;
	case CSceneManager::SceneName::Default:
		this->Default();
		break;
	default:
		break;
	}
}

void CUserCamera::Title(void)
{
	this->m_MtxView = CTitlebackground::GetCamera();
	CCamera::Update(this->m_MtxView, CDirectXDevice::GetWindowsSize().m_Width, CDirectXDevice::GetWindowsSize().m_Height, CDirectXDevice::GetD3DDevice());
}

void CUserCamera::Home(void)
{
	this->m_MtxView = nullptr;
	CCamera::Update(this->m_MtxView, CDirectXDevice::GetWindowsSize().m_Width, CDirectXDevice::GetWindowsSize().m_Height, CDirectXDevice::GetD3DDevice());
}

void CUserCamera::Game(void)
{
#if defined(_DEBUG) || defined(DEBUG)
	static int nCount = (int)CameraListCame::Default;

	if (CMouse::Release(CMouse::ButtonKey::Wheel))
	{
		nCount++;
	}

	switch ((CameraListCame)nCount)
	{
	case CameraListCame::Default:
		this->m_MtxView = CPlayer::GetCamera();
		break;
	default:
		nCount = 0;
		break;
	}
#else
#endif
	this->m_MtxView = CPlayer::GetCamera();
	CCamera::Update(this->m_MtxView, CDirectXDevice::GetWindowsSize().m_Width, CDirectXDevice::GetWindowsSize().m_Height, CDirectXDevice::GetD3DDevice());
}

void CUserCamera::Result(void)
{
	this->m_MtxView = nullptr;
	CCamera::Update(this->m_MtxView, CDirectXDevice::GetWindowsSize().m_Width, CDirectXDevice::GetWindowsSize().m_Height, CDirectXDevice::GetD3DDevice());
}

void CUserCamera::Screen_Saver(void)
{
	this->m_MtxView = nullptr;
	CCamera::Update(this->m_MtxView, CDirectXDevice::GetWindowsSize().m_Width, CDirectXDevice::GetWindowsSize().m_Height, CDirectXDevice::GetD3DDevice());
}

void CUserCamera::Practice(void)
{
	this->m_MtxView = nullptr;
	CCamera::Update(this->m_MtxView, CDirectXDevice::GetWindowsSize().m_Width, CDirectXDevice::GetWindowsSize().m_Height, CDirectXDevice::GetD3DDevice());
}

void CUserCamera::Loat(void)
{
	this->m_MtxView = nullptr;
	CCamera::Update(this->m_MtxView, CDirectXDevice::GetWindowsSize().m_Width, CDirectXDevice::GetWindowsSize().m_Height, CDirectXDevice::GetD3DDevice());
}

void CUserCamera::Default(void)
{
	this->m_MtxView = CMake::GetD3DMATRIX();
	CCamera::Update(this->m_MtxView, CDirectXDevice::GetWindowsSize().m_Width, CDirectXDevice::GetWindowsSize().m_Height, CDirectXDevice::GetD3DDevice());
}
