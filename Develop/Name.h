//==========================================================================
// ネーム入力[Name.h]
// author : tatsuya ogawa
//==========================================================================
#ifndef _Name_H_
#define _Name_H_

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "KeyboardTextureTemplate.h"

//==========================================================================
//
// class  : CName
// Content: 名前表示
//
//==========================================================================
class CName : private CTemplate, public CObject
{
public:
	CName();
	~CName();
	// 初期化
	bool Init(void);
	// 解放
	void Uninit(void);
	// 更新
	void Update(void);
	// 描画
	void Draw(void);

	static void Open(void) { m_OpenKey = true; }
	static void EndKey(void) { m_EndKey = true; }
private:
	C2DObject *m_Pos;
	C2DObject *m_MasterPos;
	C2DPolygon *m_Poly; //描画
	int m_NumData;
	int m_acount;
	bool m_MoveKey;
	int m_EndMove;

	static bool m_EndKey;
	static bool m_OpenKey;
};

#endif // !_Name_H_
