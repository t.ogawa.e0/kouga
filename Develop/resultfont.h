//==========================================================================
// resultフォント処理[resultfont.h]
// author: joja taiyo
//==========================================================================
#ifndef _RESULTFONT_H_
#define _RESULTFONT_H_

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

//==========================================================================
//
// class  : CResultfont
// Content: Result画面遷移
//
//==========================================================================
class CResultfont :private CTemplate, public CObject
{
public:
	// 初期化
	bool Init(void);
	// 解放
	void Uninit(void);
	// 更新
	void Update(void);
	// 描画
	void Draw(void);
private:
	C2DPolygon *m_Poly;
	C2DObject * m_Pos;
};

#endif	//_RESULTFONT_H_
