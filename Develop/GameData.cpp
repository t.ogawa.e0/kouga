//==========================================================================
// ゲームデータ[GameData.cpp]
// author: tatuya ogawa
//==========================================================================
#include "GameData.h"

//==========================================================================
// 読み取り専用
bool CGameData::Load(CData *& pdata, const char * pname, int * numdata)
{
	FILE *pFile;
	FILE *pLog;
	time_t timer;
	static tm *date;
	char str[256] = { 0 };

	timer = time(nullptr); // 経過時間を取得
	date = localtime(&timer); // 経過時間を時間を表す構造体 date に変換

	// メモリリーク回避用
	this->m_temp.Delete_(pdata);
	*numdata = 0;
	pdata = nullptr;

	// フォルダ生成
	if (_mkdir("resource/data") == 0) {}
	if (_mkdir("resource/data/ranking") == 0) {}
	if (_mkdir("resource/data/logdata") == 0) {}

	pFile = fopen(pname, "rb");
	pLog = fopen(this->m_FileNameLog, "w");

	// ファイルが存在
	if (pFile)
	{
		// 読み込み
		fread(numdata, sizeof(int), 1, pFile);
		this->m_temp.New_(pdata, *numdata);
		fread(&pdata[0], sizeof(CData), *numdata, pFile);
		fclose(pFile);

		if (pLog)
		{
			timer = time(nullptr); // 経過時間を取得
			date = localtime(&timer); // 経過時間を時間を表す構造体 date に変換
			strftime(str, 256, " LoadTime >> %Y/%m/%d %H:%M:%S", date); // 年月日時分秒
			fprintf(pLog, "0\n");
			fprintf(pLog, "%s\n\n", str);
			fclose(pLog);
		}
	}
	else
	{
		if (pLog)
		{
			strftime(str, 256, " LoadTime >> %Y/%m/%d %H:%M:%S", date); // 年月日時分秒
			fprintf(pLog, "0\n");
			fprintf(pLog, " Not Data\n");
			fprintf(pLog, "%s\n\n", str);
			fclose(pLog);
		}
		pdata = nullptr;
		return false;
	}

	return true;
}

//==========================================================================
// ランキング更新時の読み取り専用
bool CGameData::Update(CData *& pdata, const char * pname, int * numdata)
{
	FILE *pFile;
	FILE *pLog;
	time_t timer;
	static tm *date;
	char str[256] = { 0 };

	timer = time(nullptr); // 経過時間を取得
	date = localtime(&timer); // 経過時間を時間を表す構造体 date に変換

	// メモリリーク回避用
	this->m_temp.Delete_(pdata);
	pdata = nullptr;
	(*numdata) = 0;

	// フォルダ生成
	if (_mkdir("resource/data") == 0) {}
	if (_mkdir("resource/data/ranking") == 0) {}
	if (_mkdir("resource/data/logdata") == 0) {}

	pFile = fopen(pname, "rb");
	pLog = fopen(this->m_FileNameLog, "w");

	// ファイルが存在
	if (pFile)
	{
		// 読み込み
		fread(numdata, sizeof(int), 1, pFile);
		(*numdata)++;
		this->m_temp.New_(pdata, *numdata);
		fread(&pdata[0], sizeof(CData), *numdata, pFile);
		fclose(pFile);

		if (pLog)
		{
			timer = time(nullptr); // 経過時間を取得
			date = localtime(&timer); // 経過時間を時間を表す構造体 date に変換
			strftime(str, 256, " LoadTime >> %Y/%m/%d %H:%M:%S", date); // 年月日時分秒
			fprintf(pLog, "1\n");
			fprintf(pLog, "%s\n\n", str);
			fclose(pLog);
		}
	}
	else
	{
		if (pLog)
		{
			strftime(str, 256, " LoadTime >> %Y/%m/%d %H:%M:%S", date); // 年月日時分秒
			fprintf(pLog, "0\n");
			fprintf(pLog, " Not Data\n");
			fprintf(pLog, "%s\n\n", str);
			fclose(pLog);
		}
		pdata = nullptr;
		return false;
	}
	return true;
}

//==========================================================================
// 保存処理
void CGameData::Save(CData *& pdata, const char * pname, int * numdata)
{
	FILE *pFile;
	FILE *pLog;
	time_t timer;
	static tm *date;
	char str[256] = { 0 };

	timer = time(nullptr); // 経過時間を取得
	date = localtime(&timer); // 経過時間を時間を表す構造体 date に変換

	// フォルダ生成
	if (_mkdir("resource/data") == 0) {}
	if (_mkdir("resource/data/ranking") == 0) {}
	if (_mkdir("resource/data/logdata") == 0) {}

	pFile = fopen(pname, "wb");
	pLog = fopen(this->m_FileNameLog, "w");

	if (pFile)
	{
		fwrite(numdata, sizeof(int), 1, pFile);
		fwrite(&pdata[0], sizeof(CData), *numdata, pFile);
		fclose(pFile);

		if (pLog)
		{
			timer = time(nullptr); // 経過時間を取得
			date = localtime(&timer); // 経過時間を時間を表す構造体 date に変換
			strftime(str, 256, " SaveTime >> %Y/%m/%d %H:%M:%S", date); // 年月日時分秒
			fprintf(pLog, "0\n");
			fprintf(pLog, "%s\n\n", str);
			fclose(pLog);
		}
	}
}

//==========================================================================
// ソート処理
void CGameData::Sort(CData *& pInput, CData *& pNewData, int * numdata)
{
	bool bNot = false; // ヒット判定

	// データが存在
	if (pInput != nullptr)
	{
		int nselect = 0; // セレクト

		// 検索用for文
		for (int i = 0; i < *numdata - 1; i++)
		{
			bNot = false;
			// 比較対象より小さいデータが出るまで
			if (pInput[i].m_score <= pNewData->m_score)
			{
				nselect = i; // 配列番号を記録
				bNot = true;
				break;
			}
		}

		// ヒットしなかった場合
		if (!bNot)
		{
			nselect = *numdata - 1;
		}

		// ヒット時
		if (bNot)
		{
			// ソート用for文
			for (int i = *numdata - 2;;)
			{
				if (i == nselect)
				{
					break;
				}

				if (i == -1)
				{
					break;
				}

				// 後ろにずらしてゆく
				pInput[i + 1] = pInput[i];

				i--;
			}

			// 後ろにずらしてゆく
			pInput[nselect + 1] = pInput[nselect];
			pInput[nselect] = *pNewData;
		}
		else if (!bNot)
		{
			// ヒットしなかった場合は配列の最後に
			pInput[nselect] = *pNewData;
		}

		// 順位記述
		for (int i = 0; i < *numdata; i++)
		{
			pInput[i].m_rank = i + 1;
		}

		// ランキングの入力
		pNewData->m_rank = nselect + 1;
	}
	else // データが存在しない
	{
		pNewData->m_rank = 1;
		this->m_temp.New_(pInput);
		*pInput = *pNewData;
		(*numdata)++;
	}
}
