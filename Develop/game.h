//==========================================================================
// ゲーム[game.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _GAME_H_
#define _GAME_H_

//==========================================================================
// Include
//==========================================================================
#include "dxlib.h"
#include"SceneChange.h"
#include "pausebackground.h"
#include "pausemenu.h"

//==========================================================================
//
// class  : CGameScene
// Content: ゲームシーン
//
//==========================================================================
class CGameScene : public CBaseScene
{
public:
	CGameScene();
	~CGameScene();
	// 初期化
	bool Init(void);
	// 解放
	void Uninit(void);
	// 更新
	void Update(void);
	// 描画
	void Draw(void);
private:
	CPausemenu pausem;
	CPausebackground pausebak;

	bool m_soundkey;
	CSound m_sound;
};
#endif // !_GAME_H_
