//==========================================================================
// システムウィンドウ[SystemWindow.cpp]
// author: tatuya ogawa
//==========================================================================
#include "SystemWindow.h"

//==========================================================================
// 実体
//==========================================================================
HWND CSystemWindow::m_hWnd = nullptr;
HDC CSystemWindow::m_BackBufferDC = nullptr;
HBITMAP CSystemWindow::m_BackBufferBMP = nullptr;
HWND CSystemWindow::m_hWndButton10000 = nullptr;
HWND CSystemWindow::m_hWndButton10001 = nullptr;
HWND CSystemWindow::m_combo = nullptr;
HBITMAP CSystemWindow::m_hBitmap = nullptr;
HWND CSystemWindow::m_check = nullptr;
BITMAP CSystemWindow::m_Bitmap;
CSystemWindow::CData CSystemWindow::m_data = CSystemWindow::CData(-1, false, false);
std::vector<CSystemWindow::CWinSize> CSystemWindow::m_WindowMode; // ディスプレイの解像度格納

CSystemWindow::CSystemWindow()
{
}

CSystemWindow::~CSystemWindow()
{
}

//==========================================================================
// ウィンドウ生成
void CSystemWindow::Window(WNDCLASSEX *wcex, HINSTANCE hInstance)
{
	RECT wr, dr; // 画面サイズ

	//変数の初期化
	wcex->cbSize = sizeof(WNDCLASSEX);					// 構造体のサイズ
	wcex->style = CS_VREDRAW | CS_HREDRAW;				// ウインドウスタイル
	wcex->lpfnWndProc = (WNDPROC)this->WndProc;			// そのウインドウのメッセージを処理するコールバック関数へのポインタ
	wcex->cbClsExtra = 0;								// ウインドウクラス構造体の後ろに割り当てる補足バイト数．普通0．
	wcex->cbWndExtra = 0;								// ウインドウインスタンスの後ろに割り当てる補足バイト数．普通0．
	wcex->hInstance = hInstance;						// このクラスのためのウインドウプロシージャがあるインスタンスハンドル．
	wcex->hIcon = LoadIcon(hInstance, (LPCSTR)IDI_ICON1);// アイコンのハンドル LoadIcon(hInstance, (LPCSTR)IDI_ICON1)
	wcex->hCursor = LoadCursor(nullptr, IDC_ARROW);		// マウスカーソルのハンドル．LoadCursor(nullptr, IDC_ARROW )とか．
	wcex->hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);	// ウインドウ背景色(背景描画用ブラシのハンドル)．
	wcex->lpszMenuName = nullptr;						// デフォルトメニュー名(MAKEINTRESOURCE(メニューID))
	wcex->lpszClassName = this->Class_Name;				// このウインドウクラスにつける名前
	wcex->hIconSm = LoadIcon(hInstance, (LPCSTR)IDI_ICON1);// 16×16の小さいサイズのアイコン LoadIcon(hInstance, (LPCSTR)IDI_ICON1)

	this->m_data = CSystemWindow::CData(-1, false, false);

	// ウインドウクラスを登録します。
	RegisterClassEx(wcex);

	//デスクトップサイズ習得
	GetWindowRect(GetDesktopWindow(), &dr);

	wr = dr;

	// メイン・ウインドウ作成
	AdjustWindowRect(&wr, wcex->style, TRUE);

	wr.right = this->m_MainWidth - wr.left;
	wr.bottom = this->m_MainHeight - wr.top;
	wr.left = dr.right / 2;
	wr.top = dr.bottom / 2;

	wr.left -= this->m_MainWidth / 2;
	wr.top -= this->m_MainHeight / 2;

	// ウインドウを作成します。
	m_hWnd = CreateWindowEx(0, this->Class_Name, this->Window_Name, WINDOW_STYLE, wr.left, wr.top, wr.right, wr.bottom, nullptr, nullptr, wcex->hInstance, nullptr);
}

//==========================================================================
// 更新
void CSystemWindow::WindowUpdate(WNDCLASSEX *wcex, int nCmdShow)
{
	MSG msg; // メッセージ構造体

	// ウインドウを表示します。
	ShowWindow(m_hWnd, nCmdShow);
	UpdateWindow(m_hWnd);

	for (;;)
	{
		// メッセージ
		if (PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				break;
			}
			else
			{
				//メッセージ処理
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
	}

	UnregisterClass(wcex->lpszClassName, wcex->hInstance);
}

void CSystemWindow::setwindata(int w_, int h_)
{
	w_ = w_;
	m_WindowMode.push_back(CWinSize(w_, h_));
}

void CSystemWindow::newtex(void)
{
	for (auto i = m_WindowMode.begin(); i != m_WindowMode.end(); ++i)
	{
		char ptex[MAX_PATH] = { 0 };
		sprintf(ptex, "%d×%d", (*i).w, (*i).h);
		SendMessage(m_combo, CB_ADDSTRING, 0, (LPARAM)ptex);
	}
	SendMessage(m_combo, CB_ADDSTRING, 0, (LPARAM)TEXT("Full Screen"));
}

//==========================================================================
// ボタン
void CSystemWindow::Button(HWND hWnd, LPARAM *lParam, int posx, int posy)
{
	CTexvec<int> vpos = CTexvec<int>(posx + 120, posy, 200, 10000);
	m_combo = CreateWindowEx(
		0,
		TEXT("COMBOBOX"),
		TEXT("menu"),
		(WS_CHILD | WS_VISIBLE | CBS_DROPDOWNLIST),
		vpos.x, vpos.y, vpos.w, vpos.h,
		hWnd, (HMENU)1,
		((LPCREATESTRUCT)(*lParam))->hInstance,
		nullptr
	);

	newtex();

	vpos = CTexvec<int>(20, 440, 80, 20);

	m_check = CreateWindowEx(
		0,
		TEXT("BUTTON"), TEXT("高画質モード"),
		(WS_CHILD | WS_VISIBLE | BS_3STATE),
		vpos.x, vpos.y, vpos.w*2, vpos.h,
		hWnd, (HMENU)ID_BUTTON002,
		((LPCREATESTRUCT)(*lParam))->hInstance,
		nullptr
	);

	vpos = CTexvec<int>(vpos.x + vpos.w + 190, vpos.y, vpos.w, vpos.h);

	//ボタンを作る
	m_hWndButton10000 = CreateWindowEx
	(
		0,
		TEXT("BUTTON"),
		TEXT("PLAY!"),
		(WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON),
		vpos.x, vpos.y, vpos.w, vpos.h,
		hWnd,
		(HMENU)ID_BUTTON000,
		((LPCREATESTRUCT)(*lParam))->hInstance,
		nullptr
	);

	vpos = CTexvec<int>(vpos.x + vpos.w + 10, vpos.y, vpos.w, vpos.h);
	m_hWndButton10001 = CreateWindowEx
	(
		0,
		TEXT("BUTTON"),
		TEXT("END"),
		(WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON),
		vpos.x, vpos.y, vpos.w, vpos.h,
		hWnd,
		(HMENU)ID_BUTTON001,
		((LPCREATESTRUCT)(*lParam))->hInstance,
		nullptr
	);
}

//==========================================================================
// 設定
void CSystemWindow::Config(HDC * hDC, CTexvec<int> * vpos)
{
	CTexvec<int> pos = CTexvec<int>(vpos->x, (int)(m_MainHeight / 2), vpos->w, (int)(m_MainHeight - 80));
	HBRUSH Brush, OldBrush; // ブラシ
	HPEN Pen, OldPen; // ペン
	int nSize = 1; // 線の幅

	Brush = CreateSolidBrush(RGB(255, 255, 255));
	OldBrush = (HBRUSH)SelectObject(*hDC, Brush);

	Pen = CreatePen(PS_SOLID, nSize, RGB(200, 200, 200));
	OldPen = (HPEN)SelectObject(*hDC, Pen);

	// 長方形
	Rectangle(*hDC, pos.x, pos.y, pos.w, pos.h);

	SelectObject(*hDC, OldBrush);
	DeleteObject(Brush);
	SelectObject(*hDC, OldPen);
	DeleteObject(Pen);
}

//==========================================================================
// テキスト描画
void CSystemWindow::Text(HWND hWnd, HDC * hDC, LPCTSTR lpszStr, int posx, int posy)
{
	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(hWnd, &ps);
	CTexvec<int> vpos = CTexvec<int>(0, 0, m_MainWidth, m_MainHeight);

	BitBlt(hdc, vpos.x, vpos.y, vpos.w, vpos.h, *hDC, 0, 0, SRCCOPY);
	TextOut(hdc, posx, posy, lpszStr, lstrlen(lpszStr));
	EndPaint(hWnd, &ps);
}

//==========================================================================
// イメージデータ表示場所
void CSystemWindow::ImgeData(HDC * hDC, CTexvec<int> * vpos)
{
	CTexvec<int> pos = CTexvec<int>(vpos->x, vpos->y, vpos->w, vpos->h);
	HBRUSH Brush, OldBrush; // ブラシ
	HPEN Pen, OldPen; // ペン
	int nSize = 1; // 線の幅
	HDC bitmapDC = CreateCompatibleDC(*hDC);

	Brush = CreateSolidBrush(RGB(227, 227, 227));
	OldBrush = (HBRUSH)SelectObject(*hDC, Brush);

	Pen = CreatePen(PS_SOLID, nSize, RGB(200, 200, 200));
	OldPen = (HPEN)SelectObject(*hDC, Pen);

	// 長方形
	Rectangle(*hDC, pos.x, pos.y, pos.w, pos.h);

	SelectObject(*hDC, OldBrush);
	DeleteObject(Brush);
	SelectObject(*hDC, OldPen);
	DeleteObject(Pen);
	DeleteDC(bitmapDC);
}

//==========================================================================
// テクスチャ読み込み
void CSystemWindow::LoadTex(LPARAM * lp)
{
	// ファイル読み込み
	m_hBitmap = LoadBitmap(((LPCREATESTRUCT)(*lp))->hInstance, (LPCSTR)IDB_BITMAP2);

	//画像サイズ取得
	GetObject(m_hBitmap, sizeof(BITMAP), &m_Bitmap);
}

//==========================================================================
// テクスチャ描画
void CSystemWindow::DrawTex(HWND hWnd, HDC * hDC, CTexvec<int> * vpos)
{
	PAINTSTRUCT ps;
	HDC hBuffer = CreateCompatibleDC(*hDC);
	CTexvec<int> pos = CTexvec<int>(vpos->x, vpos->y, vpos->w, vpos->h);
	pos += CTexvec<int>(1, 1, -12, -12);
	
	SelectObject(hBuffer, m_hBitmap);

	BitBlt(*hDC, pos.x, pos.y, pos.w, pos.h, hBuffer, 0, 0, SRCCOPY);

	DeleteDC(hBuffer);
	EndPaint(hWnd, &ps);
}

//==========================================================================
// ウインドウプロシージャ
LRESULT CSystemWindow::WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	RECT rect;
	CTexvec<int> vpos = CTexvec<int>(0, 0, 0, 0);
	int nPosX = 70;
	int nPosY = 325;

	if (m_hWnd != nullptr)
	{
		hWnd = m_hWnd;
	}

	// メッセージの種類に応じて処理を分岐。
	switch (uMsg)
	{
	case WM_CREATE: // 初期化
		GetClientRect(hWnd, &rect); // 幅と高さ
		m_BackBufferDC = CreateCompatibleDC(GetDC(hWnd)); // デバイスの生成
		m_BackBufferBMP = CreateCompatibleBitmap(GetDC(hWnd), m_MainWidth, m_MainHeight);  // ビットマップの生成
		SelectObject(m_BackBufferDC, m_BackBufferBMP); // セット
		ReleaseDC(hWnd, GetDC(hWnd)); // 解放
		Button(hWnd, &lParam, nPosX, nPosY);
		LoadTex(&lParam);
		break;
	case WM_PAINT: // 描画
		vpos = CTexvec<int>(10, 10, (int)(m_MainWidth - 27), (int)((m_MainHeight / 2) - 20));
		// 画面のクリア
		GetClientRect(hWnd, &rect); // クリアする場
		FillRect(m_BackBufferDC, &rect, (HBRUSH)(COLOR_INFOTEXT)); // 塗りつぶす
		ImgeData(&m_BackBufferDC, &vpos);
		Config(&m_BackBufferDC, &vpos);
		DrawTex(hWnd, &m_BackBufferDC, &vpos);
		Text(hWnd, &m_BackBufferDC, TEXT("Window Mode"), nPosX, nPosY);
		break;
	case WM_DESTROY: // 終了
		deletetex();
		DeleteObject(m_BackBufferBMP);
		DeleteDC(m_BackBufferDC);

		//ビットマップハンドルを削除
		DeleteObject(m_hBitmap);
		PostQuitMessage(0);
		break;
	case WM_COMMAND:
		switch (HIWORD(wParam))
		{
		case CBN_SELCHANGE:
		{
			m_data.m_serect = (int)SendMessage(m_combo, CB_GETCURSEL, 0, 0);
			return 0;
		}
		break;
		default:
			break;
		}
		switch (LOWORD(wParam))
		{
		case ID_BUTTON000:
			if (m_data.m_serect == -1)
			{
				MessageBox(hWnd, TEXT("Window Modeを選択してください"), TEXT("error"), MB_OK);
			}
			else
			{
				DestroyWindow(hWnd);
				m_data.m_key = true;
			}
			break;
		case ID_BUTTON001:
			DestroyWindow(hWnd);
			m_data.m_key = false;
			break;
		case ID_BUTTON002:
			// チェック無し
			if (BST_CHECKED == SendMessage(m_check, BM_GETCHECK, 0, 0))
			{
				m_data.m_graphicmode = false;
				SendMessage(m_check, BM_SETCHECK, BST_UNCHECKED, 0);
			}
			// チェック在り
			else if (BST_UNCHECKED == SendMessage(m_check, BM_GETCHECK, 0, 0))
			{
				m_data.m_graphicmode = true;
				SendMessage(m_check, BM_SETCHECK, BST_CHECKED, 0);
			}
			break;
		default:
			break;
		}
		break;
	case WM_KEYDOWN: // キー入力
		switch (wParam)
		{
		case VK_ESCAPE: // [ESC]キーが押されたら
			DestroyWindow(hWnd);
			break;
		default:
			break;
		}
		break;
	default:
		return DefWindowProc(hWnd, uMsg, wParam, lParam);
		break;
	}
	return 0;
}
