//==========================================================================
// キーボード[keyboard.h]
// author: tatuya ogawa
//==========================================================================
#include "keyboard.h"
#include "Name.h"
#include "ResultAnnouncement.h"
#include "score.h"
#include "GameData.h"

//==========================================================================
// 実体
//==========================================================================
CGameData::CNameCase CNameKeyboard::m_name;
bool CNameKeyboard::m_OpenKey;

CNameKeyboard::CNameKeyboard()
{
	this->m_Pos = nullptr;
	this->m_Poly = nullptr;
	this->m_KeyPos = nullptr;
	this->m_KeyPoly = nullptr;
	this->m_MasterPos = nullptr;
	this->m_MoveKey = false;
	this->m_EndKey = false;
	this->m_Savekey = false;
	this->m_Serect = 0;
	this->m_OldSerect = 0;
	this->m_lineserect = 0;
	this->m_NumData = 0;
	this->m_EndMove = 0;
}

CNameKeyboard::~CNameKeyboard()
{

}

//==========================================================================
// 初期化
bool CNameKeyboard::Init(void)
{
	float fscale = 0.7f;
	const char* TexLink[] =
	{
		"resource/texture/numbertex.DDS",
		"resource/texture/namefontcase/keyboardfontlist.DDS",
		"resource/texture/namefontcase/period_underbar.DDS",
		"resource/texture/namefontcase/space.DDS",
	};
	const char* TexLink2[] =
	{
		"resource/texture/result/50_50.DDS",
		"resource/texture/result/50_100.DDS"
	};

	this->m_NumData = this->Sizeof(CKeyboardTextureTemplate::m_keyboardlist);

	// メモリ確保
	this->New(this->m_Pos, this->m_NumData);
	this->New(this->m_MasterPos, this->m_NumData);
	this->New(this->m_Poly);

	this->New(this->m_KeyPos, this->m_NumData);
	this->New(this->m_KeyPoly);

	// キーボードの文字
	if (this->m_Poly->Init(TexLink, this->Sizeof(TexLink), true))
	{
		return true;
	}

	// キーボードの文字
	for (int i = 0; i < (int)this->Sizeof(TexLink); i++)
	{
		this->m_Poly->SetTexScale(i, fscale);
	}

	// キーボード
	if (this->m_KeyPoly->Init(TexLink2, this->Sizeof(TexLink2), true))
	{
		return true;
	}

	// キーボード
	for (int i = 0; i < (int)this->Sizeof(TexLink2); i++)
	{
		this->m_KeyPoly->SetTexScale(i, 1.3f);
	}

	float fpos1 = 650.0f*fscale;
	fpos1 = fpos1*CDirectXDevice::screenbuffscale();

	float fpos4 = 100.0f*fscale;
	fpos4 = fpos4*CDirectXDevice::screenbuffscale();

	float fposX = 0.0f;
	float fposY = (float)CDirectXDevice::GetWindowsSize().m_Height - fpos1;

	// 初期化
	for (int i = 0; i < this->m_NumData; i++)
	{
		this->m_KeyPos[i].Init(0);
		this->m_KeyPos[i].SetCentralCoordinatesMood(true);
		this->m_Pos[i].Init
		(
			CKeyboardTextureTemplate::m_keyboardlist[i].m_textureID,
			1,
			CKeyboardTextureTemplate::m_keyboardlist[i].Pattern,
			CKeyboardTextureTemplate::m_keyboardlist[i].Direction
		);

		if (CKeyboardTextureTemplate::m_keyboardlist[i].m_ChangePointStart == true)
		{
			this->m_UpperBoundOfEachRow[CKeyboardTextureTemplate::m_keyboardlist[i].m_line].m_start = i;
			fposX = (float)CDirectXDevice::GetWindowsSize().m_Width / 2;

			float fpos2 = 450.0f*fscale;
			fpos2 = fpos2*CDirectXDevice::screenbuffscale();
			fposX -= fpos2;

			float fpos3 = 100.0f*fscale;
			fpos3 = fpos3*CDirectXDevice::screenbuffscale();
			fposY += fpos3;
		}

		if (CKeyboardTextureTemplate::m_keyboardlist[i].m_ChangePointEnd == true)
		{
			this->m_UpperBoundOfEachRow[CKeyboardTextureTemplate::m_keyboardlist[i].m_line].m_end = i;
			if (CKeyboardTextureTemplate::m_keyboardlist[i].m_textureID == 3)
			{
				this->m_KeyPos[i].Init(1);
				this->m_KeyPos[i].SetCentralCoordinatesMood(true);

				this->m_Pos[i].Init(CKeyboardTextureTemplate::m_keyboardlist[i].m_textureID);
				fposX = (float)CDirectXDevice::GetWindowsSize().m_Width / 2;
				fposY += this->m_Poly->GetTexSize(this->m_Pos[i].getindex())->h / 2;
			}
		}

		this->m_Pos[i].SetCentralCoordinatesMood(true);

		this->m_Pos[i].SetX(fposX);
		this->m_Pos[i].SetY(fposY);

		this->m_KeyPos[i].SetPos(*this->m_Pos[i].GetPos());

		this->m_MasterPos[i] = this->m_Pos[i];

		fposX += fpos4;
	}

	this->m_MoveKey = false;
	this->m_Savekey = false;
	this->m_Serect = 0;
	this->m_OldSerect = 0;
	this->m_lineserect = 0;
	this->m_EndMove = 0;
	this->m_name.m_namecount = 0;
	this->m_name.m_Serect = 0;
	this->m_OpenKey = false;

	// 名前の箱を初期化
	for (int i = 0; i < this->m_limitname; i++)
	{
		this->m_name.m_namedata[i] = CKeyboardTextureTemplate::m_namefontlist[(int)(this->Sizeof(CKeyboardTextureTemplate::m_namefontlist) - 1)];
	}

	for (;;)
	{
		if (CDirectXDevice::GetWindowsSize().m_Width + 400.0f <= this->m_Pos[0].GetPos()->x)
		{
			break;
		}

		for (int i = 0; i < this->m_NumData; i++)
		{
			this->m_Pos[i].SetXPlus(50.0f);
			this->m_KeyPos[i].SetPos(*this->m_Pos[i].GetPos());
		}
	}

	return false;
}

//==========================================================================
// 解放
void CNameKeyboard::Uninit(void)
{
	// 解放
	this->m_Poly->Uninit();
	this->m_KeyPoly->Uninit();

	// メモリ解放
	this->Delete(this->m_Pos);
	this->Delete(this->m_Poly);
	this->Delete(this->m_MasterPos);
	this->Delete(this->m_KeyPos);
	this->Delete(this->m_KeyPoly);

	this->m_lineserect = 0;
	this->m_Serect = 0;
	this->m_OldSerect = 0;
	this->m_name.m_Serect = 0;
	this->m_name.m_namecount = 0;
	this->m_OpenKey = false;
	this->m_EndKey = false;
	this->m_EndMove = 0;
}

//==========================================================================
// 更新
void CNameKeyboard::Update(void)
{
	this->m_Imgui.NewWindow("keyboard", true);
	this->m_Imgui.Text("OpenKey %d", this->m_OpenKey);
	this->m_Imgui.Text("MoveKey %d", this->m_MoveKey);
	this->m_Imgui.Text("Serect %d", this->m_Serect);
	this->m_Imgui.Text("EndKey %d", this->m_EndKey);
	this->m_Imgui.Text("EndMove %d", this->m_EndMove);

	// 鍵がかかっていないとき
	if (this->m_OpenKey)
	{
		if (!this->m_EndKey)
		{
			// 出現時
			if (this->m_MoveKey)
			{
				if (CKeyboard::Trigger(CKeyboard::KeyList::KEY_RIGHT) || CController::CDirection::Trigger(CController::CDirection::Ckey::LeftButtonRight))
				{
					if (this->m_lineserect != 4)
					{
						// 古い座標を記録
						this->m_OldSerect = this->m_Serect;
						this->m_Serect++;

						// 各ラインの上限を超えていないとき
						if (this->m_UpperBoundOfEachRow[this->m_lineserect].m_end < this->m_Serect)
						{
							this->m_Serect = this->m_OldSerect;
						}
					}
				}
				else if (CKeyboard::Trigger(CKeyboard::KeyList::KEY_LEFT) || CController::CDirection::Trigger(CController::CDirection::Ckey::LeftButtonLeft))
				{
					if (this->m_lineserect != 4)
					{
						// 古い座標を記録
						this->m_OldSerect = this->m_Serect;
						this->m_Serect--;

						// 各ラインの始まりより小さいとき
						if (this->m_Serect < this->m_UpperBoundOfEachRow[this->m_lineserect].m_start)
						{
							this->m_Serect = this->m_OldSerect;
						}
					}
				}
				else if (CKeyboard::Trigger(CKeyboard::KeyList::KEY_DOWN) || CController::CDirection::Trigger(CController::CDirection::Ckey::LeftButtonUnder))
				{
					int nsearch = 0; // サーチ
					bool bkey = false;

					if (4 != this->m_lineserect)
					{
						// 現在見ている場所からとりあえず上限まで回す
						for (int i = this->m_Serect; i < this->m_NumData; i++)
						{
							// 座標が一致しなくなった時
							if (this->m_Pos[i].GetPos()->y != this->m_Pos[this->m_Serect].GetPos()->y)
							{
								nsearch = i; // サーチ用変数に代入
								break;
							}
						}

						// 検索
						for (int i = nsearch; i < this->m_NumData; i++)
						{
							// 座標が一致
							bkey = false;
							if (this->m_Pos[i].GetPos()->x == this->m_Pos[this->m_Serect].GetPos()->x)
							{
								// 古い座標を記録
								this->m_OldSerect = this->m_Serect;
								this->m_Serect = i;
								this->m_lineserect++;
								bkey = true;
								break;
							}
						}
						if (!bkey)
						{
							this->m_OldSerect = this->m_Serect;
							this->m_lineserect++;
							this->m_Serect = this->m_UpperBoundOfEachRow[this->m_lineserect].m_start;
						}
					}
				}
				else if (CKeyboard::Trigger(CKeyboard::KeyList::KEY_UP) || CController::CDirection::Trigger(CController::CDirection::Ckey::LeftButtonUp))
				{
					int nsearch = 0; // サーチ

					if (0 != this->m_lineserect&&this->m_lineserect != 4)
					{
						// 現在見ている場所からとりあえず先頭まで回す
						for (int i = this->m_Serect; 0 <= i; i--)
						{
							// 座標が一致しなくなった時
							if (this->m_Pos[i].GetPos()->y != this->m_Pos[this->m_Serect].GetPos()->y)
							{
								nsearch = i; // サーチ用変数に代入
								break;
							}
						}

						// 検索
						for (int i = nsearch; 0 <= i; i--)
						{
							// 座標が一致
							if (this->m_Pos[i].GetPos()->x == this->m_Pos[this->m_Serect].GetPos()->x)
							{
								// 古い座標を記録
								this->m_OldSerect = this->m_Serect;
								this->m_Serect = i;
								this->m_lineserect--;
								break;
							}
						}
					}
					else if (this->m_lineserect == 4)
					{
						int nKey = this->m_Serect;
						this->m_Serect = this->m_OldSerect;
						this->m_OldSerect = nKey;
						this->m_lineserect--;
					}
				}

				// 入力可能上限に達していなければ繰り返す
				if (this->m_name.m_namecount != this->m_limitname)
				{
					if (CKeyboard::Trigger(CKeyboard::KeyList::KEY_RETURN) || CController::CButton::Trigger(CController::CButton::Ckey::RightButton3))
					{
						this->m_name.m_namedata[this->m_name.m_namecount] = CKeyboardTextureTemplate::m_namefontlist[this->m_Serect];
						this->m_name.m_namecount++;
					}
				}

				// 0に達していなければ繰り返す
				if (this->m_name.m_namecount != 0)
				{
					if (CKeyboard::Trigger(CKeyboard::KeyList::KEY_BACK) || CController::CButton::Trigger(CController::CButton::Ckey::RightButton2))
					{
						this->m_name.m_namecount--;
						this->m_name.m_namedata[this->m_name.m_namecount] = CKeyboardTextureTemplate::m_namefontlist[(int)(this->Sizeof(CKeyboardTextureTemplate::m_namefontlist) - 1)];
					}
				}

				// 処理
				this->m_Pos[this->m_OldSerect].SetColor(255, 255, 255, 255);
				this->m_Pos[this->m_Serect].SetColor(255, 255, 0, 255);
				this->m_name.m_Serect = this->m_Serect;

			}
			// 未出現時
			else if (!this->m_MoveKey)
			{
				// キーボード出現処理
				for (int i = 0; i < this->m_NumData; i++)
				{
					this->m_Pos[i].SetXPlus(-50.0f);
					this->m_KeyPos[i].SetPos(*this->m_Pos[i].GetPos());
				}

				// 先頭の座標が一致
				if (this->m_Pos[0].GetPos()->x <= this->m_MasterPos[0].GetPos()->x)
				{
					for (int i = 0; i < this->m_NumData; i++)
					{
						// マスター座標の代入
						this->m_Pos[i] = this->m_MasterPos[i];
						this->m_KeyPos[i].SetPos(*this->m_Pos[i].GetPos());
					}
					// 解放
					this->m_MoveKey = true;
				}
			}

			// アニメーションキーのセット
			for (int i = 0; i < this->m_NumData; i++)
			{
				this->m_Pos[i].SetAnimationCount((int)CKeyboardTextureTemplate::m_keyboardlist[i].m_key);
			}

			// 入力終了処理
			if (CKeyboard::Trigger(CKeyboard::KeyList::KEY_RSHIFT) || CController::CButton::Trigger(CController::CButton::Ckey::SOPTIONSButton) || CController::CButton::Trigger(CController::CButton::Ckey::SHAREButton) || CController::CButton::Trigger(CController::CButton::Ckey::PSButton) || CController::CButton::Trigger(CController::CButton::Ckey::TouchPad))
			{
				// 一文字も入っていない場合の処理
				if (this->m_name.m_namecount == 0)
				{
					this->m_name.m_namedata[this->m_name.m_namecount] = CKeyboardTextureTemplate::m_namefontlist[34];//N
					this->m_name.m_namecount++;
					this->m_name.m_namedata[this->m_name.m_namecount] = CKeyboardTextureTemplate::m_namefontlist[18];//o
					this->m_name.m_namecount++;
					this->m_name.m_namedata[this->m_name.m_namecount] = CKeyboardTextureTemplate::m_namefontlist[38];//
					this->m_name.m_namecount++;
					this->m_name.m_namedata[this->m_name.m_namecount] = CKeyboardTextureTemplate::m_namefontlist[34];//n
					this->m_name.m_namecount++;
					this->m_name.m_namedata[this->m_name.m_namecount] = CKeyboardTextureTemplate::m_namefontlist[20];//a
					this->m_name.m_namecount++;
					this->m_name.m_namedata[this->m_name.m_namecount] = CKeyboardTextureTemplate::m_namefontlist[35];//m
					this->m_name.m_namecount++;
					this->m_name.m_namedata[this->m_name.m_namecount] = CKeyboardTextureTemplate::m_namefontlist[12];//e
					this->m_name.m_namecount++;
				}

				this->m_Pos[this->m_OldSerect].SetColor(255, 255, 255, 255);
				this->m_Pos[this->m_Serect].SetColor(255, 255, 255, 255);
				this->m_name.m_Serect = 38;
				this->m_EndKey = true;
			}
		}
		else
		{
			if (30<this->m_EndMove&&this->m_EndMove<120)
			{
				// 保存
				if (!this->m_Savekey)
				{
					int num = 0;
					CGameData * pranling;
					CGameData::CData * dpNewcData;
					CGameData::CData * dpRankingData;

					this->New(dpNewcData);
					this->New(dpRankingData);
					this->New(pranling);

					dpNewcData->m_name = this->m_name;
					dpNewcData->m_score = CScore::Get();
					dpNewcData->m_rank = 0;

					// ランキング読み込み
					pranling->Update(dpRankingData, pranling->m_FileName, &num);

					// ソート
					pranling->Sort(dpRankingData, dpNewcData, &num);

					// ランキングの保存
					pranling->Save(dpRankingData, pranling->m_FileName, &num);

					num = 1;
					// ランキングの保存
					pranling->Save(dpNewcData, pranling->m_FileNamePin, &num);

					this->Delete(dpNewcData);
					this->Delete(dpRankingData);
					this->Delete(pranling);
					this->m_Savekey = true;
				}

				CName::EndKey();
				CResultAnnouncement::OpenKey();
				// キーボード出現処理
				for (int i = 0; i < this->m_NumData; i++)
				{
					this->m_Pos[i].SetXPlus(-50.0f);
					this->m_KeyPos[i].SetPos(*this->m_Pos[i].GetPos());
				}
				this->m_Imgui.Text("Move...");
			}
			this->m_EndMove++;
		}
	}

	this->m_Imgui.EndWindow();
}

//==========================================================================
// 描画
void CNameKeyboard::Draw(void)
{
	if (this->m_OpenKey)
	{
		if (this->m_EndMove<120)
		{
			for (int i = 0; i < this->m_NumData; i++)
			{
				if (this->m_Serect == i)
				{
					this->m_KeyPoly->Draw(&this->m_KeyPos[i], true); //描画
				}
				else
				{
					this->m_KeyPoly->Draw(&this->m_KeyPos[i]); //描画
				}

				this->m_Poly->Draw(&this->m_Pos[i]); //描画
			}
		}
	}
}