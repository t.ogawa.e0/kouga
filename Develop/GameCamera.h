//==========================================================================
// ゲームカメラ[GameCamera.h]
// author: keita yanagidate
//==========================================================================
#ifndef _GAMECAMERA_H_
#define _GAMECAMERA_H_

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

//==========================================================================
//
// class  : CGameCamera
// Content: ゲームカメラ
//
//==========================================================================
class CGameCamera :private CTemplate, public CObject
{
public:
	CGameCamera();
	~CGameCamera();
	// 初期化
	bool Init(void);
	// 解放
	void Uninit(void);
	// 更新
	void Update(void);
	// 描画
	void Draw(void);
	// カメラのゲッター
	static D3DXMATRIX *Get(void) { return m_Camera->CreateView(); }
private:
	static CCamera *m_Camera; // カメラ
	C3DObject *m_CameraPos; // 座標
};

#endif // !_GAMECAMERA_H_#pragma once
