//==========================================================================
// 撤退演出[withdrawUI.h]
// author : 
//==========================================================================
#ifndef _withdrawUI_h_
#define _withdrawUI_h_

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

//==========================================================================
//
// class  : CWithdrawUI
// Content: 撤退演出
//
//==========================================================================
class CWithdrawUI : private CTemplate, public CObject
{
public:
	CWithdrawUI();
	~CWithdrawUI();
	// 初期化
	bool Init(void);
	// 解放
	void Uninit(void);
	// 更新
	void Update(void);
	// 描画
	void Draw(void);
	// 処理のアクティブ化
	static void ActiveKey(void) { m_key = true; }
private:
	C2DObject *m_pos; // 座標
	C2DObject *m_masterpos; // マスター座標
	C2DPolygon *m_poly; // ポリゴン
	int m_numdata; // データ数
	int m_AnimCount; //フレームカウント変数
public:
	static bool m_key; // 鍵
};

#endif // !_withdrawUI_h_
