//==========================================================================
// 結果発表[ResultAnnouncement.h]
// author: tatuya ogawa
//==========================================================================
#include "ResultAnnouncement.h"
#include "ThankYouForPlaying.h"

bool CResultAnnouncement::m_OpenKey = false;

CResultAnnouncement::CResultAnnouncement()
{
	this->m_NumData = 0;
	this->m_Pos = nullptr;
	this->m_Poly = nullptr;
	this->m_MasterPos = nullptr;
	this->m_data = nullptr;
	this->m_NotData = false;
	this->m_MoveKey = false;
	this->m_initKey = false;
}

CResultAnnouncement::~CResultAnnouncement()
{
}

bool CResultAnnouncement::Init(void)
{
	float fscale = 0.7f;
	this->m_OpenKey = false;

	if (this->loadtex(fscale)) { return true; }
	this->initpos(fscale);

	return false;
}

void CResultAnnouncement::Uninit(void)
{
	this->m_Poly->Uninit();

	this->Delete(this->m_MasterPos);
	this->Delete(this->m_Pos);
	this->Delete(this->m_Poly);
	this->Delete(this->m_data);
	this->m_NumData = 0;
	this->m_NotData = false;
	this->m_MoveKey = false;
	this->m_initKey = false;
	this->m_OpenKey = false;
}

void CResultAnnouncement::Update(void)
{
	CVector<float> vspeed = CVector<float>(0.0f, 0.0f, 0.0f, 0.0f);

	this->m_ImGui.NewWindow("Ranking", true);
#if defined(_DEBUG) || defined(DEBUG)
	const char * nametexlist1[] =
	{
		"0","1","2","3","4","5","6","7","8","9",
	};
	const char * nametexlist2[] =
	{
		"A","B","C","D","E","F","G","H","I","J",
		"K","L","M","N","O","P","Q","R","S",
		"T","U","V","W","X","Y","Z"," ",
	};
	const char * nametexlist3[] =
	{
		".","_",
	};

	const char **clist[] =
	{
		nametexlist1,
		nametexlist2,
		nametexlist3,
	};
	int ndaata[10] = { 0 };
	int ntexID[10] = { 0 };

	for (int i = 0; i < this->m_NumData; i++)
	{
		this->m_data[i].m_rank;
		this->m_data[i].m_score;
		for (int s = 0; s < 10; s++)
		{
			ndaata[s] = this->m_data[i].m_name.m_namedata[s].m_key;
			ntexID[s] = this->m_data[i].m_name.m_namedata[s].m_textureID;
		}
		this->m_ImGui.Text
		(
			" lank  : %d \n score : %d \n TexID : %d %d %d %d %d %d %d %d %d %d  \n Name  : %d %d %d %d %d %d %d %d %d %d \n Name  : %s%s%s%s%s%s%s%s%s%s",
			this->m_data[i].m_rank,
			this->m_data[i].m_score,
			ntexID[0],
			ntexID[1],
			ntexID[2],
			ntexID[3],
			ntexID[4],
			ntexID[5],
			ntexID[6],
			ntexID[7],
			ntexID[8],
			ntexID[9],
			ndaata[0],
			ndaata[1],
			ndaata[2],
			ndaata[3],
			ndaata[4],
			ndaata[5],
			ndaata[6],
			ndaata[7],
			ndaata[8],
			ndaata[9],
			clist[ntexID[0]][ndaata[0]],
			clist[ntexID[1]][ndaata[1]],
			clist[ntexID[2]][ndaata[2]],
			clist[ntexID[3]][ndaata[3]],
			clist[ntexID[4]][ndaata[4]],
			clist[ntexID[5]][ndaata[5]],
			clist[ntexID[6]][ndaata[6]],
			clist[ntexID[7]][ndaata[7]],
			clist[ntexID[8]][ndaata[8]],
			clist[ntexID[9]][ndaata[9]]
		);
	}
#endif
	// ロックが解除されているとき
	if (this->m_OpenKey)
	{
		for (int i = 0; i < this->m_NumData; i++)
		{
			// 初期化キー
			if (!this->m_initKey)
			{
				// 特定のランキングの読み込み
				this->m_ranling.Load(this->m_data, this->m_ranling.m_FileNamePin, &this->m_NumData);

				// マスター情報のリセット処理
				for (int s = 0; s < this->m_MasterPos[i].m_numdata; s++)
				{
					// 型指定タイプのオリジナルVector
					CVector<float> vPos = *this->m_MasterPos[i].m_namepos[s].GetPos(); // 座標をコピー

																					   // 初期化
					this->m_MasterPos[i].m_namepos[s].Init
					(
						this->m_data[i].m_name.m_namedata[s].m_textureID,
						1,
						this->m_data[i].m_name.m_namedata[s].Pattern,
						this->m_data[i].m_name.m_namedata[s].Direction
					);
					this->m_MasterPos[i].m_namepos[s].SetCentralCoordinatesMood(true);

					// 座標を戻す
					*this->m_MasterPos[i].m_namepos[s].GetPos() = vPos;

					// 座標のコピー
					vPos = *this->m_Pos[i].m_namepos[s].GetPos();

					// マスター情報の移動
					this->m_Pos[i].m_namepos[s] = this->m_MasterPos[i].m_namepos[s];

					// 座標を戻す
					*this->m_Pos[i].m_namepos[s].GetPos() = vPos;
				}
				// ロック
				this->m_initKey = true;
			}

			// 移動キーが有効の時
			if (!this->m_MoveKey)
			{
				vspeed = CVector<float>(40.0f, 0.0f, 0.0f, 0.0f);

				*this->m_Pos[i].m_scorpos.GetPos() -= vspeed;
				*this->m_Pos[i].m_rankpos.GetPos() -= vspeed;
				*this->m_Pos[i].m_scorfontpos.GetPos() -= vspeed;
				*this->m_Pos[i].m_namefontpos.GetPos() -= vspeed;
				*this->m_Pos[i].m_rankfontpos.GetPos() -= vspeed;
				for (int s = 0; s < this->m_Pos[i].m_numdata; s++)
				{
					*this->m_Pos[i].m_namepos[s].GetPos() -= vspeed;
				}

				if (this->m_Pos[i].m_scorpos.GetPos()->x <= this->m_MasterPos[i].m_scorpos.GetPos()->x)
				{
					for (int num = 0; num < this->m_NumData; num++)
					{
						this->m_Pos[num] = this->m_MasterPos[num];
						this->m_Pos[num].m_rankpos.SetAnimationCount(this->m_data[num].m_rank);
						for (int s = 0; s < this->m_Pos[num].m_numdata; s++)
						{
							// アニメーション結果の入力
							this->m_Pos[num].m_namepos[s].SetAnimationCount(this->m_data[num].m_name.m_namedata[s].m_key);
						}
					}

					// ロック
					this->m_MoveKey = true;
				}
			}

			// 移動キーが無効時
			if (this->m_MoveKey)
			{
				if (CKeyboard::Trigger(CKeyboard::KeyList::KEY_RETURN) || CController::CButton::Trigger(CController::CButton::Ckey::SOPTIONSButton) || CController::CButton::Trigger(CController::CButton::Ckey::SHAREButton) || CController::CButton::Trigger(CController::CButton::Ckey::PSButton) || CController::CButton::Trigger(CController::CButton::Ckey::TouchPad))
				{
					CThankYouForPlaying::set();
				}
			}

			this->m_Pos[i].m_rankpos.SetAnimationCount(this->m_data[i].m_rank);
			for (int s = 0; s < this->m_Pos[i].m_numdata; s++)
			{
				// アニメーション結果の入力
				this->m_Pos[i].m_namepos[s].SetAnimationCount(this->m_data[i].m_name.m_namedata[s].m_key);
			}
		}
	}

	this->m_ImGui.EndWindow();
}

void CResultAnnouncement::Draw(void)
{
	if (this->m_OpenKey)
	{
		for (int i = 0; i < this->m_NumData; i++)
		{
			this->m_Poly->Draw(&this->m_Pos[i].m_rankfontpos); //描画
			this->m_Pos[i].m_ranknumber.Draw(this->m_Poly, &this->m_Pos[i].m_rankpos, this->m_data[i].m_rank);

			this->m_Poly->Draw(&this->m_Pos[i].m_scorfontpos); //描画
			this->m_Pos[i].m_scornumber.Draw(this->m_Poly, &this->m_Pos[i].m_scorpos, this->m_data[i].m_score);

			this->m_Poly->Draw(&this->m_Pos[i].m_namefontpos); //描画
			for (int s = 0; s < this->m_Pos[i].m_numdata; s++)
			{
				this->m_Poly->Draw(&this->m_Pos[i].m_namepos[s]); //描画
			}
		}
	}
}

bool CResultAnnouncement::loadtex(float fscale)
{
	this->New(this->m_Poly);

	const char* TexLink[] =
	{
		"resource/texture/numbertex.DDS",
		"resource/texture/namefontcase/keyboardfontlist.DDS",
		"resource/texture/namefontcase/period_underbar.DDS",
		"resource/texture/RankFont.DDS",
		"resource/texture/namefont.DDS",
		"resource/texture/ScoreFont.DDS",
	};

	if (this->m_Poly->Init(TexLink, this->Sizeof(TexLink), true))
	{
		return true;
	}

	for (int i = 0; i < (int)this->Sizeof(TexLink); i++)
	{
		this->m_Poly->SetTexScale(i, fscale);
	}

	return false;
}

void CResultAnnouncement::initpos(float fscale)
{
	CVector<float> vpos = CVector<float>(0.0f, 0.0f, 0.0f, 0.0f);
	CVector<float> vspeed = CVector<float>(0.0f, 0.0f, 0.0f, 0.0f);

	this->Delete(this->m_MasterPos);
	this->Delete(this->m_Pos);

	this->m_NumData = 0;
	float fpos2 = 0.0f;
	float fposX = 0.0f;
	float fMove = 70.0f;

	this->m_MoveKey = false;
	this->m_OpenKey = false;

	fpos2 = 0.0f;
	fposX = (float)CDirectXDevice::GetWindowsSize().m_Width / 2;
	vpos.y = (float)CDirectXDevice::GetWindowsSize().m_Height * 0.45f;
	fMove = 70.0f;
	this->m_ranling.Load(this->m_data, this->m_ranling.m_FileNamePin, &this->m_NumData);

	// メモリ確保
	this->New(this->m_Pos, this->m_NumData);
	this->New(this->m_MasterPos, this->m_NumData);

	for (int i = 0; i < this->m_NumData; i++)
	{
		vpos.x = fposX;
		fpos2 = (fMove * 5);
		fpos2 = fpos2*fscale;
		vpos.x -= fpos2;

		fpos2 = fMove*fscale;

		this->m_Pos[i].m_rankfontpos.Init(3);
		this->m_Pos[i].m_namefontpos.Init(4);
		this->m_Pos[i].m_scorfontpos.Init(5);
		this->m_Pos[i].m_scorpos.Init(0, 1, 10, 10);
		this->m_Pos[i].m_rankpos.Init(0, 1, 10, 10);
		this->m_Pos[i].m_ranknumber.Init(4, true, true);
		this->m_Pos[i].m_scornumber.Init(9, true, true);

		this->m_Pos[i].m_scorfontpos.SetCentralCoordinatesMood(true);
		this->m_Pos[i].m_rankfontpos.SetCentralCoordinatesMood(true);
		this->m_Pos[i].m_namefontpos.SetCentralCoordinatesMood(true);
		this->m_Pos[i].m_rankpos.SetCentralCoordinatesMood(true);
		this->m_Pos[i].m_scorpos.SetCentralCoordinatesMood(true);

		this->m_Pos[i].m_rankfontpos.SetPos(vpos);
		this->m_Pos[i].m_rankpos.SetX(vpos.x*this->m_Pos[i].m_scaleposX);
		this->m_Pos[i].m_rankpos.SetY(vpos.y);

		vpos.y += fpos2*this->m_Pos[i].m_scaleposY;
		this->m_Pos[i].m_scorfontpos.SetPos(vpos);
		this->m_Pos[i].m_scorpos.SetX(vpos.x*this->m_Pos[i].m_scaleposX);
		this->m_Pos[i].m_scorpos.SetY(vpos.y);

		vpos.y += fpos2*this->m_Pos[i].m_scaleposY;
		this->m_Pos[i].m_namefontpos.SetPos(vpos);

		vpos.x = (vpos.x*this->m_Pos[i].m_scaleposX);
		for (int s = 0; s < this->m_Pos[i].m_numdata; s++)
		{
			// 初期化
			this->m_Pos[i].m_namepos[s].Init
			(
				this->m_data[i].m_name.m_namedata[s].m_textureID,
				1,
				this->m_data[i].m_name.m_namedata[s].Pattern,
				this->m_data[i].m_name.m_namedata[s].Direction
			);
			this->m_Pos[i].m_namepos[s].SetCentralCoordinatesMood(true);

			this->m_Pos[i].m_namepos[s].SetPos(vpos);

			vpos.x += fpos2;
		}
		vpos.y += fpos2*this->m_scaleposY;

		this->m_MasterPos[i] = this->m_Pos[i];
	}

	vspeed = CVector<float>(50.0f, 0.0f, 0.0f, 0.0f);
	for (;;)
	{
		if (CDirectXDevice::GetWindowsSize().m_Width + 400.0f <= this->m_Pos[0].m_namefontpos.GetPos()->x)
		{
			break;
		}

		for (int i = 0; i < this->m_NumData; i++)
		{
			this->m_Pos[i].m_scorpos.SetPosPlus(vspeed);
			this->m_Pos[i].m_rankpos.SetPosPlus(vspeed);
			this->m_Pos[i].m_scorfontpos.SetPosPlus(vspeed);
			this->m_Pos[i].m_namefontpos.SetPosPlus(vspeed);
			this->m_Pos[i].m_rankfontpos.SetPosPlus(vspeed);

			for (int s = 0; s < this->m_Pos[i].m_numdata; s++)
			{
				this->m_Pos[i].m_namepos[s].SetPosPlus(vspeed);
			}
		}
	}
}
