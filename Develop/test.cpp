//==========================================================================
// テストプログラム[test.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "test.h"

/*

ここでは、各関数の使い方を説明します。

*/

//==========================================================================
//
// class  : CTest
// Content: テスト用
//
//==========================================================================
CTest::CTest()
{
}

CTest::~CTest()
{
}

bool CTest::Init(void)
{
	if (this->m_2dobj.Init()) { return true; }
	if (this->m_2danimobj.Init()) { return true; }
	if (this->m_3dgrid.Init()) { return true; }
	if (this->m_3dcube.Init()) { return true; }
	if (this->m_3dbill.Init()) { return true; }
	if (this->m_3dmesh.Init()) { return true; }
	if (this->m_3dsphere.Init()) { return true; }
	if (this->m_3dxmodel.Init()) { return true; }
	if (this->m_dxtext.Init()) { return true; }

	return false;
}

void CTest::Uninit(void)
{
	this->m_2dobj.Uninit();
	this->m_2danimobj.Uninit();
	this->m_3dgrid.Uninit();
	this->m_3dcube.Uninit();
	this->m_3dbill.Uninit();
	this->m_3dmesh.Uninit();
	this->m_3dsphere.Uninit();
	this->m_3dxmodel.Uninit();
	this->m_dxtext.Uninit();
}

void CTest::Update(void)
{
	this->m_2dobj.Update();
	this->m_2danimobj.Update();
	this->m_3dgrid.Update();
	this->m_3dcube.Update();
	this->m_3dbill.Update();
	this->m_3dmesh.Update();
	this->m_3dsphere.Update();
	this->m_3dxmodel.Update();
	this->m_dxtext.Update();
}

void CTest::Draw(void)
{
	// グリッドは最初
	this->m_3dgrid.Draw();

	this->m_3dmesh.Draw();

	this->m_3dcube.Draw();

	this->m_3dsphere.Draw();

	this->m_3dxmodel.Draw();

	// ビルボードは最後に
	this->m_3dbill.Draw();

	// 2Dは最後に
	this->m_2dobj.Draw();
	this->m_2danimobj.Draw();

	// テキストはマジ最後
	this->m_dxtext.Draw();
}

//==========================================================================
//
// class  : C2DData
// Content: 2D描画
//
//==========================================================================
C2DData::C2DData()
{
}

C2DData::~C2DData()
{
}

//==========================================================================
// 初期化
bool C2DData::Init(void)
{
	// 複数のテクスチャを読み込ませたいときの書き方
	const char * ptexcase[] =
	{
		"resource/texture/test/10550007_d1e41729.png",
		"resource/texture/test/10550007_d1e41729.png",
	};
	ptexcase; // Initの引数にすると一気に初期化ができる。

	// 初期化
	this->m_2DObjpos.Init(0);

	// 2D画像の読み込み
	return this->m_2DObjpoly.Init("resource/texture/test/10550007_d1e41729.png", true);
}

//==========================================================================
// 解放
void C2DData::Uninit(void)
{
	// 確保されたデータは必ず解放しよう
	this->m_2DObjpoly.Uninit();
}

//==========================================================================
// 更新
void C2DData::Update(void)
{
}

//==========================================================================
// 描画
void C2DData::Draw(void)
{
	// 2Dの描画
	this->m_2DObjpoly.Draw(&this->m_2DObjpos);
}

//==========================================================================
//
// class  : C2DAnimData
// Content: 2Dアニメーション描画
//
//==========================================================================
C2DAnimData::C2DAnimData()
{
}

C2DAnimData::~C2DAnimData()
{
}

//==========================================================================
// 初期化
bool C2DAnimData::Init(void)
{
	// 複数のテクスチャを読み込ませたいときの書き方
	const char * ptexcase[] =
	{
		"resource/texture/test/10550007d1e41729.png",
		"resource/texture/test/10550007d1e41729.png",
	};
	ptexcase; // Initの引数にすると一気に初期化ができる。

	// 2Dアニメーション用の初期化
	this->m_2DObjpos.Init(0, 5, 12, 5);

	// アニメーション用のカウンタの初期化
	this->m_2DObjpoly.AnimationCountInit(&this->m_animcount);

	// 2D画像の読み込み
	return this->m_2DObjpoly.Init("resource/texture/test/10550007d1e41729.png", true);
}

//==========================================================================
// 解放
void C2DAnimData::Uninit(void)
{
	// 確保されたデータは必ず解放しよう
	this->m_2DObjpoly.Uninit();
}

//==========================================================================
// 更新
void C2DAnimData::Update(void)
{
	// アニメーションの終了と同時にカウンタを初期化するための関数
	this->m_2DObjpoly.GetPattanNum(&this->m_2DObjpos, &this->m_animcount);

	// アニメーションカウンタはここで回す
	this->m_animcount++;

	// アニメーションさせる場合カウンタをセットする
	this->m_2DObjpos.SetAnimationCount(this->m_animcount);
}

//==========================================================================
// 描画
void C2DAnimData::Draw(void)
{
	// 2D描画
	this->m_2DObjpoly.Draw(&this->m_2DObjpos);
}

//==========================================================================
//
// class  : C3DGridData
// Content: グリッド描画
//
//==========================================================================
C3DGridData::C3DGridData()
{
}

C3DGridData::~C3DGridData()
{
}

//==========================================================================
// 初期化
bool C3DGridData::Init(void)
{
	// グリッドの初期化 引数はグリッドのサイズ
	this->m_grid.Init(20);
	return false;
}

//==========================================================================
// 解放
void C3DGridData::Uninit(void)
{
	// 確保されたデータは必ず解放しよう
	this->m_grid.Uninit();
}

//==========================================================================
// 更新
void C3DGridData::Update(void)
{
}

//==========================================================================
// 描画
void C3DGridData::Draw(void)
{
	// グリッドの描画
	this->m_grid.Draw();
}

//==========================================================================
//
// class  : C3DCubeData
// Content: キューブの描画
//
//==========================================================================
C3DCubeData::C3DCubeData()
{
}

C3DCubeData::~C3DCubeData()
{
}

//==========================================================================
// 初期化
bool C3DCubeData::Init(void)
{
	// 複数のテクスチャを読み込ませたいときの書き方
	const char * ptexcase[] =
	{
		"resource/texture/test/19949738_sfaees.png",
		"resource/texture/test/97465168_hsar.png",
	};
	ptexcase; // Initの引数にすると一気に初期化ができる。

	// 座標の初期化
	this->m_pos[0].Init(0);
	this->m_pos[1].Init(1);

	this->m_pos[0].MoveX(-1);
	this->m_pos[1].MoveX(1);

	// ダブルポインタ使用時の初期化
	// this->m_temp.Sizeof_を使用し、要素数を渡します
	return this->m_cube.Init(ptexcase, this->m_temp.Sizeof_(ptexcase));
}

//==========================================================================
// 解放
void C3DCubeData::Uninit(void)
{
	// 確保されたデータは必ず解放しよう
	this->m_cube.Uninit();
}

//==========================================================================
// 更新
void C3DCubeData::Update(void)
{
	// 回転させるための値を入れています
	this->m_pos[0].Info.rot.z += 0.025f;
	this->m_pos[0].Info.rot.x += 0.025f;

	this->m_pos[1].Info.rot.z -= 0.025f;
	this->m_pos[1].Info.rot.x -= 0.025f;
}

//==========================================================================
// 描画
void C3DCubeData::Draw(void)
{
	// 座標が二つ存在するのでforで回します。
	// this->m_temp.Sizeof_とは、その変数の要素数を出してくれる関数です。
	for (int i = 0; i < this->m_temp.Sizeof_(this->m_pos); i++)
	{
		// 向きベクトルの描画
		this->m_pos[i].Draw(10, CDirectXDevice::GetD3DDevice());

		// キューブの描画
		this->m_cube.Draw(&this->m_pos[i]);
	}
}

//==========================================================================
//
// class  : C3DBillboard
// Content: ビルボードの描画
//
//==========================================================================
C3DBillboard::C3DBillboard()
{
}

C3DBillboard::~C3DBillboard()
{
}

//==========================================================================
// 初期化
bool C3DBillboard::Init(void)
{
	// 複数のテクスチャを読み込ませたいときの書き方
	const char * ptexcase[] =
	{
		"resource/texture/test/qPbkxvY4_400x400.png",
		"resource/texture/test/qPbkxvY4_400x400.png",
	};
	ptexcase; // Initの引数にすると一気に初期化ができる。

	// アニメーションに使う変数を初期化します。
	this->m_bill.AnimationCountInit(&this->m_animcount);

	// 座標の初期化です
	this->m_pos[0].Init(0);
	this->m_pos[0].MoveX(-2);
	this->m_pos[0].MoveY(5);
	this->m_pos[0].Scale(1);

	// 座標の初期化です
	this->m_pos[1].Init(1);
	this->m_pos[1].MoveX(2);
	this->m_pos[1].MoveY(5);
	this->m_pos[1].Scale(1);

	// ダブルポインタ使用時の初期化コメントアウトを外せば有効化されます
	//if (this->m_bill.Init(ptexcase, this->m_temp.Sizeof_(ptexcase), CBillboard::PlateList::Vertical))
	//{
	//	return true;
	//}

	// 中はリスト構造なのでシングルポインタで小分けして初期化も可能！！
	if (this->m_bill.Init("resource/texture/test/qPbkxvY4_400x400.png", CBillboard::PlateList::Vertical))
	{
		return true;
	}

	// 中はリスト構造なのでシングルポインタで小分けして初期化も可能！！
	// なんとアニメーションの初期化まで可能！！
	if (this->m_bill.Init("resource/texture/test/12250000efa.png", 5, 12, 5, CBillboard::PlateList::Vertical))
	{
		return true;
	}

	return false;
}

//==========================================================================
// 解放
void C3DBillboard::Uninit(void)
{
	// 確保されたデータは必ず解放しよう
	this->m_bill.Uninit();
}

//==========================================================================
// 更新
void C3DBillboard::Update(void)
{
	// ビルボードアニメーション用の変数を引数に入れることにより、アニメーションを有効にします。
	this->m_bill.UpdateAnimation(&this->m_animcount);
}

//==========================================================================
// 描画
void C3DBillboard::Draw(void)
{
	// 座標が二つ存在するのでforで回します。
	// this->m_temp.Sizeof_とは、その変数の要素数を出してくれる関数です。
	for (int i = 0; i < this->m_temp.Sizeof_(this->m_pos); i++)
	{
		// 向きベクトルの描画
		this->m_pos[i].Draw(10, CDirectXDevice::GetD3DDevice());

		// ビルボードの描画
		this->m_bill.Draw(&this->m_pos[i], CUserCamera::GetView(), &this->m_animcount, false);
	}
}

//==========================================================================
//
// class  : C3DMesh
// Content: メッシュの描画
//
//==========================================================================
C3DMesh::C3DMesh()
{
}

C3DMesh::~C3DMesh()
{
}

//==========================================================================
// 初期化
bool C3DMesh::Init(void)
{
	// 座標が二つ存在するのでforで回します。
	// this->m_temp.Sizeof_とは、その変数の要素数を出してくれる関数です。
	for (int i = 0; i < this->m_temp.Sizeof_(this->m_pos); i++)
	{
		this->m_pos[i].Init(0);
		this->m_pos[i].Scale(3);
	}

	// 回転情報を直接入力し、向きを壁にします。
	this->m_pos[0].Info.rot.z = 1.57079637f;
	this->m_pos[0].Info.rot.x = 1.57079637f;
	this->m_pos[0].Info.rot.y = 1.57079637f;

	this->m_pos[1].Info.rot.z = -1.57079637f;
	this->m_pos[1].Info.rot.x = 1.57079637f;
	this->m_pos[1].Info.rot.y = 1.57079637f;
	
	// メッシュの初期化
	return this->m_mesh.Init("resource/texture/test/stand_ssr2.png", 10, 2);
}

//==========================================================================
// 解放
void C3DMesh::Uninit(void)
{
	// 確保されたデータは必ず解放しよう
	this->m_mesh.Uninit();
}

//==========================================================================
// 更新
void C3DMesh::Update(void)
{
}

//==========================================================================
// 描画
void C3DMesh::Draw(void)
{
	// 座標が二つ存在するのでforで回します。
	// this->m_temp.Sizeof_とは、その変数の要素数を出してくれる関数です。
	for (int i = 0; i < this->m_temp.Sizeof_(this->m_pos); i++)
	{
		// 向きベクトルの描画
		this->m_pos[i].Draw(20, CDirectXDevice::GetD3DDevice());

		// メッシュの描画
		this->m_mesh.Draw(&this->m_pos[i]);
	}
}

//==========================================================================
//
// class  : C3DSphere
// Content: 球体の描画
//
//==========================================================================
C3DSphere::C3DSphere()
{
}

C3DSphere::~C3DSphere()
{
}

//==========================================================================
// 初期化
bool C3DSphere::Init(void)
{
	// 座標の初期化 入れている数字は使用する球体の番号です
	this->m_pos.Init(0);

	// サイズの加算を行っています。マイナスの値を入れることにより天球になります。
	this->m_pos.Scale(-30);

	// 球体の初期化です。
	return this->m_sphere.Init("resource/texture/test/BS02s.png", 18);
}

//==========================================================================
// 解放
void C3DSphere::Uninit(void)
{
	// 確保されたデータは必ず解放しよう
	this->m_sphere.Uninit();
}

//==========================================================================
// 更新
void C3DSphere::Update(void)
{
}

//==========================================================================
// 描画
void C3DSphere::Draw(void)
{
	// 向きベクトルの描画
	this->m_pos.Draw(30,CDirectXDevice::GetD3DDevice());

	// 球体の描画
	this->m_sphere.Draw(&this->m_pos);
}

//==========================================================================
//
// class  : C3DXmodel
// Content: Xモデルの描画
//
//==========================================================================
C3DXmodel::C3DXmodel()
{
}

C3DXmodel::~C3DXmodel()
{
}

//==========================================================================
// 初期化
bool C3DXmodel::Init(void)
{
	// 座標の初期化 入れている数字は使用するモデルの番号です
	this->m_pos.Init(0);

	// サイズを加算してます
	this->m_pos.Scale(1);

	// モデルの初期化です、ファイルパスとライティングの種類の指定を行います。
	return this->m_xmodel.Init("resource/3DModel/Alicia/Alicia_solid.x", CXmodel::LightMoad::Material);
}

//==========================================================================
// 解放
void C3DXmodel::Uninit(void)
{
	// 確保されたデータは必ず解放しよう
	this->m_xmodel.Uninit();
}

//==========================================================================
// 更新
void C3DXmodel::Update(void)
{
}

//==========================================================================
// 描画
void C3DXmodel::Draw(void)
{
	// 向きベクトルの描画
	this->m_pos.Draw(30, CDirectXDevice::GetD3DDevice());

	// ｘモデルの描画
	this->m_xmodel.Draw(&this->m_pos);
}

//==========================================================================
//
// class  : C3DText
// Content: テキストの描画
//
//==========================================================================
C3DText::C3DText()
{
}

C3DText::~C3DText()
{
}

//==========================================================================
// 初期化
bool C3DText::Init(void)
{
	this->m_text.init(CDirectXDevice::GetWindowsSize().m_Width, CDirectXDevice::GetWindowsSize().m_Height);
	return false;
}

//==========================================================================
// 解放
void C3DText::Uninit(void)
{
	this->m_text.uninit();
}

//==========================================================================
// 更新
void C3DText::Update(void)
{
}

//==========================================================================
// 描画
void C3DText::Draw(void)
{
	this->m_text.textnew();
	this->m_text.text("テ");
	this->m_text.text("キ");
	this->m_text.text("ス");
	this->m_text.text("ト");
	this->m_text.textend();
}
