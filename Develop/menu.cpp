//==========================================================================
// メニュー[menu.cpp]
// author: keita yanagidate
//==========================================================================
#include "menu.h"
#include "titlelogo.h"
#include "Screen.h"
#include "ranking.h"

//==========================================================================
// 実体
//==========================================================================
int CMenu::m_menu;
bool CMenu::m_statickey;
bool CMenu::m_soundkey;
CSound CMenu::m_sound;

CMenu::CMenu()
{
	this->m_poly = nullptr;
	this->m_pos = nullptr;
	this->m_NumData = 0;
	this->m_fCount = 0.0f;
	this->m_ncollarα = nullptr;
	this->m_keylock = nullptr;
	this->m_lock = false;
	this->m_Count = 0;

	this->m_backpoly = nullptr;
	this->m_backpos = nullptr;
	this->m_backcollarα = 0;
}

CMenu::~CMenu()
{
}

//==========================================================================
// 初期化
bool CMenu::Init(void)
{
	const char * ptexlink[] =
	{
		"resource/texture/Menu/syokyu.DDS",
		"resource/texture/Menu/chuukyuu.DDS",
		"resource/texture/Menu/joukyuu.DDS",
		"resource/texture/Menu/cursor.DDS",
		"resource/texture/Menu/Mode.DDS",
	};

	this->m_NumData = this->m_temp.Sizeof_(ptexlink);
	this->m_menu = 0;
	this->m_fCount = 0.0f;
	this->m_statickey = false;
	this->m_lock = false;
	this->m_Count = 0;
	this->m_backcollarα = 0;

	this->m_temp.New_(this->m_poly);
	this->m_temp.New_(this->m_pos, this->m_NumData);
	this->m_temp.New_(this->m_ncollarα, this->m_NumData);
	this->m_temp.New_(this->m_keylock, this->m_NumData);

	this->m_temp.New_(this->m_backpoly);
	this->m_temp.New_(this->m_backpos);

	if (this->m_poly->Init(ptexlink, this->m_NumData, true))
	{
		return true;
	}

	for (int i = 0; i < this->m_NumData; i++)
	{
		this->m_ncollarα[i] = 0;
		this->m_keylock[i] = false;
		this->m_pos[i].Init(i);
		this->m_pos[i].SetCentralCoordinatesMood(true);
		this->m_pos[i].SetX((float)(CDirectXDevice::GetWindowsSize().m_Width / 2));
		this->m_pos[i].SetY((float)(CDirectXDevice::GetWindowsSize().m_Height / 2));
		this->m_pos[i].SetColor(255, 255, 255, this->m_ncollarα[i]);
	}

	this->m_pos[0].SetYPlus((float)-this->m_poly->GetTexSize(this->m_pos[1].getindex())->h);
	this->m_pos[2].SetYPlus((float)this->m_poly->GetTexSize(this->m_pos[1].getindex())->h);

	this->m_pos[3].SetY(this->m_pos[0].GetPos()->y);
	this->m_pos[3].SetX(this->m_pos[0].GetPos()->x - (this->m_poly->GetTexSize(this->m_pos[0].getindex())->w*0.7f));
	this->m_pos[3].Scale(-0.5f);

	this->m_pos[4].SetX((float)(CDirectXDevice::GetWindowsSize().m_Width / 2));
	this->m_pos[4].SetY((float)(CDirectXDevice::GetWindowsSize().m_Height * 0.17f));
	this->m_pos[4].Scale(-0.2f);

	if (this->m_backpoly->Init("resource/texture/ColorTex.DDS"))
	{
		return true;
	}

	this->m_backpos->Init(0);
	this->m_backpos->SetColor(0, 0, 0, this->m_backcollarα);

	this->m_backpoly->SetTexSize(this->m_backpos->getindex(), CDirectXDevice::GetWindowsSize().m_Width, CDirectXDevice::GetWindowsSize().m_Height);

	CSound::SoundLabel bgmlis[] =
	{
		{ "resource/sound/bgm/title/bgm.wav",-1,1.0f }
	};

	if (this->m_sound.Init(bgmlis, 1))
	{
		return false;
	}

	this->m_soundkey = false;

	return false;
}

//==========================================================================
// 解放
void CMenu::Uninit(void)
{
	this->m_poly->Uninit();
	this->m_backpoly->Uninit();
	this->m_sound.Uninit();

	this->m_temp.Delete_(this->m_poly);
	this->m_temp.Delete_(this->m_pos);
	this->m_temp.Delete_(this->m_ncollarα);
	this->m_temp.Delete_(this->m_keylock);
	this->m_temp.Delete_(this->m_backpoly);
	this->m_temp.Delete_(this->m_backpos);

	this->m_NumData = 0;
	this->m_Count = 0;
	this->m_fCount = 0.0f;
	this->m_statickey = false;
	this->m_lock = false;
	this->m_backcollarα = 0;
	this->m_soundkey = false;
}

//==========================================================================
// 更新
void CMenu::Update(void)
{
	static bool key = false;
	this->m_Imgui.NewWindow("menu window", true);
#if defined(_DEBUG) || defined(DEBUG)
	const char * charbool[] =
	{
		"false",
		"true"
	};
	const char * cdifficulty[] =
	{
		"EASY",
		"NORMAL",
		"HARD"
	};
	this->m_Imgui.Text("menu window key = %s", charbool[this->m_statickey]);
	this->m_Imgui.Text("difficulty = %s", cdifficulty[this->m_menu]);
#endif
	this->m_fCount += 0.2f;

	// 未ロック
	if (!this->m_lock)
	{
		// メニュー表示切替キー
		if (this->m_statickey)
		{
			if (CKeyboard::Trigger(CKeyboard::KeyList::KEY_BACK) || CController::CButton::Trigger(CController::CButton::Ckey::RightButton2))
			{
				this->m_statickey = false;
			}
		}
		else
		{
			if (CKeyboard::Trigger(CKeyboard::KeyList::KEY_RETURN) || CController::CButton::Trigger(CController::CButton::Ckey::RightButton3) || CController::CButton::Trigger(CController::CButton::Ckey::SOPTIONSButton) || CController::CButton::Trigger(CController::CButton::Ckey::SHAREButton) || CController::CButton::Trigger(CController::CButton::Ckey::PSButton) || CController::CButton::Trigger(CController::CButton::Ckey::TouchPad))
			{
				if (CTitlelogo::getkey())
				{
					this->m_statickey = true;
					CRanking::EndRanking();
					key = true;
				}
			}
			CRanking::startRanking();
		}

		// メニューが表示さえているとき
		if (this->m_statickey)
		{
			if (CKeyboard::Trigger(CKeyboard::KeyList::KEY_RETURN) || CController::CButton::Trigger(CController::CButton::Ckey::RightButton3))
			{
				CRanking::EndRanking();
				if (!key)
				{
					this->m_lock = true;
				}
			}
			else
			{
				key = false;
			}

			// 未選択時
			if (!this->m_lock)
			{
				if (CKeyboard::Trigger(CKeyboard::KeyList::KEY_DOWN) || CController::CDirection::Trigger(CController::CDirection::Ckey::LeftButtonUnder))
				{
					if (this->m_menu < 2)
					{
						CRanking::EndRanking();
						this->m_menu++;
						this->m_fCount = 0;
					}
				}
				else if (CKeyboard::Trigger(CKeyboard::KeyList::KEY_UP) || CController::CDirection::Trigger(CController::CDirection::Ckey::LeftButtonUp))
				{
					if (0 < this->m_menu)
					{
						CRanking::EndRanking();
						this->m_menu--;
						this->m_fCount = 0;
					}
				}
			}

			// α値操作
			for (int i = 0; i < this->m_NumData; i++)
			{
				CRanking::EndRanking();
				this->m_ncollarα[i] += 7;

				if (255 < this->m_ncollarα[i])
				{
					this->m_ncollarα[i] = 255;
				}
			}
		}
		else // 選択時
		{
			// α値操作
			for (int i = 0; i < this->m_NumData; i++)
			{
				this->m_ncollarα[i] -= 7;

				if (this->m_ncollarα[i] < 0)
				{
					this->m_menu = 0;
					this->m_ncollarα[i] = 0;
					this->m_fCount = 0.0f;
				}
			}
		}
	}
	// ロック時
	else if (this->m_lock)
	{
		// α値操作
		for (int i = 0; i < this->m_NumData; i++)
		{
			// 選択されたUIのα値を上げる
			if (this->m_menu == i)
			{
				// α値自動調整無効キーがかかっていないとき
				if (!this->m_keylock[i])
				{
					this->m_ncollarα[i] += 7;
					if (255 < this->m_ncollarα[i])
					{
						this->m_ncollarα[i] = 255;
						this->m_keylock[i] = true;
					}
				}
			}
			else // 選択されたUI以外のα値を下げる
			{
				// α値自動調整無効キーがかかっていないとき
				if (!this->m_keylock[i])
				{
					this->m_ncollarα[i] -= 7;
					if (this->m_ncollarα[i] < 0)
					{
						this->m_ncollarα[i] = 0;
						this->m_keylock[i] = true;
					}
				}
			}
		}

		// α値自動調整無効キーがかかっているとき
		if (this->m_keylock[this->m_menu])
		{
			this->m_pos[this->m_menu].Scale(0.1f);
			this->m_Count++;

			// 60フレーム後α値操作
			if (60<this->m_Count)
			{
				this->m_ncollarα[this->m_menu] -= 7;
				if (this->m_ncollarα[this->m_menu] < 0)
				{
					this->m_ncollarα[this->m_menu] = 0;
					this->m_keylock[this->m_menu] = true;
				}

				this->m_backcollarα += 7;
				if (255 < this->m_backcollarα)
				{
					this->m_backcollarα = 255;
					// 次のシーン指定
					CScreen::screenchange(CScreen::scenelist_t::Game);
				}
			}
		}
	}

	for (int i = 0; i < this->m_NumData; i++)
	{
		this->m_pos[i].SetColor(255, 255, 255, this->m_ncollarα[i]);
		this->m_Imgui.Text("id = %d \n collor a = %d", i, this->m_ncollarα[i]);
	}

	this->m_pos[3].SetX(this->m_pos[this->m_menu].GetPos()->x - (this->m_poly->GetTexSize(this->m_pos[this->m_menu].getindex())->w) + (cosf(this->m_fCount) * 5.0f));
	this->m_pos[3].SetY(this->m_pos[this->m_menu].GetPos()->y);

	this->m_Imgui.Text("Move cursor power %.2f", this->m_fCount);
	this->m_Imgui.Text("Move cursor %.2f", (cosf(this->m_fCount) * 5.0f));

	this->m_backpos->SetColor(0, 0, 0, this->m_backcollarα);

	this->m_Imgui.EndWindow();
}

//==========================================================================
// 描画
void CMenu::Draw(void)
{
	this->m_backpoly->Draw(this->m_backpos);
	for (int i = 0; i < this->m_NumData; i++)
	{
		if (this->m_menu == i)
		{
			this->m_poly->Draw(&this->m_pos[i], true); //描画
		}
		else
		{
			this->m_poly->Draw(&this->m_pos[i]); //描画
		}
	}
}

void CMenu::OpenSound(void)
{
	if (!m_soundkey)
	{
		m_sound.Play(0);
		m_soundkey = true;
	}
}
