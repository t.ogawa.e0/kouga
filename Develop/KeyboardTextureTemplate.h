#ifndef _KeyboardTextureTemplate_H_
#define _KeyboardTextureTemplate_H_

class CKeyboardTextureTemplate
{
private:
	class CKeyboard
	{
	public:
		int m_textureID; // ID
		int Pattern; // パターン
		int Direction; // 一行の数
		int m_key; // キー番号
		int m_line; // 行
		bool m_ChangePointStart; // 変更点
		bool m_ChangePointEnd; // 変更点
	};
private:
	enum class keylist
	{
		A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y, Z, space,
	};

	enum class keylist2
	{
		period, underbar,
	};
public:
	typedef CKeyboard keycase_t;
	typedef keylist keylist1_t;
	typedef keylist2 keylist2_t;
public:
	static constexpr CKeyboard m_keyboardlist[] =
	{
		// 数字
		{ 0,10,10,1,0,true,false },// 0
		{ 0,10,10,2,0,false,false },// 1
		{ 0,10,10,3,0,false,false },// 2
		{ 0,10,10,4,0,false,false },// 3
		{ 0,10,10,5,0,false,false },// 4
		{ 0,10,10,6,0,false,false },// 5
		{ 0,10,10,7,0,false,false },// 6
		{ 0,10,10,8,0,false,false },// 7
		{ 0,10,10,9,0,false,false },// 8
		{ 0,10,10,0,0,false,true },// 9

		// アルファベット
		{ 1,27,9,(int)keylist::Q,1,true,false },// 10
		{ 1,27,9,(int)keylist::W,1,false,false },// 11
		{ 1,27,9,(int)keylist::E,1,false,false },// 12
		{ 1,27,9,(int)keylist::R,1,false,false },// 13
		{ 1,27,9,(int)keylist::T,1,false,false },// 14
		{ 1,27,9,(int)keylist::Y,1,false,false },// 15
		{ 1,27,9,(int)keylist::U,1,false,false },// 16
		{ 1,27,9,(int)keylist::I,1,false,false },// 17
		{ 1,27,9,(int)keylist::O,1,false,false },// 18
		{ 1,27,9,(int)keylist::P,1,false,true },// 19

		{ 1,27,9,(int)keylist::A,2,true,false },// 20
		{ 1,27,9,(int)keylist::S,2,false,false },// 21
		{ 1,27,9,(int)keylist::D,2,false,false },// 22
		{ 1,27,9,(int)keylist::F,2,false,false },// 23
		{ 1,27,9,(int)keylist::G,2,false,false },// 24
		{ 1,27,9,(int)keylist::H,2,false,false },// 25
		{ 1,27,9,(int)keylist::J,2,false,false },// 26
		{ 1,27,9,(int)keylist::K,2,false,false },// 27
		{ 1,27,9,(int)keylist::L,2,false,true },// 28

		{ 1,27,9,(int)keylist::Z,3,true,false },// 29
		{ 1,27,9,(int)keylist::X,3,false,false },// 30
		{ 1,27,9,(int)keylist::C,3,false,false },// 31
		{ 1,27,9,(int)keylist::V,3,false,false },// 32
		{ 1,27,9,(int)keylist::B,3,false,false },// 33
		{ 1,27,9,(int)keylist::N,3,false,false },// 34
		{ 1,27,9,(int)keylist::M,3,false,false },// 35
		{ 2,2,2,(int)keylist2::period,3,false,false },// 36
		{ 2,2,2,(int)keylist2::underbar,3,false,true },// 37

		{ 3,27,9,(int)keylist::space,4,true,true },// 38
	};

	static constexpr CKeyboard m_namefontlist[] =
	{
		// 数字
		{ 0,10,10,1,0,true,false },// 0
		{ 0,10,10,2,0,false,false },// 1
		{ 0,10,10,3,0,false,false },// 2
		{ 0,10,10,4,0,false,false },// 3
		{ 0,10,10,5,0,false,false },// 4
		{ 0,10,10,6,0,false,false },// 5
		{ 0,10,10,7,0,false,false },// 6
		{ 0,10,10,8,0,false,false },// 7
		{ 0,10,10,9,0,false,false },// 8
		{ 0,10,10,0,0,false,true },// 9

		// アルファベット
		{ 1,27,9,(int)keylist::Q,1,true,false },// 10
		{ 1,27,9,(int)keylist::W,1,false,false },// 11
		{ 1,27,9,(int)keylist::E,1,false,false },// 12
		{ 1,27,9,(int)keylist::R,1,false,false },// 13
		{ 1,27,9,(int)keylist::T,1,false,false },// 14
		{ 1,27,9,(int)keylist::Y,1,false,false },// 15
		{ 1,27,9,(int)keylist::U,1,false,false },// 16
		{ 1,27,9,(int)keylist::I,1,false,false },// 17
		{ 1,27,9,(int)keylist::O,1,false,false },// 18
		{ 1,27,9,(int)keylist::P,1,false,true },// 19

		{ 1,27,9,(int)keylist::A,2,true,false },// 20
		{ 1,27,9,(int)keylist::S,2,false,false },// 21
		{ 1,27,9,(int)keylist::D,2,false,false },// 22
		{ 1,27,9,(int)keylist::F,2,false,false },// 23
		{ 1,27,9,(int)keylist::G,2,false,false },// 24
		{ 1,27,9,(int)keylist::H,2,false,false },// 25
		{ 1,27,9,(int)keylist::J,2,false,false },// 26
		{ 1,27,9,(int)keylist::K,2,false,false },// 27
		{ 1,27,9,(int)keylist::L,2,false,true },// 28

		{ 1,27,9,(int)keylist::Z,3,true,false },// 29
		{ 1,27,9,(int)keylist::X,3,false,false },// 30
		{ 1,27,9,(int)keylist::C,3,false,false },// 31
		{ 1,27,9,(int)keylist::V,3,false,false },// 32
		{ 1,27,9,(int)keylist::B,3,false,false },// 33
		{ 1,27,9,(int)keylist::N,3,false,false },// 34
		{ 1,27,9,(int)keylist::M,3,false,false },// 35
		{ 2,2,2,(int)keylist2::period,3,false,false },// 36
		{ 2,2,2,(int)keylist2::underbar,3,false,true },// 37

		{ 1,27,9,(int)keylist::space,4,true,true },// 38
	};
};

#endif // !_KeyboardTextureTemplate_H_
