//==========================================================================
// カメラ[camera.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _CAERA_H_
#define _CAERA_H_

//==========================================================================
// Include
//==========================================================================
#include "dxlib.h"
#include "SceneChange.h"

//==========================================================================
//
// class  : CUserCamera
// Content: user用カメラ
//
//==========================================================================
class CUserCamera
{
public:
	enum class CameraListCame
	{
		Default,
	};
public:
	CUserCamera();
	~CUserCamera();

	// 初期化
	void Set(CSceneManager::SceneName Input) { this->m_Name = Input; };
	void Update(void);
	static D3DXMATRIX * GetView(void) { return m_MtxView; }
private:
	void Title(void);
	void Home(void);
	void Game(void);
	void Result(void);
	void Screen_Saver(void);
	void Practice(void);
	void Loat(void);
	void Default(void);
private:
	CSceneManager::SceneName m_Name;
	static D3DXMATRIX * m_MtxView;
};

#endif // !_CAERA_H_
