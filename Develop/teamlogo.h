//==========================================================================
// `[S[teamlogo.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _teamlogo_h_
#define _teamlogo_h_

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

//==========================================================================
//
// class  : CTeamlogo
// Content: `[S
//
//==========================================================================
class CTeamlogo : public CObject
{
public:
	CTeamlogo();
	~CTeamlogo();
	// ú»
	bool Init(void);
	// ðú
	void Uninit(void);
	// XV
	void Update(void);
	// `æ
	void Draw(void);

	static bool endkey(void) { return m_updateend; }
private:
	C2DPolygon * m_poly; // Ûè²ñ
	C2DObject * m_pos; // ÀW
	int m_¿; // ¿l

	C2DObject * m_bakpos; // wiÀW
	int m_bak¿; // ¿l
	int m_frame; // t[
	bool m_end; // 

	CTemplates m_temp;
	CImGui_Dx9 m_Imgui;

	static bool m_updateend; // I¹
};

#endif // !_teamlogo_h_
