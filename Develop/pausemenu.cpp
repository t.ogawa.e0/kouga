//==========================================================================
// ポーズのメニュー[pausemenu.cpp]
// author : 
//==========================================================================
#include "pausemenu.h"
#include "pausebackground.h"
#include "Screen.h"

//==========================================================================
// 実体化
//==========================================================================
bool CPausemenu::m_key = false;

CPausemenu::CPausemenu()
{
	this->m_masterpos = nullptr;
	this->m_poly = nullptr;
	this->m_pos = nullptr;
	this->m_numdata = 0;
	this->m_MENU = 0;
}

CPausemenu::~CPausemenu()
{
}

//==========================================================================
// 初期化
bool CPausemenu::Init(void)
{
	// テクスチャのパス
	const char * pTexList[]=
	{
		"resource/texture/Game/UI/saikai.DDS",
		"resource/texture/Game/UI/tettai.DDS",
		"resource/texture/Game/UI/cursor.DDS",
	};

	float fUIScale = 0.3f; // UIスケール調整用

	// データ数記録
	this->m_numdata = this->Sizeof(pTexList);

	// 動的メモリ確保 確保領域1
	this->New(this->m_masterpos, this->m_numdata);
	this->New(this->m_pos, this->m_numdata);
	this->New(this->m_poly);

	// 非アクティブ化
	this->m_key = false;

	// テクスチャの初期化
	if (this->m_poly->Init(pTexList, this->m_numdata, true))
	{
		return true;
	}

	// テクスチャのサイズ補正
	for (int i = 0; i < this->m_numdata; i++)
	{
		this->m_poly->SetTexScale(i, fUIScale);
	}

	// 初期化処理
	for (int i = 0; i < this->m_numdata; i++)
	{
		this->m_pos[i].Init(i);
		this->m_pos[i].SetCentralCoordinatesMood(true);
		this->m_pos[i].SetX((float)(CDirectXDevice::GetWindowsSize().m_Width / 2));
		this->m_pos[i].SetY((float)(CDirectXDevice::GetWindowsSize().m_Height / 2.5));
	}

	// ここにUIの座標設定
	this->m_pos[1].SetYPlus((float)(CDirectXDevice::GetWindowsSize().m_Height * -0.2f ));
	this->m_pos[1].SetYPlus(m_pos[0].GetPos()->y);

	this->m_pos[2].SetY(m_pos[0].GetPos()->y);
	this->m_pos[2].SetX(m_pos[0].GetPos()->x - (float)(CDirectXDevice::GetWindowsSize().m_Height * 0.22f ));

	m_MENU = 0;

	// データのコピー
	for (int i = 0; i < this->m_numdata; i++)
	{
		this->m_masterpos[i] = this->m_pos[i];
	}

	return false;
}

//==========================================================================
// 解放
void CPausemenu::Uninit(void)
{
	// マテリアルの解放
	this->m_poly->Uninit();

	// メモリ解放
	this->Delete(this->m_masterpos);
	this->Delete(this->m_poly);
	this->Delete(this->m_pos);

	// 非アクティブ化
	this->m_key = false;
	this->m_numdata = 0;
}

//==========================================================================
// 更新
void CPausemenu::Update(void)
{
	this->m_imgui.NewWindow("geeaiue", true);

	this->m_imgui.Text("%d", CController::CDirection::Trigger(CController::CDirection::Ckey::LeftButtonUp));
	this->m_imgui.Text("%d", CController::CDirection::Trigger(CController::CDirection::Ckey::LeftButtonUnder));
	// アクティブな時のみ処理
	if (CPausebackground::GetKey())
	{
		// 演出処理
		static float nCount = 0;

		nCount += 0.2f;

		if (CKeyboard::Trigger(CKeyboard::KeyList::KEY_DOWN) || CController::CDirection::Trigger(CController::CDirection::Ckey::LeftButtonUnder))
		{
			if (this->m_MENU < 1)
			{
				this->m_MENU++;
				nCount = 0;
			}
		}

		if (CKeyboard::Trigger(CKeyboard::KeyList::KEY_UP) || CController::CDirection::Trigger(CController::CDirection::Ckey::LeftButtonUp))
		{
			if (0 < this->m_MENU)
			{
				this->m_MENU--;
				nCount = 0;
			}
		}

		if (CKeyboard::Trigger(CKeyboard::KeyList::KEY_RETURN) || CController::CButton::Trigger(CController::CButton::Ckey::RightButton3))
		{
			switch (this->m_MENU)
			{
			case 0:
				CPausebackground::SetLockKey();
				break;
			case 1:
				CScreen::screenchange(CScreen::scenelist_t::Title);
				break;
			default:
				break;
			}
		}

		this->m_pos[2].SetX(this->m_pos[this->m_MENU].GetPos()->x - (float)(CDirectXDevice::GetWindowsSize().m_Height * 0.22f) + (cosf(nCount) * 5.0f));
		this->m_pos[2].SetY(this->m_pos[this->m_MENU].GetPos()->y);
	}

	this->m_imgui.EndWindow();
}

//==========================================================================
// 描画
void CPausemenu::Draw(void)
{
	// アクティブな時のみ処理
	if (CPausebackground::GetKey())
	{
		// 描画処理
		for (int i = 0; i < this->m_numdata; i++)
		{
			this->m_poly->Draw(&this->m_pos[i]);
		}
	}
}
