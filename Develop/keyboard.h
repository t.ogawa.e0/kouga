//==========================================================================
// キーボード[keyboard.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _NAME_H_
#define _NAME_H_

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "GameData.h"

//==========================================================================
//
// class  : CNameKeyboard
// Content: CNameKeyboard
//
//==========================================================================
class CNameKeyboard : private CTemplate, public CObject
{
private:
	static constexpr int m_limitname = 10;
private:
	class CLine
	{
	public:
		int m_start;
		int m_end;
	};
public:
	CNameKeyboard();
	~CNameKeyboard();
	// 初期化
	bool Init(void);
	// 解放
	void Uninit(void);
	// 更新
	void Update(void);
	// 描画
	void Draw(void);
	// 名前上限
	static int GetNameLimit(void) { return m_limitname; }
	// 名前のゲッター
	static CGameData::CNameCase GetName(void) { return m_name; }

	static void Open(void) { m_OpenKey = true; }
private:
	C2DObject *m_Pos;
	C2DPolygon *m_Poly; //描画
	C2DObject *m_MasterPos;

	C2DObject *m_KeyPos;
	C2DPolygon *m_KeyPoly; //描画

	int m_Serect;
	int m_OldSerect;
	CLine m_UpperBoundOfEachRow[5]; // 各行の上限値
	int m_lineserect;
	int m_NumData;
	bool m_MoveKey;
	bool m_EndKey;
	int m_EndMove;

	CImGui_Dx9 m_Imgui;

	bool m_Savekey;

	static bool m_OpenKey;
	static CGameData::CNameCase m_name;
};

#endif // !_NAME_H_