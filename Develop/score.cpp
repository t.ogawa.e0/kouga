//==========================================================================
// scoreフォント処理[score.h]
// author: joja taiyo
//==========================================================================
#include "score.h"
#include "keyboard.h"
#include "Name.h"
#include "Base.h"

int CScore::m_Score;

CScore::CScore()
{
	this->m_ScoerPoly = nullptr;
	this->m_ScoerFontPoly = nullptr;
	this->m_ScoerPos = nullptr;
	this->m_ScoerFontPos = nullptr;
	this->m_Num = nullptr;
	this->m_Flame = 0;
	this->m_movekey = false;
}

CScore::~CScore()
{
}

bool CScore::Init()
{
	this->New(this->m_ScoerPos);
	this->New(this->m_ScoerFontPos);
	this->New(this->m_Num);
	this->New(this->m_ScoerPoly);
	this->New(this->m_ScoerFontPoly);

	this->m_Score = 0;

	this->m_ScoerPos->Init(0, 1, 10, 10);
	this->m_ScoerFontPos->Init(0);
	this->m_Num->Init(9, true, true);

	if (this->m_ScoerPoly->Init("resource/texture/numbertex.DDS", true))
	{
		return true;
	}

	if (this->m_ScoerFontPoly->Init("resource/texture/ScoreFont.DDS", true))
	{
		return true;
	}
	this->m_ScoerFontPos->SetX(CDirectXDevice::GetWindowsSize().m_Width*0.35f);
	this->m_ScoerFontPos->SetY(CDirectXDevice::GetWindowsSize().m_Height / 2.0f);

	this->m_ScoerPos->SetX(this->m_ScoerFontPos->GetPos()->x);
	this->m_ScoerPos->SetY(this->m_ScoerFontPos->GetPos()->y);

	this->m_ScoerPos->SetXPlus((this->m_ScoerFontPoly->GetTexSize(this->m_ScoerFontPos->getindex())->w / 2.0f)*1.4f);

	this->m_ScoerPos->SetCentralCoordinatesMood(true);
	this->m_ScoerFontPos->SetCentralCoordinatesMood(true);

	this->m_movekey = false;
	this->m_Flame = 0;

	return false;
}

void CScore::Uninit()
{
	this->m_ScoerPoly->Uninit();
	this->m_ScoerFontPoly->Uninit();
	this->m_Num->Uninit();

	this->Delete(this->m_ScoerPos);
	this->Delete(this->m_ScoerFontPos);
	this->Delete(this->m_Num);
	this->Delete(this->m_ScoerPoly);
	this->Delete(this->m_ScoerFontPoly);

	this->m_Flame = 0;
	this->m_Score = 0;
	this->m_movekey = false;
}

void CScore::Update()
{
	this->m_Imgui.NewWindow("DrawScore", true);
	this->m_Imgui.Text("Score : %d", this->m_Score);
#if defined(_DEBUG) || defined(DEBUG)
	const char * cbooltex[] =
	{
		"false","true"
	};

	this->m_Imgui.Text("key : %s", cbooltex[this->m_movekey]);
#endif

	this->m_Imgui.Text("Flame : %d", this->m_Flame);

	this->m_Score = CBase::getscor();

	if (this->m_Flame < this->m_LimitFlame)
	{
		if (this->m_movekey)
		{
			this->m_ScoerPos->SetXPlus(-40.0f);
			this->m_ScoerFontPos->SetXPlus(-40.0f);

			this->m_Flame++;
			this->m_Imgui.Text("count...");
		}

		if (!this->m_movekey)
		{
			if (CKeyboard::Trigger(CKeyboard::KeyList::KEY_RETURN) || CController::CButton::Trigger(CController::CButton::Ckey::RightButton3))
			{
				this->m_movekey = true;
				CNameKeyboard::Open();
				CName::Open();
			}
			this->m_Imgui.Text("not key...");
		}
	}

	this->m_Imgui.EndWindow();
}

void CScore::Draw()
{
	if (this->m_Flame<this->m_LimitFlame)
	{
		this->m_ScoerFontPoly->Draw(this->m_ScoerFontPos);
		this->m_Num->Draw(this->m_ScoerPoly, this->m_ScoerPos, this->m_Score);
	}
}

