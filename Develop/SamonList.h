//=============================================================================
//
//		�������X�g [SamonList.h]
//		Author : keita yanagidate
//
//=============================================================================
#ifndef _SAMON_LIST_H_
#define _SAMON_LIST_H_

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

//==========================================================================
//
// class  : CSamonList
// Content: �������X�g
//
//==========================================================================
class CSamonList :private CTemplate, public CObject
{
private:
	static constexpr int m_NumData = 3;
public:
	CSamonList();
	~CSamonList();
	// ������
	bool Init(void);
	// �I��
	void Uninit(void);
	// �X�V
	void Update(void);
	// �`��
	void Draw(void);
private:
	C2DObject *m_Pos; //�������X�g1��
	C2DPolygon *m_Poly; //�`��
};

#endif // !_SAMON_LIST_H_#pragma once