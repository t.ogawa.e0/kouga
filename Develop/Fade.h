//==========================================================================
// フェード[Fade.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _Fade_H_
#define _Fade_H_

#include "dxlib.h"

//==========================================================================
//
// class  : CFade
// Content: フェード
//
//==========================================================================
class CFade : private CTemplate
{
private:
	// パラメータ
	class CParam
	{
	public:
		int m_a; // α
		bool m_Change; // change
		bool m_Key; // 鍵
		bool m_Draw; // 描画判定
		bool m_In; // フェードイン判定
	};
public:
	CFade();
	~CFade();
	// 初期化
	bool Init(void);
	// 解放
	void Uninit(void);
	// 更新
	bool Update(void);
	// 描画
	void Draw(void);
	// フェードイン
	void In(void);
	// フェードアウト
	void Out(void);
	// フェードイン終了判定
	bool FeadInEnd(void);
	bool GetDraw(void) { return this->m_Param->m_Draw; }
private:
	C2DObject * m_Pos; // 座標
	C2DPolygon * m_Poly; // ポリゴン
	CParam * m_Param; // パラメータ
};

#endif // !_Fade_H_
