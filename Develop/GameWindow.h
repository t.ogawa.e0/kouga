//==========================================================================
// ゲームウィンドウ[GameWindow.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _GameWindow_H_
#define _GameWindow_H_

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <stdio.h>
#include "dxlib.h"
#include "Screen.h"

//==========================================================================
// マクロ定義
//==========================================================================
#define WINDOW_STYLE (WS_OVERLAPPEDWINDOW& ~(WS_THICKFRAME | WS_MAXIMIZEBOX | WS_MINIMIZEBOX))

//==========================================================================
//
// class  : CGameWindow
// Content: ゲームウィンドウ
//
//==========================================================================
class CGameWindow
{
private:
	const char *Class_Name = "光牙";
	const char *Window_Name = "光牙";
private:
	class CWinSize
	{
	public:
		CWinSize() {};
		CWinSize(int _w, int _h)
		{
			this->w = _w;
			this->h = _h;
		}
	public:
		int w;
		int h;
	};
	class CData
	{
	public:
		CData() {};
		CData(int serect, bool key)
		{
			this->m_serect = serect;
			this->m_key = key;
		}
	public:
		int m_serect;
		bool m_key;
	};
public:
	CGameWindow();
	~CGameWindow();
	// ウィンドウ生成
	bool Window(WNDCLASSEX * wcex, HINSTANCE hInstance);
	// ウィンドウ更新
	int WindowUpdate(WNDCLASSEX *wcex, int nCmdShow);
	// ウィンドウモードの数
	int NumWindowSize(void);
	// ウィンドウサイズの取得
	CWinSize GetWinSize(int serect);
	// 選択されたIndexのインプット
	void SetSerectMode(int serect);
	// グラフィックモード
	void SetGraphicMode(bool serect);
private:
	// ウィンドウプロシージャ
	static LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
	// ゲームループ
	int GameLoop(HINSTANCE hInstance, HWND hWnd);
	bool Init(HINSTANCE hInstance, HWND hWnd); //初期化
	void Uninit(void);//終了処理
	bool Update(void);//更新
	void Draw(void);//描画
private:
	CScreen m_screne;
	int m_serect;
};

#endif // !_GameWindow_H_
