//==========================================================================
// リザルト[result.cpp]
// author: tatuya ogawa
//==========================================================================
#include "result.h"
#include "ThankYouForPlaying.h"

#include "titlebackground.h"

#include "world.h"
#include "GameCamera.h"
#include "keyboard.h"
#include "Name.h"
#include "ranking.h"
#include "resultfont.h"
#include "score.h"

#include "ResultAnnouncement.h"

CResultScene::CResultScene()
{
}

CResultScene::~CResultScene()
{
}

//==========================================================================
// 初期化
bool CResultScene::Init(void)
{
	CLight light;
	CGameCamera * camera;
	CWorld * wprld;
	CTitlebackground * titlebackground;
	CResultfont * presultfont;
	CThankYouForPlaying * pThankYouForPlaying;
	CScore * psscore;
	CNameKeyboard * pnamekey;
	CName* pname;
	CResultAnnouncement * rank;

	light.Init({ -1,-1,0 });

	CObject::NewObject(camera);
	CObject::NewObject(wprld);

	CObject::NewObject(titlebackground);
	CObject::NewObject(presultfont);
	CObject::NewObject(psscore);
	CObject::NewObject(pnamekey);
	CObject::NewObject(pname);
	CObject::NewObject(rank);

	CObject::NewObject(pThankYouForPlaying);

	CSound::SoundLabel bgmlis[] =
	{
		{ "resource/sound/bgm/result/bgm.wav",-1,1.0f }
	};

	if (this->m_sound.Init(bgmlis, 1))
	{
		return false;
	}

	this->m_soundkey = false;


	return CObject::InitAll();
}

//==========================================================================
// 解放
void CResultScene::Uninit(void)
{
	this->m_sound.Uninit();
	CObject::UninitAll();
}

//==========================================================================
// 更新
void CResultScene::Update(void)
{
	if (!this->m_soundkey)
	{
		this->m_sound.Play(0);
		this->m_soundkey = true;
	}
	CObject::UpdateAll();
}

//==========================================================================
// 描画
void CResultScene::Draw(void)
{
	CObject::DrawAll();
}
