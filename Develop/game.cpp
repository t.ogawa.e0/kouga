//==========================================================================
// ゲーム[game.cpp]
// author: tatuya ogawa
//==========================================================================
#include "game.h"
#include "Field.h"
#include "Player.h"
#include "Ally_AI.h"
#include "Base.h"
#include "Enemy.h"
#include "PlayerParam.h"
#include "NoEntry.h"
#include "Circle.h"
#include "Summonseffect.h"
#include "ExplosionEffect.h"
#include "failureUI.h"
#include "GameEndUI.h"
#include "startUI.h"
#include "successUI.h"
#include "withdrawUI.h"
#include "time.h"
#include "SummonsKey.h"

CGameScene::CGameScene()
{
}

CGameScene::~CGameScene()
{
}

//==========================================================================
// 初期化
bool CGameScene::Init(void)
{
	CLight light;
	CFIELD * field;
	CPlayer * player;
	CAlly_AI * ally;
	CBase * base;
	CEnemy * enemy;
	CCircle * circle;
	CSummonseffect * summonsefe;
	CExplosionEffect * expefe;
	CPlayerParam * param;
	CTime * ptimme;
	CFailureUI *failu;
	CWithdrawUI *withdr;
	CSuccessUI *success;
	CStartUI *startui;
	CGameEndUI *endgame;
	CSummonsKey * sumoonkey;
	CNoEntry * noent;

	light.Init({ -1,-1,0 });

	//==========================================================================
	// 3Dオブジェクト
	//==========================================================================
	CObject::NewObject(field);
	CObject::NewObject(base);
	CObject::NewObject(enemy);
	CObject::NewObject(player);
	CObject::NewObject(ally);
	CObject::NewObject(noent);

	//==========================================================================
	// 3Dエフェクト
	//==========================================================================
	CObject::NewObject(circle);
	CObject::NewObject(summonsefe);
	CObject::NewObject(expefe);

	//==========================================================================
	// 2Dオブジェクト
	//==========================================================================
	CObject::NewObject(param);
	CObject::NewObject(ptimme);
	CObject::NewObject(failu);
	CObject::NewObject(withdr);
	CObject::NewObject(success);
	CObject::NewObject(startui);
	CObject::NewObject(endgame);
	CObject::NewObject(sumoonkey);

	if (pausem.Init())
	{
		return true;
	}

	if (pausebak.Init())
	{
		return true;
	}

	CSound::SoundLabel bgmlis[] =
	{
		{ "resource/sound/bgm/game/bgm.wav",-1,1.0f }
	};

	if (this->m_sound.Init(bgmlis, 1))
	{
		return false;
	}

	this->m_soundkey = false;

	return CObject::InitAll();
}

//==========================================================================
// 解放
void CGameScene::Uninit(void)
{
	CObject::UninitAll();
	this->pausebak.Uninit();
	this->pausem.Uninit();
	this->m_sound.Uninit();
}

//==========================================================================
// 更新
void CGameScene::Update(void)
{
	if (!this->m_soundkey)
	{
		this->m_soundkey = true;
		this->m_sound.Play(0);
	}
	this->pausebak.Update();
	this->pausem.Update();
	if (!CPausebackground::GetKey())
	{
		CObject::UpdateAll();
	}
}

//==========================================================================
// 描画
void CGameScene::Draw(void)
{
	CObject::DrawAll();
	this->pausebak.Draw();
	this->pausem.Draw();
}
