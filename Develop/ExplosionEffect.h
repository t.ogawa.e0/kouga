//==========================================================================
// 爆発エフェクト[ExplosionEffect.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _ExplosionEffect_H_
#define _ExplosionEffect_H_

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

class CExplosionEffect : private CTemplate, public CObject
{
private:
	// リスト構造クラス
	class CData
	{
	public:
		int m_ID; // 管理ID 無くてもいい
		int m_AnimCounter; // アニメーションカウンタ
		C3DObject m_pos;
		CData *m_pNext; // 次のアドレス
		CData *m_pBack; // 前のアドレス
	};
public:
	CExplosionEffect();
	~CExplosionEffect();
	// 初期化
	bool Init(void);
	// 解放
	void Uninit(void);
	// 更新
	void Update(void);
	// 描画
	void Draw(void);
	// エフェクト生成
	static void create(const C3DObject * Input);
private:
	// 特定のデータ消去
	void PinpointDelete(const int * ID);
private:
	CBillboard * m_poly;
	static CData *m_pStartAddress; // 先頭アドレス
	static int m_NumData; // データ数
	static int m_IDCount; // IDカウンタ
};

#endif // !_ExplosionEffect_H_
