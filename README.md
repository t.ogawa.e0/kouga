**光牙**
====

**Overview**
====

*2018/3月に完成した作品です。*
*チームで制作したゲームです。*
*  [Execution Data](Execution%20Data) - 光牙 の実行データです。
*  [Develop](Develop) - 光牙 の開発データです。
*  [DirectX SDK Installer](DirectX%20SDK%20Installer) - DirectX SDK の インストールプログラムです。

**Requirement**
====

*  [IDE (Integrated Development Environment)](https://my.visualstudio.com/Downloads?q=visual%20studio%202015&pgroup=) - visual studio 2015
*  [End-User Runtime](https://www.microsoft.com/ja-jp/download/details.aspx?id=34429) - DirectX 9.0c End-User Runtime

##

![VisualStudio2015](Asset/VisualStudio2015.png)![c++](Asset/C__.png)![DirectX9SDK](Asset/DirectX9SDK.png)